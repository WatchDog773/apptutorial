/*
               File: type_Sdtcategoria
        Description: categoria
             Author: GeneXus C# Generator version 16_0_6-136889
       Generated on: 12/4/2019 23:7:43.21
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "categoria" )]
   [XmlType(TypeName =  "categoria" , Namespace = "TravelAgency" )]
   [Serializable]
   public class Sdtcategoria : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public Sdtcategoria( )
      {
      }

      public Sdtcategoria( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV11categoriaId )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV11categoriaId});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"categoriaId", typeof(int)}}) ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "categoria");
         metadata.Set("BT", "categoria");
         metadata.Set("PK", "[ \"categoriaId\" ]");
         metadata.Set("PKAssigned", "[ \"categoriaId\" ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override GeneXus.Utils.GxStringCollection StateAttributes( )
      {
         GeneXus.Utils.GxStringCollection state = new GeneXus.Utils.GxStringCollection() ;
         state.Add("gxTpr_Mode");
         state.Add("gxTpr_Initialized");
         state.Add("gxTpr_Categoriaid_Z");
         state.Add("gxTpr_Categorianombre_Z");
         state.Add("gxTpr_Categoriaid_N");
         return state ;
      }

      public override void Copy( GxUserType source )
      {
         Sdtcategoria sdt ;
         sdt = (Sdtcategoria)(source);
         gxTv_Sdtcategoria_Categoriaid = sdt.gxTv_Sdtcategoria_Categoriaid ;
         gxTv_Sdtcategoria_Categorianombre = sdt.gxTv_Sdtcategoria_Categorianombre ;
         gxTv_Sdtcategoria_Mode = sdt.gxTv_Sdtcategoria_Mode ;
         gxTv_Sdtcategoria_Initialized = sdt.gxTv_Sdtcategoria_Initialized ;
         gxTv_Sdtcategoria_Categoriaid_Z = sdt.gxTv_Sdtcategoria_Categoriaid_Z ;
         gxTv_Sdtcategoria_Categorianombre_Z = sdt.gxTv_Sdtcategoria_Categorianombre_Z ;
         gxTv_Sdtcategoria_Categoriaid_N = sdt.gxTv_Sdtcategoria_Categoriaid_N ;
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         ToJSON( includeState, true) ;
         return  ;
      }

      public override void ToJSON( bool includeState ,
                                   bool includeNonInitialized )
      {
         AddObjectProperty("categoriaId", gxTv_Sdtcategoria_Categoriaid, false, includeNonInitialized);
         AddObjectProperty("categoriaId_N", gxTv_Sdtcategoria_Categoriaid_N, false, includeNonInitialized);
         AddObjectProperty("categoriaNombre", gxTv_Sdtcategoria_Categorianombre, false, includeNonInitialized);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_Sdtcategoria_Mode, false, includeNonInitialized);
            AddObjectProperty("Initialized", gxTv_Sdtcategoria_Initialized, false, includeNonInitialized);
            AddObjectProperty("categoriaId_Z", gxTv_Sdtcategoria_Categoriaid_Z, false, includeNonInitialized);
            AddObjectProperty("categoriaNombre_Z", gxTv_Sdtcategoria_Categorianombre_Z, false, includeNonInitialized);
            AddObjectProperty("categoriaId_N", gxTv_Sdtcategoria_Categoriaid_N, false, includeNonInitialized);
         }
         return  ;
      }

      public void UpdateDirties( Sdtcategoria sdt )
      {
         if ( sdt.IsDirty("categoriaId") )
         {
            gxTv_Sdtcategoria_Categoriaid = sdt.gxTv_Sdtcategoria_Categoriaid ;
         }
         if ( sdt.IsDirty("categoriaNombre") )
         {
            gxTv_Sdtcategoria_Categorianombre = sdt.gxTv_Sdtcategoria_Categorianombre ;
         }
         return  ;
      }

      [  SoapElement( ElementName = "categoriaId" )]
      [  XmlElement( ElementName = "categoriaId"   )]
      public int gxTpr_Categoriaid
      {
         get {
            return gxTv_Sdtcategoria_Categoriaid ;
         }

         set {
            if ( gxTv_Sdtcategoria_Categoriaid != value )
            {
               gxTv_Sdtcategoria_Mode = "INS";
               this.gxTv_Sdtcategoria_Categoriaid_Z_SetNull( );
               this.gxTv_Sdtcategoria_Categorianombre_Z_SetNull( );
            }
            gxTv_Sdtcategoria_Categoriaid = value;
            SetDirty("Categoriaid");
         }

      }

      [  SoapElement( ElementName = "categoriaNombre" )]
      [  XmlElement( ElementName = "categoriaNombre"   )]
      public String gxTpr_Categorianombre
      {
         get {
            return gxTv_Sdtcategoria_Categorianombre ;
         }

         set {
            gxTv_Sdtcategoria_Categorianombre = value;
            SetDirty("Categorianombre");
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_Sdtcategoria_Mode ;
         }

         set {
            gxTv_Sdtcategoria_Mode = value;
            SetDirty("Mode");
         }

      }

      public void gxTv_Sdtcategoria_Mode_SetNull( )
      {
         gxTv_Sdtcategoria_Mode = "";
         return  ;
      }

      public bool gxTv_Sdtcategoria_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_Sdtcategoria_Initialized ;
         }

         set {
            gxTv_Sdtcategoria_Initialized = value;
            SetDirty("Initialized");
         }

      }

      public void gxTv_Sdtcategoria_Initialized_SetNull( )
      {
         gxTv_Sdtcategoria_Initialized = 0;
         return  ;
      }

      public bool gxTv_Sdtcategoria_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "categoriaId_Z" )]
      [  XmlElement( ElementName = "categoriaId_Z"   )]
      public int gxTpr_Categoriaid_Z
      {
         get {
            return gxTv_Sdtcategoria_Categoriaid_Z ;
         }

         set {
            gxTv_Sdtcategoria_Categoriaid_Z = value;
            SetDirty("Categoriaid_Z");
         }

      }

      public void gxTv_Sdtcategoria_Categoriaid_Z_SetNull( )
      {
         gxTv_Sdtcategoria_Categoriaid_Z = 0;
         return  ;
      }

      public bool gxTv_Sdtcategoria_Categoriaid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "categoriaNombre_Z" )]
      [  XmlElement( ElementName = "categoriaNombre_Z"   )]
      public String gxTpr_Categorianombre_Z
      {
         get {
            return gxTv_Sdtcategoria_Categorianombre_Z ;
         }

         set {
            gxTv_Sdtcategoria_Categorianombre_Z = value;
            SetDirty("Categorianombre_Z");
         }

      }

      public void gxTv_Sdtcategoria_Categorianombre_Z_SetNull( )
      {
         gxTv_Sdtcategoria_Categorianombre_Z = "";
         return  ;
      }

      public bool gxTv_Sdtcategoria_Categorianombre_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "categoriaId_N" )]
      [  XmlElement( ElementName = "categoriaId_N"   )]
      public short gxTpr_Categoriaid_N
      {
         get {
            return gxTv_Sdtcategoria_Categoriaid_N ;
         }

         set {
            gxTv_Sdtcategoria_Categoriaid_N = value;
            SetDirty("Categoriaid_N");
         }

      }

      public void gxTv_Sdtcategoria_Categoriaid_N_SetNull( )
      {
         gxTv_Sdtcategoria_Categoriaid_N = 0;
         return  ;
      }

      public bool gxTv_Sdtcategoria_Categoriaid_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_Sdtcategoria_Categorianombre = "";
         gxTv_Sdtcategoria_Mode = "";
         gxTv_Sdtcategoria_Categorianombre_Z = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "categoria", "GeneXus.Programs.categoria_bc", new Object[] {context}, constructorCallingAssembly);;
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_Sdtcategoria_Initialized ;
      private short gxTv_Sdtcategoria_Categoriaid_N ;
      private int gxTv_Sdtcategoria_Categoriaid ;
      private int gxTv_Sdtcategoria_Categoriaid_Z ;
      private String gxTv_Sdtcategoria_Categorianombre ;
      private String gxTv_Sdtcategoria_Mode ;
      private String gxTv_Sdtcategoria_Categorianombre_Z ;
   }

   [DataContract(Name = @"categoria", Namespace = "TravelAgency")]
   public class Sdtcategoria_RESTInterface : GxGenericCollectionItem<Sdtcategoria>, System.Web.SessionState.IRequiresSessionState
   {
      public Sdtcategoria_RESTInterface( ) : base()
      {
      }

      public Sdtcategoria_RESTInterface( Sdtcategoria psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "categoriaId" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Categoriaid
      {
         get {
            return sdt.gxTpr_Categoriaid ;
         }

         set {
            sdt.gxTpr_Categoriaid = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "categoriaNombre" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Categorianombre
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Categorianombre) ;
         }

         set {
            sdt.gxTpr_Categorianombre = value;
         }

      }

      public Sdtcategoria sdt
      {
         get {
            return (Sdtcategoria)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new Sdtcategoria() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 2 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
