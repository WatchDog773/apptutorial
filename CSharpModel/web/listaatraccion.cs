/*
               File: ListaAtraccion
        Description: Stub for ListaAtraccion
             Author: GeneXus C# Generator version 16_0_6-136889
       Generated on: 12/2/2019 23:13:47.2
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class listaatraccion : GXProcedure
   {
      public listaatraccion( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public listaatraccion( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_paisId )
      {
         this.AV2paisId = aP0_paisId;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_paisId )
      {
         listaatraccion objlistaatraccion;
         objlistaatraccion = new listaatraccion();
         objlistaatraccion.AV2paisId = aP0_paisId;
         objlistaatraccion.context.SetSubmitInitialConfig(context);
         objlistaatraccion.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objlistaatraccion);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((listaatraccion)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(int)AV2paisId} ;
         ClassLoader.Execute("alistaatraccion","GeneXus.Programs","alistaatraccion", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 1 ) )
         {
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV2paisId ;
      private IGxDataStore dsDefault ;
      private Object[] args ;
   }

}
