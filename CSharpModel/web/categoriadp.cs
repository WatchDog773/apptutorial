/*
               File: categoriaDP
        Description: categoria DP
             Author: GeneXus C# Generator version 16_0_6-136889
       Generated on: 12/3/2019 23:48:53.91
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class categoriadp : GXProcedure
   {
      public categoriadp( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public categoriadp( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( out GXBCCollection<Sdtcategoria> aP0_Gxm2rootcol )
      {
         this.Gxm2rootcol = new GXBCCollection<Sdtcategoria>( context, "categoria", "TravelAgency") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      public GXBCCollection<Sdtcategoria> executeUdp( )
      {
         this.Gxm2rootcol = new GXBCCollection<Sdtcategoria>( context, "categoria", "TravelAgency") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( out GXBCCollection<Sdtcategoria> aP0_Gxm2rootcol )
      {
         categoriadp objcategoriadp;
         objcategoriadp = new categoriadp();
         objcategoriadp.Gxm2rootcol = new GXBCCollection<Sdtcategoria>( context, "categoria", "TravelAgency") ;
         objcategoriadp.context.SetSubmitInitialConfig(context);
         objcategoriadp.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objcategoriadp);
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((categoriadp)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         Gxm1categoria = new Sdtcategoria(context);
         Gxm2rootcol.Add(Gxm1categoria, 0);
         Gxm1categoria.gxTpr_Categorianombre = "Museo";
         Gxm1categoria = new Sdtcategoria(context);
         Gxm2rootcol.Add(Gxm1categoria, 0);
         Gxm1categoria.gxTpr_Categorianombre = "Monumento";
         Gxm1categoria = new Sdtcategoria(context);
         Gxm2rootcol.Add(Gxm1categoria, 0);
         Gxm1categoria.gxTpr_Categorianombre = "Sitio Turistico";
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gxm1categoria = new Sdtcategoria(context);
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private GXBCCollection<Sdtcategoria> aP0_Gxm2rootcol ;
      private GXBCCollection<Sdtcategoria> Gxm2rootcol ;
      private Sdtcategoria Gxm1categoria ;
   }

}
