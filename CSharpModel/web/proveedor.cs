/*
               File: proveedor
        Description: proveedor
             Author: GeneXus C# Generator version 16_0_6-136889
       Generated on: 12/3/2019 21:6:3.79
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class proveedor : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_3") == 0 )
         {
            A7atraccionId = (int)(NumberUtil.Val( GetNextPar( ), "."));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_3( A7atraccionId) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridproveedor_atraccion") == 0 )
         {
            nRC_GXsfl_53 = (int)(NumberUtil.Val( GetNextPar( ), "."));
            nGXsfl_53_idx = (int)(NumberUtil.Val( GetNextPar( ), "."));
            sGXsfl_53_idx = GetNextPar( );
            Gx_mode = GetNextPar( );
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxnrGridproveedor_atraccion_newrow( ) ;
            return  ;
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_web_controls( ) ;
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 16_0_6-136889", 0) ;
            Form.Meta.addItem("description", "proveedor", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtproveedorId_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("Carmine");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public proveedor( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public proveedor( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdmasterpage", "GeneXus.Programs.rwdmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "no-store");
               }
               if ( ! context.WillRedirect( ) )
               {
                  AddString( context.getJSONResponse( )) ;
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            DrawControls( ) ;
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void DrawControls( )
      {
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "WWAdvancedContainer", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divTitlecontainer_Internalname, 1, 0, "px", 0, "px", "TableTop", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         /* Text block */
         GxWebStd.gx_label_ctrl( context, lblTitle_Internalname, "proveedor", "", "", lblTitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_proveedor.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         ClassString = "ErrorViewer";
         StyleString = "";
         GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divFormcontainer_Internalname, 1, 0, "px", 0, "px", "FormContainer", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divToolbarcell_Internalname, 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3 ToolbarCellClass", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group ActionGroup", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "btn-group", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
         ClassString = "BtnFirst";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_first_Internalname, "", "", bttBtn_first_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_first_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", TempTags, "", context.GetButtonType( ), "HLP_proveedor.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
         ClassString = "BtnPrevious";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_previous_Internalname, "", "", bttBtn_previous_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_previous_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", TempTags, "", context.GetButtonType( ), "HLP_proveedor.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
         ClassString = "BtnNext";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_next_Internalname, "", "", bttBtn_next_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_next_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", TempTags, "", context.GetButtonType( ), "HLP_proveedor.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
         ClassString = "BtnLast";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_last_Internalname, "", "", bttBtn_last_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_last_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ELAST."+"'", TempTags, "", context.GetButtonType( ), "HLP_proveedor.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
         ClassString = "BtnSelect";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_select_Internalname, "", "Select", bttBtn_select_Jsonclick, 4, "Select", "", StyleString, ClassString, bttBtn_select_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", TempTags, "gx.popup.openPrompt('"+"gx00a0.aspx"+"',["+"{Ctrl:gx.dom.el('"+"PROVEEDORID"+"'), id:'"+"PROVEEDORID"+"'"+",IOType:'out',isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");"+"return false;", 2, "HLP_proveedor.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCellAdvanced", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtproveedorId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtproveedorId_Internalname, "Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtproveedorId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A48proveedorId), 6, 0, ".", "")), ((edtproveedorId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A48proveedorId), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A48proveedorId), "ZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtproveedorId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtproveedorId_Enabled, 0, "number", "1", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "", "HLP_proveedor.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtproveedorNombre_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtproveedorNombre_Internalname, "Nombre", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtproveedorNombre_Internalname, StringUtil.RTrim( A49proveedorNombre), StringUtil.RTrim( context.localUtil.Format( A49proveedorNombre, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtproveedorNombre_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtproveedorNombre_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "Name", "left", true, "", "HLP_proveedor.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtproveedorDireccion_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtproveedorDireccion_Internalname, "Direccion", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Multiple line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
         ClassString = "Attribute";
         StyleString = "";
         ClassString = "Attribute";
         StyleString = "";
         GxWebStd.gx_html_textarea( context, edtproveedorDireccion_Internalname, A50proveedorDireccion, "http://maps.google.com/maps?q="+GXUtil.UrlEncode( A50proveedorDireccion), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", 0, 1, edtproveedorDireccion_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "", "1024", -1, 0, "_blank", "", 0, true, "GeneXus\\Address", "'"+""+"'"+",false,"+"'"+""+"'", 0, "HLP_proveedor.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 LevelTable", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divAtracciontable_Internalname, 1, 0, "px", 0, "px", "LevelTable", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Text block */
         GxWebStd.gx_label_ctrl( context, lblTitleatraccion_Internalname, "atraccion", "", "", lblTitleatraccion_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_proveedor.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         gxdraw_Gridproveedor_atraccion( ) ;
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
         ClassString = "BtnEnter";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirm", bttBtn_enter_Jsonclick, 5, "Confirm", "", StyleString, ClassString, bttBtn_enter_Visible, bttBtn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_proveedor.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
         ClassString = "BtnCancel";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Cancel", bttBtn_cancel_Jsonclick, 1, "Cancel", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_proveedor.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
         ClassString = "BtnDelete";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Delete", bttBtn_delete_Jsonclick, 5, "Delete", "", StyleString, ClassString, bttBtn_delete_Visible, bttBtn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_proveedor.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "Center", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
      }

      protected void gxdraw_Gridproveedor_atraccion( )
      {
         /*  Grid Control  */
         Gridproveedor_atraccionContainer.AddObjectProperty("GridName", "Gridproveedor_atraccion");
         Gridproveedor_atraccionContainer.AddObjectProperty("Header", subGridproveedor_atraccion_Header);
         Gridproveedor_atraccionContainer.AddObjectProperty("Class", "Grid");
         Gridproveedor_atraccionContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         Gridproveedor_atraccionContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Gridproveedor_atraccionContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridproveedor_atraccion_Backcolorstyle), 1, 0, ".", "")));
         Gridproveedor_atraccionContainer.AddObjectProperty("CmpContext", "");
         Gridproveedor_atraccionContainer.AddObjectProperty("InMasterPage", "false");
         Gridproveedor_atraccionColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridproveedor_atraccionColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A7atraccionId), 6, 0, ".", "")));
         Gridproveedor_atraccionColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtatraccionId_Enabled), 5, 0, ".", "")));
         Gridproveedor_atraccionContainer.AddColumnProperties(Gridproveedor_atraccionColumn);
         Gridproveedor_atraccionColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridproveedor_atraccionContainer.AddColumnProperties(Gridproveedor_atraccionColumn);
         Gridproveedor_atraccionColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridproveedor_atraccionColumn.AddObjectProperty("Value", StringUtil.RTrim( A8atraccionNombre));
         Gridproveedor_atraccionColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtatraccionNombre_Enabled), 5, 0, ".", "")));
         Gridproveedor_atraccionContainer.AddColumnProperties(Gridproveedor_atraccionColumn);
         Gridproveedor_atraccionColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridproveedor_atraccionColumn.AddObjectProperty("Value", context.convertURL( A13atraccionFoto));
         Gridproveedor_atraccionColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtatraccionFoto_Enabled), 5, 0, ".", "")));
         Gridproveedor_atraccionContainer.AddColumnProperties(Gridproveedor_atraccionColumn);
         Gridproveedor_atraccionContainer.AddObjectProperty("Selectedindex", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridproveedor_atraccion_Selectedindex), 4, 0, ".", "")));
         Gridproveedor_atraccionContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridproveedor_atraccion_Allowselection), 1, 0, ".", "")));
         Gridproveedor_atraccionContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridproveedor_atraccion_Selectioncolor), 9, 0, ".", "")));
         Gridproveedor_atraccionContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridproveedor_atraccion_Allowhovering), 1, 0, ".", "")));
         Gridproveedor_atraccionContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridproveedor_atraccion_Hoveringcolor), 9, 0, ".", "")));
         Gridproveedor_atraccionContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridproveedor_atraccion_Allowcollapsing), 1, 0, ".", "")));
         Gridproveedor_atraccionContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridproveedor_atraccion_Collapsed), 1, 0, ".", "")));
         nGXsfl_53_idx = 0;
         if ( ( nKeyPressed == 1 ) && ( AnyError == 0 ) )
         {
            /* Enter key processing. */
            nBlankRcdCount11 = 5;
            if ( ! IsIns( ) )
            {
               /* Display confirmed (stored) records */
               nRcdExists_11 = 1;
               ScanStart0811( ) ;
               while ( RcdFound11 != 0 )
               {
                  init_level_properties11( ) ;
                  getByPrimaryKey0811( ) ;
                  AddRow0811( ) ;
                  ScanNext0811( ) ;
               }
               ScanEnd0811( ) ;
               nBlankRcdCount11 = 5;
            }
         }
         else if ( ( nKeyPressed == 3 ) || ( nKeyPressed == 4 ) || ( ( nKeyPressed == 1 ) && ( AnyError != 0 ) ) )
         {
            /* Button check  or addlines. */
            standaloneNotModal0811( ) ;
            standaloneModal0811( ) ;
            sMode11 = Gx_mode;
            while ( nGXsfl_53_idx < nRC_GXsfl_53 )
            {
               bGXsfl_53_Refreshing = true;
               ReadRow0811( ) ;
               edtatraccionId_Enabled = (int)(context.localUtil.CToN( cgiGet( "ATRACCIONID_"+sGXsfl_53_idx+"Enabled"), ".", ","));
               AssignProp("", false, edtatraccionId_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtatraccionId_Enabled), 5, 0), !bGXsfl_53_Refreshing);
               edtatraccionNombre_Enabled = (int)(context.localUtil.CToN( cgiGet( "ATRACCIONNOMBRE_"+sGXsfl_53_idx+"Enabled"), ".", ","));
               AssignProp("", false, edtatraccionNombre_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtatraccionNombre_Enabled), 5, 0), !bGXsfl_53_Refreshing);
               edtatraccionFoto_Enabled = (int)(context.localUtil.CToN( cgiGet( "ATRACCIONFOTO_"+sGXsfl_53_idx+"Enabled"), ".", ","));
               AssignProp("", false, edtatraccionFoto_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtatraccionFoto_Enabled), 5, 0), !bGXsfl_53_Refreshing);
               imgprompt_7_Link = cgiGet( "PROMPT_7_"+sGXsfl_53_idx+"Link");
               if ( ( nRcdExists_11 == 0 ) && ! IsIns( ) )
               {
                  Gx_mode = "INS";
                  AssignAttri("", false, "Gx_mode", Gx_mode);
                  standaloneModal0811( ) ;
               }
               SendRow0811( ) ;
               bGXsfl_53_Refreshing = false;
            }
            Gx_mode = sMode11;
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            /* Get or get-alike key processing. */
            nBlankRcdCount11 = 5;
            nRcdExists_11 = 1;
            if ( ! IsIns( ) )
            {
               ScanStart0811( ) ;
               while ( RcdFound11 != 0 )
               {
                  sGXsfl_53_idx = StringUtil.PadL( StringUtil.LTrimStr( (decimal)(nGXsfl_53_idx+1), 4, 0), 4, "0");
                  SubsflControlProps_5311( ) ;
                  init_level_properties11( ) ;
                  standaloneNotModal0811( ) ;
                  getByPrimaryKey0811( ) ;
                  standaloneModal0811( ) ;
                  AddRow0811( ) ;
                  ScanNext0811( ) ;
               }
               ScanEnd0811( ) ;
            }
         }
         /* Initialize fields for 'new' records and send them. */
         sMode11 = Gx_mode;
         Gx_mode = "INS";
         AssignAttri("", false, "Gx_mode", Gx_mode);
         sGXsfl_53_idx = StringUtil.PadL( StringUtil.LTrimStr( (decimal)(nGXsfl_53_idx+1), 4, 0), 4, "0");
         SubsflControlProps_5311( ) ;
         InitAll0811( ) ;
         init_level_properties11( ) ;
         nRcdExists_11 = 0;
         nIsMod_11 = 0;
         nRcdDeleted_11 = 0;
         nBlankRcdCount11 = (short)(nBlankRcdUsr11+nBlankRcdCount11);
         fRowAdded = 0;
         while ( nBlankRcdCount11 > 0 )
         {
            standaloneNotModal0811( ) ;
            standaloneModal0811( ) ;
            AddRow0811( ) ;
            if ( ( nKeyPressed == 4 ) && ( fRowAdded == 0 ) )
            {
               fRowAdded = 1;
               GX_FocusControl = edtatraccionId_Internalname;
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nBlankRcdCount11 = (short)(nBlankRcdCount11-1);
         }
         Gx_mode = sMode11;
         AssignAttri("", false, "Gx_mode", Gx_mode);
         sStyleString = "";
         context.WriteHtmlText( "<div id=\""+"Gridproveedor_atraccionContainer"+"Div\" "+sStyleString+">"+"</div>") ;
         context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridproveedor_atraccion", Gridproveedor_atraccionContainer);
         if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
         {
            GxWebStd.gx_hidden_field( context, "Gridproveedor_atraccionContainerData", Gridproveedor_atraccionContainer.ToJavascriptSource());
         }
         if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
         {
            GxWebStd.gx_hidden_field( context, "Gridproveedor_atraccionContainerData"+"V", Gridproveedor_atraccionContainer.GridValuesHidden());
         }
         else
         {
            context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Gridproveedor_atraccionContainerData"+"V"+"\" value='"+Gridproveedor_atraccionContainer.GridValuesHidden()+"'/>") ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read saved values. */
            Z48proveedorId = (int)(context.localUtil.CToN( cgiGet( "Z48proveedorId"), ".", ","));
            Z49proveedorNombre = cgiGet( "Z49proveedorNombre");
            Z50proveedorDireccion = cgiGet( "Z50proveedorDireccion");
            IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ".", ","));
            IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ".", ","));
            Gx_mode = cgiGet( "Mode");
            nRC_GXsfl_53 = (int)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_53"), ".", ","));
            Gx_mode = cgiGet( "vMODE");
            A40000atraccionFoto_GXI = cgiGet( "ATRACCIONFOTO_GXI");
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtproveedorId_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtproveedorId_Internalname), ".", ",") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PROVEEDORID");
               AnyError = 1;
               GX_FocusControl = edtproveedorId_Internalname;
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A48proveedorId = 0;
               AssignAttri("", false, "A48proveedorId", StringUtil.LTrimStr( (decimal)(A48proveedorId), 6, 0));
            }
            else
            {
               A48proveedorId = (int)(context.localUtil.CToN( cgiGet( edtproveedorId_Internalname), ".", ","));
               AssignAttri("", false, "A48proveedorId", StringUtil.LTrimStr( (decimal)(A48proveedorId), 6, 0));
            }
            A49proveedorNombre = cgiGet( edtproveedorNombre_Internalname);
            AssignAttri("", false, "A49proveedorNombre", A49proveedorNombre);
            A50proveedorDireccion = cgiGet( edtproveedorDireccion_Internalname);
            AssignAttri("", false, "A50proveedorDireccion", A50proveedorDireccion);
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            standaloneNotModal( ) ;
         }
         else
         {
            standaloneNotModal( ) ;
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
            {
               Gx_mode = "DSP";
               AssignAttri("", false, "Gx_mode", Gx_mode);
               A48proveedorId = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AssignAttri("", false, "A48proveedorId", StringUtil.LTrimStr( (decimal)(A48proveedorId), 6, 0));
               getEqualNoModal( ) ;
               Gx_mode = "DSP";
               AssignAttri("", false, "Gx_mode", Gx_mode);
               disable_std_buttons_dsp( ) ;
               standaloneModal( ) ;
            }
            else
            {
               Gx_mode = "INS";
               AssignAttri("", false, "Gx_mode", Gx_mode);
               standaloneModal( ) ;
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( IsIns( )  )
            {
               /* Clear variables for new insertion. */
               InitAll0810( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( IsIns( ) )
         {
            bttBtn_delete_Enabled = 0;
            AssignProp("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Enabled), 5, 0), true);
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         bttBtn_delete_Visible = 0;
         AssignProp("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Visible), 5, 0), true);
         bttBtn_first_Visible = 0;
         AssignProp("", false, bttBtn_first_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_first_Visible), 5, 0), true);
         bttBtn_previous_Visible = 0;
         AssignProp("", false, bttBtn_previous_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_previous_Visible), 5, 0), true);
         bttBtn_next_Visible = 0;
         AssignProp("", false, bttBtn_next_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_next_Visible), 5, 0), true);
         bttBtn_last_Visible = 0;
         AssignProp("", false, bttBtn_last_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_last_Visible), 5, 0), true);
         bttBtn_select_Visible = 0;
         AssignProp("", false, bttBtn_select_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_select_Visible), 5, 0), true);
         bttBtn_delete_Visible = 0;
         AssignProp("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Visible), 5, 0), true);
         if ( IsDsp( ) )
         {
            bttBtn_enter_Visible = 0;
            AssignProp("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_enter_Visible), 5, 0), true);
         }
         DisableAttributes0810( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( IsDlt( ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_0811( )
      {
         nGXsfl_53_idx = 0;
         while ( nGXsfl_53_idx < nRC_GXsfl_53 )
         {
            ReadRow0811( ) ;
            if ( ( nRcdExists_11 != 0 ) || ( nIsMod_11 != 0 ) )
            {
               GetKey0811( ) ;
               if ( ( nRcdExists_11 == 0 ) && ( nRcdDeleted_11 == 0 ) )
               {
                  if ( RcdFound11 == 0 )
                  {
                     Gx_mode = "INS";
                     AssignAttri("", false, "Gx_mode", Gx_mode);
                     BeforeValidate0811( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable0811( ) ;
                        CloseExtendedTableCursors0811( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                           AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
                        }
                     }
                  }
                  else
                  {
                     GXCCtl = "ATRACCIONID_" + sGXsfl_53_idx;
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, GXCCtl);
                     AnyError = 1;
                     GX_FocusControl = edtatraccionId_Internalname;
                     AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( RcdFound11 != 0 )
                  {
                     if ( nRcdDeleted_11 != 0 )
                     {
                        Gx_mode = "DLT";
                        AssignAttri("", false, "Gx_mode", Gx_mode);
                        getByPrimaryKey0811( ) ;
                        Load0811( ) ;
                        BeforeValidate0811( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls0811( ) ;
                        }
                     }
                     else
                     {
                        if ( nIsMod_11 != 0 )
                        {
                           Gx_mode = "UPD";
                           AssignAttri("", false, "Gx_mode", Gx_mode);
                           BeforeValidate0811( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable0811( ) ;
                              CloseExtendedTableCursors0811( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                                 AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
                              }
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_11 == 0 )
                     {
                        GXCCtl = "ATRACCIONID_" + sGXsfl_53_idx;
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, GXCCtl);
                        AnyError = 1;
                        GX_FocusControl = edtatraccionId_Internalname;
                        AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
            ChangePostValue( edtatraccionId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A7atraccionId), 6, 0, ".", ""))) ;
            ChangePostValue( edtatraccionNombre_Internalname, StringUtil.RTrim( A8atraccionNombre)) ;
            ChangePostValue( edtatraccionFoto_Internalname, A13atraccionFoto) ;
            ChangePostValue( "ZT_"+"Z7atraccionId_"+sGXsfl_53_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z7atraccionId), 6, 0, ".", ""))) ;
            ChangePostValue( "nRcdDeleted_11_"+sGXsfl_53_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_11), 4, 0, ".", ""))) ;
            ChangePostValue( "nRcdExists_11_"+sGXsfl_53_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_11), 4, 0, ".", ""))) ;
            ChangePostValue( "nIsMod_11_"+sGXsfl_53_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_11), 4, 0, ".", ""))) ;
            if ( nIsMod_11 != 0 )
            {
               ChangePostValue( "ATRACCIONID_"+sGXsfl_53_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtatraccionId_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "ATRACCIONNOMBRE_"+sGXsfl_53_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtatraccionNombre_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "ATRACCIONFOTO_"+sGXsfl_53_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtatraccionFoto_Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
      }

      protected void ResetCaption080( )
      {
      }

      protected void ZM0810( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            if ( ! IsIns( ) )
            {
               Z49proveedorNombre = T00086_A49proveedorNombre[0];
               Z50proveedorDireccion = T00086_A50proveedorDireccion[0];
            }
            else
            {
               Z49proveedorNombre = A49proveedorNombre;
               Z50proveedorDireccion = A50proveedorDireccion;
            }
         }
         if ( GX_JID == -1 )
         {
            Z48proveedorId = A48proveedorId;
            Z49proveedorNombre = A49proveedorNombre;
            Z50proveedorDireccion = A50proveedorDireccion;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            AssignProp("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Enabled), 5, 0), true);
         }
         else
         {
            bttBtn_delete_Enabled = 1;
            AssignProp("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Enabled), 5, 0), true);
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Enabled = 0;
            AssignProp("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_enter_Enabled), 5, 0), true);
         }
         else
         {
            bttBtn_enter_Enabled = 1;
            AssignProp("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_enter_Enabled), 5, 0), true);
         }
      }

      protected void Load0810( )
      {
         /* Using cursor T00087 */
         pr_default.execute(5, new Object[] {A48proveedorId});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound10 = 1;
            A49proveedorNombre = T00087_A49proveedorNombre[0];
            AssignAttri("", false, "A49proveedorNombre", A49proveedorNombre);
            A50proveedorDireccion = T00087_A50proveedorDireccion[0];
            AssignAttri("", false, "A50proveedorDireccion", A50proveedorDireccion);
            ZM0810( -1) ;
         }
         pr_default.close(5);
         OnLoadActions0810( ) ;
      }

      protected void OnLoadActions0810( )
      {
      }

      protected void CheckExtendedTable0810( )
      {
         nIsDirty_10 = 0;
         Gx_BScreen = 1;
         standaloneModal( ) ;
      }

      protected void CloseExtendedTableCursors0810( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey0810( )
      {
         /* Using cursor T00088 */
         pr_default.execute(6, new Object[] {A48proveedorId});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound10 = 1;
         }
         else
         {
            RcdFound10 = 0;
         }
         pr_default.close(6);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00086 */
         pr_default.execute(4, new Object[] {A48proveedorId});
         if ( (pr_default.getStatus(4) != 101) )
         {
            ZM0810( 1) ;
            RcdFound10 = 1;
            A48proveedorId = T00086_A48proveedorId[0];
            AssignAttri("", false, "A48proveedorId", StringUtil.LTrimStr( (decimal)(A48proveedorId), 6, 0));
            A49proveedorNombre = T00086_A49proveedorNombre[0];
            AssignAttri("", false, "A49proveedorNombre", A49proveedorNombre);
            A50proveedorDireccion = T00086_A50proveedorDireccion[0];
            AssignAttri("", false, "A50proveedorDireccion", A50proveedorDireccion);
            Z48proveedorId = A48proveedorId;
            sMode10 = Gx_mode;
            Gx_mode = "DSP";
            AssignAttri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load0810( ) ;
            if ( AnyError == 1 )
            {
               RcdFound10 = 0;
               InitializeNonKey0810( ) ;
            }
            Gx_mode = sMode10;
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound10 = 0;
            InitializeNonKey0810( ) ;
            sMode10 = Gx_mode;
            Gx_mode = "DSP";
            AssignAttri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode10;
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(4);
      }

      protected void getEqualNoModal( )
      {
         GetKey0810( ) ;
         if ( RcdFound10 == 0 )
         {
            Gx_mode = "INS";
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound10 = 0;
         /* Using cursor T00089 */
         pr_default.execute(7, new Object[] {A48proveedorId});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T00089_A48proveedorId[0] < A48proveedorId ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T00089_A48proveedorId[0] > A48proveedorId ) ) )
            {
               A48proveedorId = T00089_A48proveedorId[0];
               AssignAttri("", false, "A48proveedorId", StringUtil.LTrimStr( (decimal)(A48proveedorId), 6, 0));
               RcdFound10 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void move_previous( )
      {
         RcdFound10 = 0;
         /* Using cursor T000810 */
         pr_default.execute(8, new Object[] {A48proveedorId});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T000810_A48proveedorId[0] > A48proveedorId ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T000810_A48proveedorId[0] < A48proveedorId ) ) )
            {
               A48proveedorId = T000810_A48proveedorId[0];
               AssignAttri("", false, "A48proveedorId", StringUtil.LTrimStr( (decimal)(A48proveedorId), 6, 0));
               RcdFound10 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0810( ) ;
         if ( IsIns( ) )
         {
            /* Insert record */
            GX_FocusControl = edtproveedorId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0810( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound10 == 1 )
            {
               if ( A48proveedorId != Z48proveedorId )
               {
                  A48proveedorId = Z48proveedorId;
                  AssignAttri("", false, "A48proveedorId", StringUtil.LTrimStr( (decimal)(A48proveedorId), 6, 0));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "PROVEEDORID");
                  AnyError = 1;
                  GX_FocusControl = edtproveedorId_Internalname;
                  AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( IsDlt( ) )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtproveedorId_Internalname;
                  AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  AssignAttri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update0810( ) ;
                  GX_FocusControl = edtproveedorId_Internalname;
                  AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A48proveedorId != Z48proveedorId )
               {
                  Gx_mode = "INS";
                  AssignAttri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtproveedorId_Internalname;
                  AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0810( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "PROVEEDORID");
                     AnyError = 1;
                     GX_FocusControl = edtproveedorId_Internalname;
                     AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     AssignAttri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtproveedorId_Internalname;
                     AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0810( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A48proveedorId != Z48proveedorId )
         {
            A48proveedorId = Z48proveedorId;
            AssignAttri("", false, "A48proveedorId", StringUtil.LTrimStr( (decimal)(A48proveedorId), 6, 0));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "PROVEEDORID");
            AnyError = 1;
            GX_FocusControl = edtproveedorId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtproveedorId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         getEqualNoModal( ) ;
         if ( RcdFound10 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "PROVEEDORID");
            AnyError = 1;
            GX_FocusControl = edtproveedorId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtproveedorNombre_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         ScanStart0810( ) ;
         if ( RcdFound10 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtproveedorNombre_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd0810( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         move_previous( ) ;
         if ( RcdFound10 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtproveedorNombre_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         move_next( ) ;
         if ( RcdFound10 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtproveedorNombre_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         ScanStart0810( ) ;
         if ( RcdFound10 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound10 != 0 )
            {
               ScanNext0810( ) ;
            }
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtproveedorNombre_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd0810( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency0810( )
      {
         if ( ! IsIns( ) )
         {
            /* Using cursor T00085 */
            pr_default.execute(3, new Object[] {A48proveedorId});
            if ( (pr_default.getStatus(3) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"proveedor"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(3) == 101) || ( StringUtil.StrCmp(Z49proveedorNombre, T00085_A49proveedorNombre[0]) != 0 ) || ( StringUtil.StrCmp(Z50proveedorDireccion, T00085_A50proveedorDireccion[0]) != 0 ) )
            {
               if ( StringUtil.StrCmp(Z49proveedorNombre, T00085_A49proveedorNombre[0]) != 0 )
               {
                  GXUtil.WriteLog("proveedor:[seudo value changed for attri]"+"proveedorNombre");
                  GXUtil.WriteLogRaw("Old: ",Z49proveedorNombre);
                  GXUtil.WriteLogRaw("Current: ",T00085_A49proveedorNombre[0]);
               }
               if ( StringUtil.StrCmp(Z50proveedorDireccion, T00085_A50proveedorDireccion[0]) != 0 )
               {
                  GXUtil.WriteLog("proveedor:[seudo value changed for attri]"+"proveedorDireccion");
                  GXUtil.WriteLogRaw("Old: ",Z50proveedorDireccion);
                  GXUtil.WriteLogRaw("Current: ",T00085_A50proveedorDireccion[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"proveedor"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0810( )
      {
         BeforeValidate0810( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0810( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0810( 0) ;
            CheckOptimisticConcurrency0810( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0810( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0810( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000811 */
                     pr_default.execute(9, new Object[] {A49proveedorNombre, A50proveedorDireccion});
                     A48proveedorId = T000811_A48proveedorId[0];
                     AssignAttri("", false, "A48proveedorId", StringUtil.LTrimStr( (decimal)(A48proveedorId), 6, 0));
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("proveedor") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel0810( ) ;
                           if ( AnyError == 0 )
                           {
                              /* Save values for previous() function. */
                              GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                              ResetCaption080( ) ;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0810( ) ;
            }
            EndLevel0810( ) ;
         }
         CloseExtendedTableCursors0810( ) ;
      }

      protected void Update0810( )
      {
         BeforeValidate0810( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0810( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0810( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0810( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0810( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000812 */
                     pr_default.execute(10, new Object[] {A49proveedorNombre, A50proveedorDireccion, A48proveedorId});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("proveedor") ;
                     if ( (pr_default.getStatus(10) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"proveedor"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0810( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel0810( ) ;
                           if ( AnyError == 0 )
                           {
                              getByPrimaryKey( ) ;
                              GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), "SuccessfullyUpdated", 0, "", true);
                              ResetCaption080( ) ;
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0810( ) ;
         }
         CloseExtendedTableCursors0810( ) ;
      }

      protected void DeferredUpdate0810( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         AssignAttri("", false, "Gx_mode", Gx_mode);
         BeforeValidate0810( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0810( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0810( ) ;
            AfterConfirm0810( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0810( ) ;
               if ( AnyError == 0 )
               {
                  ScanStart0811( ) ;
                  while ( RcdFound11 != 0 )
                  {
                     getByPrimaryKey0811( ) ;
                     Delete0811( ) ;
                     ScanNext0811( ) ;
                  }
                  ScanEnd0811( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000813 */
                     pr_default.execute(11, new Object[] {A48proveedorId});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("proveedor") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( delete) rules */
                        /* End of After( delete) rules */
                        if ( AnyError == 0 )
                        {
                           move_next( ) ;
                           if ( RcdFound10 == 0 )
                           {
                              InitAll0810( ) ;
                              Gx_mode = "INS";
                              AssignAttri("", false, "Gx_mode", Gx_mode);
                           }
                           else
                           {
                              getByPrimaryKey( ) ;
                              Gx_mode = "UPD";
                              AssignAttri("", false, "Gx_mode", Gx_mode);
                           }
                           GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), "SuccessfullyDeleted", 0, "", true);
                           ResetCaption080( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
         sMode10 = Gx_mode;
         Gx_mode = "DLT";
         AssignAttri("", false, "Gx_mode", Gx_mode);
         EndLevel0810( ) ;
         Gx_mode = sMode10;
         AssignAttri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls0810( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void ProcessNestedLevel0811( )
      {
         nGXsfl_53_idx = 0;
         while ( nGXsfl_53_idx < nRC_GXsfl_53 )
         {
            ReadRow0811( ) ;
            if ( ( nRcdExists_11 != 0 ) || ( nIsMod_11 != 0 ) )
            {
               standaloneNotModal0811( ) ;
               GetKey0811( ) ;
               if ( ( nRcdExists_11 == 0 ) && ( nRcdDeleted_11 == 0 ) )
               {
                  Gx_mode = "INS";
                  AssignAttri("", false, "Gx_mode", Gx_mode);
                  Insert0811( ) ;
               }
               else
               {
                  if ( RcdFound11 != 0 )
                  {
                     if ( ( nRcdDeleted_11 != 0 ) && ( nRcdExists_11 != 0 ) )
                     {
                        Gx_mode = "DLT";
                        AssignAttri("", false, "Gx_mode", Gx_mode);
                        Delete0811( ) ;
                     }
                     else
                     {
                        if ( nRcdExists_11 != 0 )
                        {
                           Gx_mode = "UPD";
                           AssignAttri("", false, "Gx_mode", Gx_mode);
                           Update0811( ) ;
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_11 == 0 )
                     {
                        GXCCtl = "ATRACCIONID_" + sGXsfl_53_idx;
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, GXCCtl);
                        AnyError = 1;
                        GX_FocusControl = edtatraccionId_Internalname;
                        AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
            ChangePostValue( edtatraccionId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A7atraccionId), 6, 0, ".", ""))) ;
            ChangePostValue( edtatraccionNombre_Internalname, StringUtil.RTrim( A8atraccionNombre)) ;
            ChangePostValue( edtatraccionFoto_Internalname, A13atraccionFoto) ;
            ChangePostValue( "ZT_"+"Z7atraccionId_"+sGXsfl_53_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z7atraccionId), 6, 0, ".", ""))) ;
            ChangePostValue( "nRcdDeleted_11_"+sGXsfl_53_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_11), 4, 0, ".", ""))) ;
            ChangePostValue( "nRcdExists_11_"+sGXsfl_53_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_11), 4, 0, ".", ""))) ;
            ChangePostValue( "nIsMod_11_"+sGXsfl_53_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_11), 4, 0, ".", ""))) ;
            if ( nIsMod_11 != 0 )
            {
               ChangePostValue( "ATRACCIONID_"+sGXsfl_53_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtatraccionId_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "ATRACCIONNOMBRE_"+sGXsfl_53_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtatraccionNombre_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "ATRACCIONFOTO_"+sGXsfl_53_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtatraccionFoto_Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
         InitAll0811( ) ;
         if ( AnyError != 0 )
         {
         }
         nRcdExists_11 = 0;
         nIsMod_11 = 0;
         nRcdDeleted_11 = 0;
      }

      protected void ProcessLevel0810( )
      {
         /* Save parent mode. */
         sMode10 = Gx_mode;
         ProcessNestedLevel0811( ) ;
         if ( AnyError != 0 )
         {
         }
         /* Restore parent mode. */
         Gx_mode = sMode10;
         AssignAttri("", false, "Gx_mode", Gx_mode);
         /* ' Update level parameters */
      }

      protected void EndLevel0810( )
      {
         if ( ! IsIns( ) )
         {
            pr_default.close(3);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0810( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(4);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(2);
            context.CommitDataStores("proveedor",pr_default);
            if ( AnyError == 0 )
            {
               ConfirmValues080( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(4);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(2);
            context.RollbackDataStores("proveedor",pr_default);
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0810( )
      {
         /* Using cursor T000814 */
         pr_default.execute(12);
         RcdFound10 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound10 = 1;
            A48proveedorId = T000814_A48proveedorId[0];
            AssignAttri("", false, "A48proveedorId", StringUtil.LTrimStr( (decimal)(A48proveedorId), 6, 0));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0810( )
      {
         /* Scan next routine */
         pr_default.readNext(12);
         RcdFound10 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound10 = 1;
            A48proveedorId = T000814_A48proveedorId[0];
            AssignAttri("", false, "A48proveedorId", StringUtil.LTrimStr( (decimal)(A48proveedorId), 6, 0));
         }
      }

      protected void ScanEnd0810( )
      {
         pr_default.close(12);
      }

      protected void AfterConfirm0810( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0810( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0810( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0810( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0810( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0810( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0810( )
      {
         edtproveedorId_Enabled = 0;
         AssignProp("", false, edtproveedorId_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtproveedorId_Enabled), 5, 0), true);
         edtproveedorNombre_Enabled = 0;
         AssignProp("", false, edtproveedorNombre_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtproveedorNombre_Enabled), 5, 0), true);
         edtproveedorDireccion_Enabled = 0;
         AssignProp("", false, edtproveedorDireccion_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtproveedorDireccion_Enabled), 5, 0), true);
      }

      protected void ZM0811( short GX_JID )
      {
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            if ( ! IsIns( ) )
            {
            }
            else
            {
            }
         }
         if ( GX_JID == -2 )
         {
            Z48proveedorId = A48proveedorId;
            Z7atraccionId = A7atraccionId;
            Z8atraccionNombre = A8atraccionNombre;
            Z13atraccionFoto = A13atraccionFoto;
            Z40000atraccionFoto_GXI = A40000atraccionFoto_GXI;
         }
      }

      protected void standaloneNotModal0811( )
      {
      }

      protected void standaloneModal0811( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            edtatraccionId_Enabled = 0;
            AssignProp("", false, edtatraccionId_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtatraccionId_Enabled), 5, 0), !bGXsfl_53_Refreshing);
         }
         else
         {
            edtatraccionId_Enabled = 1;
            AssignProp("", false, edtatraccionId_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtatraccionId_Enabled), 5, 0), !bGXsfl_53_Refreshing);
         }
      }

      protected void Load0811( )
      {
         /* Using cursor T000815 */
         pr_default.execute(13, new Object[] {A48proveedorId, A7atraccionId});
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound11 = 1;
            A8atraccionNombre = T000815_A8atraccionNombre[0];
            A40000atraccionFoto_GXI = T000815_A40000atraccionFoto_GXI[0];
            AssignProp("", false, edtatraccionFoto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A13atraccionFoto)) ? A40000atraccionFoto_GXI : context.convertURL( context.PathToRelativeUrl( A13atraccionFoto))), !bGXsfl_53_Refreshing);
            AssignProp("", false, edtatraccionFoto_Internalname, "SrcSet", context.GetImageSrcSet( A13atraccionFoto), true);
            A13atraccionFoto = T000815_A13atraccionFoto[0];
            AssignProp("", false, edtatraccionFoto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A13atraccionFoto)) ? A40000atraccionFoto_GXI : context.convertURL( context.PathToRelativeUrl( A13atraccionFoto))), !bGXsfl_53_Refreshing);
            AssignProp("", false, edtatraccionFoto_Internalname, "SrcSet", context.GetImageSrcSet( A13atraccionFoto), true);
            ZM0811( -2) ;
         }
         pr_default.close(13);
         OnLoadActions0811( ) ;
      }

      protected void OnLoadActions0811( )
      {
      }

      protected void CheckExtendedTable0811( )
      {
         nIsDirty_11 = 0;
         Gx_BScreen = 1;
         standaloneModal0811( ) ;
         /* Using cursor T00084 */
         pr_default.execute(2, new Object[] {A7atraccionId});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GXCCtl = "ATRACCIONID_" + sGXsfl_53_idx;
            GX_msglist.addItem("No matching 'atraccion'.", "ForeignKeyNotFound", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtatraccionId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A8atraccionNombre = T00084_A8atraccionNombre[0];
         A40000atraccionFoto_GXI = T00084_A40000atraccionFoto_GXI[0];
         AssignProp("", false, edtatraccionFoto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A13atraccionFoto)) ? A40000atraccionFoto_GXI : context.convertURL( context.PathToRelativeUrl( A13atraccionFoto))), !bGXsfl_53_Refreshing);
         AssignProp("", false, edtatraccionFoto_Internalname, "SrcSet", context.GetImageSrcSet( A13atraccionFoto), true);
         A13atraccionFoto = T00084_A13atraccionFoto[0];
         AssignProp("", false, edtatraccionFoto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A13atraccionFoto)) ? A40000atraccionFoto_GXI : context.convertURL( context.PathToRelativeUrl( A13atraccionFoto))), !bGXsfl_53_Refreshing);
         AssignProp("", false, edtatraccionFoto_Internalname, "SrcSet", context.GetImageSrcSet( A13atraccionFoto), true);
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors0811( )
      {
         pr_default.close(2);
      }

      protected void enableDisable0811( )
      {
      }

      protected void gxLoad_3( int A7atraccionId )
      {
         /* Using cursor T000816 */
         pr_default.execute(14, new Object[] {A7atraccionId});
         if ( (pr_default.getStatus(14) == 101) )
         {
            GXCCtl = "ATRACCIONID_" + sGXsfl_53_idx;
            GX_msglist.addItem("No matching 'atraccion'.", "ForeignKeyNotFound", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtatraccionId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A8atraccionNombre = T000816_A8atraccionNombre[0];
         A40000atraccionFoto_GXI = T000816_A40000atraccionFoto_GXI[0];
         AssignProp("", false, edtatraccionFoto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A13atraccionFoto)) ? A40000atraccionFoto_GXI : context.convertURL( context.PathToRelativeUrl( A13atraccionFoto))), !bGXsfl_53_Refreshing);
         AssignProp("", false, edtatraccionFoto_Internalname, "SrcSet", context.GetImageSrcSet( A13atraccionFoto), true);
         A13atraccionFoto = T000816_A13atraccionFoto[0];
         AssignProp("", false, edtatraccionFoto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A13atraccionFoto)) ? A40000atraccionFoto_GXI : context.convertURL( context.PathToRelativeUrl( A13atraccionFoto))), !bGXsfl_53_Refreshing);
         AssignProp("", false, edtatraccionFoto_Internalname, "SrcSet", context.GetImageSrcSet( A13atraccionFoto), true);
         GxWebStd.set_html_headers( context, 0, "", "");
         AddString( "[[") ;
         AddString( "\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A8atraccionNombre))+"\""+","+"\""+GXUtil.EncodeJSConstant( A13atraccionFoto)+"\""+","+"\""+GXUtil.EncodeJSConstant( A40000atraccionFoto_GXI)+"\"") ;
         AddString( "]") ;
         if ( (pr_default.getStatus(14) == 101) )
         {
            AddString( ",") ;
            AddString( "101") ;
         }
         AddString( "]") ;
         pr_default.close(14);
      }

      protected void GetKey0811( )
      {
         /* Using cursor T000817 */
         pr_default.execute(15, new Object[] {A48proveedorId, A7atraccionId});
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound11 = 1;
         }
         else
         {
            RcdFound11 = 0;
         }
         pr_default.close(15);
      }

      protected void getByPrimaryKey0811( )
      {
         /* Using cursor T00083 */
         pr_default.execute(1, new Object[] {A48proveedorId, A7atraccionId});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0811( 2) ;
            RcdFound11 = 1;
            InitializeNonKey0811( ) ;
            A7atraccionId = T00083_A7atraccionId[0];
            Z48proveedorId = A48proveedorId;
            Z7atraccionId = A7atraccionId;
            sMode11 = Gx_mode;
            Gx_mode = "DSP";
            AssignAttri("", false, "Gx_mode", Gx_mode);
            standaloneModal0811( ) ;
            Load0811( ) ;
            Gx_mode = sMode11;
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound11 = 0;
            InitializeNonKey0811( ) ;
            sMode11 = Gx_mode;
            Gx_mode = "DSP";
            AssignAttri("", false, "Gx_mode", Gx_mode);
            standaloneModal0811( ) ;
            Gx_mode = sMode11;
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         if ( IsDsp( ) || IsDlt( ) )
         {
            DisableAttributes0811( ) ;
         }
         pr_default.close(1);
      }

      protected void CheckOptimisticConcurrency0811( )
      {
         if ( ! IsIns( ) )
         {
            /* Using cursor T00082 */
            pr_default.execute(0, new Object[] {A48proveedorId, A7atraccionId});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"proveedoratraccion"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"proveedoratraccion"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0811( )
      {
         BeforeValidate0811( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0811( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0811( 0) ;
            CheckOptimisticConcurrency0811( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0811( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0811( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000818 */
                     pr_default.execute(16, new Object[] {A48proveedorId, A7atraccionId});
                     pr_default.close(16);
                     dsDefault.SmartCacheProvider.SetUpdated("proveedoratraccion") ;
                     if ( (pr_default.getStatus(16) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0811( ) ;
            }
            EndLevel0811( ) ;
         }
         CloseExtendedTableCursors0811( ) ;
      }

      protected void Update0811( )
      {
         BeforeValidate0811( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0811( ) ;
         }
         if ( ( nIsMod_11 != 0 ) || ( nIsDirty_11 != 0 ) )
         {
            if ( AnyError == 0 )
            {
               CheckOptimisticConcurrency0811( ) ;
               if ( AnyError == 0 )
               {
                  AfterConfirm0811( ) ;
                  if ( AnyError == 0 )
                  {
                     BeforeUpdate0811( ) ;
                     if ( AnyError == 0 )
                     {
                        /* No attributes to update on table [proveedoratraccion] */
                        DeferredUpdate0811( ) ;
                        if ( AnyError == 0 )
                        {
                           /* Start of After( update) rules */
                           /* End of After( update) rules */
                           if ( AnyError == 0 )
                           {
                              getByPrimaryKey0811( ) ;
                           }
                        }
                        else
                        {
                           GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                           AnyError = 1;
                        }
                     }
                  }
               }
               EndLevel0811( ) ;
            }
         }
         CloseExtendedTableCursors0811( ) ;
      }

      protected void DeferredUpdate0811( )
      {
      }

      protected void Delete0811( )
      {
         Gx_mode = "DLT";
         AssignAttri("", false, "Gx_mode", Gx_mode);
         BeforeValidate0811( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0811( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0811( ) ;
            AfterConfirm0811( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0811( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000819 */
                  pr_default.execute(17, new Object[] {A48proveedorId, A7atraccionId});
                  pr_default.close(17);
                  dsDefault.SmartCacheProvider.SetUpdated("proveedoratraccion") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode11 = Gx_mode;
         Gx_mode = "DLT";
         AssignAttri("", false, "Gx_mode", Gx_mode);
         EndLevel0811( ) ;
         Gx_mode = sMode11;
         AssignAttri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls0811( )
      {
         standaloneModal0811( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T000820 */
            pr_default.execute(18, new Object[] {A7atraccionId});
            A8atraccionNombre = T000820_A8atraccionNombre[0];
            A40000atraccionFoto_GXI = T000820_A40000atraccionFoto_GXI[0];
            AssignProp("", false, edtatraccionFoto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A13atraccionFoto)) ? A40000atraccionFoto_GXI : context.convertURL( context.PathToRelativeUrl( A13atraccionFoto))), !bGXsfl_53_Refreshing);
            AssignProp("", false, edtatraccionFoto_Internalname, "SrcSet", context.GetImageSrcSet( A13atraccionFoto), true);
            A13atraccionFoto = T000820_A13atraccionFoto[0];
            AssignProp("", false, edtatraccionFoto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A13atraccionFoto)) ? A40000atraccionFoto_GXI : context.convertURL( context.PathToRelativeUrl( A13atraccionFoto))), !bGXsfl_53_Refreshing);
            AssignProp("", false, edtatraccionFoto_Internalname, "SrcSet", context.GetImageSrcSet( A13atraccionFoto), true);
            pr_default.close(18);
         }
      }

      protected void EndLevel0811( )
      {
         if ( ! IsIns( ) )
         {
            pr_default.close(0);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0811( )
      {
         /* Scan By routine */
         /* Using cursor T000821 */
         pr_default.execute(19, new Object[] {A48proveedorId});
         RcdFound11 = 0;
         if ( (pr_default.getStatus(19) != 101) )
         {
            RcdFound11 = 1;
            A7atraccionId = T000821_A7atraccionId[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0811( )
      {
         /* Scan next routine */
         pr_default.readNext(19);
         RcdFound11 = 0;
         if ( (pr_default.getStatus(19) != 101) )
         {
            RcdFound11 = 1;
            A7atraccionId = T000821_A7atraccionId[0];
         }
      }

      protected void ScanEnd0811( )
      {
         pr_default.close(19);
      }

      protected void AfterConfirm0811( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0811( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0811( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0811( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0811( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0811( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0811( )
      {
         edtatraccionId_Enabled = 0;
         AssignProp("", false, edtatraccionId_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtatraccionId_Enabled), 5, 0), !bGXsfl_53_Refreshing);
         edtatraccionNombre_Enabled = 0;
         AssignProp("", false, edtatraccionNombre_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtatraccionNombre_Enabled), 5, 0), !bGXsfl_53_Refreshing);
         edtatraccionFoto_Enabled = 0;
         AssignProp("", false, edtatraccionFoto_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtatraccionFoto_Enabled), 5, 0), !bGXsfl_53_Refreshing);
      }

      protected void send_integrity_lvl_hashes0811( )
      {
      }

      protected void send_integrity_lvl_hashes0810( )
      {
      }

      protected void SubsflControlProps_5311( )
      {
         edtatraccionId_Internalname = "ATRACCIONID_"+sGXsfl_53_idx;
         imgprompt_7_Internalname = "PROMPT_7_"+sGXsfl_53_idx;
         edtatraccionNombre_Internalname = "ATRACCIONNOMBRE_"+sGXsfl_53_idx;
         edtatraccionFoto_Internalname = "ATRACCIONFOTO_"+sGXsfl_53_idx;
      }

      protected void SubsflControlProps_fel_5311( )
      {
         edtatraccionId_Internalname = "ATRACCIONID_"+sGXsfl_53_fel_idx;
         imgprompt_7_Internalname = "PROMPT_7_"+sGXsfl_53_fel_idx;
         edtatraccionNombre_Internalname = "ATRACCIONNOMBRE_"+sGXsfl_53_fel_idx;
         edtatraccionFoto_Internalname = "ATRACCIONFOTO_"+sGXsfl_53_fel_idx;
      }

      protected void AddRow0811( )
      {
         nGXsfl_53_idx = (int)(nGXsfl_53_idx+1);
         sGXsfl_53_idx = StringUtil.PadL( StringUtil.LTrimStr( (decimal)(nGXsfl_53_idx), 4, 0), 4, "0");
         SubsflControlProps_5311( ) ;
         SendRow0811( ) ;
      }

      protected void SendRow0811( )
      {
         Gridproveedor_atraccionRow = GXWebRow.GetNew(context);
         if ( subGridproveedor_atraccion_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGridproveedor_atraccion_Backstyle = 0;
            if ( StringUtil.StrCmp(subGridproveedor_atraccion_Class, "") != 0 )
            {
               subGridproveedor_atraccion_Linesclass = subGridproveedor_atraccion_Class+"Odd";
            }
         }
         else if ( subGridproveedor_atraccion_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGridproveedor_atraccion_Backstyle = 0;
            subGridproveedor_atraccion_Backcolor = subGridproveedor_atraccion_Allbackcolor;
            if ( StringUtil.StrCmp(subGridproveedor_atraccion_Class, "") != 0 )
            {
               subGridproveedor_atraccion_Linesclass = subGridproveedor_atraccion_Class+"Uniform";
            }
         }
         else if ( subGridproveedor_atraccion_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGridproveedor_atraccion_Backstyle = 1;
            if ( StringUtil.StrCmp(subGridproveedor_atraccion_Class, "") != 0 )
            {
               subGridproveedor_atraccion_Linesclass = subGridproveedor_atraccion_Class+"Odd";
            }
            subGridproveedor_atraccion_Backcolor = (int)(0x0);
         }
         else if ( subGridproveedor_atraccion_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGridproveedor_atraccion_Backstyle = 1;
            if ( ((int)((nGXsfl_53_idx) % (2))) == 0 )
            {
               subGridproveedor_atraccion_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridproveedor_atraccion_Class, "") != 0 )
               {
                  subGridproveedor_atraccion_Linesclass = subGridproveedor_atraccion_Class+"Even";
               }
            }
            else
            {
               subGridproveedor_atraccion_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridproveedor_atraccion_Class, "") != 0 )
               {
                  subGridproveedor_atraccion_Linesclass = subGridproveedor_atraccion_Class+"Odd";
               }
            }
         }
         imgprompt_7_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0)||(StringUtil.StrCmp(Gx_mode, "UPD")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0020.aspx"+"',["+"{Ctrl:gx.dom.el('"+"ATRACCIONID_"+sGXsfl_53_idx+"'), id:'"+"ATRACCIONID_"+sGXsfl_53_idx+"'"+",IOType:'out'}"+"],"+"gx.dom.form()."+"nIsMod_11_"+sGXsfl_53_idx+","+"'', false"+","+"false"+");");
         /* Subfile cell */
         /* Single line edit */
         TempTags = " data-gxoch1=\"gx.fn.setControlValue('nIsMod_11_" + sGXsfl_53_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_53_idx + "',53)\"";
         ROClassString = "Attribute";
         Gridproveedor_atraccionRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtatraccionId_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A7atraccionId), 6, 0, ".", "")),StringUtil.LTrim( context.localUtil.Format( (decimal)(A7atraccionId), "ZZZZZ9")),TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,54);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtatraccionId_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(int)edtatraccionId_Enabled,(short)1,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)53,(short)1,(short)-1,(short)0,(bool)true,(String)"Id",(String)"right",(bool)false,(String)""});
         /* Subfile cell */
         /* Static images/pictures */
         ClassString = "gx-prompt Image";
         StyleString = "";
         sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
         Gridproveedor_atraccionRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)imgprompt_7_Internalname,(String)sImgUrl,(String)imgprompt_7_Link,(String)"",(String)"",context.GetTheme( ),(int)imgprompt_7_Visible,(short)1,(String)"",(String)"",(short)0,(short)0,(short)0,(String)"",(short)0,(String)"",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)false,(bool)false,context.GetImageSrcSet( sImgUrl)});
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "Attribute";
         Gridproveedor_atraccionRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtatraccionNombre_Internalname,StringUtil.RTrim( A8atraccionNombre),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtatraccionNombre_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(int)edtatraccionNombre_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)53,(short)1,(short)-1,(short)-1,(bool)true,(String)"Name",(String)"left",(bool)true,(String)""});
         /* Subfile cell */
         /* Static Bitmap Variable */
         ClassString = "ImageAttribute";
         StyleString = "";
         A13atraccionFoto_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( A13atraccionFoto))&&String.IsNullOrEmpty(StringUtil.RTrim( A40000atraccionFoto_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( A13atraccionFoto)));
         sImgUrl = (String.IsNullOrEmpty(StringUtil.RTrim( A13atraccionFoto)) ? A40000atraccionFoto_GXI : context.PathToRelativeUrl( A13atraccionFoto));
         Gridproveedor_atraccionRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtatraccionFoto_Internalname,(String)sImgUrl,(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtatraccionFoto_Enabled,(String)"",(String)"",(short)1,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)0,(bool)A13atraccionFoto_IsBlob,(bool)true,context.GetImageSrcSet( sImgUrl)});
         AssignProp("", false, edtatraccionFoto_Internalname, "URL", (String.IsNullOrEmpty(StringUtil.RTrim( A13atraccionFoto)) ? A40000atraccionFoto_GXI : context.PathToRelativeUrl( A13atraccionFoto)), !bGXsfl_53_Refreshing);
         AssignProp("", false, edtatraccionFoto_Internalname, "IsBlob", StringUtil.BoolToStr( A13atraccionFoto_IsBlob), !bGXsfl_53_Refreshing);
         context.httpAjaxContext.ajax_sending_grid_row(Gridproveedor_atraccionRow);
         send_integrity_lvl_hashes0811( ) ;
         GXCCtl = "Z7atraccionId_" + sGXsfl_53_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z7atraccionId), 6, 0, ".", "")));
         GXCCtl = "nRcdDeleted_11_" + sGXsfl_53_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_11), 4, 0, ".", "")));
         GXCCtl = "nRcdExists_11_" + sGXsfl_53_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_11), 4, 0, ".", "")));
         GXCCtl = "nIsMod_11_" + sGXsfl_53_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_11), 4, 0, ".", "")));
         GXCCtl = "ATRACCIONFOTO_" + sGXsfl_53_idx;
         GXCCtlgxBlob = GXCCtl + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, A13atraccionFoto);
         GxWebStd.gx_hidden_field( context, "ATRACCIONID_"+sGXsfl_53_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtatraccionId_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "ATRACCIONNOMBRE_"+sGXsfl_53_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtatraccionNombre_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "ATRACCIONFOTO_"+sGXsfl_53_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtatraccionFoto_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "PROMPT_7_"+sGXsfl_53_idx+"Link", StringUtil.RTrim( imgprompt_7_Link));
         context.httpAjaxContext.ajax_sending_grid_row(null);
         Gridproveedor_atraccionContainer.AddRow(Gridproveedor_atraccionRow);
      }

      protected void ReadRow0811( )
      {
         nGXsfl_53_idx = (int)(nGXsfl_53_idx+1);
         sGXsfl_53_idx = StringUtil.PadL( StringUtil.LTrimStr( (decimal)(nGXsfl_53_idx), 4, 0), 4, "0");
         SubsflControlProps_5311( ) ;
         edtatraccionId_Enabled = (int)(context.localUtil.CToN( cgiGet( "ATRACCIONID_"+sGXsfl_53_idx+"Enabled"), ".", ","));
         edtatraccionNombre_Enabled = (int)(context.localUtil.CToN( cgiGet( "ATRACCIONNOMBRE_"+sGXsfl_53_idx+"Enabled"), ".", ","));
         edtatraccionFoto_Enabled = (int)(context.localUtil.CToN( cgiGet( "ATRACCIONFOTO_"+sGXsfl_53_idx+"Enabled"), ".", ","));
         imgprompt_7_Link = cgiGet( "PROMPT_7_"+sGXsfl_53_idx+"Link");
         if ( ( ( context.localUtil.CToN( cgiGet( edtatraccionId_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtatraccionId_Internalname), ".", ",") > Convert.ToDecimal( 999999 )) ) )
         {
            GXCCtl = "ATRACCIONID_" + sGXsfl_53_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtatraccionId_Internalname;
            wbErr = true;
            A7atraccionId = 0;
         }
         else
         {
            A7atraccionId = (int)(context.localUtil.CToN( cgiGet( edtatraccionId_Internalname), ".", ","));
         }
         A8atraccionNombre = cgiGet( edtatraccionNombre_Internalname);
         A13atraccionFoto = cgiGet( edtatraccionFoto_Internalname);
         GXCCtl = "Z7atraccionId_" + sGXsfl_53_idx;
         Z7atraccionId = (int)(context.localUtil.CToN( cgiGet( GXCCtl), ".", ","));
         GXCCtl = "nRcdDeleted_11_" + sGXsfl_53_idx;
         nRcdDeleted_11 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ".", ","));
         GXCCtl = "nRcdExists_11_" + sGXsfl_53_idx;
         nRcdExists_11 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ".", ","));
         GXCCtl = "nIsMod_11_" + sGXsfl_53_idx;
         nIsMod_11 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ".", ","));
         getMultimediaValue(edtatraccionFoto_Internalname, ref  A13atraccionFoto, ref  A40000atraccionFoto_GXI);
      }

      protected void assign_properties_default( )
      {
         defedtatraccionId_Enabled = edtatraccionId_Enabled;
      }

      protected void ConfirmValues080( )
      {
         nGXsfl_53_idx = 0;
         sGXsfl_53_idx = StringUtil.PadL( StringUtil.LTrimStr( (decimal)(nGXsfl_53_idx), 4, 0), 4, "0");
         SubsflControlProps_5311( ) ;
         while ( nGXsfl_53_idx < nRC_GXsfl_53 )
         {
            nGXsfl_53_idx = (int)(nGXsfl_53_idx+1);
            sGXsfl_53_idx = StringUtil.PadL( StringUtil.LTrimStr( (decimal)(nGXsfl_53_idx), 4, 0), 4, "0");
            SubsflControlProps_5311( ) ;
            ChangePostValue( "Z7atraccionId_"+sGXsfl_53_idx, cgiGet( "ZT_"+"Z7atraccionId_"+sGXsfl_53_idx)) ;
            DeletePostValue( "ZT_"+"Z7atraccionId_"+sGXsfl_53_idx) ;
         }
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 136889), false, true);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 136889), false, true);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 136889), false, true);
         context.AddJavascriptSource("gxcfg.js", "?2019123216490", false, true);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + "-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("proveedor.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" title=\"submit\" style=\"display:none\" disabled>") ;
         AssignProp("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "Z48proveedorId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z48proveedorId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z49proveedorNombre", StringUtil.RTrim( Z49proveedorNombre));
         GxWebStd.gx_hidden_field( context, "Z50proveedorDireccion", Z50proveedorDireccion);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_53", StringUtil.LTrim( StringUtil.NToC( (decimal)(nGXsfl_53_idx), 8, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "ATRACCIONFOTO_GXI", A40000atraccionFoto_GXI);
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendSecurityToken(sPrefix);
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("proveedor.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "proveedor" ;
      }

      public override String GetPgmdesc( )
      {
         return "proveedor" ;
      }

      protected void InitializeNonKey0810( )
      {
         A49proveedorNombre = "";
         AssignAttri("", false, "A49proveedorNombre", A49proveedorNombre);
         A50proveedorDireccion = "";
         AssignAttri("", false, "A50proveedorDireccion", A50proveedorDireccion);
         Z49proveedorNombre = "";
         Z50proveedorDireccion = "";
      }

      protected void InitAll0810( )
      {
         A48proveedorId = 0;
         AssignAttri("", false, "A48proveedorId", StringUtil.LTrimStr( (decimal)(A48proveedorId), 6, 0));
         InitializeNonKey0810( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void InitializeNonKey0811( )
      {
         A8atraccionNombre = "";
         A13atraccionFoto = "";
         AssignProp("", false, edtatraccionFoto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A13atraccionFoto)) ? A40000atraccionFoto_GXI : context.convertURL( context.PathToRelativeUrl( A13atraccionFoto))), !bGXsfl_53_Refreshing);
         AssignProp("", false, edtatraccionFoto_Internalname, "SrcSet", context.GetImageSrcSet( A13atraccionFoto), true);
         A40000atraccionFoto_GXI = "";
         AssignProp("", false, edtatraccionFoto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A13atraccionFoto)) ? A40000atraccionFoto_GXI : context.convertURL( context.PathToRelativeUrl( A13atraccionFoto))), !bGXsfl_53_Refreshing);
         AssignProp("", false, edtatraccionFoto_Internalname, "SrcSet", context.GetImageSrcSet( A13atraccionFoto), true);
      }

      protected void InitAll0811( )
      {
         A7atraccionId = 0;
         InitializeNonKey0811( ) ;
      }

      protected void StandaloneModalInsert0811( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ));
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2019123216494", true, true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.eng.js", "?"+GetCacheInvalidationToken( ), false, true);
         context.AddJavascriptSource("proveedor.js", "?2019123216494", false, true);
         /* End function include_jscripts */
      }

      protected void init_level_properties11( )
      {
         edtatraccionId_Enabled = defedtatraccionId_Enabled;
         AssignProp("", false, edtatraccionId_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtatraccionId_Enabled), 5, 0), !bGXsfl_53_Refreshing);
      }

      protected void init_default_properties( )
      {
         lblTitle_Internalname = "TITLE";
         divTitlecontainer_Internalname = "TITLECONTAINER";
         bttBtn_first_Internalname = "BTN_FIRST";
         bttBtn_previous_Internalname = "BTN_PREVIOUS";
         bttBtn_next_Internalname = "BTN_NEXT";
         bttBtn_last_Internalname = "BTN_LAST";
         bttBtn_select_Internalname = "BTN_SELECT";
         divToolbarcell_Internalname = "TOOLBARCELL";
         edtproveedorId_Internalname = "PROVEEDORID";
         edtproveedorNombre_Internalname = "PROVEEDORNOMBRE";
         edtproveedorDireccion_Internalname = "PROVEEDORDIRECCION";
         lblTitleatraccion_Internalname = "TITLEATRACCION";
         edtatraccionId_Internalname = "ATRACCIONID";
         edtatraccionNombre_Internalname = "ATRACCIONNOMBRE";
         edtatraccionFoto_Internalname = "ATRACCIONFOTO";
         divAtracciontable_Internalname = "ATRACCIONTABLE";
         divFormcontainer_Internalname = "FORMCONTAINER";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
         imgprompt_7_Internalname = "PROMPT_7";
         subGridproveedor_atraccion_Internalname = "GRIDPROVEEDOR_ATRACCION";
      }

      public override void initialize_properties( )
      {
         context.SetDefaultTheme("Carmine");
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "proveedor";
         edtatraccionNombre_Jsonclick = "";
         imgprompt_7_Visible = 1;
         imgprompt_7_Link = "";
         imgprompt_7_Visible = 1;
         edtatraccionId_Jsonclick = "";
         subGridproveedor_atraccion_Class = "Grid";
         subGridproveedor_atraccion_Backcolorstyle = 0;
         subGridproveedor_atraccion_Allowcollapsing = 0;
         subGridproveedor_atraccion_Allowselection = 0;
         edtatraccionFoto_Enabled = 0;
         edtatraccionNombre_Enabled = 0;
         edtatraccionId_Enabled = 1;
         subGridproveedor_atraccion_Header = "";
         bttBtn_delete_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Enabled = 1;
         bttBtn_enter_Visible = 1;
         edtproveedorDireccion_Enabled = 1;
         edtproveedorNombre_Jsonclick = "";
         edtproveedorNombre_Enabled = 1;
         edtproveedorId_Jsonclick = "";
         edtproveedorId_Enabled = 1;
         bttBtn_select_Visible = 1;
         bttBtn_last_Visible = 1;
         bttBtn_next_Visible = 1;
         bttBtn_previous_Visible = 1;
         bttBtn_first_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGridproveedor_atraccion_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         Gx_mode = "INS";
         AssignAttri("", false, "Gx_mode", Gx_mode);
         SubsflControlProps_5311( ) ;
         while ( nGXsfl_53_idx <= nRC_GXsfl_53 )
         {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            standaloneNotModal0811( ) ;
            standaloneModal0811( ) ;
            init_web_controls( ) ;
            dynload_actions( ) ;
            SendRow0811( ) ;
            nGXsfl_53_idx = (int)(nGXsfl_53_idx+1);
            sGXsfl_53_idx = StringUtil.PadL( StringUtil.LTrimStr( (decimal)(nGXsfl_53_idx), 4, 0), 4, "0");
            SubsflControlProps_5311( ) ;
         }
         AddString( context.httpAjaxContext.getJSONContainerResponse( Gridproveedor_atraccionContainer)) ;
         /* End function gxnrGridproveedor_atraccion_newrow */
      }

      protected void init_web_controls( )
      {
         /* End function init_web_controls */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         getEqualNoModal( ) ;
         GX_FocusControl = edtproveedorNombre_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      protected bool IsIns( )
      {
         return ((StringUtil.StrCmp(Gx_mode, "INS")==0) ? true : false) ;
      }

      protected bool IsDlt( )
      {
         return ((StringUtil.StrCmp(Gx_mode, "DLT")==0) ? true : false) ;
      }

      protected bool IsUpd( )
      {
         return ((StringUtil.StrCmp(Gx_mode, "UPD")==0) ? true : false) ;
      }

      protected bool IsDsp( )
      {
         return ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? true : false) ;
      }

      public void Valid_Proveedorid( )
      {
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         send_integrity_footer_hashes( ) ;
         dynload_actions( ) ;
         /*  Sending validation outputs */
         AssignAttri("", false, "A49proveedorNombre", StringUtil.RTrim( A49proveedorNombre));
         AssignAttri("", false, "A50proveedorDireccion", A50proveedorDireccion);
         AssignAttri("", false, "Gx_mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "Z48proveedorId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z48proveedorId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z49proveedorNombre", StringUtil.RTrim( Z49proveedorNombre));
         GxWebStd.gx_hidden_field( context, "Z50proveedorDireccion", Z50proveedorDireccion);
         AssignProp("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Enabled), 5, 0), true);
         AssignProp("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_enter_Enabled), 5, 0), true);
         SendCloseFormHiddens( ) ;
      }

      public void Valid_Atraccionid( )
      {
         /* Using cursor T000820 */
         pr_default.execute(18, new Object[] {A7atraccionId});
         if ( (pr_default.getStatus(18) == 101) )
         {
            GX_msglist.addItem("No matching 'atraccion'.", "ForeignKeyNotFound", 1, "ATRACCIONID");
            AnyError = 1;
            GX_FocusControl = edtatraccionId_Internalname;
         }
         A8atraccionNombre = T000820_A8atraccionNombre[0];
         A40000atraccionFoto_GXI = T000820_A40000atraccionFoto_GXI[0];
         A13atraccionFoto = T000820_A13atraccionFoto[0];
         pr_default.close(18);
         dynload_actions( ) ;
         /*  Sending validation outputs */
         AssignAttri("", false, "A8atraccionNombre", StringUtil.RTrim( A8atraccionNombre));
         AssignAttri("", false, "A13atraccionFoto", context.PathToRelativeUrl( A13atraccionFoto));
         AssignAttri("", false, "A40000atraccionFoto_GXI", A40000atraccionFoto_GXI);
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}]");
         setEventMetadata("ENTER",",oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[]");
         setEventMetadata("REFRESH",",oparms:[]}");
         setEventMetadata("VALID_PROVEEDORID","{handler:'Valid_Proveedorid',iparms:[{av:'A48proveedorId',fld:'PROVEEDORID',pic:'ZZZZZ9'},{av:'Gx_mode',fld:'vMODE',pic:'@!'}]");
         setEventMetadata("VALID_PROVEEDORID",",oparms:[{av:'A49proveedorNombre',fld:'PROVEEDORNOMBRE',pic:''},{av:'A50proveedorDireccion',fld:'PROVEEDORDIRECCION',pic:''},{av:'Gx_mode',fld:'vMODE',pic:'@!'},{av:'Z48proveedorId'},{av:'Z49proveedorNombre'},{av:'Z50proveedorDireccion'},{ctrl:'BTN_DELETE',prop:'Enabled'},{ctrl:'BTN_ENTER',prop:'Enabled'}]}");
         setEventMetadata("VALID_ATRACCIONID","{handler:'Valid_Atraccionid',iparms:[{av:'A7atraccionId',fld:'ATRACCIONID',pic:'ZZZZZ9'},{av:'A8atraccionNombre',fld:'ATRACCIONNOMBRE',pic:''},{av:'A13atraccionFoto',fld:'ATRACCIONFOTO',pic:''},{av:'A40000atraccionFoto_GXI',fld:'ATRACCIONFOTO_GXI',pic:''}]");
         setEventMetadata("VALID_ATRACCIONID",",oparms:[{av:'A8atraccionNombre',fld:'ATRACCIONNOMBRE',pic:''},{av:'A13atraccionFoto',fld:'ATRACCIONFOTO',pic:''},{av:'A40000atraccionFoto_GXI',fld:'ATRACCIONFOTO_GXI',pic:''}]}");
         setEventMetadata("NULL","{handler:'Valid_Atraccionfoto',iparms:[]");
         setEventMetadata("NULL",",oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(18);
         pr_default.close(4);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z49proveedorNombre = "";
         Z50proveedorDireccion = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         Gx_mode = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         lblTitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_first_Jsonclick = "";
         bttBtn_previous_Jsonclick = "";
         bttBtn_next_Jsonclick = "";
         bttBtn_last_Jsonclick = "";
         bttBtn_select_Jsonclick = "";
         A49proveedorNombre = "";
         A50proveedorDireccion = "";
         lblTitleatraccion_Jsonclick = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         Gridproveedor_atraccionContainer = new GXWebGrid( context);
         Gridproveedor_atraccionColumn = new GXWebColumn();
         A8atraccionNombre = "";
         A13atraccionFoto = "";
         sMode11 = "";
         sStyleString = "";
         A40000atraccionFoto_GXI = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         T00087_A48proveedorId = new int[1] ;
         T00087_A49proveedorNombre = new String[] {""} ;
         T00087_A50proveedorDireccion = new String[] {""} ;
         T00088_A48proveedorId = new int[1] ;
         T00086_A48proveedorId = new int[1] ;
         T00086_A49proveedorNombre = new String[] {""} ;
         T00086_A50proveedorDireccion = new String[] {""} ;
         sMode10 = "";
         T00089_A48proveedorId = new int[1] ;
         T000810_A48proveedorId = new int[1] ;
         T00085_A48proveedorId = new int[1] ;
         T00085_A49proveedorNombre = new String[] {""} ;
         T00085_A50proveedorDireccion = new String[] {""} ;
         T000811_A48proveedorId = new int[1] ;
         T000814_A48proveedorId = new int[1] ;
         Z8atraccionNombre = "";
         Z13atraccionFoto = "";
         Z40000atraccionFoto_GXI = "";
         T000815_A48proveedorId = new int[1] ;
         T000815_A8atraccionNombre = new String[] {""} ;
         T000815_A40000atraccionFoto_GXI = new String[] {""} ;
         T000815_A7atraccionId = new int[1] ;
         T000815_A13atraccionFoto = new String[] {""} ;
         T00084_A8atraccionNombre = new String[] {""} ;
         T00084_A40000atraccionFoto_GXI = new String[] {""} ;
         T00084_A13atraccionFoto = new String[] {""} ;
         T000816_A8atraccionNombre = new String[] {""} ;
         T000816_A40000atraccionFoto_GXI = new String[] {""} ;
         T000816_A13atraccionFoto = new String[] {""} ;
         T000817_A48proveedorId = new int[1] ;
         T000817_A7atraccionId = new int[1] ;
         T00083_A48proveedorId = new int[1] ;
         T00083_A7atraccionId = new int[1] ;
         T00082_A48proveedorId = new int[1] ;
         T00082_A7atraccionId = new int[1] ;
         T000820_A8atraccionNombre = new String[] {""} ;
         T000820_A40000atraccionFoto_GXI = new String[] {""} ;
         T000820_A13atraccionFoto = new String[] {""} ;
         T000821_A48proveedorId = new int[1] ;
         T000821_A7atraccionId = new int[1] ;
         Gridproveedor_atraccionRow = new GXWebRow();
         subGridproveedor_atraccion_Linesclass = "";
         ROClassString = "";
         sImgUrl = "";
         GXCCtlgxBlob = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         ZZ49proveedorNombre = "";
         ZZ50proveedorDireccion = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.proveedor__default(),
            new Object[][] {
                new Object[] {
               T00082_A48proveedorId, T00082_A7atraccionId
               }
               , new Object[] {
               T00083_A48proveedorId, T00083_A7atraccionId
               }
               , new Object[] {
               T00084_A8atraccionNombre, T00084_A40000atraccionFoto_GXI, T00084_A13atraccionFoto
               }
               , new Object[] {
               T00085_A48proveedorId, T00085_A49proveedorNombre, T00085_A50proveedorDireccion
               }
               , new Object[] {
               T00086_A48proveedorId, T00086_A49proveedorNombre, T00086_A50proveedorDireccion
               }
               , new Object[] {
               T00087_A48proveedorId, T00087_A49proveedorNombre, T00087_A50proveedorDireccion
               }
               , new Object[] {
               T00088_A48proveedorId
               }
               , new Object[] {
               T00089_A48proveedorId
               }
               , new Object[] {
               T000810_A48proveedorId
               }
               , new Object[] {
               T000811_A48proveedorId
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000814_A48proveedorId
               }
               , new Object[] {
               T000815_A48proveedorId, T000815_A8atraccionNombre, T000815_A40000atraccionFoto_GXI, T000815_A7atraccionId, T000815_A13atraccionFoto
               }
               , new Object[] {
               T000816_A8atraccionNombre, T000816_A40000atraccionFoto_GXI, T000816_A13atraccionFoto
               }
               , new Object[] {
               T000817_A48proveedorId, T000817_A7atraccionId
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000820_A8atraccionNombre, T000820_A40000atraccionFoto_GXI, T000820_A13atraccionFoto
               }
               , new Object[] {
               T000821_A48proveedorId, T000821_A7atraccionId
               }
            }
         );
      }

      private short nIsMod_11 ;
      private short nRcdDeleted_11 ;
      private short nRcdExists_11 ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short subGridproveedor_atraccion_Backcolorstyle ;
      private short subGridproveedor_atraccion_Allowselection ;
      private short subGridproveedor_atraccion_Allowhovering ;
      private short subGridproveedor_atraccion_Allowcollapsing ;
      private short subGridproveedor_atraccion_Collapsed ;
      private short nBlankRcdCount11 ;
      private short RcdFound11 ;
      private short nBlankRcdUsr11 ;
      private short GX_JID ;
      private short RcdFound10 ;
      private short nIsDirty_10 ;
      private short Gx_BScreen ;
      private short nIsDirty_11 ;
      private short subGridproveedor_atraccion_Backstyle ;
      private short gxajaxcallmode ;
      private int Z48proveedorId ;
      private int nRC_GXsfl_53 ;
      private int nGXsfl_53_idx=1 ;
      private int Z7atraccionId ;
      private int A7atraccionId ;
      private int trnEnded ;
      private int bttBtn_first_Visible ;
      private int bttBtn_previous_Visible ;
      private int bttBtn_next_Visible ;
      private int bttBtn_last_Visible ;
      private int bttBtn_select_Visible ;
      private int A48proveedorId ;
      private int edtproveedorId_Enabled ;
      private int edtproveedorNombre_Enabled ;
      private int edtproveedorDireccion_Enabled ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_enter_Enabled ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int bttBtn_delete_Enabled ;
      private int edtatraccionId_Enabled ;
      private int edtatraccionNombre_Enabled ;
      private int edtatraccionFoto_Enabled ;
      private int subGridproveedor_atraccion_Selectedindex ;
      private int subGridproveedor_atraccion_Selectioncolor ;
      private int subGridproveedor_atraccion_Hoveringcolor ;
      private int fRowAdded ;
      private int subGridproveedor_atraccion_Backcolor ;
      private int subGridproveedor_atraccion_Allbackcolor ;
      private int imgprompt_7_Visible ;
      private int defedtatraccionId_Enabled ;
      private int idxLst ;
      private int ZZ48proveedorId ;
      private long GRIDPROVEEDOR_ATRACCION_nFirstRecordOnPage ;
      private String sPrefix ;
      private String sGXsfl_53_idx="0001" ;
      private String Z49proveedorNombre ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtproveedorId_Internalname ;
      private String divMaintable_Internalname ;
      private String divTitlecontainer_Internalname ;
      private String lblTitle_Internalname ;
      private String lblTitle_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String divFormcontainer_Internalname ;
      private String divToolbarcell_Internalname ;
      private String TempTags ;
      private String bttBtn_first_Internalname ;
      private String bttBtn_first_Jsonclick ;
      private String bttBtn_previous_Internalname ;
      private String bttBtn_previous_Jsonclick ;
      private String bttBtn_next_Internalname ;
      private String bttBtn_next_Jsonclick ;
      private String bttBtn_last_Internalname ;
      private String bttBtn_last_Jsonclick ;
      private String bttBtn_select_Internalname ;
      private String bttBtn_select_Jsonclick ;
      private String edtproveedorId_Jsonclick ;
      private String edtproveedorNombre_Internalname ;
      private String A49proveedorNombre ;
      private String edtproveedorNombre_Jsonclick ;
      private String edtproveedorDireccion_Internalname ;
      private String divAtracciontable_Internalname ;
      private String lblTitleatraccion_Internalname ;
      private String lblTitleatraccion_Jsonclick ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String subGridproveedor_atraccion_Header ;
      private String A8atraccionNombre ;
      private String sMode11 ;
      private String edtatraccionId_Internalname ;
      private String edtatraccionNombre_Internalname ;
      private String edtatraccionFoto_Internalname ;
      private String imgprompt_7_Link ;
      private String sStyleString ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GXCCtl ;
      private String sMode10 ;
      private String Z8atraccionNombre ;
      private String imgprompt_7_Internalname ;
      private String sGXsfl_53_fel_idx="0001" ;
      private String subGridproveedor_atraccion_Class ;
      private String subGridproveedor_atraccion_Linesclass ;
      private String ROClassString ;
      private String edtatraccionId_Jsonclick ;
      private String sImgUrl ;
      private String edtatraccionNombre_Jsonclick ;
      private String GXCCtlgxBlob ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String subGridproveedor_atraccion_Internalname ;
      private String ZZ49proveedorNombre ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool bGXsfl_53_Refreshing=false ;
      private bool A13atraccionFoto_IsBlob ;
      private String Z50proveedorDireccion ;
      private String A50proveedorDireccion ;
      private String A40000atraccionFoto_GXI ;
      private String Z40000atraccionFoto_GXI ;
      private String ZZ50proveedorDireccion ;
      private String A13atraccionFoto ;
      private String Z13atraccionFoto ;
      private GXWebGrid Gridproveedor_atraccionContainer ;
      private GXWebRow Gridproveedor_atraccionRow ;
      private GXWebColumn Gridproveedor_atraccionColumn ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T00087_A48proveedorId ;
      private String[] T00087_A49proveedorNombre ;
      private String[] T00087_A50proveedorDireccion ;
      private int[] T00088_A48proveedorId ;
      private int[] T00086_A48proveedorId ;
      private String[] T00086_A49proveedorNombre ;
      private String[] T00086_A50proveedorDireccion ;
      private int[] T00089_A48proveedorId ;
      private int[] T000810_A48proveedorId ;
      private int[] T00085_A48proveedorId ;
      private String[] T00085_A49proveedorNombre ;
      private String[] T00085_A50proveedorDireccion ;
      private int[] T000811_A48proveedorId ;
      private int[] T000814_A48proveedorId ;
      private int[] T000815_A48proveedorId ;
      private String[] T000815_A8atraccionNombre ;
      private String[] T000815_A40000atraccionFoto_GXI ;
      private int[] T000815_A7atraccionId ;
      private String[] T000815_A13atraccionFoto ;
      private String[] T00084_A8atraccionNombre ;
      private String[] T00084_A40000atraccionFoto_GXI ;
      private String[] T00084_A13atraccionFoto ;
      private String[] T000816_A8atraccionNombre ;
      private String[] T000816_A40000atraccionFoto_GXI ;
      private String[] T000816_A13atraccionFoto ;
      private int[] T000817_A48proveedorId ;
      private int[] T000817_A7atraccionId ;
      private int[] T00083_A48proveedorId ;
      private int[] T00083_A7atraccionId ;
      private int[] T00082_A48proveedorId ;
      private int[] T00082_A7atraccionId ;
      private String[] T000820_A8atraccionNombre ;
      private String[] T000820_A40000atraccionFoto_GXI ;
      private String[] T000820_A13atraccionFoto ;
      private int[] T000821_A48proveedorId ;
      private int[] T000821_A7atraccionId ;
      private GXWebForm Form ;
   }

   public class proveedor__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new UpdateCursor(def[16])
         ,new UpdateCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00087 ;
          prmT00087 = new Object[] {
          new Object[] {"@proveedorId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00088 ;
          prmT00088 = new Object[] {
          new Object[] {"@proveedorId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00086 ;
          prmT00086 = new Object[] {
          new Object[] {"@proveedorId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00089 ;
          prmT00089 = new Object[] {
          new Object[] {"@proveedorId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000810 ;
          prmT000810 = new Object[] {
          new Object[] {"@proveedorId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00085 ;
          prmT00085 = new Object[] {
          new Object[] {"@proveedorId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000811 ;
          prmT000811 = new Object[] {
          new Object[] {"@proveedorNombre",SqlDbType.NChar,20,0} ,
          new Object[] {"@proveedorDireccion",SqlDbType.NVarChar,1024,0}
          } ;
          Object[] prmT000812 ;
          prmT000812 = new Object[] {
          new Object[] {"@proveedorNombre",SqlDbType.NChar,20,0} ,
          new Object[] {"@proveedorDireccion",SqlDbType.NVarChar,1024,0} ,
          new Object[] {"@proveedorId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000813 ;
          prmT000813 = new Object[] {
          new Object[] {"@proveedorId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000814 ;
          prmT000814 = new Object[] {
          } ;
          Object[] prmT000815 ;
          prmT000815 = new Object[] {
          new Object[] {"@proveedorId",SqlDbType.Int,6,0} ,
          new Object[] {"@atraccionId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00084 ;
          prmT00084 = new Object[] {
          new Object[] {"@atraccionId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000816 ;
          prmT000816 = new Object[] {
          new Object[] {"@atraccionId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000817 ;
          prmT000817 = new Object[] {
          new Object[] {"@proveedorId",SqlDbType.Int,6,0} ,
          new Object[] {"@atraccionId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00083 ;
          prmT00083 = new Object[] {
          new Object[] {"@proveedorId",SqlDbType.Int,6,0} ,
          new Object[] {"@atraccionId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00082 ;
          prmT00082 = new Object[] {
          new Object[] {"@proveedorId",SqlDbType.Int,6,0} ,
          new Object[] {"@atraccionId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000818 ;
          prmT000818 = new Object[] {
          new Object[] {"@proveedorId",SqlDbType.Int,6,0} ,
          new Object[] {"@atraccionId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000819 ;
          prmT000819 = new Object[] {
          new Object[] {"@proveedorId",SqlDbType.Int,6,0} ,
          new Object[] {"@atraccionId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000821 ;
          prmT000821 = new Object[] {
          new Object[] {"@proveedorId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000820 ;
          prmT000820 = new Object[] {
          new Object[] {"@atraccionId",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00082", "SELECT [proveedorId], [atraccionId] FROM [proveedoratraccion] WITH (UPDLOCK) WHERE [proveedorId] = @proveedorId AND [atraccionId] = @atraccionId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00082,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00083", "SELECT [proveedorId], [atraccionId] FROM [proveedoratraccion] WHERE [proveedorId] = @proveedorId AND [atraccionId] = @atraccionId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00083,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00084", "SELECT [atraccionNombre], [atraccionFoto_GXI], [atraccionFoto] FROM [atraccion] WHERE [atraccionId] = @atraccionId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00084,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00085", "SELECT [proveedorId], [proveedorNombre], [proveedorDireccion] FROM [proveedor] WITH (UPDLOCK) WHERE [proveedorId] = @proveedorId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00085,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00086", "SELECT [proveedorId], [proveedorNombre], [proveedorDireccion] FROM [proveedor] WHERE [proveedorId] = @proveedorId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00086,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00087", "SELECT TM1.[proveedorId], TM1.[proveedorNombre], TM1.[proveedorDireccion] FROM [proveedor] TM1 WHERE TM1.[proveedorId] = @proveedorId ORDER BY TM1.[proveedorId]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00087,100, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00088", "SELECT [proveedorId] FROM [proveedor] WHERE [proveedorId] = @proveedorId  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00088,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00089", "SELECT TOP 1 [proveedorId] FROM [proveedor] WHERE ( [proveedorId] > @proveedorId) ORDER BY [proveedorId]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00089,1, GxCacheFrequency.OFF ,true,true )
             ,new CursorDef("T000810", "SELECT TOP 1 [proveedorId] FROM [proveedor] WHERE ( [proveedorId] < @proveedorId) ORDER BY [proveedorId] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000810,1, GxCacheFrequency.OFF ,true,true )
             ,new CursorDef("T000811", "INSERT INTO [proveedor]([proveedorNombre], [proveedorDireccion]) VALUES(@proveedorNombre, @proveedorDireccion); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000811)
             ,new CursorDef("T000812", "UPDATE [proveedor] SET [proveedorNombre]=@proveedorNombre, [proveedorDireccion]=@proveedorDireccion  WHERE [proveedorId] = @proveedorId", GxErrorMask.GX_NOMASK,prmT000812)
             ,new CursorDef("T000813", "DELETE FROM [proveedor]  WHERE [proveedorId] = @proveedorId", GxErrorMask.GX_NOMASK,prmT000813)
             ,new CursorDef("T000814", "SELECT [proveedorId] FROM [proveedor] ORDER BY [proveedorId]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000814,100, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000815", "SELECT T1.[proveedorId], T2.[atraccionNombre], T2.[atraccionFoto_GXI], T1.[atraccionId], T2.[atraccionFoto] FROM ([proveedoratraccion] T1 INNER JOIN [atraccion] T2 ON T2.[atraccionId] = T1.[atraccionId]) WHERE T1.[proveedorId] = @proveedorId and T1.[atraccionId] = @atraccionId ORDER BY T1.[proveedorId], T1.[atraccionId] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000815,11, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000816", "SELECT [atraccionNombre], [atraccionFoto_GXI], [atraccionFoto] FROM [atraccion] WHERE [atraccionId] = @atraccionId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000816,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000817", "SELECT [proveedorId], [atraccionId] FROM [proveedoratraccion] WHERE [proveedorId] = @proveedorId AND [atraccionId] = @atraccionId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000817,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000818", "INSERT INTO [proveedoratraccion]([proveedorId], [atraccionId]) VALUES(@proveedorId, @atraccionId)", GxErrorMask.GX_NOMASK,prmT000818)
             ,new CursorDef("T000819", "DELETE FROM [proveedoratraccion]  WHERE [proveedorId] = @proveedorId AND [atraccionId] = @atraccionId", GxErrorMask.GX_NOMASK,prmT000819)
             ,new CursorDef("T000820", "SELECT [atraccionNombre], [atraccionFoto_GXI], [atraccionFoto] FROM [atraccion] WHERE [atraccionId] = @atraccionId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000820,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000821", "SELECT [proveedorId], [atraccionId] FROM [proveedoratraccion] WHERE [proveedorId] = @proveedorId ORDER BY [proveedorId], [atraccionId] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000821,11, GxCacheFrequency.OFF ,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getMultimediaUri(2) ;
                ((String[]) buf[2])[0] = rslt.getMultimediaFile(3, rslt.getVarchar(2)) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getMultimediaUri(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getMultimediaFile(5, rslt.getVarchar(3)) ;
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getMultimediaUri(2) ;
                ((String[]) buf[2])[0] = rslt.getMultimediaFile(3, rslt.getVarchar(2)) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getMultimediaUri(2) ;
                ((String[]) buf[2])[0] = rslt.getMultimediaFile(3, rslt.getVarchar(2)) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
