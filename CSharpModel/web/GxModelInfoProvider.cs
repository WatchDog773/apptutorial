/*
               File: GxModelInfoProvider
        Description: No description for object
             Author: GeneXus C# Generator version 16_0_6-136889
       Generated on: 11/25/2019 21:57:37.65
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Reorg;
using System.Threading;
using GeneXus.Programs;
using System.Web.Services;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
[assembly: GeneXusCommonAssemblyAttribute()]
namespace GeneXus.Programs {
   public class GxModelInfoProvider
   {
      static public String GetNamespaceName( )
      {
         return "GeneXus.Programs" ;
      }

   }

}
