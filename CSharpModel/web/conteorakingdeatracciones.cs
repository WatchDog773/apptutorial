/*
               File: ConteoRakingdeAtracciones
        Description: Conteo Rakingde Atracciones
             Author: GeneXus C# Generator version 16_0_6-136889
       Generated on: 12/3/2019 21:6:5.56
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class conteorakingdeatracciones : GXProcedure
   {
      public conteorakingdeatracciones( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public conteorakingdeatracciones( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( out GXBaseCollection<SdtSDTpais_SDTpaisItem> aP0_Gxm2rootcol )
      {
         this.Gxm2rootcol = new GXBaseCollection<SdtSDTpais_SDTpaisItem>( context, "SDTpaisItem", "TravelAgency") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      public GXBaseCollection<SdtSDTpais_SDTpaisItem> executeUdp( )
      {
         this.Gxm2rootcol = new GXBaseCollection<SdtSDTpais_SDTpaisItem>( context, "SDTpaisItem", "TravelAgency") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( out GXBaseCollection<SdtSDTpais_SDTpaisItem> aP0_Gxm2rootcol )
      {
         conteorakingdeatracciones objconteorakingdeatracciones;
         objconteorakingdeatracciones = new conteorakingdeatracciones();
         objconteorakingdeatracciones.Gxm2rootcol = new GXBaseCollection<SdtSDTpais_SDTpaisItem>( context, "SDTpaisItem", "TravelAgency") ;
         objconteorakingdeatracciones.context.SetSubmitInitialConfig(context);
         objconteorakingdeatracciones.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objconteorakingdeatracciones);
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((conteorakingdeatracciones)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00013 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A9paisId = P00013_A9paisId[0];
            A10paisNombre = P00013_A10paisNombre[0];
            A40000GXC1 = P00013_A40000GXC1[0];
            n40000GXC1 = P00013_n40000GXC1[0];
            A40000GXC1 = P00013_A40000GXC1[0];
            n40000GXC1 = P00013_n40000GXC1[0];
            Gxm1sdtpais = new SdtSDTpais_SDTpaisItem(context);
            Gxm2rootcol.Add(Gxm1sdtpais, 0);
            Gxm1sdtpais.gxTpr_Id = A9paisId;
            Gxm1sdtpais.gxTpr_Nombre = A10paisNombre;
            Gxm1sdtpais.gxTpr_Cantidadatracciones = (short)(A40000GXC1);
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00013_A9paisId = new int[1] ;
         P00013_A10paisNombre = new String[] {""} ;
         P00013_A40000GXC1 = new int[1] ;
         P00013_n40000GXC1 = new bool[] {false} ;
         A10paisNombre = "";
         Gxm1sdtpais = new SdtSDTpais_SDTpaisItem(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.conteorakingdeatracciones__default(),
            new Object[][] {
                new Object[] {
               P00013_A9paisId, P00013_A10paisNombre, P00013_A40000GXC1, P00013_n40000GXC1
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A9paisId ;
      private int A40000GXC1 ;
      private String scmdbuf ;
      private String A10paisNombre ;
      private bool n40000GXC1 ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00013_A9paisId ;
      private String[] P00013_A10paisNombre ;
      private int[] P00013_A40000GXC1 ;
      private bool[] P00013_n40000GXC1 ;
      private GXBaseCollection<SdtSDTpais_SDTpaisItem> aP0_Gxm2rootcol ;
      private GXBaseCollection<SdtSDTpais_SDTpaisItem> Gxm2rootcol ;
      private SdtSDTpais_SDTpaisItem Gxm1sdtpais ;
   }

   public class conteorakingdeatracciones__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00013 ;
          prmP00013 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("P00013", "SELECT T1.[paisId], T1.[paisNombre], COALESCE( T2.[GXC1], 0) AS GXC1 FROM ([pais] T1 LEFT JOIN (SELECT COUNT(*) AS GXC1, [paisId] FROM [atraccion] GROUP BY [paisId] ) T2 ON T2.[paisId] = T1.[paisId]) ORDER BY T1.[paisId] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00013,100, GxCacheFrequency.OFF ,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
