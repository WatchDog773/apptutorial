/*
               File: CategoriaAtracciones
        Description: Categoria Atracciones
             Author: GeneXus C# Generator version 16_0_6-136889
       Generated on: 12/2/2019 1:24:52.77
       Program type: Main program
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class acategoriaatracciones : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      public override void webExecute( )
      {
         context.SetDefaultTheme("Carmine");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public acategoriaatracciones( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public acategoriaatracciones( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         initialize();
         executePrivate();
      }

      public void executeSubmit( )
      {
         acategoriaatracciones objacategoriaatracciones;
         objacategoriaatracciones = new acategoriaatracciones();
         objacategoriaatracciones.context.SetSubmitInitialConfig(context);
         objacategoriaatracciones.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objacategoriaatracciones);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((acategoriaatracciones)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 6;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 256, 16834, 11909, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*6));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            H0E0( false, 34) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 14, true, false, false, false, 0, 25, 25, 112, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Categoria y sus Atracciones", 233, Gx_line+0, 573, Gx_line+27, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+34);
            /* Using cursor P000E2 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               A12categoriaNombre = P000E2_A12categoriaNombre[0];
               A11categoriaId = P000E2_A11categoriaId[0];
               n11categoriaId = P000E2_n11categoriaId[0];
               H0E0( false, 43) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 12, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Categoria", 40, Gx_line+0, 153, Gx_line+27, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A11categoriaId), "ZZZZZ9")), 167, Gx_line+13, 203, Gx_line+27, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A12categoriaNombre, "")), 247, Gx_line+13, 348, Gx_line+27, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+43);
               H0E0( false, 43) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Id", 159, Gx_line+13, 169, Gx_line+27, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("Nombre", 287, Gx_line+13, 329, Gx_line+27, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("Pais", 480, Gx_line+13, 504, Gx_line+27, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("Foto", 633, Gx_line+13, 657, Gx_line+27, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawLine(147, Gx_line+27, 667, Gx_line+27, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Foto", 633, Gx_line+13, 657, Gx_line+27, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+43);
               /* Using cursor P000E3 */
               pr_default.execute(1, new Object[] {n11categoriaId, A11categoriaId});
               while ( (pr_default.getStatus(1) != 101) )
               {
                  A9paisId = P000E3_A9paisId[0];
                  A15ciudadId = P000E3_A15ciudadId[0];
                  A11categoriaId = P000E3_A11categoriaId[0];
                  n11categoriaId = P000E3_n11categoriaId[0];
                  A40000atraccionFoto_GXI = P000E3_A40000atraccionFoto_GXI[0];
                  A17ciudadNombre = P000E3_A17ciudadNombre[0];
                  A8atraccionNombre = P000E3_A8atraccionNombre[0];
                  A7atraccionId = P000E3_A7atraccionId[0];
                  A13atraccionFoto = P000E3_A13atraccionFoto[0];
                  A17ciudadNombre = P000E3_A17ciudadNombre[0];
                  H0E0( false, 63) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A7atraccionId), "ZZZZZ9")), 133, Gx_line+13, 169, Gx_line+27, 2+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A8atraccionNombre, "")), 228, Gx_line+13, 329, Gx_line+27, 0+256, 0, 0, 0) ;
                  sImgUrl = (String.IsNullOrEmpty(StringUtil.RTrim( A13atraccionFoto)) ? A40000atraccionFoto_GXI : A13atraccionFoto);
                  getPrinter().GxDrawBitMap(sImgUrl, 610, Gx_line+13, 657, Gx_line+53) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A17ciudadNombre, "")), 403, Gx_line+13, 504, Gx_line+27, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+63);
                  pr_default.readNext(1);
               }
               pr_default.close(1);
               pr_default.readNext(0);
            }
            pr_default.close(0);
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H0E0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( context.WillRedirect( ) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void H0E0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         scmdbuf = "";
         P000E2_A12categoriaNombre = new String[] {""} ;
         P000E2_A11categoriaId = new int[1] ;
         P000E2_n11categoriaId = new bool[] {false} ;
         A12categoriaNombre = "";
         P000E3_A9paisId = new int[1] ;
         P000E3_A15ciudadId = new int[1] ;
         P000E3_A11categoriaId = new int[1] ;
         P000E3_n11categoriaId = new bool[] {false} ;
         P000E3_A40000atraccionFoto_GXI = new String[] {""} ;
         P000E3_A17ciudadNombre = new String[] {""} ;
         P000E3_A8atraccionNombre = new String[] {""} ;
         P000E3_A7atraccionId = new int[1] ;
         P000E3_A13atraccionFoto = new String[] {""} ;
         A40000atraccionFoto_GXI = "";
         A17ciudadNombre = "";
         A8atraccionNombre = "";
         A13atraccionFoto = "";
         sImgUrl = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.acategoriaatracciones__default(),
            new Object[][] {
                new Object[] {
               P000E2_A12categoriaNombre, P000E2_A11categoriaId
               }
               , new Object[] {
               P000E3_A9paisId, P000E3_A15ciudadId, P000E3_A11categoriaId, P000E3_n11categoriaId, P000E3_A40000atraccionFoto_GXI, P000E3_A17ciudadNombre, P000E3_A8atraccionNombre, P000E3_A7atraccionId, P000E3_A13atraccionFoto
               }
            }
         );
         /* GeneXus formulas. */
         Gx_line = 0;
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short GxWebError ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int Gx_OldLine ;
      private int A11categoriaId ;
      private int A9paisId ;
      private int A15ciudadId ;
      private int A7atraccionId ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String scmdbuf ;
      private String A12categoriaNombre ;
      private String A17ciudadNombre ;
      private String A8atraccionNombre ;
      private String sImgUrl ;
      private bool entryPointCalled ;
      private bool n11categoriaId ;
      private String A40000atraccionFoto_GXI ;
      private String A13atraccionFoto ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P000E2_A12categoriaNombre ;
      private int[] P000E2_A11categoriaId ;
      private bool[] P000E2_n11categoriaId ;
      private int[] P000E3_A9paisId ;
      private int[] P000E3_A15ciudadId ;
      private int[] P000E3_A11categoriaId ;
      private bool[] P000E3_n11categoriaId ;
      private String[] P000E3_A40000atraccionFoto_GXI ;
      private String[] P000E3_A17ciudadNombre ;
      private String[] P000E3_A8atraccionNombre ;
      private int[] P000E3_A7atraccionId ;
      private String[] P000E3_A13atraccionFoto ;
   }

   public class acategoriaatracciones__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP000E2 ;
          prmP000E2 = new Object[] {
          } ;
          Object[] prmP000E3 ;
          prmP000E3 = new Object[] {
          new Object[] {"@categoriaId",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P000E2", "SELECT [categoriaNombre], [categoriaId] FROM [categoria] ORDER BY [categoriaId] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000E2,100, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("P000E3", "SELECT T1.[paisId], T1.[ciudadId], T1.[categoriaId], T1.[atraccionFoto_GXI], T2.[ciudadNombre], T1.[atraccionNombre], T1.[atraccionId], T1.[atraccionFoto] FROM ([atraccion] T1 INNER JOIN [paisciudad] T2 ON T2.[paisId] = T1.[paisId] AND T2.[ciudadId] = T1.[ciudadId]) WHERE T1.[categoriaId] = @categoriaId ORDER BY T1.[atraccionId] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000E3,100, GxCacheFrequency.OFF ,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getMultimediaUri(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 20) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 20) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((String[]) buf[8])[0] = rslt.getMultimediaFile(8, rslt.getVarchar(4)) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
