/*
               File: cliente
        Description: cliente
             Author: GeneXus C# Generator version 16_0_6-136889
       Generated on: 12/2/2019 0:46:10.30
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class cliente : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_web_controls( ) ;
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 16_0_6-136889", 0) ;
            Form.Meta.addItem("description", "cliente", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtclienteId_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("Carmine");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public cliente( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public cliente( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdmasterpage", "GeneXus.Programs.rwdmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "no-store");
               }
               if ( ! context.WillRedirect( ) )
               {
                  AddString( context.getJSONResponse( )) ;
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            DrawControls( ) ;
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void DrawControls( )
      {
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "WWAdvancedContainer", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divTitlecontainer_Internalname, 1, 0, "px", 0, "px", "TableTop", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         /* Text block */
         GxWebStd.gx_label_ctrl( context, lblTitle_Internalname, "cliente", "", "", lblTitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_cliente.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         ClassString = "ErrorViewer";
         StyleString = "";
         GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divFormcontainer_Internalname, 1, 0, "px", 0, "px", "FormContainer", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divToolbarcell_Internalname, 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3 ToolbarCellClass", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group ActionGroup", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "btn-group", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
         ClassString = "BtnFirst";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_first_Internalname, "", "", bttBtn_first_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_first_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", TempTags, "", context.GetButtonType( ), "HLP_cliente.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
         ClassString = "BtnPrevious";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_previous_Internalname, "", "", bttBtn_previous_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_previous_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", TempTags, "", context.GetButtonType( ), "HLP_cliente.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
         ClassString = "BtnNext";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_next_Internalname, "", "", bttBtn_next_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_next_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", TempTags, "", context.GetButtonType( ), "HLP_cliente.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
         ClassString = "BtnLast";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_last_Internalname, "", "", bttBtn_last_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_last_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ELAST."+"'", TempTags, "", context.GetButtonType( ), "HLP_cliente.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
         ClassString = "BtnSelect";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_select_Internalname, "", "Select", bttBtn_select_Jsonclick, 4, "Select", "", StyleString, ClassString, bttBtn_select_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", TempTags, "gx.popup.openPrompt('"+"gx00d0.aspx"+"',["+"{Ctrl:gx.dom.el('"+"CLIENTEID"+"'), id:'"+"CLIENTEID"+"'"+",IOType:'out',isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");"+"return false;", 2, "HLP_cliente.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCellAdvanced", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtclienteId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtclienteId_Internalname, "Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtclienteId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1clienteId), 6, 0, ".", "")), ((edtclienteId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1clienteId), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1clienteId), "ZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtclienteId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtclienteId_Enabled, 0, "number", "1", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "", "HLP_cliente.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtclienteDNI_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtclienteDNI_Internalname, "DNI", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtclienteDNI_Internalname, StringUtil.RTrim( A54clienteDNI), StringUtil.RTrim( context.localUtil.Format( A54clienteDNI, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtclienteDNI_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtclienteDNI_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "", "HLP_cliente.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtclienteNombre_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtclienteNombre_Internalname, "Nombre", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtclienteNombre_Internalname, StringUtil.RTrim( A2clienteNombre), StringUtil.RTrim( context.localUtil.Format( A2clienteNombre, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtclienteNombre_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtclienteNombre_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "Name", "left", true, "", "HLP_cliente.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtclienteApellido_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtclienteApellido_Internalname, "Apellido", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtclienteApellido_Internalname, StringUtil.RTrim( A3clienteApellido), StringUtil.RTrim( context.localUtil.Format( A3clienteApellido, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtclienteApellido_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtclienteApellido_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "Name", "left", true, "", "HLP_cliente.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtclienteNumeroPasaporte_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtclienteNumeroPasaporte_Internalname, "Numero Pasaporte", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtclienteNumeroPasaporte_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A55clienteNumeroPasaporte), 4, 0, ".", "")), ((edtclienteNumeroPasaporte_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A55clienteNumeroPasaporte), "ZZZ9")) : context.localUtil.Format( (decimal)(A55clienteNumeroPasaporte), "ZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtclienteNumeroPasaporte_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtclienteNumeroPasaporte_Enabled, 0, "number", "1", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "", "HLP_cliente.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtclienteDireccion_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtclienteDireccion_Internalname, "Direccion", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Multiple line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
         ClassString = "Attribute";
         StyleString = "";
         ClassString = "Attribute";
         StyleString = "";
         GxWebStd.gx_html_textarea( context, edtclienteDireccion_Internalname, A4clienteDireccion, "http://maps.google.com/maps?q="+GXUtil.UrlEncode( A4clienteDireccion), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", 0, 1, edtclienteDireccion_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "", "1024", -1, 0, "_blank", "", 0, true, "GeneXus\\Address", "'"+""+"'"+",false,"+"'"+""+"'", 0, "HLP_cliente.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtclienteTelefono_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtclienteTelefono_Internalname, "Telefono", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         if ( context.isSmartDevice( ) )
         {
            gxphoneLink = "tel:" + StringUtil.RTrim( A5clienteTelefono);
         }
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtclienteTelefono_Internalname, StringUtil.RTrim( A5clienteTelefono), StringUtil.RTrim( context.localUtil.Format( A5clienteTelefono, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", gxphoneLink, "", "", "", edtclienteTelefono_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtclienteTelefono_Enabled, 0, "tel", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, 0, true, "GeneXus\\Phone", "left", true, "", "HLP_cliente.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtclienteCorreo_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtclienteCorreo_Internalname, "Correo", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtclienteCorreo_Internalname, A6clienteCorreo, StringUtil.RTrim( context.localUtil.Format( A6clienteCorreo, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "mailto:"+A6clienteCorreo, "", "", "", edtclienteCorreo_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtclienteCorreo_Enabled, 0, "email", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, 0, true, "GeneXus\\Email", "left", true, "", "HLP_cliente.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtclienteFecha_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtclienteFecha_Internalname, "Fecha", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         context.WriteHtmlText( "<div id=\""+edtclienteFecha_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
         GxWebStd.gx_single_line_edit( context, edtclienteFecha_Internalname, context.localUtil.Format(A14clienteFecha, "99/99/99"), context.localUtil.Format( A14clienteFecha, "99/99/99"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtclienteFecha_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtclienteFecha_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "", "HLP_cliente.htm");
         GxWebStd.gx_bitmap( context, edtclienteFecha_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtclienteFecha_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", "", 1, false, false, "", "HLP_cliente.htm");
         context.WriteHtmlTextNl( "</div>") ;
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
         ClassString = "BtnEnter";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirm", bttBtn_enter_Jsonclick, 5, "Confirm", "", StyleString, ClassString, bttBtn_enter_Visible, bttBtn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_cliente.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'',0)\"";
         ClassString = "BtnCancel";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Cancel", bttBtn_cancel_Jsonclick, 1, "Cancel", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_cliente.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'',0)\"";
         ClassString = "BtnDelete";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Delete", bttBtn_delete_Jsonclick, 5, "Delete", "", StyleString, ClassString, bttBtn_delete_Visible, bttBtn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_cliente.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "Center", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read saved values. */
            Z1clienteId = (int)(context.localUtil.CToN( cgiGet( "Z1clienteId"), ".", ","));
            Z54clienteDNI = cgiGet( "Z54clienteDNI");
            Z2clienteNombre = cgiGet( "Z2clienteNombre");
            Z3clienteApellido = cgiGet( "Z3clienteApellido");
            Z55clienteNumeroPasaporte = (short)(context.localUtil.CToN( cgiGet( "Z55clienteNumeroPasaporte"), ".", ","));
            Z4clienteDireccion = cgiGet( "Z4clienteDireccion");
            Z5clienteTelefono = cgiGet( "Z5clienteTelefono");
            Z6clienteCorreo = cgiGet( "Z6clienteCorreo");
            Z14clienteFecha = context.localUtil.CToD( cgiGet( "Z14clienteFecha"), 0);
            IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ".", ","));
            IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ".", ","));
            Gx_mode = cgiGet( "Mode");
            Gx_date = context.localUtil.CToD( cgiGet( "vTODAY"), 0);
            Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ".", ","));
            Gx_mode = cgiGet( "vMODE");
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtclienteId_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtclienteId_Internalname), ".", ",") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CLIENTEID");
               AnyError = 1;
               GX_FocusControl = edtclienteId_Internalname;
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A1clienteId = 0;
               AssignAttri("", false, "A1clienteId", StringUtil.LTrimStr( (decimal)(A1clienteId), 6, 0));
            }
            else
            {
               A1clienteId = (int)(context.localUtil.CToN( cgiGet( edtclienteId_Internalname), ".", ","));
               AssignAttri("", false, "A1clienteId", StringUtil.LTrimStr( (decimal)(A1clienteId), 6, 0));
            }
            A54clienteDNI = cgiGet( edtclienteDNI_Internalname);
            AssignAttri("", false, "A54clienteDNI", A54clienteDNI);
            A2clienteNombre = cgiGet( edtclienteNombre_Internalname);
            AssignAttri("", false, "A2clienteNombre", A2clienteNombre);
            A3clienteApellido = cgiGet( edtclienteApellido_Internalname);
            AssignAttri("", false, "A3clienteApellido", A3clienteApellido);
            if ( ( ( context.localUtil.CToN( cgiGet( edtclienteNumeroPasaporte_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtclienteNumeroPasaporte_Internalname), ".", ",") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CLIENTENUMEROPASAPORTE");
               AnyError = 1;
               GX_FocusControl = edtclienteNumeroPasaporte_Internalname;
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A55clienteNumeroPasaporte = 0;
               AssignAttri("", false, "A55clienteNumeroPasaporte", StringUtil.LTrimStr( (decimal)(A55clienteNumeroPasaporte), 4, 0));
            }
            else
            {
               A55clienteNumeroPasaporte = (short)(context.localUtil.CToN( cgiGet( edtclienteNumeroPasaporte_Internalname), ".", ","));
               AssignAttri("", false, "A55clienteNumeroPasaporte", StringUtil.LTrimStr( (decimal)(A55clienteNumeroPasaporte), 4, 0));
            }
            A4clienteDireccion = cgiGet( edtclienteDireccion_Internalname);
            AssignAttri("", false, "A4clienteDireccion", A4clienteDireccion);
            A5clienteTelefono = cgiGet( edtclienteTelefono_Internalname);
            AssignAttri("", false, "A5clienteTelefono", A5clienteTelefono);
            A6clienteCorreo = cgiGet( edtclienteCorreo_Internalname);
            AssignAttri("", false, "A6clienteCorreo", A6clienteCorreo);
            A14clienteFecha = context.localUtil.CToD( cgiGet( edtclienteFecha_Internalname), 1);
            AssignAttri("", false, "A14clienteFecha", context.localUtil.Format(A14clienteFecha, "99/99/99"));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = new GXProperties();
            forbiddenHiddens.Add("hshsalt", "hsh"+"cliente");
            A14clienteFecha = context.localUtil.CToD( cgiGet( edtclienteFecha_Internalname), 1);
            AssignAttri("", false, "A14clienteFecha", context.localUtil.Format(A14clienteFecha, "99/99/99"));
            forbiddenHiddens.Add("clienteFecha", context.localUtil.Format(A14clienteFecha, "99/99/99"));
            hsh = cgiGet( "hsh");
            if ( ( ! ( ( A1clienteId != Z1clienteId ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens.ToString(), hsh, GXKey) )
            {
               GXUtil.WriteLog("cliente:[ SecurityCheckFailed value for]"+forbiddenHiddens.ToJSonString());
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               AnyError = 1;
               return  ;
            }
            standaloneNotModal( ) ;
         }
         else
         {
            standaloneNotModal( ) ;
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
            {
               Gx_mode = "DSP";
               AssignAttri("", false, "Gx_mode", Gx_mode);
               A1clienteId = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AssignAttri("", false, "A1clienteId", StringUtil.LTrimStr( (decimal)(A1clienteId), 6, 0));
               getEqualNoModal( ) ;
               Gx_mode = "DSP";
               AssignAttri("", false, "Gx_mode", Gx_mode);
               disable_std_buttons_dsp( ) ;
               standaloneModal( ) ;
            }
            else
            {
               Gx_mode = "INS";
               AssignAttri("", false, "Gx_mode", Gx_mode);
               standaloneModal( ) ;
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( IsIns( )  )
            {
               /* Clear variables for new insertion. */
               InitAll0113( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( IsIns( ) )
         {
            bttBtn_delete_Enabled = 0;
            AssignProp("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Enabled), 5, 0), true);
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         bttBtn_delete_Visible = 0;
         AssignProp("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Visible), 5, 0), true);
         bttBtn_first_Visible = 0;
         AssignProp("", false, bttBtn_first_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_first_Visible), 5, 0), true);
         bttBtn_previous_Visible = 0;
         AssignProp("", false, bttBtn_previous_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_previous_Visible), 5, 0), true);
         bttBtn_next_Visible = 0;
         AssignProp("", false, bttBtn_next_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_next_Visible), 5, 0), true);
         bttBtn_last_Visible = 0;
         AssignProp("", false, bttBtn_last_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_last_Visible), 5, 0), true);
         bttBtn_select_Visible = 0;
         AssignProp("", false, bttBtn_select_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_select_Visible), 5, 0), true);
         bttBtn_delete_Visible = 0;
         AssignProp("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Visible), 5, 0), true);
         if ( IsDsp( ) )
         {
            bttBtn_enter_Visible = 0;
            AssignProp("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_enter_Visible), 5, 0), true);
         }
         DisableAttributes0113( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( IsDlt( ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption010( )
      {
      }

      protected void ZM0113( short GX_JID )
      {
         if ( ( GX_JID == 7 ) || ( GX_JID == 0 ) )
         {
            if ( ! IsIns( ) )
            {
               Z54clienteDNI = T00013_A54clienteDNI[0];
               Z2clienteNombre = T00013_A2clienteNombre[0];
               Z3clienteApellido = T00013_A3clienteApellido[0];
               Z55clienteNumeroPasaporte = T00013_A55clienteNumeroPasaporte[0];
               Z4clienteDireccion = T00013_A4clienteDireccion[0];
               Z5clienteTelefono = T00013_A5clienteTelefono[0];
               Z6clienteCorreo = T00013_A6clienteCorreo[0];
               Z14clienteFecha = T00013_A14clienteFecha[0];
            }
            else
            {
               Z54clienteDNI = A54clienteDNI;
               Z2clienteNombre = A2clienteNombre;
               Z3clienteApellido = A3clienteApellido;
               Z55clienteNumeroPasaporte = A55clienteNumeroPasaporte;
               Z4clienteDireccion = A4clienteDireccion;
               Z5clienteTelefono = A5clienteTelefono;
               Z6clienteCorreo = A6clienteCorreo;
               Z14clienteFecha = A14clienteFecha;
            }
         }
         if ( GX_JID == -7 )
         {
            Z1clienteId = A1clienteId;
            Z54clienteDNI = A54clienteDNI;
            Z2clienteNombre = A2clienteNombre;
            Z3clienteApellido = A3clienteApellido;
            Z55clienteNumeroPasaporte = A55clienteNumeroPasaporte;
            Z4clienteDireccion = A4clienteDireccion;
            Z5clienteTelefono = A5clienteTelefono;
            Z6clienteCorreo = A6clienteCorreo;
            Z14clienteFecha = A14clienteFecha;
         }
      }

      protected void standaloneNotModal( )
      {
         edtclienteFecha_Enabled = 0;
         AssignProp("", false, edtclienteFecha_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtclienteFecha_Enabled), 5, 0), true);
         Gx_BScreen = 0;
         AssignAttri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         Gx_date = DateTimeUtil.Today( context);
         AssignAttri("", false, "Gx_date", context.localUtil.Format(Gx_date, "99/99/99"));
         edtclienteFecha_Enabled = 0;
         AssignProp("", false, edtclienteFecha_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtclienteFecha_Enabled), 5, 0), true);
      }

      protected void standaloneModal( )
      {
         if ( IsIns( )  && (DateTime.MinValue==A14clienteFecha) && ( Gx_BScreen == 0 ) )
         {
            A14clienteFecha = Gx_date;
            AssignAttri("", false, "A14clienteFecha", context.localUtil.Format(A14clienteFecha, "99/99/99"));
         }
         else
         {
            if ( IsIns( )  )
            {
               A14clienteFecha = Gx_date;
               AssignAttri("", false, "A14clienteFecha", context.localUtil.Format(A14clienteFecha, "99/99/99"));
            }
         }
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            AssignProp("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Enabled), 5, 0), true);
         }
         else
         {
            bttBtn_delete_Enabled = 1;
            AssignProp("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Enabled), 5, 0), true);
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Enabled = 0;
            AssignProp("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_enter_Enabled), 5, 0), true);
         }
         else
         {
            bttBtn_enter_Enabled = 1;
            AssignProp("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_enter_Enabled), 5, 0), true);
         }
      }

      protected void Load0113( )
      {
         /* Using cursor T00014 */
         pr_default.execute(2, new Object[] {A1clienteId});
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound13 = 1;
            A54clienteDNI = T00014_A54clienteDNI[0];
            AssignAttri("", false, "A54clienteDNI", A54clienteDNI);
            A2clienteNombre = T00014_A2clienteNombre[0];
            AssignAttri("", false, "A2clienteNombre", A2clienteNombre);
            A3clienteApellido = T00014_A3clienteApellido[0];
            AssignAttri("", false, "A3clienteApellido", A3clienteApellido);
            A55clienteNumeroPasaporte = T00014_A55clienteNumeroPasaporte[0];
            AssignAttri("", false, "A55clienteNumeroPasaporte", StringUtil.LTrimStr( (decimal)(A55clienteNumeroPasaporte), 4, 0));
            A4clienteDireccion = T00014_A4clienteDireccion[0];
            AssignAttri("", false, "A4clienteDireccion", A4clienteDireccion);
            A5clienteTelefono = T00014_A5clienteTelefono[0];
            AssignAttri("", false, "A5clienteTelefono", A5clienteTelefono);
            A6clienteCorreo = T00014_A6clienteCorreo[0];
            AssignAttri("", false, "A6clienteCorreo", A6clienteCorreo);
            A14clienteFecha = T00014_A14clienteFecha[0];
            AssignAttri("", false, "A14clienteFecha", context.localUtil.Format(A14clienteFecha, "99/99/99"));
            ZM0113( -7) ;
         }
         pr_default.close(2);
         OnLoadActions0113( ) ;
      }

      protected void OnLoadActions0113( )
      {
      }

      protected void CheckExtendedTable0113( )
      {
         nIsDirty_13 = 0;
         Gx_BScreen = 1;
         AssignAttri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T00015 */
         pr_default.execute(3, new Object[] {A54clienteDNI, A1clienteId});
         if ( (pr_default.getStatus(3) != 101) )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_1004", new   object[]  {"cliente DNI"}), 1, "CLIENTEDNI");
            AnyError = 1;
            GX_FocusControl = edtclienteDNI_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(3);
         /* Using cursor T00016 */
         pr_default.execute(4, new Object[] {A55clienteNumeroPasaporte, A1clienteId});
         if ( (pr_default.getStatus(4) != 101) )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_1004", new   object[]  {"cliente Numero Pasaporte"}), 1, "CLIENTENUMEROPASAPORTE");
            AnyError = 1;
            GX_FocusControl = edtclienteNumeroPasaporte_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(4);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A2clienteNombre)) )
         {
            GX_msglist.addItem("Ingrese el nombre del cliente por favor", 1, "CLIENTENOMBRE");
            AnyError = 1;
            GX_FocusControl = edtclienteNombre_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A3clienteApellido)) )
         {
            GX_msglist.addItem("Ingrese el apellido del cliente por favor", 1, "CLIENTEAPELLIDO");
            AnyError = 1;
            GX_FocusControl = edtclienteApellido_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A5clienteTelefono)) )
         {
            GX_msglist.addItem("El telefono del cliente esta vacio", 0, "CLIENTETELEFONO");
         }
         if ( ! ( GxRegex.IsMatch(A6clienteCorreo,"^((\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*)|(\\s*))$") ) )
         {
            GX_msglist.addItem("Field cliente Correo does not match the specified pattern", "OutOfRange", 1, "CLIENTECORREO");
            AnyError = 1;
            GX_FocusControl = edtclienteCorreo_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors0113( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey0113( )
      {
         /* Using cursor T00017 */
         pr_default.execute(5, new Object[] {A1clienteId});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound13 = 1;
         }
         else
         {
            RcdFound13 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00013 */
         pr_default.execute(1, new Object[] {A1clienteId});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0113( 7) ;
            RcdFound13 = 1;
            A1clienteId = T00013_A1clienteId[0];
            AssignAttri("", false, "A1clienteId", StringUtil.LTrimStr( (decimal)(A1clienteId), 6, 0));
            A54clienteDNI = T00013_A54clienteDNI[0];
            AssignAttri("", false, "A54clienteDNI", A54clienteDNI);
            A2clienteNombre = T00013_A2clienteNombre[0];
            AssignAttri("", false, "A2clienteNombre", A2clienteNombre);
            A3clienteApellido = T00013_A3clienteApellido[0];
            AssignAttri("", false, "A3clienteApellido", A3clienteApellido);
            A55clienteNumeroPasaporte = T00013_A55clienteNumeroPasaporte[0];
            AssignAttri("", false, "A55clienteNumeroPasaporte", StringUtil.LTrimStr( (decimal)(A55clienteNumeroPasaporte), 4, 0));
            A4clienteDireccion = T00013_A4clienteDireccion[0];
            AssignAttri("", false, "A4clienteDireccion", A4clienteDireccion);
            A5clienteTelefono = T00013_A5clienteTelefono[0];
            AssignAttri("", false, "A5clienteTelefono", A5clienteTelefono);
            A6clienteCorreo = T00013_A6clienteCorreo[0];
            AssignAttri("", false, "A6clienteCorreo", A6clienteCorreo);
            A14clienteFecha = T00013_A14clienteFecha[0];
            AssignAttri("", false, "A14clienteFecha", context.localUtil.Format(A14clienteFecha, "99/99/99"));
            Z1clienteId = A1clienteId;
            sMode13 = Gx_mode;
            Gx_mode = "DSP";
            AssignAttri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load0113( ) ;
            if ( AnyError == 1 )
            {
               RcdFound13 = 0;
               InitializeNonKey0113( ) ;
            }
            Gx_mode = sMode13;
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound13 = 0;
            InitializeNonKey0113( ) ;
            sMode13 = Gx_mode;
            Gx_mode = "DSP";
            AssignAttri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode13;
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0113( ) ;
         if ( RcdFound13 == 0 )
         {
            Gx_mode = "INS";
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound13 = 0;
         /* Using cursor T00018 */
         pr_default.execute(6, new Object[] {A1clienteId});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T00018_A1clienteId[0] < A1clienteId ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T00018_A1clienteId[0] > A1clienteId ) ) )
            {
               A1clienteId = T00018_A1clienteId[0];
               AssignAttri("", false, "A1clienteId", StringUtil.LTrimStr( (decimal)(A1clienteId), 6, 0));
               RcdFound13 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound13 = 0;
         /* Using cursor T00019 */
         pr_default.execute(7, new Object[] {A1clienteId});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T00019_A1clienteId[0] > A1clienteId ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T00019_A1clienteId[0] < A1clienteId ) ) )
            {
               A1clienteId = T00019_A1clienteId[0];
               AssignAttri("", false, "A1clienteId", StringUtil.LTrimStr( (decimal)(A1clienteId), 6, 0));
               RcdFound13 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0113( ) ;
         if ( IsIns( ) )
         {
            /* Insert record */
            GX_FocusControl = edtclienteId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0113( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound13 == 1 )
            {
               if ( A1clienteId != Z1clienteId )
               {
                  A1clienteId = Z1clienteId;
                  AssignAttri("", false, "A1clienteId", StringUtil.LTrimStr( (decimal)(A1clienteId), 6, 0));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CLIENTEID");
                  AnyError = 1;
                  GX_FocusControl = edtclienteId_Internalname;
                  AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( IsDlt( ) )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtclienteId_Internalname;
                  AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  AssignAttri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update0113( ) ;
                  GX_FocusControl = edtclienteId_Internalname;
                  AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1clienteId != Z1clienteId )
               {
                  Gx_mode = "INS";
                  AssignAttri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtclienteId_Internalname;
                  AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0113( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CLIENTEID");
                     AnyError = 1;
                     GX_FocusControl = edtclienteId_Internalname;
                     AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     AssignAttri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtclienteId_Internalname;
                     AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0113( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A1clienteId != Z1clienteId )
         {
            A1clienteId = Z1clienteId;
            AssignAttri("", false, "A1clienteId", StringUtil.LTrimStr( (decimal)(A1clienteId), 6, 0));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CLIENTEID");
            AnyError = 1;
            GX_FocusControl = edtclienteId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtclienteId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         getEqualNoModal( ) ;
         if ( RcdFound13 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "CLIENTEID");
            AnyError = 1;
            GX_FocusControl = edtclienteId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtclienteDNI_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         ScanStart0113( ) ;
         if ( RcdFound13 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtclienteDNI_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd0113( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         move_previous( ) ;
         if ( RcdFound13 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtclienteDNI_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         move_next( ) ;
         if ( RcdFound13 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtclienteDNI_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         ScanStart0113( ) ;
         if ( RcdFound13 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound13 != 0 )
            {
               ScanNext0113( ) ;
            }
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtclienteDNI_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd0113( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency0113( )
      {
         if ( ! IsIns( ) )
         {
            /* Using cursor T00012 */
            pr_default.execute(0, new Object[] {A1clienteId});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"cliente"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z54clienteDNI, T00012_A54clienteDNI[0]) != 0 ) || ( StringUtil.StrCmp(Z2clienteNombre, T00012_A2clienteNombre[0]) != 0 ) || ( StringUtil.StrCmp(Z3clienteApellido, T00012_A3clienteApellido[0]) != 0 ) || ( Z55clienteNumeroPasaporte != T00012_A55clienteNumeroPasaporte[0] ) || ( StringUtil.StrCmp(Z4clienteDireccion, T00012_A4clienteDireccion[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z5clienteTelefono, T00012_A5clienteTelefono[0]) != 0 ) || ( StringUtil.StrCmp(Z6clienteCorreo, T00012_A6clienteCorreo[0]) != 0 ) || ( Z14clienteFecha != T00012_A14clienteFecha[0] ) )
            {
               if ( StringUtil.StrCmp(Z54clienteDNI, T00012_A54clienteDNI[0]) != 0 )
               {
                  GXUtil.WriteLog("cliente:[seudo value changed for attri]"+"clienteDNI");
                  GXUtil.WriteLogRaw("Old: ",Z54clienteDNI);
                  GXUtil.WriteLogRaw("Current: ",T00012_A54clienteDNI[0]);
               }
               if ( StringUtil.StrCmp(Z2clienteNombre, T00012_A2clienteNombre[0]) != 0 )
               {
                  GXUtil.WriteLog("cliente:[seudo value changed for attri]"+"clienteNombre");
                  GXUtil.WriteLogRaw("Old: ",Z2clienteNombre);
                  GXUtil.WriteLogRaw("Current: ",T00012_A2clienteNombre[0]);
               }
               if ( StringUtil.StrCmp(Z3clienteApellido, T00012_A3clienteApellido[0]) != 0 )
               {
                  GXUtil.WriteLog("cliente:[seudo value changed for attri]"+"clienteApellido");
                  GXUtil.WriteLogRaw("Old: ",Z3clienteApellido);
                  GXUtil.WriteLogRaw("Current: ",T00012_A3clienteApellido[0]);
               }
               if ( Z55clienteNumeroPasaporte != T00012_A55clienteNumeroPasaporte[0] )
               {
                  GXUtil.WriteLog("cliente:[seudo value changed for attri]"+"clienteNumeroPasaporte");
                  GXUtil.WriteLogRaw("Old: ",Z55clienteNumeroPasaporte);
                  GXUtil.WriteLogRaw("Current: ",T00012_A55clienteNumeroPasaporte[0]);
               }
               if ( StringUtil.StrCmp(Z4clienteDireccion, T00012_A4clienteDireccion[0]) != 0 )
               {
                  GXUtil.WriteLog("cliente:[seudo value changed for attri]"+"clienteDireccion");
                  GXUtil.WriteLogRaw("Old: ",Z4clienteDireccion);
                  GXUtil.WriteLogRaw("Current: ",T00012_A4clienteDireccion[0]);
               }
               if ( StringUtil.StrCmp(Z5clienteTelefono, T00012_A5clienteTelefono[0]) != 0 )
               {
                  GXUtil.WriteLog("cliente:[seudo value changed for attri]"+"clienteTelefono");
                  GXUtil.WriteLogRaw("Old: ",Z5clienteTelefono);
                  GXUtil.WriteLogRaw("Current: ",T00012_A5clienteTelefono[0]);
               }
               if ( StringUtil.StrCmp(Z6clienteCorreo, T00012_A6clienteCorreo[0]) != 0 )
               {
                  GXUtil.WriteLog("cliente:[seudo value changed for attri]"+"clienteCorreo");
                  GXUtil.WriteLogRaw("Old: ",Z6clienteCorreo);
                  GXUtil.WriteLogRaw("Current: ",T00012_A6clienteCorreo[0]);
               }
               if ( Z14clienteFecha != T00012_A14clienteFecha[0] )
               {
                  GXUtil.WriteLog("cliente:[seudo value changed for attri]"+"clienteFecha");
                  GXUtil.WriteLogRaw("Old: ",Z14clienteFecha);
                  GXUtil.WriteLogRaw("Current: ",T00012_A14clienteFecha[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"cliente"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0113( )
      {
         BeforeValidate0113( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0113( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0113( 0) ;
            CheckOptimisticConcurrency0113( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0113( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0113( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000110 */
                     pr_default.execute(8, new Object[] {A54clienteDNI, A2clienteNombre, A3clienteApellido, A55clienteNumeroPasaporte, A4clienteDireccion, A5clienteTelefono, A6clienteCorreo, A14clienteFecha});
                     A1clienteId = T000110_A1clienteId[0];
                     AssignAttri("", false, "A1clienteId", StringUtil.LTrimStr( (decimal)(A1clienteId), 6, 0));
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("cliente") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                           ResetCaption010( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0113( ) ;
            }
            EndLevel0113( ) ;
         }
         CloseExtendedTableCursors0113( ) ;
      }

      protected void Update0113( )
      {
         BeforeValidate0113( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0113( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0113( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0113( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0113( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000111 */
                     pr_default.execute(9, new Object[] {A54clienteDNI, A2clienteNombre, A3clienteApellido, A55clienteNumeroPasaporte, A4clienteDireccion, A5clienteTelefono, A6clienteCorreo, A14clienteFecha, A1clienteId});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("cliente") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"cliente"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0113( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), "SuccessfullyUpdated", 0, "", true);
                           ResetCaption010( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0113( ) ;
         }
         CloseExtendedTableCursors0113( ) ;
      }

      protected void DeferredUpdate0113( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         AssignAttri("", false, "Gx_mode", Gx_mode);
         BeforeValidate0113( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0113( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0113( ) ;
            AfterConfirm0113( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0113( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000112 */
                  pr_default.execute(10, new Object[] {A1clienteId});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("cliente") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound13 == 0 )
                        {
                           InitAll0113( ) ;
                           Gx_mode = "INS";
                           AssignAttri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           AssignAttri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), "SuccessfullyDeleted", 0, "", true);
                        ResetCaption010( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode13 = Gx_mode;
         Gx_mode = "DLT";
         AssignAttri("", false, "Gx_mode", Gx_mode);
         EndLevel0113( ) ;
         Gx_mode = sMode13;
         AssignAttri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls0113( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor T000113 */
            pr_default.execute(11, new Object[] {A1clienteId});
            if ( (pr_default.getStatus(11) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"cuenta Banco"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(11);
         }
      }

      protected void EndLevel0113( )
      {
         if ( ! IsIns( ) )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0113( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores("cliente",pr_default);
            if ( AnyError == 0 )
            {
               ConfirmValues010( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores("cliente",pr_default);
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0113( )
      {
         /* Using cursor T000114 */
         pr_default.execute(12);
         RcdFound13 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound13 = 1;
            A1clienteId = T000114_A1clienteId[0];
            AssignAttri("", false, "A1clienteId", StringUtil.LTrimStr( (decimal)(A1clienteId), 6, 0));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0113( )
      {
         /* Scan next routine */
         pr_default.readNext(12);
         RcdFound13 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound13 = 1;
            A1clienteId = T000114_A1clienteId[0];
            AssignAttri("", false, "A1clienteId", StringUtil.LTrimStr( (decimal)(A1clienteId), 6, 0));
         }
      }

      protected void ScanEnd0113( )
      {
         pr_default.close(12);
      }

      protected void AfterConfirm0113( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0113( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0113( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0113( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0113( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0113( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0113( )
      {
         edtclienteId_Enabled = 0;
         AssignProp("", false, edtclienteId_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtclienteId_Enabled), 5, 0), true);
         edtclienteDNI_Enabled = 0;
         AssignProp("", false, edtclienteDNI_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtclienteDNI_Enabled), 5, 0), true);
         edtclienteNombre_Enabled = 0;
         AssignProp("", false, edtclienteNombre_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtclienteNombre_Enabled), 5, 0), true);
         edtclienteApellido_Enabled = 0;
         AssignProp("", false, edtclienteApellido_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtclienteApellido_Enabled), 5, 0), true);
         edtclienteNumeroPasaporte_Enabled = 0;
         AssignProp("", false, edtclienteNumeroPasaporte_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtclienteNumeroPasaporte_Enabled), 5, 0), true);
         edtclienteDireccion_Enabled = 0;
         AssignProp("", false, edtclienteDireccion_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtclienteDireccion_Enabled), 5, 0), true);
         edtclienteTelefono_Enabled = 0;
         AssignProp("", false, edtclienteTelefono_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtclienteTelefono_Enabled), 5, 0), true);
         edtclienteCorreo_Enabled = 0;
         AssignProp("", false, edtclienteCorreo_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtclienteCorreo_Enabled), 5, 0), true);
         edtclienteFecha_Enabled = 0;
         AssignProp("", false, edtclienteFecha_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtclienteFecha_Enabled), 5, 0), true);
      }

      protected void send_integrity_lvl_hashes0113( )
      {
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues010( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 136889), false, true);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 136889), false, true);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 136889), false, true);
         context.AddJavascriptSource("gxcfg.js", "?20191220461122", false, true);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 136889), false, true);
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 136889), false, true);
         context.AddJavascriptSource("calendar-en.js", "?"+context.GetBuildNumber( 136889), false, true);
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + "-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("cliente.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" title=\"submit\" style=\"display:none\" disabled>") ;
         AssignProp("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = new GXProperties();
         forbiddenHiddens.Add("hshsalt", "hsh"+"cliente");
         forbiddenHiddens.Add("clienteFecha", context.localUtil.Format(A14clienteFecha, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "hsh", GetEncryptedHash( forbiddenHiddens.ToString(), GXKey));
         GXUtil.WriteLog("cliente:[ SendSecurityCheck value for]"+forbiddenHiddens.ToJSonString());
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "Z1clienteId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1clienteId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z54clienteDNI", StringUtil.RTrim( Z54clienteDNI));
         GxWebStd.gx_hidden_field( context, "Z2clienteNombre", StringUtil.RTrim( Z2clienteNombre));
         GxWebStd.gx_hidden_field( context, "Z3clienteApellido", StringUtil.RTrim( Z3clienteApellido));
         GxWebStd.gx_hidden_field( context, "Z55clienteNumeroPasaporte", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z55clienteNumeroPasaporte), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z4clienteDireccion", Z4clienteDireccion);
         GxWebStd.gx_hidden_field( context, "Z5clienteTelefono", StringUtil.RTrim( Z5clienteTelefono));
         GxWebStd.gx_hidden_field( context, "Z6clienteCorreo", Z6clienteCorreo);
         GxWebStd.gx_hidden_field( context, "Z14clienteFecha", context.localUtil.DToC( Z14clienteFecha, 0, "/"));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vTODAY", context.localUtil.DToC( Gx_date, 0, "/"));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendSecurityToken(sPrefix);
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("cliente.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "cliente" ;
      }

      public override String GetPgmdesc( )
      {
         return "cliente" ;
      }

      protected void InitializeNonKey0113( )
      {
         A54clienteDNI = "";
         AssignAttri("", false, "A54clienteDNI", A54clienteDNI);
         A2clienteNombre = "";
         AssignAttri("", false, "A2clienteNombre", A2clienteNombre);
         A3clienteApellido = "";
         AssignAttri("", false, "A3clienteApellido", A3clienteApellido);
         A55clienteNumeroPasaporte = 0;
         AssignAttri("", false, "A55clienteNumeroPasaporte", StringUtil.LTrimStr( (decimal)(A55clienteNumeroPasaporte), 4, 0));
         A4clienteDireccion = "";
         AssignAttri("", false, "A4clienteDireccion", A4clienteDireccion);
         A5clienteTelefono = "";
         AssignAttri("", false, "A5clienteTelefono", A5clienteTelefono);
         A6clienteCorreo = "";
         AssignAttri("", false, "A6clienteCorreo", A6clienteCorreo);
         A14clienteFecha = Gx_date;
         AssignAttri("", false, "A14clienteFecha", context.localUtil.Format(A14clienteFecha, "99/99/99"));
         Z54clienteDNI = "";
         Z2clienteNombre = "";
         Z3clienteApellido = "";
         Z55clienteNumeroPasaporte = 0;
         Z4clienteDireccion = "";
         Z5clienteTelefono = "";
         Z6clienteCorreo = "";
         Z14clienteFecha = DateTime.MinValue;
      }

      protected void InitAll0113( )
      {
         A1clienteId = 0;
         AssignAttri("", false, "A1clienteId", StringUtil.LTrimStr( (decimal)(A1clienteId), 6, 0));
         InitializeNonKey0113( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A14clienteFecha = i14clienteFecha;
         AssignAttri("", false, "A14clienteFecha", context.localUtil.Format(A14clienteFecha, "99/99/99"));
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ));
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20191220461129", true, true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.eng.js", "?"+GetCacheInvalidationToken( ), false, true);
         context.AddJavascriptSource("cliente.js", "?20191220461129", false, true);
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTitle_Internalname = "TITLE";
         divTitlecontainer_Internalname = "TITLECONTAINER";
         bttBtn_first_Internalname = "BTN_FIRST";
         bttBtn_previous_Internalname = "BTN_PREVIOUS";
         bttBtn_next_Internalname = "BTN_NEXT";
         bttBtn_last_Internalname = "BTN_LAST";
         bttBtn_select_Internalname = "BTN_SELECT";
         divToolbarcell_Internalname = "TOOLBARCELL";
         edtclienteId_Internalname = "CLIENTEID";
         edtclienteDNI_Internalname = "CLIENTEDNI";
         edtclienteNombre_Internalname = "CLIENTENOMBRE";
         edtclienteApellido_Internalname = "CLIENTEAPELLIDO";
         edtclienteNumeroPasaporte_Internalname = "CLIENTENUMEROPASAPORTE";
         edtclienteDireccion_Internalname = "CLIENTEDIRECCION";
         edtclienteTelefono_Internalname = "CLIENTETELEFONO";
         edtclienteCorreo_Internalname = "CLIENTECORREO";
         edtclienteFecha_Internalname = "CLIENTEFECHA";
         divFormcontainer_Internalname = "FORMCONTAINER";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         context.SetDefaultTheme("Carmine");
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "cliente";
         bttBtn_delete_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Enabled = 1;
         bttBtn_enter_Visible = 1;
         edtclienteFecha_Jsonclick = "";
         edtclienteFecha_Enabled = 0;
         edtclienteCorreo_Jsonclick = "";
         edtclienteCorreo_Enabled = 1;
         edtclienteTelefono_Jsonclick = "";
         edtclienteTelefono_Enabled = 1;
         edtclienteDireccion_Enabled = 1;
         edtclienteNumeroPasaporte_Jsonclick = "";
         edtclienteNumeroPasaporte_Enabled = 1;
         edtclienteApellido_Jsonclick = "";
         edtclienteApellido_Enabled = 1;
         edtclienteNombre_Jsonclick = "";
         edtclienteNombre_Enabled = 1;
         edtclienteDNI_Jsonclick = "";
         edtclienteDNI_Enabled = 1;
         edtclienteId_Jsonclick = "";
         edtclienteId_Enabled = 1;
         bttBtn_select_Visible = 1;
         bttBtn_last_Visible = 1;
         bttBtn_next_Visible = 1;
         bttBtn_previous_Visible = 1;
         bttBtn_first_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void init_web_controls( )
      {
         /* End function init_web_controls */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         getEqualNoModal( ) ;
         GX_FocusControl = edtclienteDNI_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      protected bool IsIns( )
      {
         return ((StringUtil.StrCmp(Gx_mode, "INS")==0) ? true : false) ;
      }

      protected bool IsDlt( )
      {
         return ((StringUtil.StrCmp(Gx_mode, "DLT")==0) ? true : false) ;
      }

      protected bool IsUpd( )
      {
         return ((StringUtil.StrCmp(Gx_mode, "UPD")==0) ? true : false) ;
      }

      protected bool IsDsp( )
      {
         return ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? true : false) ;
      }

      public void Valid_Clienteid( )
      {
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         send_integrity_footer_hashes( ) ;
         dynload_actions( ) ;
         /*  Sending validation outputs */
         AssignAttri("", false, "A54clienteDNI", StringUtil.RTrim( A54clienteDNI));
         AssignAttri("", false, "A2clienteNombre", StringUtil.RTrim( A2clienteNombre));
         AssignAttri("", false, "A3clienteApellido", StringUtil.RTrim( A3clienteApellido));
         AssignAttri("", false, "A55clienteNumeroPasaporte", StringUtil.LTrim( StringUtil.NToC( (decimal)(A55clienteNumeroPasaporte), 4, 0, ".", "")));
         AssignAttri("", false, "A4clienteDireccion", A4clienteDireccion);
         AssignAttri("", false, "A5clienteTelefono", StringUtil.RTrim( A5clienteTelefono));
         AssignAttri("", false, "A6clienteCorreo", A6clienteCorreo);
         AssignAttri("", false, "A14clienteFecha", context.localUtil.Format(A14clienteFecha, "99/99/99"));
         AssignAttri("", false, "Gx_mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "Z1clienteId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1clienteId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z54clienteDNI", StringUtil.RTrim( Z54clienteDNI));
         GxWebStd.gx_hidden_field( context, "Z2clienteNombre", StringUtil.RTrim( Z2clienteNombre));
         GxWebStd.gx_hidden_field( context, "Z3clienteApellido", StringUtil.RTrim( Z3clienteApellido));
         GxWebStd.gx_hidden_field( context, "Z55clienteNumeroPasaporte", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z55clienteNumeroPasaporte), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z4clienteDireccion", Z4clienteDireccion);
         GxWebStd.gx_hidden_field( context, "Z5clienteTelefono", StringUtil.RTrim( Z5clienteTelefono));
         GxWebStd.gx_hidden_field( context, "Z6clienteCorreo", Z6clienteCorreo);
         GxWebStd.gx_hidden_field( context, "Z14clienteFecha", context.localUtil.Format(Z14clienteFecha, "99/99/99"));
         AssignProp("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Enabled), 5, 0), true);
         AssignProp("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_enter_Enabled), 5, 0), true);
         SendCloseFormHiddens( ) ;
      }

      public void Valid_Clientedni( )
      {
         /* Using cursor T000115 */
         pr_default.execute(13, new Object[] {A54clienteDNI, A1clienteId});
         if ( (pr_default.getStatus(13) != 101) )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_1004", new   object[]  {"cliente DNI"}), 1, "CLIENTEDNI");
            AnyError = 1;
            GX_FocusControl = edtclienteDNI_Internalname;
         }
         pr_default.close(13);
         dynload_actions( ) ;
         /*  Sending validation outputs */
      }

      public void Valid_Clientenumeropasaporte( )
      {
         /* Using cursor T000116 */
         pr_default.execute(14, new Object[] {A55clienteNumeroPasaporte, A1clienteId});
         if ( (pr_default.getStatus(14) != 101) )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_1004", new   object[]  {"cliente Numero Pasaporte"}), 1, "CLIENTENUMEROPASAPORTE");
            AnyError = 1;
            GX_FocusControl = edtclienteNumeroPasaporte_Internalname;
         }
         pr_default.close(14);
         dynload_actions( ) ;
         /*  Sending validation outputs */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}]");
         setEventMetadata("ENTER",",oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'A14clienteFecha',fld:'CLIENTEFECHA',pic:''}]");
         setEventMetadata("REFRESH",",oparms:[]}");
         setEventMetadata("VALID_CLIENTEID","{handler:'Valid_Clienteid',iparms:[{av:'A1clienteId',fld:'CLIENTEID',pic:'ZZZZZ9'},{av:'Gx_date',fld:'vTODAY',pic:''},{av:'Gx_BScreen',fld:'vGXBSCREEN',pic:'9'},{av:'Gx_mode',fld:'vMODE',pic:'@!'},{av:'A14clienteFecha',fld:'CLIENTEFECHA',pic:''}]");
         setEventMetadata("VALID_CLIENTEID",",oparms:[{av:'A54clienteDNI',fld:'CLIENTEDNI',pic:''},{av:'A2clienteNombre',fld:'CLIENTENOMBRE',pic:''},{av:'A3clienteApellido',fld:'CLIENTEAPELLIDO',pic:''},{av:'A55clienteNumeroPasaporte',fld:'CLIENTENUMEROPASAPORTE',pic:'ZZZ9'},{av:'A4clienteDireccion',fld:'CLIENTEDIRECCION',pic:''},{av:'A5clienteTelefono',fld:'CLIENTETELEFONO',pic:''},{av:'A6clienteCorreo',fld:'CLIENTECORREO',pic:''},{av:'A14clienteFecha',fld:'CLIENTEFECHA',pic:''},{av:'Gx_mode',fld:'vMODE',pic:'@!'},{av:'Z1clienteId'},{av:'Z54clienteDNI'},{av:'Z2clienteNombre'},{av:'Z3clienteApellido'},{av:'Z55clienteNumeroPasaporte'},{av:'Z4clienteDireccion'},{av:'Z5clienteTelefono'},{av:'Z6clienteCorreo'},{av:'Z14clienteFecha'},{ctrl:'BTN_DELETE',prop:'Enabled'},{ctrl:'BTN_ENTER',prop:'Enabled'}]}");
         setEventMetadata("VALID_CLIENTEDNI","{handler:'Valid_Clientedni',iparms:[{av:'A54clienteDNI',fld:'CLIENTEDNI',pic:''},{av:'A1clienteId',fld:'CLIENTEID',pic:'ZZZZZ9'}]");
         setEventMetadata("VALID_CLIENTEDNI",",oparms:[]}");
         setEventMetadata("VALID_CLIENTENOMBRE","{handler:'Valid_Clientenombre',iparms:[]");
         setEventMetadata("VALID_CLIENTENOMBRE",",oparms:[]}");
         setEventMetadata("VALID_CLIENTEAPELLIDO","{handler:'Valid_Clienteapellido',iparms:[]");
         setEventMetadata("VALID_CLIENTEAPELLIDO",",oparms:[]}");
         setEventMetadata("VALID_CLIENTENUMEROPASAPORTE","{handler:'Valid_Clientenumeropasaporte',iparms:[{av:'A55clienteNumeroPasaporte',fld:'CLIENTENUMEROPASAPORTE',pic:'ZZZ9'},{av:'A1clienteId',fld:'CLIENTEID',pic:'ZZZZZ9'}]");
         setEventMetadata("VALID_CLIENTENUMEROPASAPORTE",",oparms:[]}");
         setEventMetadata("VALID_CLIENTETELEFONO","{handler:'Valid_Clientetelefono',iparms:[]");
         setEventMetadata("VALID_CLIENTETELEFONO",",oparms:[]}");
         setEventMetadata("VALID_CLIENTECORREO","{handler:'Valid_Clientecorreo',iparms:[]");
         setEventMetadata("VALID_CLIENTECORREO",",oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z54clienteDNI = "";
         Z2clienteNombre = "";
         Z3clienteApellido = "";
         Z4clienteDireccion = "";
         Z5clienteTelefono = "";
         Z6clienteCorreo = "";
         Z14clienteFecha = DateTime.MinValue;
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         lblTitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_first_Jsonclick = "";
         bttBtn_previous_Jsonclick = "";
         bttBtn_next_Jsonclick = "";
         bttBtn_last_Jsonclick = "";
         bttBtn_select_Jsonclick = "";
         A54clienteDNI = "";
         A2clienteNombre = "";
         A3clienteApellido = "";
         A4clienteDireccion = "";
         gxphoneLink = "";
         A5clienteTelefono = "";
         A6clienteCorreo = "";
         A14clienteFecha = DateTime.MinValue;
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         Gx_mode = "";
         Gx_date = DateTime.MinValue;
         forbiddenHiddens = new GXProperties();
         hsh = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         T00014_A1clienteId = new int[1] ;
         T00014_A54clienteDNI = new String[] {""} ;
         T00014_A2clienteNombre = new String[] {""} ;
         T00014_A3clienteApellido = new String[] {""} ;
         T00014_A55clienteNumeroPasaporte = new short[1] ;
         T00014_A4clienteDireccion = new String[] {""} ;
         T00014_A5clienteTelefono = new String[] {""} ;
         T00014_A6clienteCorreo = new String[] {""} ;
         T00014_A14clienteFecha = new DateTime[] {DateTime.MinValue} ;
         T00015_A54clienteDNI = new String[] {""} ;
         T00016_A55clienteNumeroPasaporte = new short[1] ;
         T00017_A1clienteId = new int[1] ;
         T00013_A1clienteId = new int[1] ;
         T00013_A54clienteDNI = new String[] {""} ;
         T00013_A2clienteNombre = new String[] {""} ;
         T00013_A3clienteApellido = new String[] {""} ;
         T00013_A55clienteNumeroPasaporte = new short[1] ;
         T00013_A4clienteDireccion = new String[] {""} ;
         T00013_A5clienteTelefono = new String[] {""} ;
         T00013_A6clienteCorreo = new String[] {""} ;
         T00013_A14clienteFecha = new DateTime[] {DateTime.MinValue} ;
         sMode13 = "";
         T00018_A1clienteId = new int[1] ;
         T00019_A1clienteId = new int[1] ;
         T00012_A1clienteId = new int[1] ;
         T00012_A54clienteDNI = new String[] {""} ;
         T00012_A2clienteNombre = new String[] {""} ;
         T00012_A3clienteApellido = new String[] {""} ;
         T00012_A55clienteNumeroPasaporte = new short[1] ;
         T00012_A4clienteDireccion = new String[] {""} ;
         T00012_A5clienteTelefono = new String[] {""} ;
         T00012_A6clienteCorreo = new String[] {""} ;
         T00012_A14clienteFecha = new DateTime[] {DateTime.MinValue} ;
         T000110_A1clienteId = new int[1] ;
         T000113_A56cuentaBancoId = new int[1] ;
         T000114_A1clienteId = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         i14clienteFecha = DateTime.MinValue;
         ZZ54clienteDNI = "";
         ZZ2clienteNombre = "";
         ZZ3clienteApellido = "";
         ZZ4clienteDireccion = "";
         ZZ5clienteTelefono = "";
         ZZ6clienteCorreo = "";
         ZZ14clienteFecha = DateTime.MinValue;
         T000115_A54clienteDNI = new String[] {""} ;
         T000116_A55clienteNumeroPasaporte = new short[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.cliente__default(),
            new Object[][] {
                new Object[] {
               T00012_A1clienteId, T00012_A54clienteDNI, T00012_A2clienteNombre, T00012_A3clienteApellido, T00012_A55clienteNumeroPasaporte, T00012_A4clienteDireccion, T00012_A5clienteTelefono, T00012_A6clienteCorreo, T00012_A14clienteFecha
               }
               , new Object[] {
               T00013_A1clienteId, T00013_A54clienteDNI, T00013_A2clienteNombre, T00013_A3clienteApellido, T00013_A55clienteNumeroPasaporte, T00013_A4clienteDireccion, T00013_A5clienteTelefono, T00013_A6clienteCorreo, T00013_A14clienteFecha
               }
               , new Object[] {
               T00014_A1clienteId, T00014_A54clienteDNI, T00014_A2clienteNombre, T00014_A3clienteApellido, T00014_A55clienteNumeroPasaporte, T00014_A4clienteDireccion, T00014_A5clienteTelefono, T00014_A6clienteCorreo, T00014_A14clienteFecha
               }
               , new Object[] {
               T00015_A54clienteDNI
               }
               , new Object[] {
               T00016_A55clienteNumeroPasaporte
               }
               , new Object[] {
               T00017_A1clienteId
               }
               , new Object[] {
               T00018_A1clienteId
               }
               , new Object[] {
               T00019_A1clienteId
               }
               , new Object[] {
               T000110_A1clienteId
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000113_A56cuentaBancoId
               }
               , new Object[] {
               T000114_A1clienteId
               }
               , new Object[] {
               T000115_A54clienteDNI
               }
               , new Object[] {
               T000116_A55clienteNumeroPasaporte
               }
            }
         );
         Z14clienteFecha = DateTime.MinValue;
         A14clienteFecha = DateTime.MinValue;
         i14clienteFecha = DateTime.MinValue;
         Gx_date = DateTimeUtil.Today( context);
      }

      private short Z55clienteNumeroPasaporte ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A55clienteNumeroPasaporte ;
      private short Gx_BScreen ;
      private short GX_JID ;
      private short RcdFound13 ;
      private short nIsDirty_13 ;
      private short gxajaxcallmode ;
      private short ZZ55clienteNumeroPasaporte ;
      private int Z1clienteId ;
      private int trnEnded ;
      private int bttBtn_first_Visible ;
      private int bttBtn_previous_Visible ;
      private int bttBtn_next_Visible ;
      private int bttBtn_last_Visible ;
      private int bttBtn_select_Visible ;
      private int A1clienteId ;
      private int edtclienteId_Enabled ;
      private int edtclienteDNI_Enabled ;
      private int edtclienteNombre_Enabled ;
      private int edtclienteApellido_Enabled ;
      private int edtclienteNumeroPasaporte_Enabled ;
      private int edtclienteDireccion_Enabled ;
      private int edtclienteTelefono_Enabled ;
      private int edtclienteCorreo_Enabled ;
      private int edtclienteFecha_Enabled ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_enter_Enabled ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int bttBtn_delete_Enabled ;
      private int idxLst ;
      private int ZZ1clienteId ;
      private String sPrefix ;
      private String Z54clienteDNI ;
      private String Z2clienteNombre ;
      private String Z3clienteApellido ;
      private String Z5clienteTelefono ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtclienteId_Internalname ;
      private String divMaintable_Internalname ;
      private String divTitlecontainer_Internalname ;
      private String lblTitle_Internalname ;
      private String lblTitle_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String divFormcontainer_Internalname ;
      private String divToolbarcell_Internalname ;
      private String TempTags ;
      private String bttBtn_first_Internalname ;
      private String bttBtn_first_Jsonclick ;
      private String bttBtn_previous_Internalname ;
      private String bttBtn_previous_Jsonclick ;
      private String bttBtn_next_Internalname ;
      private String bttBtn_next_Jsonclick ;
      private String bttBtn_last_Internalname ;
      private String bttBtn_last_Jsonclick ;
      private String bttBtn_select_Internalname ;
      private String bttBtn_select_Jsonclick ;
      private String edtclienteId_Jsonclick ;
      private String edtclienteDNI_Internalname ;
      private String A54clienteDNI ;
      private String edtclienteDNI_Jsonclick ;
      private String edtclienteNombre_Internalname ;
      private String A2clienteNombre ;
      private String edtclienteNombre_Jsonclick ;
      private String edtclienteApellido_Internalname ;
      private String A3clienteApellido ;
      private String edtclienteApellido_Jsonclick ;
      private String edtclienteNumeroPasaporte_Internalname ;
      private String edtclienteNumeroPasaporte_Jsonclick ;
      private String edtclienteDireccion_Internalname ;
      private String edtclienteTelefono_Internalname ;
      private String gxphoneLink ;
      private String A5clienteTelefono ;
      private String edtclienteTelefono_Jsonclick ;
      private String edtclienteCorreo_Internalname ;
      private String edtclienteCorreo_Jsonclick ;
      private String edtclienteFecha_Internalname ;
      private String edtclienteFecha_Jsonclick ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String Gx_mode ;
      private String hsh ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode13 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String ZZ54clienteDNI ;
      private String ZZ2clienteNombre ;
      private String ZZ3clienteApellido ;
      private String ZZ5clienteTelefono ;
      private DateTime Z14clienteFecha ;
      private DateTime A14clienteFecha ;
      private DateTime Gx_date ;
      private DateTime i14clienteFecha ;
      private DateTime ZZ14clienteFecha ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool Gx_longc ;
      private String Z4clienteDireccion ;
      private String Z6clienteCorreo ;
      private String A4clienteDireccion ;
      private String A6clienteCorreo ;
      private String ZZ4clienteDireccion ;
      private String ZZ6clienteCorreo ;
      private GXProperties forbiddenHiddens ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T00014_A1clienteId ;
      private String[] T00014_A54clienteDNI ;
      private String[] T00014_A2clienteNombre ;
      private String[] T00014_A3clienteApellido ;
      private short[] T00014_A55clienteNumeroPasaporte ;
      private String[] T00014_A4clienteDireccion ;
      private String[] T00014_A5clienteTelefono ;
      private String[] T00014_A6clienteCorreo ;
      private DateTime[] T00014_A14clienteFecha ;
      private String[] T00015_A54clienteDNI ;
      private short[] T00016_A55clienteNumeroPasaporte ;
      private int[] T00017_A1clienteId ;
      private int[] T00013_A1clienteId ;
      private String[] T00013_A54clienteDNI ;
      private String[] T00013_A2clienteNombre ;
      private String[] T00013_A3clienteApellido ;
      private short[] T00013_A55clienteNumeroPasaporte ;
      private String[] T00013_A4clienteDireccion ;
      private String[] T00013_A5clienteTelefono ;
      private String[] T00013_A6clienteCorreo ;
      private DateTime[] T00013_A14clienteFecha ;
      private int[] T00018_A1clienteId ;
      private int[] T00019_A1clienteId ;
      private int[] T00012_A1clienteId ;
      private String[] T00012_A54clienteDNI ;
      private String[] T00012_A2clienteNombre ;
      private String[] T00012_A3clienteApellido ;
      private short[] T00012_A55clienteNumeroPasaporte ;
      private String[] T00012_A4clienteDireccion ;
      private String[] T00012_A5clienteTelefono ;
      private String[] T00012_A6clienteCorreo ;
      private DateTime[] T00012_A14clienteFecha ;
      private int[] T000110_A1clienteId ;
      private int[] T000113_A56cuentaBancoId ;
      private int[] T000114_A1clienteId ;
      private String[] T000115_A54clienteDNI ;
      private short[] T000116_A55clienteNumeroPasaporte ;
      private GXWebForm Form ;
   }

   public class cliente__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00014 ;
          prmT00014 = new Object[] {
          new Object[] {"@clienteId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00015 ;
          prmT00015 = new Object[] {
          new Object[] {"@clienteDNI",SqlDbType.NChar,20,0} ,
          new Object[] {"@clienteId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00016 ;
          prmT00016 = new Object[] {
          new Object[] {"@clienteNumeroPasaporte",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@clienteId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00017 ;
          prmT00017 = new Object[] {
          new Object[] {"@clienteId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00013 ;
          prmT00013 = new Object[] {
          new Object[] {"@clienteId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00018 ;
          prmT00018 = new Object[] {
          new Object[] {"@clienteId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00019 ;
          prmT00019 = new Object[] {
          new Object[] {"@clienteId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00012 ;
          prmT00012 = new Object[] {
          new Object[] {"@clienteId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000110 ;
          prmT000110 = new Object[] {
          new Object[] {"@clienteDNI",SqlDbType.NChar,20,0} ,
          new Object[] {"@clienteNombre",SqlDbType.NChar,20,0} ,
          new Object[] {"@clienteApellido",SqlDbType.NChar,20,0} ,
          new Object[] {"@clienteNumeroPasaporte",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@clienteDireccion",SqlDbType.NVarChar,1024,0} ,
          new Object[] {"@clienteTelefono",SqlDbType.NChar,20,0} ,
          new Object[] {"@clienteCorreo",SqlDbType.NVarChar,100,0} ,
          new Object[] {"@clienteFecha",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmT000111 ;
          prmT000111 = new Object[] {
          new Object[] {"@clienteDNI",SqlDbType.NChar,20,0} ,
          new Object[] {"@clienteNombre",SqlDbType.NChar,20,0} ,
          new Object[] {"@clienteApellido",SqlDbType.NChar,20,0} ,
          new Object[] {"@clienteNumeroPasaporte",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@clienteDireccion",SqlDbType.NVarChar,1024,0} ,
          new Object[] {"@clienteTelefono",SqlDbType.NChar,20,0} ,
          new Object[] {"@clienteCorreo",SqlDbType.NVarChar,100,0} ,
          new Object[] {"@clienteFecha",SqlDbType.DateTime,8,0} ,
          new Object[] {"@clienteId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000112 ;
          prmT000112 = new Object[] {
          new Object[] {"@clienteId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000113 ;
          prmT000113 = new Object[] {
          new Object[] {"@clienteId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000114 ;
          prmT000114 = new Object[] {
          } ;
          Object[] prmT000115 ;
          prmT000115 = new Object[] {
          new Object[] {"@clienteDNI",SqlDbType.NChar,20,0} ,
          new Object[] {"@clienteId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000116 ;
          prmT000116 = new Object[] {
          new Object[] {"@clienteNumeroPasaporte",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@clienteId",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00012", "SELECT [clienteId], [clienteDNI], [clienteNombre], [clienteApellido], [clienteNumeroPasaporte], [clienteDireccion], [clienteTelefono], [clienteCorreo], [clienteFecha] FROM [cliente] WITH (UPDLOCK) WHERE [clienteId] = @clienteId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00012,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00013", "SELECT [clienteId], [clienteDNI], [clienteNombre], [clienteApellido], [clienteNumeroPasaporte], [clienteDireccion], [clienteTelefono], [clienteCorreo], [clienteFecha] FROM [cliente] WHERE [clienteId] = @clienteId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00013,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00014", "SELECT TM1.[clienteId], TM1.[clienteDNI], TM1.[clienteNombre], TM1.[clienteApellido], TM1.[clienteNumeroPasaporte], TM1.[clienteDireccion], TM1.[clienteTelefono], TM1.[clienteCorreo], TM1.[clienteFecha] FROM [cliente] TM1 WHERE TM1.[clienteId] = @clienteId ORDER BY TM1.[clienteId]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00014,100, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00015", "SELECT [clienteDNI] FROM [cliente] WHERE ([clienteDNI] = @clienteDNI) AND (Not ( [clienteId] = @clienteId)) ",true, GxErrorMask.GX_NOMASK, false, this,prmT00015,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00016", "SELECT [clienteNumeroPasaporte] FROM [cliente] WHERE ([clienteNumeroPasaporte] = @clienteNumeroPasaporte) AND (Not ( [clienteId] = @clienteId)) ",true, GxErrorMask.GX_NOMASK, false, this,prmT00016,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00017", "SELECT [clienteId] FROM [cliente] WHERE [clienteId] = @clienteId  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00017,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00018", "SELECT TOP 1 [clienteId] FROM [cliente] WHERE ( [clienteId] > @clienteId) ORDER BY [clienteId]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00018,1, GxCacheFrequency.OFF ,true,true )
             ,new CursorDef("T00019", "SELECT TOP 1 [clienteId] FROM [cliente] WHERE ( [clienteId] < @clienteId) ORDER BY [clienteId] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00019,1, GxCacheFrequency.OFF ,true,true )
             ,new CursorDef("T000110", "INSERT INTO [cliente]([clienteDNI], [clienteNombre], [clienteApellido], [clienteNumeroPasaporte], [clienteDireccion], [clienteTelefono], [clienteCorreo], [clienteFecha]) VALUES(@clienteDNI, @clienteNombre, @clienteApellido, @clienteNumeroPasaporte, @clienteDireccion, @clienteTelefono, @clienteCorreo, @clienteFecha); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000110)
             ,new CursorDef("T000111", "UPDATE [cliente] SET [clienteDNI]=@clienteDNI, [clienteNombre]=@clienteNombre, [clienteApellido]=@clienteApellido, [clienteNumeroPasaporte]=@clienteNumeroPasaporte, [clienteDireccion]=@clienteDireccion, [clienteTelefono]=@clienteTelefono, [clienteCorreo]=@clienteCorreo, [clienteFecha]=@clienteFecha  WHERE [clienteId] = @clienteId", GxErrorMask.GX_NOMASK,prmT000111)
             ,new CursorDef("T000112", "DELETE FROM [cliente]  WHERE [clienteId] = @clienteId", GxErrorMask.GX_NOMASK,prmT000112)
             ,new CursorDef("T000113", "SELECT TOP 1 [cuentaBancoId] FROM [cuentaBanco] WHERE [clienteId] = @clienteId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000113,1, GxCacheFrequency.OFF ,true,true )
             ,new CursorDef("T000114", "SELECT [clienteId] FROM [cliente] ORDER BY [clienteId]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000114,100, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000115", "SELECT [clienteDNI] FROM [cliente] WHERE ([clienteDNI] = @clienteDNI) AND (Not ( [clienteId] = @clienteId)) ",true, GxErrorMask.GX_NOMASK, false, this,prmT000115,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000116", "SELECT [clienteNumeroPasaporte] FROM [cliente] WHERE ([clienteNumeroPasaporte] = @clienteNumeroPasaporte) AND (Not ( [clienteId] = @clienteId)) ",true, GxErrorMask.GX_NOMASK, false, this,prmT000116,1, GxCacheFrequency.OFF ,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 20) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(8) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(9) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 20) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(8) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(9) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 20) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(8) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(9) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 4 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 14 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                stmt.SetParameter(5, (String)parms[4]);
                stmt.SetParameter(6, (String)parms[5]);
                stmt.SetParameter(7, (String)parms[6]);
                stmt.SetParameter(8, (DateTime)parms[7]);
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                stmt.SetParameter(5, (String)parms[4]);
                stmt.SetParameter(6, (String)parms[5]);
                stmt.SetParameter(7, (String)parms[6]);
                stmt.SetParameter(8, (DateTime)parms[7]);
                stmt.SetParameter(9, (int)parms[8]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 14 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
