/*
               File: paisciudadConversion
        Description: Conversion for table paisciudad
             Author: GeneXus C# Generator version 16_0_6-136889
       Generated on: 11/25/2019 22:34:39.52
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Reorg;
using System.Threading;
using GeneXus.Programs;
using System.Web.Services;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
namespace GeneXus.Programs {
   public class paisciudadconversion : GXProcedure
   {
      public paisciudadconversion( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public paisciudadconversion( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         initialize();
         executePrivate();
      }

      public void executeSubmit( )
      {
         paisciudadconversion objpaisciudadconversion;
         objpaisciudadconversion = new paisciudadconversion();
         objpaisciudadconversion.context.SetSubmitInitialConfig(context);
         objpaisciudadconversion.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objpaisciudadconversion);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((paisciudadconversion)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor PAISCIUDAD2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A15ciudadId = PAISCIUDAD2_A15ciudadId[0];
            A9paisId = PAISCIUDAD2_A9paisId[0];
            AV5GXV17 = "";
            /* Using cursor PAISCIUDAD3 */
            pr_default.execute(1, new Object[] {A9paisId, A15ciudadId});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A17ciudadNombre = PAISCIUDAD3_A17ciudadNombre[0];
               A7atraccionId = PAISCIUDAD3_A7atraccionId[0];
               AV5GXV17 = A17ciudadNombre;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(1);
            }
            pr_default.close(1);
            /*
               INSERT RECORD ON TABLE GXA0005

            */
            AV2paisId = A9paisId;
            AV3ciudadId = A15ciudadId;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV5GXV17)) )
            {
               AV4ciudadNombre = " ";
            }
            else
            {
               AV4ciudadNombre = AV5GXV17;
            }
            /* Using cursor PAISCIUDAD4 */
            pr_default.execute(2, new Object[] {AV2paisId, AV3ciudadId, AV4ciudadNombre});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("GXA0005") ;
            if ( (pr_default.getStatus(2) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(GXResourceManager.GetMessage("GXM_noupdate"));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            /* End Insert */
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PAISCIUDAD2_A15ciudadId = new int[1] ;
         PAISCIUDAD2_A9paisId = new int[1] ;
         AV5GXV17 = "";
         PAISCIUDAD3_A9paisId = new int[1] ;
         PAISCIUDAD3_A15ciudadId = new int[1] ;
         PAISCIUDAD3_A17ciudadNombre = new String[] {""} ;
         PAISCIUDAD3_A7atraccionId = new int[1] ;
         A17ciudadNombre = "";
         AV4ciudadNombre = "";
         Gx_emsg = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.paisciudadconversion__default(),
            new Object[][] {
                new Object[] {
               PAISCIUDAD2_A15ciudadId, PAISCIUDAD2_A9paisId
               }
               , new Object[] {
               PAISCIUDAD3_A9paisId, PAISCIUDAD3_A15ciudadId, PAISCIUDAD3_A17ciudadNombre, PAISCIUDAD3_A7atraccionId
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A15ciudadId ;
      private int A9paisId ;
      private int A7atraccionId ;
      private int GIGXA0005 ;
      private int AV2paisId ;
      private int AV3ciudadId ;
      private String scmdbuf ;
      private String AV5GXV17 ;
      private String A17ciudadNombre ;
      private String AV4ciudadNombre ;
      private String Gx_emsg ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] PAISCIUDAD2_A15ciudadId ;
      private int[] PAISCIUDAD2_A9paisId ;
      private int[] PAISCIUDAD3_A9paisId ;
      private int[] PAISCIUDAD3_A15ciudadId ;
      private String[] PAISCIUDAD3_A17ciudadNombre ;
      private int[] PAISCIUDAD3_A7atraccionId ;
   }

   public class paisciudadconversion__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmPAISCIUDAD2 ;
          prmPAISCIUDAD2 = new Object[] {
          } ;
          Object[] prmPAISCIUDAD3 ;
          prmPAISCIUDAD3 = new Object[] {
          new Object[] {"@paisId",SqlDbType.Int,6,0} ,
          new Object[] {"@ciudadId",SqlDbType.Int,6,0}
          } ;
          Object[] prmPAISCIUDAD4 ;
          prmPAISCIUDAD4 = new Object[] {
          new Object[] {"@AV2paisId",SqlDbType.Int,6,0} ,
          new Object[] {"@AV3ciudadId",SqlDbType.Int,6,0} ,
          new Object[] {"@AV4ciudadNombre",SqlDbType.NChar,20,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("PAISCIUDAD2", "SELECT [ciudadId], [paisId] FROM [paisciudad] ORDER BY [paisId], [ciudadId] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmPAISCIUDAD2,100, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("PAISCIUDAD3", "SELECT TOP 1 [paisId], [ciudadId], [ciudadNombre], [atraccionId] FROM [atraccion] WHERE [paisId] = @paisId and [ciudadId] = @ciudadId ORDER BY [paisId], [ciudadId] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmPAISCIUDAD3,1, GxCacheFrequency.OFF ,false,true )
             ,new CursorDef("PAISCIUDAD4", "INSERT INTO [GXA0005]([paisId], [ciudadId], [ciudadNombre]) VALUES(@AV2paisId, @AV3ciudadId, @AV4ciudadNombre)", GxErrorMask.GX_NOMASK,prmPAISCIUDAD4)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
       }
    }

 }

}
