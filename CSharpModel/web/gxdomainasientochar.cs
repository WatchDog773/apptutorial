/*
               File: AsientoChar
        Description: AsientoChar
             Author: GeneXus C# Generator version 16_0_6-136889
       Generated on: 12/5/2019 12:9:3.51
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Reorg;
using System.Threading;
using GeneXus.Programs;
using System.Web.Services;
using System.Data;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
namespace GeneXus.Programs {
   public class gxdomainasientochar
   {
      private static Hashtable domain = new Hashtable();
      private static Hashtable domainMap;
      static gxdomainasientochar ()
      {
         domain["A"] = "A";
         domain["B"] = "B";
         domain["C"] = "C";
         domain["D"] = "D";
         domain["E"] = "E";
         domain["F"] = "F";
      }

      public static string getDescription( IGxContext context ,
                                           String key )
      {
         string rtkey ;
         String value ;
         rtkey = ((key==null) ? "" : StringUtil.Trim( (String)(key)));
         value = (String)(domain[rtkey]==null?"":domain[rtkey]);
         return value ;
      }

      public static GxSimpleCollection<String> getValues( )
      {
         GxSimpleCollection<String> value = new GxSimpleCollection<String>();
         ArrayList aKeys = new ArrayList(domain.Keys);
         aKeys.Sort();
         foreach (String key in aKeys)
         {
            value.Add(key);
         }
         return value;
      }

      [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.Synchronized)]
      public static String getValue( string key )
      {
         if(domainMap == null)
         {
            domainMap = new Hashtable();
            domainMap["A"] = "A";
            domainMap["B"] = "B";
            domainMap["C"] = "C";
            domainMap["D"] = "D";
            domainMap["E"] = "E";
            domainMap["F"] = "F";
         }
         return (String)domainMap[key] ;
      }

   }

}
