/*
				   File: type_SdtSDTpais_SDTpaisItem
			Description: SDTpais
				 Author: Nemo 🐠 for C# version 16.0.6.136889
		   Program type: Callable routine
			  Main DBMS: 
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Reflection;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web.Services.Protocols;


namespace GeneXus.Programs
{
	[XmlSerializerFormat]
	[XmlRoot(ElementName="SDTpaisItem")]
	[XmlType(TypeName="SDTpaisItem" , Namespace="TravelAgency" )]
	[Serializable]
	public class SdtSDTpais_SDTpaisItem : GxUserType
	{
		public SdtSDTpais_SDTpaisItem( )
		{
			/* Constructor for serialization */
			gxTv_SdtSDTpais_SDTpaisItem_Nombre = "";

		}

		public SdtSDTpais_SDTpaisItem(IGxContext context)
		{
			this.context = context;
			initialize();
		}

		#region Json
		private static Hashtable mapper;
		public override String JsonMap(String value)
		{
			if (mapper == null)
			{
				mapper = new Hashtable();
			}
			return (String)mapper[value]; ;
		}

		public override void ToJSON()
		{
			ToJSON(true) ;
			return;
		}

		public override void ToJSON(bool includeState)
		{
			AddObjectProperty("Id", gxTpr_Id, false);


			AddObjectProperty("Nombre", gxTpr_Nombre, false);


			AddObjectProperty("cantidadAtracciones", gxTpr_Cantidadatracciones, false);

			return;
		}
		#endregion

		#region Properties

		[SoapElement(ElementName="Id")]
		[XmlElement(ElementName="Id")]
		public int gxTpr_Id
		{
			get { 
				return gxTv_SdtSDTpais_SDTpaisItem_Id; 
			}
			set { 
				gxTv_SdtSDTpais_SDTpaisItem_Id = value;
				SetDirty("Id");
			}
		}




		[SoapElement(ElementName="Nombre")]
		[XmlElement(ElementName="Nombre")]
		public String gxTpr_Nombre
		{
			get { 
				return gxTv_SdtSDTpais_SDTpaisItem_Nombre; 
			}
			set { 
				gxTv_SdtSDTpais_SDTpaisItem_Nombre = value;
				SetDirty("Nombre");
			}
		}




		[SoapElement(ElementName="cantidadAtracciones")]
		[XmlElement(ElementName="cantidadAtracciones")]
		public short gxTpr_Cantidadatracciones
		{
			get { 
				return gxTv_SdtSDTpais_SDTpaisItem_Cantidadatracciones; 
			}
			set { 
				gxTv_SdtSDTpais_SDTpaisItem_Cantidadatracciones = value;
				SetDirty("Cantidadatracciones");
			}
		}




		#endregion

		#region Initialization

		public void initialize( )
		{
			gxTv_SdtSDTpais_SDTpaisItem_Nombre = "";

			return  ;
		}



		#endregion

		#region Declaration

		protected int gxTv_SdtSDTpais_SDTpaisItem_Id;
		 

		protected String gxTv_SdtSDTpais_SDTpaisItem_Nombre;
		 

		protected short gxTv_SdtSDTpais_SDTpaisItem_Cantidadatracciones;
		 


		#endregion
	}
	#region Rest interface
	[DataContract(Name=@"SDTpaisItem", Namespace="TravelAgency")]
	public class SdtSDTpais_SDTpaisItem_RESTInterface : GxGenericCollectionItem<SdtSDTpais_SDTpaisItem>, System.Web.SessionState.IRequiresSessionState
	{
		public SdtSDTpais_SDTpaisItem_RESTInterface( ) : base()
		{
		}

		public SdtSDTpais_SDTpaisItem_RESTInterface( SdtSDTpais_SDTpaisItem psdt ) : base(psdt)
		{
		}

		#region Rest Properties
		[DataMember(Name="Id", Order=0)]
		public int gxTpr_Id
		{
			get { 
				return sdt.gxTpr_Id;

			}
			set { 
				sdt.gxTpr_Id = value;
			}
		}

		[DataMember(Name="Nombre", Order=1)]
		public  String gxTpr_Nombre
		{
			get { 
				return StringUtil.RTrim( sdt.gxTpr_Nombre);

			}
			set { 
				 sdt.gxTpr_Nombre = value;
			}
		}

		[DataMember(Name="cantidadAtracciones", Order=2)]
		public short gxTpr_Cantidadatracciones
		{
			get { 
				return sdt.gxTpr_Cantidadatracciones;

			}
			set { 
				sdt.gxTpr_Cantidadatracciones = value;
			}
		}


		#endregion

		public SdtSDTpais_SDTpaisItem sdt
		{
			get { 
				return (SdtSDTpais_SDTpaisItem)Sdt;
			}
			set { 
				Sdt = value;
			}
		}

		[OnDeserializing]
		void checkSdt( StreamingContext ctx )
		{
			if ( sdt == null )
			{
				sdt = new SdtSDTpais_SDTpaisItem() ;
			}
		}
	}
	#endregion
}