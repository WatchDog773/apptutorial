using System;
using GeneXus.Builder;
using System.IO;
public class bldDevelopermenu : GxBaseBuilder
{
   string cs_path = "." ;
   public bldDevelopermenu( ) : base()
   {
   }

   public override int BeforeCompile( )
   {
      return 0 ;
   }

   public override int AfterCompile( )
   {
      int ErrCode ;
      ErrCode = 0;
      if ( ! File.Exists(@"bin\client.exe.config") || checkTime(@"bin\client.exe.config",cs_path + @"\client.exe.config") )
      {
         File.Copy( cs_path + @"\client.exe.config", @"bin\client.exe.config", true);
      }
      return ErrCode ;
   }

   static public int Main( string[] args )
   {
      bldDevelopermenu x = new bldDevelopermenu() ;
      x.SetMainSourceFile( "bldDevelopermenu.cs");
      x.LoadVariables( args);
      return x.CompileAll( );
   }

   public override ItemCollection GetSortedBuildList( )
   {
      ItemCollection sc = new ItemCollection() ;
      sc.Add( @"bin\GeneXus.Programs.Common.dll", cs_path + @"\genexus.programs.common.rsp");
      return sc ;
   }

   public override TargetCollection GetRuntimeBuildList( )
   {
      TargetCollection sc = new TargetCollection() ;
      sc.Add( @"acategoria_dataprovider", "dll");
      sc.Add( @"alistaatraccion", "dll");
      sc.Add( @"alistaatraccion", "dll");
      sc.Add( @"acategoriaatracciones", "dll");
      sc.Add( @"acategoriaatracciones", "dll");
      sc.Add( @"numerodeatraccionporpais", "dll");
      sc.Add( @"aatraccionpornombre", "dll");
      sc.Add( @"aatraccionpornombre", "dll");
      sc.Add( @"aatraccionreporte", "dll");
      sc.Add( @"aatraccionreporte", "dll");
      sc.Add( @"aimprimirranking", "dll");
      sc.Add( @"aimprimirranking", "dll");
      sc.Add( @"appmasterpage", "dll");
      sc.Add( @"recentlinks", "dll");
      sc.Add( @"promptmasterpage", "dll");
      sc.Add( @"rwdmasterpage", "dll");
      sc.Add( @"rwdrecentlinks", "dll");
      sc.Add( @"rwdpromptmasterpage", "dll");
      sc.Add( @"gx0020", "dll");
      sc.Add( @"gx0030", "dll");
      sc.Add( @"gx0040", "dll");
      sc.Add( @"gx0051", "dll");
      sc.Add( @"home", "dll");
      sc.Add( @"home", "dll");
      sc.Add( @"notauthorized", "dll");
      sc.Add( @"tabbedview", "dll");
      sc.Add( @"viewpais", "dll");
      sc.Add( @"wwpais", "dll");
      sc.Add( @"paisgeneral", "dll");
      sc.Add( @"paisciudadwc", "dll");
      sc.Add( @"viewatraccion", "dll");
      sc.Add( @"wwatraccion", "dll");
      sc.Add( @"atracciongeneral", "dll");
      sc.Add( @"gx0060", "dll");
      sc.Add( @"gx0070", "dll");
      sc.Add( @"gx0080", "dll");
      sc.Add( @"gx0091", "dll");
      sc.Add( @"gx00a0", "dll");
      sc.Add( @"atraccionatraccionwc", "dll");
      sc.Add( @"gx00b1", "dll");
      sc.Add( @"gx00d0", "dll");
      sc.Add( @"gx00e0", "dll");
      sc.Add( @"filtroatracciones", "dll");
      sc.Add( @"categoriasyatracciones", "dll");
      sc.Add( @"masivoinsertarremover", "dll");
      sc.Add( @"wwwattraccionfromscratch", "dll");
      sc.Add( @"wwwattraccionfromscratch_2", "dll");
      sc.Add( @"wpqueryattractions", "dll");
      sc.Add( @"wpattractions", "dll");
      sc.Add( @"cliente", "dll");
      sc.Add( @"atraccion", "dll");
      sc.Add( @"pais", "dll");
      sc.Add( @"categoria", "dll");
      sc.Add( @"vuelo", "dll");
      sc.Add( @"aeropuerto", "dll");
      sc.Add( @"aerolinea", "dll");
      sc.Add( @"proveedor", "dll");
      sc.Add( @"cuentabanco", "dll");
      sc.Add( @"atraccionconotrosparametros", "dll");
      return sc ;
   }

   public override ItemCollection GetResBuildList( )
   {
      ItemCollection sc = new ItemCollection() ;
      sc.Add( @"bin\messages.eng.dll", cs_path + @"\messages.eng.txt");
      return sc ;
   }

   public override bool ToBuild( String obj )
   {
      if (checkTime(obj, cs_path + @"\bin\GxClasses.dll" ))
         return true;
      if ( obj == @"bin\GeneXus.Programs.Common.dll" )
      {
         if (checkTime(obj, cs_path + @"\GxWebStd.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\SoapParm.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GxObjectCollection.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GxFullTextSearchReindexer.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GxModelInfoProvider.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\genexus.programs.sdt.rsp" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_Sdtatraccion.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_Sdtcategoria.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainpage.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainlocation.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainasientochar.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryviewercharttype.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryvieweroutputtype.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryviewerplotseries.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryviewershowdataas.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryviewerorientation.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryviewertrendperiod.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryviewerxaxislabels.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryvieweraggregationtype.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryvieweraxisordertype.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryvieweraxistype.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryviewerconditionoperator.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryviewerexpandcollapse.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryviewerfiltertype.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryviewerobjecttype.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryviewersubtotals.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryviewershowdatalabelsin.cs" ))
            return true;
      }
      if ( obj == @"bin\messages.eng.dll" )
      {
         if (checkTime(obj, cs_path + @"\messages.eng.txt" ))
            return true;
      }
      return false ;
   }

}

