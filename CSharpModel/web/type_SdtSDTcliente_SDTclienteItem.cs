/*
				   File: type_SdtSDTcliente_SDTclienteItem
			Description: SDTcliente
				 Author: Nemo 🐠 for C# version 16.0.6.136889
		   Program type: Callable routine
			  Main DBMS: 
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Reflection;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web.Services.Protocols;


namespace GeneXus.Programs
{
	[XmlSerializerFormat]
	[XmlRoot(ElementName="SDTclienteItem")]
	[XmlType(TypeName="SDTclienteItem" , Namespace="TravelAgency" )]
	[Serializable]
	public class SdtSDTcliente_SDTclienteItem : GxUserType
	{
		public SdtSDTcliente_SDTclienteItem( )
		{
			/* Constructor for serialization */
			gxTv_SdtSDTcliente_SDTclienteItem_Nombre = "";

			gxTv_SdtSDTcliente_SDTclienteItem_Direccion = "";

		}

		public SdtSDTcliente_SDTclienteItem(IGxContext context)
		{
			this.context = context;
			initialize();
		}

		#region Json
		private static Hashtable mapper;
		public override String JsonMap(String value)
		{
			if (mapper == null)
			{
				mapper = new Hashtable();
			}
			return (String)mapper[value]; ;
		}

		public override void ToJSON()
		{
			ToJSON(true) ;
			return;
		}

		public override void ToJSON(bool includeState)
		{
			AddObjectProperty("Id", gxTpr_Id, false);


			AddObjectProperty("Nombre", gxTpr_Nombre, false);


			AddObjectProperty("Direccion", gxTpr_Direccion, false);

			return;
		}
		#endregion

		#region Properties

		[SoapElement(ElementName="Id")]
		[XmlElement(ElementName="Id")]
		public int gxTpr_Id
		{
			get { 
				return gxTv_SdtSDTcliente_SDTclienteItem_Id; 
			}
			set { 
				gxTv_SdtSDTcliente_SDTclienteItem_Id = value;
				SetDirty("Id");
			}
		}




		[SoapElement(ElementName="Nombre")]
		[XmlElement(ElementName="Nombre")]
		public String gxTpr_Nombre
		{
			get { 
				return gxTv_SdtSDTcliente_SDTclienteItem_Nombre; 
			}
			set { 
				gxTv_SdtSDTcliente_SDTclienteItem_Nombre = value;
				SetDirty("Nombre");
			}
		}




		[SoapElement(ElementName="Direccion")]
		[XmlElement(ElementName="Direccion")]
		public String gxTpr_Direccion
		{
			get { 
				return gxTv_SdtSDTcliente_SDTclienteItem_Direccion; 
			}
			set { 
				gxTv_SdtSDTcliente_SDTclienteItem_Direccion = value;
				SetDirty("Direccion");
			}
		}




		#endregion

		#region Initialization

		public void initialize( )
		{
			gxTv_SdtSDTcliente_SDTclienteItem_Nombre = "";
			gxTv_SdtSDTcliente_SDTclienteItem_Direccion = "";
			return  ;
		}



		#endregion

		#region Declaration

		protected int gxTv_SdtSDTcliente_SDTclienteItem_Id;
		 

		protected String gxTv_SdtSDTcliente_SDTclienteItem_Nombre;
		 

		protected String gxTv_SdtSDTcliente_SDTclienteItem_Direccion;
		 


		#endregion
	}
	#region Rest interface
	[DataContract(Name=@"SDTclienteItem", Namespace="TravelAgency")]
	public class SdtSDTcliente_SDTclienteItem_RESTInterface : GxGenericCollectionItem<SdtSDTcliente_SDTclienteItem>, System.Web.SessionState.IRequiresSessionState
	{
		public SdtSDTcliente_SDTclienteItem_RESTInterface( ) : base()
		{
		}

		public SdtSDTcliente_SDTclienteItem_RESTInterface( SdtSDTcliente_SDTclienteItem psdt ) : base(psdt)
		{
		}

		#region Rest Properties
		[DataMember(Name="Id", Order=0)]
		public int gxTpr_Id
		{
			get { 
				return sdt.gxTpr_Id;

			}
			set { 
				sdt.gxTpr_Id = value;
			}
		}

		[DataMember(Name="Nombre", Order=1)]
		public  String gxTpr_Nombre
		{
			get { 
				return StringUtil.RTrim( sdt.gxTpr_Nombre);

			}
			set { 
				 sdt.gxTpr_Nombre = value;
			}
		}

		[DataMember(Name="Direccion", Order=2)]
		public  String gxTpr_Direccion
		{
			get { 
				return sdt.gxTpr_Direccion;

			}
			set { 
				 sdt.gxTpr_Direccion = value;
			}
		}


		#endregion

		public SdtSDTcliente_SDTclienteItem sdt
		{
			get { 
				return (SdtSDTcliente_SDTclienteItem)Sdt;
			}
			set { 
				Sdt = value;
			}
		}

		[OnDeserializing]
		void checkSdt( StreamingContext ctx )
		{
			if ( sdt == null )
			{
				sdt = new SdtSDTcliente_SDTclienteItem() ;
			}
		}
	}
	#endregion
}