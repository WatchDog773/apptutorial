/*
               File: atraccionesDP
        Description: atracciones DP
             Author: GeneXus C# Generator version 16_0_6-136889
       Generated on: 12/3/2019 23:59:54.29
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class atraccionesdp : GXProcedure
   {
      public atraccionesdp( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public atraccionesdp( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( out GXBCCollection<Sdtatraccion> aP0_Gxm2rootcol )
      {
         this.Gxm2rootcol = new GXBCCollection<Sdtatraccion>( context, "atraccion", "TravelAgency") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      public GXBCCollection<Sdtatraccion> executeUdp( )
      {
         this.Gxm2rootcol = new GXBCCollection<Sdtatraccion>( context, "atraccion", "TravelAgency") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( out GXBCCollection<Sdtatraccion> aP0_Gxm2rootcol )
      {
         atraccionesdp objatraccionesdp;
         objatraccionesdp = new atraccionesdp();
         objatraccionesdp.Gxm2rootcol = new GXBCCollection<Sdtatraccion>( context, "atraccion", "TravelAgency") ;
         objatraccionesdp.context.SetSubmitInitialConfig(context);
         objatraccionesdp.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objatraccionesdp);
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((atraccionesdp)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00043 */
         pr_default.execute(0);
         if ( (pr_default.getStatus(0) != 101) )
         {
            A40000paisId = P00043_A40000paisId[0];
            n40000paisId = P00043_n40000paisId[0];
         }
         else
         {
            A40000paisId = 0;
            n40000paisId = false;
         }
         pr_default.close(0);
         /* Using cursor P00045 */
         pr_default.execute(1);
         if ( (pr_default.getStatus(1) != 101) )
         {
            A40001categoriaId = P00045_A40001categoriaId[0];
            n40001categoriaId = P00045_n40001categoriaId[0];
         }
         else
         {
            A40001categoriaId = 0;
            n40001categoriaId = false;
         }
         pr_default.close(1);
         /* Using cursor P00047 */
         pr_default.execute(2);
         if ( (pr_default.getStatus(2) != 101) )
         {
            A40002ciudadId = P00047_A40002ciudadId[0];
            n40002ciudadId = P00047_n40002ciudadId[0];
         }
         else
         {
            A40002ciudadId = 0;
            n40002ciudadId = false;
         }
         pr_default.close(2);
         /* Using cursor P00049 */
         pr_default.execute(3);
         if ( (pr_default.getStatus(3) != 101) )
         {
            A40003paisId = P00049_A40003paisId[0];
            n40003paisId = P00049_n40003paisId[0];
         }
         else
         {
            A40003paisId = 0;
            n40003paisId = false;
         }
         pr_default.close(3);
         /* Using cursor P000411 */
         pr_default.execute(4);
         if ( (pr_default.getStatus(4) != 101) )
         {
            A40004categoriaId = P000411_A40004categoriaId[0];
            n40004categoriaId = P000411_n40004categoriaId[0];
         }
         else
         {
            A40004categoriaId = 0;
            n40004categoriaId = false;
         }
         pr_default.close(4);
         /* Using cursor P000413 */
         pr_default.execute(5);
         if ( (pr_default.getStatus(5) != 101) )
         {
            A40005ciudadId = P000413_A40005ciudadId[0];
            n40005ciudadId = P000413_n40005ciudadId[0];
         }
         else
         {
            A40005ciudadId = 0;
            n40005ciudadId = false;
         }
         pr_default.close(5);
         /* Using cursor P000415 */
         pr_default.execute(6);
         if ( (pr_default.getStatus(6) != 101) )
         {
            A40006categoriaId = P000415_A40006categoriaId[0];
            n40006categoriaId = P000415_n40006categoriaId[0];
         }
         else
         {
            A40006categoriaId = 0;
            n40006categoriaId = false;
         }
         pr_default.close(6);
         /* Using cursor P000417 */
         pr_default.execute(7);
         if ( (pr_default.getStatus(7) != 101) )
         {
            A40007paisId = P000417_A40007paisId[0];
            n40007paisId = P000417_n40007paisId[0];
         }
         else
         {
            A40007paisId = 0;
            n40007paisId = false;
         }
         pr_default.close(7);
         /* Using cursor P000419 */
         pr_default.execute(8);
         if ( (pr_default.getStatus(8) != 101) )
         {
            A40008ciudadId = P000419_A40008ciudadId[0];
            n40008ciudadId = P000419_n40008ciudadId[0];
         }
         else
         {
            A40008ciudadId = 0;
            n40008ciudadId = false;
         }
         pr_default.close(8);
         Gxm1atraccion = new Sdtatraccion(context);
         Gxm2rootcol.Add(Gxm1atraccion, 0);
         Gxm1atraccion.gxTpr_Atraccionnombre = "Museo Louvre";
         Gxm1atraccion.gxTpr_Paisid = A40000paisId;
         Gxm1atraccion.gxTpr_Categoriaid = A40001categoriaId;
         Gxm1atraccion.gxTpr_Ciudadid = A40002ciudadId;
         Gxm1atraccion.gxTpr_Atraccionfoto = context.convertURL( (String)(context.GetImagePath( "216b8d27-dcc2-4f49-b3eb-5b5be44cf7dd", "", context.GetTheme( ))));
         Gxm1atraccion = new Sdtatraccion(context);
         Gxm2rootcol.Add(Gxm1atraccion, 0);
         Gxm1atraccion.gxTpr_Atraccionnombre = "La gran muralla";
         Gxm1atraccion.gxTpr_Paisid = A40003paisId;
         Gxm1atraccion.gxTpr_Categoriaid = A40004categoriaId;
         Gxm1atraccion.gxTpr_Ciudadid = A40005ciudadId;
         Gxm1atraccion.gxTpr_Atraccionfoto = context.convertURL( (String)(context.GetImagePath( "216b8d27-dcc2-4f49-b3eb-5b5be44cf7dd", "", context.GetTheme( ))));
         Gxm1atraccion = new Sdtatraccion(context);
         Gxm2rootcol.Add(Gxm1atraccion, 0);
         Gxm1atraccion.gxTpr_Atraccionnombre = "Torre Eiffel";
         Gxm1atraccion.gxTpr_Paisid = A40000paisId;
         Gxm1atraccion.gxTpr_Categoriaid = A40006categoriaId;
         Gxm1atraccion.gxTpr_Ciudadid = A40002ciudadId;
         Gxm1atraccion.gxTpr_Atraccionfoto = context.convertURL( (String)(context.GetImagePath( "216b8d27-dcc2-4f49-b3eb-5b5be44cf7dd", "", context.GetTheme( ))));
         Gxm1atraccion = new Sdtatraccion(context);
         Gxm2rootcol.Add(Gxm1atraccion, 0);
         Gxm1atraccion.gxTpr_Atraccionnombre = "Cristo del Picacho";
         Gxm1atraccion.gxTpr_Paisid = A40007paisId;
         Gxm1atraccion.gxTpr_Categoriaid = A40006categoriaId;
         Gxm1atraccion.gxTpr_Ciudadid = A40008ciudadId;
         Gxm1atraccion.gxTpr_Atraccionfoto = context.convertURL( (String)(context.GetImagePath( "216b8d27-dcc2-4f49-b3eb-5b5be44cf7dd", "", context.GetTheme( ))));
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00043_A40000paisId = new int[1] ;
         P00043_n40000paisId = new bool[] {false} ;
         P00045_A40001categoriaId = new int[1] ;
         P00045_n40001categoriaId = new bool[] {false} ;
         P00047_A40002ciudadId = new int[1] ;
         P00047_n40002ciudadId = new bool[] {false} ;
         P00049_A40003paisId = new int[1] ;
         P00049_n40003paisId = new bool[] {false} ;
         P000411_A40004categoriaId = new int[1] ;
         P000411_n40004categoriaId = new bool[] {false} ;
         P000413_A40005ciudadId = new int[1] ;
         P000413_n40005ciudadId = new bool[] {false} ;
         P000415_A40006categoriaId = new int[1] ;
         P000415_n40006categoriaId = new bool[] {false} ;
         P000417_A40007paisId = new int[1] ;
         P000417_n40007paisId = new bool[] {false} ;
         P000419_A40008ciudadId = new int[1] ;
         P000419_n40008ciudadId = new bool[] {false} ;
         Gxm1atraccion = new Sdtatraccion(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.atraccionesdp__default(),
            new Object[][] {
                new Object[] {
               P00043_A40000paisId, P00043_n40000paisId
               }
               , new Object[] {
               P00045_A40001categoriaId, P00045_n40001categoriaId
               }
               , new Object[] {
               P00047_A40002ciudadId, P00047_n40002ciudadId
               }
               , new Object[] {
               P00049_A40003paisId, P00049_n40003paisId
               }
               , new Object[] {
               P000411_A40004categoriaId, P000411_n40004categoriaId
               }
               , new Object[] {
               P000413_A40005ciudadId, P000413_n40005ciudadId
               }
               , new Object[] {
               P000415_A40006categoriaId, P000415_n40006categoriaId
               }
               , new Object[] {
               P000417_A40007paisId, P000417_n40007paisId
               }
               , new Object[] {
               P000419_A40008ciudadId, P000419_n40008ciudadId
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A40000paisId ;
      private int A40001categoriaId ;
      private int A40002ciudadId ;
      private int A40003paisId ;
      private int A40004categoriaId ;
      private int A40005ciudadId ;
      private int A40006categoriaId ;
      private int A40007paisId ;
      private int A40008ciudadId ;
      private String scmdbuf ;
      private bool n40000paisId ;
      private bool n40001categoriaId ;
      private bool n40002ciudadId ;
      private bool n40003paisId ;
      private bool n40004categoriaId ;
      private bool n40005ciudadId ;
      private bool n40006categoriaId ;
      private bool n40007paisId ;
      private bool n40008ciudadId ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00043_A40000paisId ;
      private bool[] P00043_n40000paisId ;
      private int[] P00045_A40001categoriaId ;
      private bool[] P00045_n40001categoriaId ;
      private int[] P00047_A40002ciudadId ;
      private bool[] P00047_n40002ciudadId ;
      private int[] P00049_A40003paisId ;
      private bool[] P00049_n40003paisId ;
      private int[] P000411_A40004categoriaId ;
      private bool[] P000411_n40004categoriaId ;
      private int[] P000413_A40005ciudadId ;
      private bool[] P000413_n40005ciudadId ;
      private int[] P000415_A40006categoriaId ;
      private bool[] P000415_n40006categoriaId ;
      private int[] P000417_A40007paisId ;
      private bool[] P000417_n40007paisId ;
      private int[] P000419_A40008ciudadId ;
      private bool[] P000419_n40008ciudadId ;
      private GXBCCollection<Sdtatraccion> aP0_Gxm2rootcol ;
      private GXBCCollection<Sdtatraccion> Gxm2rootcol ;
      private Sdtatraccion Gxm1atraccion ;
   }

   public class atraccionesdp__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00043 ;
          prmP00043 = new Object[] {
          } ;
          Object[] prmP00045 ;
          prmP00045 = new Object[] {
          } ;
          Object[] prmP00047 ;
          prmP00047 = new Object[] {
          } ;
          Object[] prmP00049 ;
          prmP00049 = new Object[] {
          } ;
          Object[] prmP000411 ;
          prmP000411 = new Object[] {
          } ;
          Object[] prmP000413 ;
          prmP000413 = new Object[] {
          } ;
          Object[] prmP000415 ;
          prmP000415 = new Object[] {
          } ;
          Object[] prmP000417 ;
          prmP000417 = new Object[] {
          } ;
          Object[] prmP000419 ;
          prmP000419 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("P00043", "SELECT COALESCE( T1.[paisId], 0) AS paisId FROM (SELECT MIN([paisId]) AS paisId FROM [pais] WHERE [paisNombre] = 'Francia' ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00043,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("P00045", "SELECT COALESCE( T1.[categoriaId], 0) AS categoriaId FROM (SELECT MIN([categoriaId]) AS categoriaId FROM [categoria] WHERE [categoriaNombre] = 'Museo' ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00045,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("P00047", "SELECT COALESCE( T1.[ciudadId], 0) AS ciudadId FROM (SELECT MIN([ciudadId]) AS ciudadId FROM [paisciudad] WHERE [ciudadNombre] = 'Paris' ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00047,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("P00049", "SELECT COALESCE( T1.[paisId], 0) AS paisId FROM (SELECT MIN([paisId]) AS paisId FROM [pais] WHERE [paisNombre] = 'China' ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00049,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("P000411", "SELECT COALESCE( T1.[categoriaId], 0) AS categoriaId FROM (SELECT MIN([categoriaId]) AS categoriaId FROM [categoria] WHERE [categoriaNombre] = 'Sitio Turistico' ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000411,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("P000413", "SELECT COALESCE( T1.[ciudadId], 0) AS ciudadId FROM (SELECT MIN([ciudadId]) AS ciudadId FROM [paisciudad] WHERE [ciudadNombre] = 'Beigin' ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000413,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("P000415", "SELECT COALESCE( T1.[categoriaId], 0) AS categoriaId FROM (SELECT MIN([categoriaId]) AS categoriaId FROM [categoria] WHERE [categoriaNombre] = 'Monumento' ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000415,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("P000417", "SELECT COALESCE( T1.[paisId], 0) AS paisId FROM (SELECT MIN([paisId]) AS paisId FROM [pais] WHERE [paisNombre] = 'Brasil' ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000417,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("P000419", "SELECT COALESCE( T1.[ciudadId], 0) AS ciudadId FROM (SELECT MIN([ciudadId]) AS ciudadId FROM [paisciudad] WHERE [ciudadNombre] = 'Rio de Janeiro' ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000419,1, GxCacheFrequency.OFF ,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
