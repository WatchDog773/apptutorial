/*
				   File: type_SdtSDTpaises_SDTpaisesItem
			Description: SDTpaises
				 Author: Nemo 🐠 for C# version 16.0.6.136889
		   Program type: Callable routine
			  Main DBMS: 
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Reflection;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web.Services.Protocols;


namespace GeneXus.Programs
{
	[XmlSerializerFormat]
	[XmlRoot(ElementName="SDTpaisesItem")]
	[XmlType(TypeName="SDTpaisesItem" , Namespace="TravelAgency" )]
	[Serializable]
	public class SdtSDTpaises_SDTpaisesItem : GxUserType
	{
		public SdtSDTpaises_SDTpaisesItem( )
		{
			/* Constructor for serialization */
			gxTv_SdtSDTpaises_SDTpaisesItem_Nombre = "";

		}

		public SdtSDTpaises_SDTpaisesItem(IGxContext context)
		{
			this.context = context;
			initialize();
		}

		#region Json
		private static Hashtable mapper;
		public override String JsonMap(String value)
		{
			if (mapper == null)
			{
				mapper = new Hashtable();
			}
			return (String)mapper[value]; ;
		}

		public override void ToJSON()
		{
			ToJSON(true) ;
			return;
		}

		public override void ToJSON(bool includeState)
		{
			AddObjectProperty("Id", gxTpr_Id, false);


			AddObjectProperty("Nombre", gxTpr_Nombre, false);


			AddObjectProperty("cantidadAtracciones", gxTpr_Cantidadatracciones, false);

			return;
		}
		#endregion

		#region Properties

		[SoapElement(ElementName="Id")]
		[XmlElement(ElementName="Id")]
		public int gxTpr_Id
		{
			get { 
				return gxTv_SdtSDTpaises_SDTpaisesItem_Id; 
			}
			set { 
				gxTv_SdtSDTpaises_SDTpaisesItem_Id = value;
				SetDirty("Id");
			}
		}




		[SoapElement(ElementName="Nombre")]
		[XmlElement(ElementName="Nombre")]
		public String gxTpr_Nombre
		{
			get { 
				return gxTv_SdtSDTpaises_SDTpaisesItem_Nombre; 
			}
			set { 
				gxTv_SdtSDTpaises_SDTpaisesItem_Nombre = value;
				SetDirty("Nombre");
			}
		}




		[SoapElement(ElementName="cantidadAtracciones")]
		[XmlElement(ElementName="cantidadAtracciones")]
		public short gxTpr_Cantidadatracciones
		{
			get { 
				return gxTv_SdtSDTpaises_SDTpaisesItem_Cantidadatracciones; 
			}
			set { 
				gxTv_SdtSDTpaises_SDTpaisesItem_Cantidadatracciones = value;
				SetDirty("Cantidadatracciones");
			}
		}




		#endregion

		#region Initialization

		public void initialize( )
		{
			gxTv_SdtSDTpaises_SDTpaisesItem_Nombre = "";

			return  ;
		}



		#endregion

		#region Declaration

		protected int gxTv_SdtSDTpaises_SDTpaisesItem_Id;
		 

		protected String gxTv_SdtSDTpaises_SDTpaisesItem_Nombre;
		 

		protected short gxTv_SdtSDTpaises_SDTpaisesItem_Cantidadatracciones;
		 


		#endregion
	}
	#region Rest interface
	[DataContract(Name=@"SDTpaisesItem", Namespace="TravelAgency")]
	public class SdtSDTpaises_SDTpaisesItem_RESTInterface : GxGenericCollectionItem<SdtSDTpaises_SDTpaisesItem>, System.Web.SessionState.IRequiresSessionState
	{
		public SdtSDTpaises_SDTpaisesItem_RESTInterface( ) : base()
		{
		}

		public SdtSDTpaises_SDTpaisesItem_RESTInterface( SdtSDTpaises_SDTpaisesItem psdt ) : base(psdt)
		{
		}

		#region Rest Properties
		[DataMember(Name="Id", Order=0)]
		public int gxTpr_Id
		{
			get { 
				return sdt.gxTpr_Id;

			}
			set { 
				sdt.gxTpr_Id = value;
			}
		}

		[DataMember(Name="Nombre", Order=1)]
		public  String gxTpr_Nombre
		{
			get { 
				return StringUtil.RTrim( sdt.gxTpr_Nombre);

			}
			set { 
				 sdt.gxTpr_Nombre = value;
			}
		}

		[DataMember(Name="cantidadAtracciones", Order=2)]
		public short gxTpr_Cantidadatracciones
		{
			get { 
				return sdt.gxTpr_Cantidadatracciones;

			}
			set { 
				sdt.gxTpr_Cantidadatracciones = value;
			}
		}


		#endregion

		public SdtSDTpaises_SDTpaisesItem sdt
		{
			get { 
				return (SdtSDTpaises_SDTpaisesItem)Sdt;
			}
			set { 
				Sdt = value;
			}
		}

		[OnDeserializing]
		void checkSdt( StreamingContext ctx )
		{
			if ( sdt == null )
			{
				sdt = new SdtSDTpaises_SDTpaisesItem() ;
			}
		}
	}
	#endregion
}