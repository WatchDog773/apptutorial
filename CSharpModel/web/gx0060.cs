/*
               File: Gx0060
        Description: Selection List vuelo
             Author: GeneXus C# Generator version 16_0_6-136889
       Generated on: 12/5/2019 12:9:40.10
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gx0060 : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public gx0060( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public gx0060( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( out int aP0_pvueloId )
      {
         this.AV9pvueloId = 0 ;
         executePrivate();
         aP0_pvueloId=this.AV9pvueloId;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid1") == 0 )
            {
               nRC_GXsfl_84 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_84_idx = (int)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_84_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid1_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid1") == 0 )
            {
               subGrid1_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV6cvueloId = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV7cvueloAeropuertoSalidaId = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV8cvueloAeropuertoLlegadaId = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13cvueloPrecio = NumberUtil.Val( GetNextPar( ), ".");
               AV14cvueloDescuentoPorcentage = (short)(NumberUtil.Val( GetNextPar( ), "."));
               AV15caerolineaId = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV16cvueloFecha = context.localUtil.ParseDateParm( GetNextPar( ));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid1_refresh( subGrid1_Rows, AV6cvueloId, AV7cvueloAeropuertoSalidaId, AV8cvueloAeropuertoLlegadaId, AV13cvueloPrecio, AV14cvueloDescuentoPorcentage, AV15caerolineaId, AV16cvueloFecha) ;
               GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               AddString( context.getJSONResponse( )) ;
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV9pvueloId = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               AssignAttri("", false, "AV9pvueloId", StringUtil.LTrimStr( (decimal)(AV9pvueloId), 6, 0));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdpromptmasterpage", "GeneXus.Programs.rwdpromptmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,true);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "no-store");
               }
               if ( ! context.WillRedirect( ) )
               {
                  AddString( context.getJSONResponse( )) ;
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA0M2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START0M2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 136889), false, true);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 136889), false, true);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 136889), false, true);
         context.AddJavascriptSource("gxcfg.js", "?20191251294018", false, true);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 136889), false, true);
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 136889), false, true);
         context.AddJavascriptSource("calendar-en.js", "?"+context.GetBuildNumber( 136889), false, true);
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + "-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("gx0060.aspx") + "?" + UrlEncode("" +AV9pvueloId)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" title=\"submit\" style=\"display:none\" disabled>") ;
         AssignProp("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vCVUELOID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6cvueloId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCVUELOAEROPUERTOSALIDAID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7cvueloAeropuertoSalidaId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCVUELOAEROPUERTOLLEGADAID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8cvueloAeropuertoLlegadaId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCVUELOPRECIO", StringUtil.LTrim( StringUtil.NToC( AV13cvueloPrecio, 9, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCVUELODESCUENTOPORCENTAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14cvueloDescuentoPorcentage), 3, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCAEROLINEAID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15caerolineaId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCVUELOFECHA", context.localUtil.Format(AV16cvueloFecha, "99/99/99"));
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_84", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_84), 8, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vPVUELOID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9pvueloId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "ADVANCEDCONTAINER_Class", StringUtil.RTrim( divAdvancedcontainer_Class));
         GxWebStd.gx_hidden_field( context, "BTNTOGGLE_Class", StringUtil.RTrim( bttBtntoggle_Class));
         GxWebStd.gx_hidden_field( context, "VUELOIDFILTERCONTAINER_Class", StringUtil.RTrim( divVueloidfiltercontainer_Class));
         GxWebStd.gx_hidden_field( context, "VUELOAEROPUERTOSALIDAIDFILTERCONTAINER_Class", StringUtil.RTrim( divVueloaeropuertosalidaidfiltercontainer_Class));
         GxWebStd.gx_hidden_field( context, "VUELOAEROPUERTOLLEGADAIDFILTERCONTAINER_Class", StringUtil.RTrim( divVueloaeropuertollegadaidfiltercontainer_Class));
         GxWebStd.gx_hidden_field( context, "VUELOPRECIOFILTERCONTAINER_Class", StringUtil.RTrim( divVuelopreciofiltercontainer_Class));
         GxWebStd.gx_hidden_field( context, "VUELODESCUENTOPORCENTAGEFILTERCONTAINER_Class", StringUtil.RTrim( divVuelodescuentoporcentagefiltercontainer_Class));
         GxWebStd.gx_hidden_field( context, "AEROLINEAIDFILTERCONTAINER_Class", StringUtil.RTrim( divAerolineaidfiltercontainer_Class));
         GxWebStd.gx_hidden_field( context, "VUELOFECHAFILTERCONTAINER_Class", StringUtil.RTrim( divVuelofechafiltercontainer_Class));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", "notset");
         SendAjaxEncryptionKey();
         SendSecurityToken((String)(sPrefix));
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE0M2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT0M2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("gx0060.aspx") + "?" + UrlEncode("" +AV9pvueloId) ;
      }

      public override String GetPgmname( )
      {
         return "Gx0060" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selection List vuelo" ;
      }

      protected void WB0M0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMain_Internalname, 1, 0, "px", 0, "px", "ContainerFluid PromptContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-3 PromptAdvancedBarCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divAdvancedcontainer_Internalname, 1, 0, "px", 0, "px", divAdvancedcontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divVueloidfiltercontainer_Internalname, 1, 0, "px", 0, "px", divVueloidfiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblvueloidfilter_Internalname, "vuelo Id", "", "", lblLblvueloidfilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e110m1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx0060.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCvueloid_Internalname, "vuelo Id", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCvueloid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6cvueloId), 6, 0, ".", "")), ((edtavCvueloid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV6cvueloId), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV6cvueloId), "ZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,16);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCvueloid_Jsonclick, 0, "Attribute", "", "", "", "", edtavCvueloid_Visible, edtavCvueloid_Enabled, 0, "number", "1", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "", "HLP_Gx0060.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divVueloaeropuertosalidaidfiltercontainer_Internalname, 1, 0, "px", 0, "px", divVueloaeropuertosalidaidfiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblvueloaeropuertosalidaidfilter_Internalname, "vuelo Aeropuerto Salida Id", "", "", lblLblvueloaeropuertosalidaidfilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e120m1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx0060.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCvueloaeropuertosalidaid_Internalname, "vuelo Aeropuerto Salida Id", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCvueloaeropuertosalidaid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7cvueloAeropuertoSalidaId), 6, 0, ".", "")), ((edtavCvueloaeropuertosalidaid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV7cvueloAeropuertoSalidaId), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV7cvueloAeropuertoSalidaId), "ZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,26);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCvueloaeropuertosalidaid_Jsonclick, 0, "Attribute", "", "", "", "", edtavCvueloaeropuertosalidaid_Visible, edtavCvueloaeropuertosalidaid_Enabled, 0, "number", "1", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "", "HLP_Gx0060.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divVueloaeropuertollegadaidfiltercontainer_Internalname, 1, 0, "px", 0, "px", divVueloaeropuertollegadaidfiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblvueloaeropuertollegadaidfilter_Internalname, "vuelo Aeropuerto Llegada Id", "", "", lblLblvueloaeropuertollegadaidfilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e130m1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx0060.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCvueloaeropuertollegadaid_Internalname, "vuelo Aeropuerto Llegada Id", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCvueloaeropuertollegadaid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8cvueloAeropuertoLlegadaId), 6, 0, ".", "")), ((edtavCvueloaeropuertollegadaid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV8cvueloAeropuertoLlegadaId), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV8cvueloAeropuertoLlegadaId), "ZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCvueloaeropuertollegadaid_Jsonclick, 0, "Attribute", "", "", "", "", edtavCvueloaeropuertollegadaid_Visible, edtavCvueloaeropuertollegadaid_Enabled, 0, "number", "1", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "", "HLP_Gx0060.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divVuelopreciofiltercontainer_Internalname, 1, 0, "px", 0, "px", divVuelopreciofiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblvuelopreciofilter_Internalname, "vuelo Precio", "", "", lblLblvuelopreciofilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e140m1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx0060.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCvueloprecio_Internalname, "vuelo Precio", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCvueloprecio_Internalname, StringUtil.LTrim( StringUtil.NToC( AV13cvueloPrecio, 9, 2, ".", "")), ((edtavCvueloprecio_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV13cvueloPrecio, "ZZZZZ9.99")) : context.localUtil.Format( AV13cvueloPrecio, "ZZZZZ9.99")), TempTags+" onchange=\""+"gx.num.valid_decimal( this, ',','.','2');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_decimal( this, ',','.','2');"+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCvueloprecio_Jsonclick, 0, "Attribute", "", "", "", "", edtavCvueloprecio_Visible, edtavCvueloprecio_Enabled, 0, "text", "", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "", "HLP_Gx0060.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divVuelodescuentoporcentagefiltercontainer_Internalname, 1, 0, "px", 0, "px", divVuelodescuentoporcentagefiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblvuelodescuentoporcentagefilter_Internalname, "vuelo Descuento Porcentage", "", "", lblLblvuelodescuentoporcentagefilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e150m1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx0060.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCvuelodescuentoporcentage_Internalname, "vuelo Descuento Porcentage", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCvuelodescuentoporcentage_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14cvueloDescuentoPorcentage), 3, 0, ".", "")), ((edtavCvuelodescuentoporcentage_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV14cvueloDescuentoPorcentage), "ZZ9")) : context.localUtil.Format( (decimal)(AV14cvueloDescuentoPorcentage), "ZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,56);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCvuelodescuentoporcentage_Jsonclick, 0, "Attribute", "", "", "", "", edtavCvuelodescuentoporcentage_Visible, edtavCvuelodescuentoporcentage_Enabled, 0, "number", "1", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "", "HLP_Gx0060.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divAerolineaidfiltercontainer_Internalname, 1, 0, "px", 0, "px", divAerolineaidfiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblaerolineaidfilter_Internalname, "Aerolinea Id", "", "", lblLblaerolineaidfilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e160m1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx0060.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCaerolineaid_Internalname, "aerolinea Id", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCaerolineaid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15caerolineaId), 6, 0, ".", "")), ((edtavCaerolineaid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV15caerolineaId), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV15caerolineaId), "ZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,66);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCaerolineaid_Jsonclick, 0, "Attribute", "", "", "", "", edtavCaerolineaid_Visible, edtavCaerolineaid_Enabled, 0, "number", "1", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "", "HLP_Gx0060.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divVuelofechafiltercontainer_Internalname, 1, 0, "px", 0, "px", divVuelofechafiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblvuelofechafilter_Internalname, "vuelo Fecha", "", "", lblLblvuelofechafilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e170m1_client"+"'", "", "WWAdvancedLabel WWDateFilterLabel", 7, "", 1, 1, 1, "HLP_Gx0060.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCvuelofecha_Internalname, "vuelo Fecha", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_84_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavCvuelofecha_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavCvuelofecha_Internalname, context.localUtil.Format(AV16cvueloFecha, "99/99/99"), context.localUtil.Format( AV16cvueloFecha, "99/99/99"), TempTags+" onchange=\""+"gx.date.valid_date(this, 8,'MDY',0,12,'eng',false,0);"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'MDY',0,12,'eng',false,0);"+";gx.evt.onblur(this,76);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCvuelofecha_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavCvuelofecha_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "", "HLP_Gx0060.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 WWGridCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divGridtable_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 hidden-sm hidden-md hidden-lg ToggleCell", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'',0)\"";
            ClassString = bttBtntoggle_Class;
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtntoggle_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(84), 2, 0)+","+"null"+");", "|||", bttBtntoggle_Jsonclick, 7, "|||", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"e180m1_client"+"'", TempTags, "", 2, "HLP_Gx0060.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /*  Grid Control  */
            Grid1Container.SetWrapped(nGXWrapped);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"DivS\" data-gxgridid=\"84\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid1_Internalname, subGrid1_Internalname, "", "PromptGrid", 0, "", "", 1, 2, sStyleString, "", "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid1_Backcolorstyle == 0 )
               {
                  subGrid1_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid1_Class) > 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Title";
                  }
               }
               else
               {
                  subGrid1_Titlebackstyle = 1;
                  if ( subGrid1_Backcolorstyle == 1 )
                  {
                     subGrid1_Titlebackcolor = subGrid1_Allbackcolor;
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+"SelectionAttribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Id") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Aeropuerto Salida Id") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Aeropuerto Llegada Id") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Precio") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Descuento Porcentage") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "aerolinea Id") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"DescriptionAttribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Fecha") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               Grid1Container.AddObjectProperty("GridName", "Grid1");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  Grid1Container = new GXWebGrid( context);
               }
               else
               {
                  Grid1Container.Clear();
               }
               Grid1Container.SetWrapped(nGXWrapped);
               Grid1Container.AddObjectProperty("GridName", "Grid1");
               Grid1Container.AddObjectProperty("Header", subGrid1_Header);
               Grid1Container.AddObjectProperty("Class", "PromptGrid");
               Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("CmpContext", "");
               Grid1Container.AddObjectProperty("InMasterPage", "false");
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", context.convertURL( AV5LinkSelection));
               Grid1Column.AddObjectProperty("Link", StringUtil.RTrim( edtavLinkselection_Link));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A18vueloId), 6, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A24vueloAeropuertoSalidaId), 6, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A26vueloAeropuertoLlegadaId), 6, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A38vueloPrecio, 9, 2, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39vueloDescuentoPorcentage), 3, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A41aerolineaId), 6, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", context.localUtil.Format(A58vueloFecha, "99/99/99"));
               Grid1Column.AddObjectProperty("Link", StringUtil.RTrim( edtvueloFecha_Link));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Container.AddObjectProperty("Selectedindex", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Selectedindex), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowselection), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Selectioncolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowhovering), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Hoveringcolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowcollapsing), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 84 )
         {
            wbEnd = 0;
            nRC_GXsfl_84 = (int)(nGXsfl_84_idx-1);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               Grid1Container.AddObjectProperty("GRID1_nEOF", GRID1_nEOF);
               Grid1Container.AddObjectProperty("GRID1_nFirstRecordOnPage", GRID1_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid1", Grid1Container);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData", Grid1Container.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData"+"V", Grid1Container.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V"+"\" value='"+Grid1Container.GridValuesHidden()+"'/>") ;
               }
            }
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(84), 2, 0)+","+"null"+");", "Cancel", bttBtn_cancel_Jsonclick, 1, "Cancel", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Gx0060.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
         }
         if ( wbEnd == 84 )
         {
            wbEnd = 0;
            if ( isFullAjaxMode( ) )
            {
               if ( Grid1Container.GetWrapped() == 1 )
               {
                  context.WriteHtmlText( "</table>") ;
                  context.WriteHtmlText( "</div>") ;
               }
               else
               {
                  Grid1Container.AddObjectProperty("GRID1_nEOF", GRID1_nEOF);
                  Grid1Container.AddObjectProperty("GRID1_nFirstRecordOnPage", GRID1_nFirstRecordOnPage);
                  sStyleString = "";
                  context.WriteHtmlText( "<div id=\""+"Grid1Container"+"Div\" "+sStyleString+">"+"</div>") ;
                  context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid1", Grid1Container);
                  if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
                  {
                     GxWebStd.gx_hidden_field( context, "Grid1ContainerData", Grid1Container.ToJavascriptSource());
                  }
                  if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
                  {
                     GxWebStd.gx_hidden_field( context, "Grid1ContainerData"+"V", Grid1Container.GridValuesHidden());
                  }
                  else
                  {
                     context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V"+"\" value='"+Grid1Container.GridValuesHidden()+"'/>") ;
                  }
               }
            }
         }
         wbLoad = true;
      }

      protected void START0M2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 16_0_6-136889", 0) ;
            Form.Meta.addItem("description", "Selection List vuelo", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP0M0( ) ;
      }

      protected void WS0M2( )
      {
         START0M2( ) ;
         EVT0M2( ) ;
      }

      protected void EVT0M2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRID1PAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              sEvt = cgiGet( "GRID1PAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid1_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid1_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid1_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid1_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 4), "LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                           {
                              nGXsfl_84_idx = (int)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrimStr( (decimal)(nGXsfl_84_idx), 4, 0), 4, "0");
                              SubsflControlProps_842( ) ;
                              AV5LinkSelection = cgiGet( edtavLinkselection_Internalname);
                              AssignProp("", false, edtavLinkselection_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV5LinkSelection)) ? AV19Linkselection_GXI : context.convertURL( context.PathToRelativeUrl( AV5LinkSelection))), !bGXsfl_84_Refreshing);
                              AssignProp("", false, edtavLinkselection_Internalname, "SrcSet", context.GetImageSrcSet( AV5LinkSelection), true);
                              A18vueloId = (int)(context.localUtil.CToN( cgiGet( edtvueloId_Internalname), ".", ","));
                              A24vueloAeropuertoSalidaId = (int)(context.localUtil.CToN( cgiGet( edtvueloAeropuertoSalidaId_Internalname), ".", ","));
                              A26vueloAeropuertoLlegadaId = (int)(context.localUtil.CToN( cgiGet( edtvueloAeropuertoLlegadaId_Internalname), ".", ","));
                              A38vueloPrecio = context.localUtil.CToN( cgiGet( edtvueloPrecio_Internalname), ".", ",");
                              A39vueloDescuentoPorcentage = (short)(context.localUtil.CToN( cgiGet( edtvueloDescuentoPorcentage_Internalname), ".", ","));
                              A41aerolineaId = (int)(context.localUtil.CToN( cgiGet( edtaerolineaId_Internalname), ".", ","));
                              n41aerolineaId = false;
                              A58vueloFecha = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtvueloFecha_Internalname), 0));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: Start */
                                    E190M2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: Load */
                                    E200M2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Cvueloid Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCVUELOID"), ".", ",") != Convert.ToDecimal( AV6cvueloId )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cvueloaeropuertosalidaid Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCVUELOAEROPUERTOSALIDAID"), ".", ",") != Convert.ToDecimal( AV7cvueloAeropuertoSalidaId )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cvueloaeropuertollegadaid Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCVUELOAEROPUERTOLLEGADAID"), ".", ",") != Convert.ToDecimal( AV8cvueloAeropuertoLlegadaId )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cvueloprecio Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vCVUELOPRECIO"), ".", ",") != AV13cvueloPrecio )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cvuelodescuentoporcentage Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCVUELODESCUENTOPORCENTAGE"), ".", ",") != Convert.ToDecimal( AV14cvueloDescuentoPorcentage )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Caerolineaid Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCAEROLINEAID"), ".", ",") != Convert.ToDecimal( AV15caerolineaId )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cvuelofecha Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCVUELOFECHA"), 0) != AV16cvueloFecha )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                          /* Execute user event: Enter */
                                          E210M2 ();
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE0M2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA0M2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            init_web_controls( ) ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid1_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_842( ) ;
         while ( nGXsfl_84_idx <= nRC_GXsfl_84 )
         {
            sendrow_842( ) ;
            nGXsfl_84_idx = ((subGrid1_Islastpage==1)&&(nGXsfl_84_idx+1>subGrid1_fnc_Recordsperpage( )) ? 1 : nGXsfl_84_idx+1);
            sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrimStr( (decimal)(nGXsfl_84_idx), 4, 0), 4, "0");
            SubsflControlProps_842( ) ;
         }
         AddString( context.httpAjaxContext.getJSONContainerResponse( Grid1Container)) ;
         /* End function gxnrGrid1_newrow */
      }

      protected void gxgrGrid1_refresh( int subGrid1_Rows ,
                                        int AV6cvueloId ,
                                        int AV7cvueloAeropuertoSalidaId ,
                                        int AV8cvueloAeropuertoLlegadaId ,
                                        decimal AV13cvueloPrecio ,
                                        short AV14cvueloDescuentoPorcentage ,
                                        int AV15caerolineaId ,
                                        DateTime AV16cvueloFecha )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID1_nCurrentRecord = 0;
         RF0M2( ) ;
         /* End function gxgrGrid1_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_VUELOID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A18vueloId), "ZZZZZ9"), context));
         GxWebStd.gx_hidden_field( context, "VUELOID", StringUtil.LTrim( StringUtil.NToC( (decimal)(A18vueloId), 6, 0, ".", "")));
      }

      protected void clear_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
            dynload_actions( ) ;
         }
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF0M2( ) ;
         if ( isFullAjaxMode( ) )
         {
            send_integrity_footer_hashes( ) ;
         }
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF0M2( )
      {
         initialize_formulas( ) ;
         clear_multi_value_controls( ) ;
         if ( isAjaxCallMode( ) )
         {
            Grid1Container.ClearRows();
         }
         wbStart = 84;
         nGXsfl_84_idx = 1;
         sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrimStr( (decimal)(nGXsfl_84_idx), 4, 0), 4, "0");
         SubsflControlProps_842( ) ;
         bGXsfl_84_Refreshing = true;
         Grid1Container.AddObjectProperty("GridName", "Grid1");
         Grid1Container.AddObjectProperty("CmpContext", "");
         Grid1Container.AddObjectProperty("InMasterPage", "false");
         Grid1Container.AddObjectProperty("Class", "PromptGrid");
         Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
         Grid1Container.PageSize = subGrid1_fnc_Recordsperpage( );
         gxdyncontrolsrefreshing = true;
         fix_multi_value_controls( ) ;
         gxdyncontrolsrefreshing = false;
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_842( ) ;
            GXPagingFrom2 = (int)(GRID1_nFirstRecordOnPage);
            GXPagingTo2 = (int)(subGrid1_fnc_Recordsperpage( )+1);
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV7cvueloAeropuertoSalidaId ,
                                                 AV8cvueloAeropuertoLlegadaId ,
                                                 AV13cvueloPrecio ,
                                                 AV14cvueloDescuentoPorcentage ,
                                                 AV15caerolineaId ,
                                                 AV16cvueloFecha ,
                                                 A24vueloAeropuertoSalidaId ,
                                                 A26vueloAeropuertoLlegadaId ,
                                                 A38vueloPrecio ,
                                                 A39vueloDescuentoPorcentage ,
                                                 A41aerolineaId ,
                                                 A58vueloFecha ,
                                                 AV6cvueloId } ,
                                                 new int[]{
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.SHORT,
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.INT
                                                 }
            } ) ;
            /* Using cursor H000M2 */
            pr_default.execute(0, new Object[] {AV6cvueloId, AV7cvueloAeropuertoSalidaId, AV8cvueloAeropuertoLlegadaId, AV13cvueloPrecio, AV14cvueloDescuentoPorcentage, AV15caerolineaId, AV16cvueloFecha, GXPagingFrom2, GXPagingTo2, GXPagingTo2});
            nGXsfl_84_idx = 1;
            sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrimStr( (decimal)(nGXsfl_84_idx), 4, 0), 4, "0");
            SubsflControlProps_842( ) ;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( GRID1_nCurrentRecord < subGrid1_fnc_Recordsperpage( ) ) ) )
            {
               A58vueloFecha = H000M2_A58vueloFecha[0];
               A41aerolineaId = H000M2_A41aerolineaId[0];
               n41aerolineaId = H000M2_n41aerolineaId[0];
               A39vueloDescuentoPorcentage = H000M2_A39vueloDescuentoPorcentage[0];
               A38vueloPrecio = H000M2_A38vueloPrecio[0];
               A26vueloAeropuertoLlegadaId = H000M2_A26vueloAeropuertoLlegadaId[0];
               A24vueloAeropuertoSalidaId = H000M2_A24vueloAeropuertoSalidaId[0];
               A18vueloId = H000M2_A18vueloId[0];
               /* Execute user event: Load */
               E200M2 ();
               pr_default.readNext(0);
            }
            GRID1_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 84;
            WB0M0( ) ;
         }
         bGXsfl_84_Refreshing = true;
      }

      protected void send_integrity_lvl_hashes0M2( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_VUELOID"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sGXsfl_84_idx, context.localUtil.Format( (decimal)(A18vueloId), "ZZZZZ9"), context));
      }

      protected int subGrid1_fnc_Pagecount( )
      {
         GRID1_nRecordCount = subGrid1_fnc_Recordcount( );
         if ( ((int)((GRID1_nRecordCount) % (subGrid1_fnc_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID1_nRecordCount/ (decimal)(subGrid1_fnc_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID1_nRecordCount/ (decimal)(subGrid1_fnc_Recordsperpage( ))))+1) ;
      }

      protected int subGrid1_fnc_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV7cvueloAeropuertoSalidaId ,
                                              AV8cvueloAeropuertoLlegadaId ,
                                              AV13cvueloPrecio ,
                                              AV14cvueloDescuentoPorcentage ,
                                              AV15caerolineaId ,
                                              AV16cvueloFecha ,
                                              A24vueloAeropuertoSalidaId ,
                                              A26vueloAeropuertoLlegadaId ,
                                              A38vueloPrecio ,
                                              A39vueloDescuentoPorcentage ,
                                              A41aerolineaId ,
                                              A58vueloFecha ,
                                              AV6cvueloId } ,
                                              new int[]{
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.SHORT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.INT
                                              }
         } ) ;
         /* Using cursor H000M3 */
         pr_default.execute(1, new Object[] {AV6cvueloId, AV7cvueloAeropuertoSalidaId, AV8cvueloAeropuertoLlegadaId, AV13cvueloPrecio, AV14cvueloDescuentoPorcentage, AV15caerolineaId, AV16cvueloFecha});
         GRID1_nRecordCount = H000M3_AGRID1_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID1_nRecordCount) ;
      }

      protected int subGrid1_fnc_Recordsperpage( )
      {
         return (int)(10*1) ;
      }

      protected int subGrid1_fnc_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID1_nFirstRecordOnPage/ (decimal)(subGrid1_fnc_Recordsperpage( ))))+1) ;
      }

      protected short subgrid1_firstpage( )
      {
         GRID1_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cvueloId, AV7cvueloAeropuertoSalidaId, AV8cvueloAeropuertoLlegadaId, AV13cvueloPrecio, AV14cvueloDescuentoPorcentage, AV15caerolineaId, AV16cvueloFecha) ;
         }
         send_integrity_footer_hashes( ) ;
         return 0 ;
      }

      protected short subgrid1_nextpage( )
      {
         GRID1_nRecordCount = subGrid1_fnc_Recordcount( );
         if ( ( GRID1_nRecordCount >= subGrid1_fnc_Recordsperpage( ) ) && ( GRID1_nEOF == 0 ) )
         {
            GRID1_nFirstRecordOnPage = (long)(GRID1_nFirstRecordOnPage+subGrid1_fnc_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         Grid1Container.AddObjectProperty("GRID1_nFirstRecordOnPage", GRID1_nFirstRecordOnPage);
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cvueloId, AV7cvueloAeropuertoSalidaId, AV8cvueloAeropuertoLlegadaId, AV13cvueloPrecio, AV14cvueloDescuentoPorcentage, AV15caerolineaId, AV16cvueloFecha) ;
         }
         send_integrity_footer_hashes( ) ;
         return (short)(((GRID1_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid1_previouspage( )
      {
         if ( GRID1_nFirstRecordOnPage >= subGrid1_fnc_Recordsperpage( ) )
         {
            GRID1_nFirstRecordOnPage = (long)(GRID1_nFirstRecordOnPage-subGrid1_fnc_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cvueloId, AV7cvueloAeropuertoSalidaId, AV8cvueloAeropuertoLlegadaId, AV13cvueloPrecio, AV14cvueloDescuentoPorcentage, AV15caerolineaId, AV16cvueloFecha) ;
         }
         send_integrity_footer_hashes( ) ;
         return 0 ;
      }

      protected short subgrid1_lastpage( )
      {
         GRID1_nRecordCount = subGrid1_fnc_Recordcount( );
         if ( GRID1_nRecordCount > subGrid1_fnc_Recordsperpage( ) )
         {
            if ( ((int)((GRID1_nRecordCount) % (subGrid1_fnc_Recordsperpage( )))) == 0 )
            {
               GRID1_nFirstRecordOnPage = (long)(GRID1_nRecordCount-subGrid1_fnc_Recordsperpage( ));
            }
            else
            {
               GRID1_nFirstRecordOnPage = (long)(GRID1_nRecordCount-((int)((GRID1_nRecordCount) % (subGrid1_fnc_Recordsperpage( )))));
            }
         }
         else
         {
            GRID1_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cvueloId, AV7cvueloAeropuertoSalidaId, AV8cvueloAeropuertoLlegadaId, AV13cvueloPrecio, AV14cvueloDescuentoPorcentage, AV15caerolineaId, AV16cvueloFecha) ;
         }
         send_integrity_footer_hashes( ) ;
         return 0 ;
      }

      protected int subgrid1_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID1_nFirstRecordOnPage = (long)(subGrid1_fnc_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID1_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cvueloId, AV7cvueloAeropuertoSalidaId, AV8cvueloAeropuertoLlegadaId, AV13cvueloPrecio, AV14cvueloDescuentoPorcentage, AV15caerolineaId, AV16cvueloFecha) ;
         }
         send_integrity_footer_hashes( ) ;
         return (int)(0) ;
      }

      protected void STRUP0M0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E190M2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read saved values. */
            nRC_GXsfl_84 = (int)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_84"), ".", ","));
            GRID1_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID1_nFirstRecordOnPage"), ".", ","));
            GRID1_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID1_nEOF"), ".", ","));
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCvueloid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCvueloid_Internalname), ".", ",") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCVUELOID");
               GX_FocusControl = edtavCvueloid_Internalname;
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV6cvueloId = 0;
               AssignAttri("", false, "AV6cvueloId", StringUtil.LTrimStr( (decimal)(AV6cvueloId), 6, 0));
            }
            else
            {
               AV6cvueloId = (int)(context.localUtil.CToN( cgiGet( edtavCvueloid_Internalname), ".", ","));
               AssignAttri("", false, "AV6cvueloId", StringUtil.LTrimStr( (decimal)(AV6cvueloId), 6, 0));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCvueloaeropuertosalidaid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCvueloaeropuertosalidaid_Internalname), ".", ",") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCVUELOAEROPUERTOSALIDAID");
               GX_FocusControl = edtavCvueloaeropuertosalidaid_Internalname;
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV7cvueloAeropuertoSalidaId = 0;
               AssignAttri("", false, "AV7cvueloAeropuertoSalidaId", StringUtil.LTrimStr( (decimal)(AV7cvueloAeropuertoSalidaId), 6, 0));
            }
            else
            {
               AV7cvueloAeropuertoSalidaId = (int)(context.localUtil.CToN( cgiGet( edtavCvueloaeropuertosalidaid_Internalname), ".", ","));
               AssignAttri("", false, "AV7cvueloAeropuertoSalidaId", StringUtil.LTrimStr( (decimal)(AV7cvueloAeropuertoSalidaId), 6, 0));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCvueloaeropuertollegadaid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCvueloaeropuertollegadaid_Internalname), ".", ",") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCVUELOAEROPUERTOLLEGADAID");
               GX_FocusControl = edtavCvueloaeropuertollegadaid_Internalname;
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV8cvueloAeropuertoLlegadaId = 0;
               AssignAttri("", false, "AV8cvueloAeropuertoLlegadaId", StringUtil.LTrimStr( (decimal)(AV8cvueloAeropuertoLlegadaId), 6, 0));
            }
            else
            {
               AV8cvueloAeropuertoLlegadaId = (int)(context.localUtil.CToN( cgiGet( edtavCvueloaeropuertollegadaid_Internalname), ".", ","));
               AssignAttri("", false, "AV8cvueloAeropuertoLlegadaId", StringUtil.LTrimStr( (decimal)(AV8cvueloAeropuertoLlegadaId), 6, 0));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCvueloprecio_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCvueloprecio_Internalname), ".", ",") > 999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCVUELOPRECIO");
               GX_FocusControl = edtavCvueloprecio_Internalname;
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13cvueloPrecio = 0;
               AssignAttri("", false, "AV13cvueloPrecio", StringUtil.LTrimStr( AV13cvueloPrecio, 9, 2));
            }
            else
            {
               AV13cvueloPrecio = context.localUtil.CToN( cgiGet( edtavCvueloprecio_Internalname), ".", ",");
               AssignAttri("", false, "AV13cvueloPrecio", StringUtil.LTrimStr( AV13cvueloPrecio, 9, 2));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCvuelodescuentoporcentage_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCvuelodescuentoporcentage_Internalname), ".", ",") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCVUELODESCUENTOPORCENTAGE");
               GX_FocusControl = edtavCvuelodescuentoporcentage_Internalname;
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV14cvueloDescuentoPorcentage = 0;
               AssignAttri("", false, "AV14cvueloDescuentoPorcentage", StringUtil.LTrimStr( (decimal)(AV14cvueloDescuentoPorcentage), 3, 0));
            }
            else
            {
               AV14cvueloDescuentoPorcentage = (short)(context.localUtil.CToN( cgiGet( edtavCvuelodescuentoporcentage_Internalname), ".", ","));
               AssignAttri("", false, "AV14cvueloDescuentoPorcentage", StringUtil.LTrimStr( (decimal)(AV14cvueloDescuentoPorcentage), 3, 0));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCaerolineaid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCaerolineaid_Internalname), ".", ",") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCAEROLINEAID");
               GX_FocusControl = edtavCaerolineaid_Internalname;
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV15caerolineaId = 0;
               AssignAttri("", false, "AV15caerolineaId", StringUtil.LTrimStr( (decimal)(AV15caerolineaId), 6, 0));
            }
            else
            {
               AV15caerolineaId = (int)(context.localUtil.CToN( cgiGet( edtavCaerolineaid_Internalname), ".", ","));
               AssignAttri("", false, "AV15caerolineaId", StringUtil.LTrimStr( (decimal)(AV15caerolineaId), 6, 0));
            }
            if ( context.localUtil.VCDate( cgiGet( edtavCvuelofecha_Internalname), 1) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"vuelo Fecha"}), 1, "vCVUELOFECHA");
               GX_FocusControl = edtavCvuelofecha_Internalname;
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV16cvueloFecha = DateTime.MinValue;
               AssignAttri("", false, "AV16cvueloFecha", context.localUtil.Format(AV16cvueloFecha, "99/99/99"));
            }
            else
            {
               AV16cvueloFecha = context.localUtil.CToD( cgiGet( edtavCvuelofecha_Internalname), 1);
               AssignAttri("", false, "AV16cvueloFecha", context.localUtil.Format(AV16cvueloFecha, "99/99/99"));
            }
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCVUELOID"), ".", ",") != Convert.ToDecimal( AV6cvueloId )) )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCVUELOAEROPUERTOSALIDAID"), ".", ",") != Convert.ToDecimal( AV7cvueloAeropuertoSalidaId )) )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCVUELOAEROPUERTOLLEGADAID"), ".", ",") != Convert.ToDecimal( AV8cvueloAeropuertoLlegadaId )) )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vCVUELOPRECIO"), ".", ",") != AV13cvueloPrecio )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCVUELODESCUENTOPORCENTAGE"), ".", ",") != Convert.ToDecimal( AV14cvueloDescuentoPorcentage )) )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCAEROLINEAID"), ".", ",") != Convert.ToDecimal( AV15caerolineaId )) )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToD( cgiGet( "GXH_vCVUELOFECHA"), 1) != AV16cvueloFecha )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: Start */
         E190M2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E190M2( )
      {
         /* Start Routine */
         Form.Caption = StringUtil.Format( "Selection List %1", "vuelo", "", "", "", "", "", "", "", "");
         AssignProp("", false, "FORM", "Caption", Form.Caption, true);
         AV10ADVANCED_LABEL_TEMPLATE = "%1 <strong>%2</strong>";
      }

      private void E200M2( )
      {
         /* Load Routine */
         AV5LinkSelection = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         AssignAttri("", false, edtavLinkselection_Internalname, AV5LinkSelection);
         AV19Linkselection_GXI = GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         sendrow_842( ) ;
         GRID1_nCurrentRecord = (long)(GRID1_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ! bGXsfl_84_Refreshing )
         {
            context.DoAjaxLoad(84, Grid1Row);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: Enter */
         E210M2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E210M2( )
      {
         /* Enter Routine */
         AV9pvueloId = A18vueloId;
         AssignAttri("", false, "AV9pvueloId", StringUtil.LTrimStr( (decimal)(AV9pvueloId), 6, 0));
         context.setWebReturnParms(new Object[] {(int)AV9pvueloId});
         context.setWebReturnParmsMetadata(new Object[] {"AV9pvueloId"});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
         /*  Sending Event outputs  */
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV9pvueloId = Convert.ToInt32(getParm(obj,0));
         AssignAttri("", false, "AV9pvueloId", StringUtil.LTrimStr( (decimal)(AV9pvueloId), 6, 0));
      }

      public override String getresponse( String sGXDynURL )
      {
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA0M2( ) ;
         WS0M2( ) ;
         WE0M2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ));
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20191251294095", true, true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.eng.js", "?"+GetCacheInvalidationToken( ), false, true);
         context.AddJavascriptSource("gx0060.js", "?20191251294096", false, true);
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_842( )
      {
         edtavLinkselection_Internalname = "vLINKSELECTION_"+sGXsfl_84_idx;
         edtvueloId_Internalname = "VUELOID_"+sGXsfl_84_idx;
         edtvueloAeropuertoSalidaId_Internalname = "VUELOAEROPUERTOSALIDAID_"+sGXsfl_84_idx;
         edtvueloAeropuertoLlegadaId_Internalname = "VUELOAEROPUERTOLLEGADAID_"+sGXsfl_84_idx;
         edtvueloPrecio_Internalname = "VUELOPRECIO_"+sGXsfl_84_idx;
         edtvueloDescuentoPorcentage_Internalname = "VUELODESCUENTOPORCENTAGE_"+sGXsfl_84_idx;
         edtaerolineaId_Internalname = "AEROLINEAID_"+sGXsfl_84_idx;
         edtvueloFecha_Internalname = "VUELOFECHA_"+sGXsfl_84_idx;
      }

      protected void SubsflControlProps_fel_842( )
      {
         edtavLinkselection_Internalname = "vLINKSELECTION_"+sGXsfl_84_fel_idx;
         edtvueloId_Internalname = "VUELOID_"+sGXsfl_84_fel_idx;
         edtvueloAeropuertoSalidaId_Internalname = "VUELOAEROPUERTOSALIDAID_"+sGXsfl_84_fel_idx;
         edtvueloAeropuertoLlegadaId_Internalname = "VUELOAEROPUERTOLLEGADAID_"+sGXsfl_84_fel_idx;
         edtvueloPrecio_Internalname = "VUELOPRECIO_"+sGXsfl_84_fel_idx;
         edtvueloDescuentoPorcentage_Internalname = "VUELODESCUENTOPORCENTAGE_"+sGXsfl_84_fel_idx;
         edtaerolineaId_Internalname = "AEROLINEAID_"+sGXsfl_84_fel_idx;
         edtvueloFecha_Internalname = "VUELOFECHA_"+sGXsfl_84_fel_idx;
      }

      protected void sendrow_842( )
      {
         SubsflControlProps_842( ) ;
         WB0M0( ) ;
         if ( ( 10 * 1 == 0 ) || ( nGXsfl_84_idx <= subGrid1_fnc_Recordsperpage( ) * 1 ) )
         {
            Grid1Row = GXWebRow.GetNew(context,Grid1Container);
            if ( subGrid1_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid1_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd";
               }
            }
            else if ( subGrid1_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid1_Backstyle = 0;
               subGrid1_Backcolor = subGrid1_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Uniform";
               }
            }
            else if ( subGrid1_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid1_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd";
               }
               subGrid1_Backcolor = (int)(0x0);
            }
            else if ( subGrid1_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid1_Backstyle = 1;
               if ( ((int)((nGXsfl_84_idx) % (2))) == 0 )
               {
                  subGrid1_Backcolor = (int)(0x0);
                  if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Even";
                  }
               }
               else
               {
                  subGrid1_Backcolor = (int)(0x0);
                  if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Odd";
                  }
               }
            }
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+"PromptGrid"+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_84_idx+"\">") ;
            }
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            edtavLinkselection_Link = "javascript:gx.popup.gxReturn(["+"'"+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A18vueloId), 6, 0, ".", "")))+"'"+"]);";
            AssignProp("", false, edtavLinkselection_Internalname, "Link", edtavLinkselection_Link, !bGXsfl_84_Refreshing);
            ClassString = "SelectionAttribute";
            StyleString = "";
            AV5LinkSelection_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV5LinkSelection))&&String.IsNullOrEmpty(StringUtil.RTrim( AV19Linkselection_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV5LinkSelection)));
            sImgUrl = (String.IsNullOrEmpty(StringUtil.RTrim( AV5LinkSelection)) ? AV19Linkselection_GXI : context.PathToRelativeUrl( AV5LinkSelection));
            Grid1Row.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavLinkselection_Internalname,(String)sImgUrl,(String)edtavLinkselection_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)"",(short)1,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"WWActionColumn",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV5LinkSelection_IsBlob,(bool)false,context.GetImageSrcSet( sImgUrl)});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtvueloId_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A18vueloId), 6, 0, ".", "")),context.localUtil.Format( (decimal)(A18vueloId), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtvueloId_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"WWColumn",(String)"",(short)-1,(short)0,(short)0,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"Id",(String)"right",(bool)false,(String)""});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtvueloAeropuertoSalidaId_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A24vueloAeropuertoSalidaId), 6, 0, ".", "")),context.localUtil.Format( (decimal)(A24vueloAeropuertoSalidaId), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtvueloAeropuertoSalidaId_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"WWColumn OptionalColumn",(String)"",(short)-1,(short)0,(short)0,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"Id",(String)"right",(bool)false,(String)""});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtvueloAeropuertoLlegadaId_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A26vueloAeropuertoLlegadaId), 6, 0, ".", "")),context.localUtil.Format( (decimal)(A26vueloAeropuertoLlegadaId), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtvueloAeropuertoLlegadaId_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"WWColumn OptionalColumn",(String)"",(short)-1,(short)0,(short)0,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"Id",(String)"right",(bool)false,(String)""});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtvueloPrecio_Internalname,StringUtil.LTrim( StringUtil.NToC( A38vueloPrecio, 9, 2, ".", "")),context.localUtil.Format( A38vueloPrecio, "ZZZZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtvueloPrecio_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"WWColumn OptionalColumn",(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)9,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"Price",(String)"right",(bool)false,(String)""});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtvueloDescuentoPorcentage_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A39vueloDescuentoPorcentage), 3, 0, ".", "")),context.localUtil.Format( (decimal)(A39vueloDescuentoPorcentage), "ZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtvueloDescuentoPorcentage_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"WWColumn OptionalColumn",(String)"",(short)-1,(short)0,(short)0,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"Porcentage",(String)"right",(bool)false,(String)""});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtaerolineaId_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A41aerolineaId), 6, 0, ".", "")),context.localUtil.Format( (decimal)(A41aerolineaId), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtaerolineaId_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"WWColumn OptionalColumn",(String)"",(short)-1,(short)0,(short)0,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"Id",(String)"right",(bool)false,(String)""});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "DescriptionAttribute";
            edtvueloFecha_Link = "javascript:gx.popup.gxReturn(["+"'"+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A18vueloId), 6, 0, ".", "")))+"'"+"]);";
            AssignProp("", false, edtvueloFecha_Internalname, "Link", edtvueloFecha_Link, !bGXsfl_84_Refreshing);
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtvueloFecha_Internalname,context.localUtil.Format(A58vueloFecha, "99/99/99"),context.localUtil.Format( A58vueloFecha, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtvueloFecha_Link,(String)"",(String)"",(String)"",(String)edtvueloFecha_Jsonclick,(short)0,(String)"DescriptionAttribute",(String)"",(String)ROClassString,(String)"WWColumn",(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false,(String)""});
            send_integrity_lvl_hashes0M2( ) ;
            Grid1Container.AddRow(Grid1Row);
            nGXsfl_84_idx = ((subGrid1_Islastpage==1)&&(nGXsfl_84_idx+1>subGrid1_fnc_Recordsperpage( )) ? 1 : nGXsfl_84_idx+1);
            sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrimStr( (decimal)(nGXsfl_84_idx), 4, 0), 4, "0");
            SubsflControlProps_842( ) ;
         }
         /* End function sendrow_842 */
      }

      protected void init_web_controls( )
      {
         /* End function init_web_controls */
      }

      protected void init_default_properties( )
      {
         lblLblvueloidfilter_Internalname = "LBLVUELOIDFILTER";
         edtavCvueloid_Internalname = "vCVUELOID";
         divVueloidfiltercontainer_Internalname = "VUELOIDFILTERCONTAINER";
         lblLblvueloaeropuertosalidaidfilter_Internalname = "LBLVUELOAEROPUERTOSALIDAIDFILTER";
         edtavCvueloaeropuertosalidaid_Internalname = "vCVUELOAEROPUERTOSALIDAID";
         divVueloaeropuertosalidaidfiltercontainer_Internalname = "VUELOAEROPUERTOSALIDAIDFILTERCONTAINER";
         lblLblvueloaeropuertollegadaidfilter_Internalname = "LBLVUELOAEROPUERTOLLEGADAIDFILTER";
         edtavCvueloaeropuertollegadaid_Internalname = "vCVUELOAEROPUERTOLLEGADAID";
         divVueloaeropuertollegadaidfiltercontainer_Internalname = "VUELOAEROPUERTOLLEGADAIDFILTERCONTAINER";
         lblLblvuelopreciofilter_Internalname = "LBLVUELOPRECIOFILTER";
         edtavCvueloprecio_Internalname = "vCVUELOPRECIO";
         divVuelopreciofiltercontainer_Internalname = "VUELOPRECIOFILTERCONTAINER";
         lblLblvuelodescuentoporcentagefilter_Internalname = "LBLVUELODESCUENTOPORCENTAGEFILTER";
         edtavCvuelodescuentoporcentage_Internalname = "vCVUELODESCUENTOPORCENTAGE";
         divVuelodescuentoporcentagefiltercontainer_Internalname = "VUELODESCUENTOPORCENTAGEFILTERCONTAINER";
         lblLblaerolineaidfilter_Internalname = "LBLAEROLINEAIDFILTER";
         edtavCaerolineaid_Internalname = "vCAEROLINEAID";
         divAerolineaidfiltercontainer_Internalname = "AEROLINEAIDFILTERCONTAINER";
         lblLblvuelofechafilter_Internalname = "LBLVUELOFECHAFILTER";
         edtavCvuelofecha_Internalname = "vCVUELOFECHA";
         divVuelofechafiltercontainer_Internalname = "VUELOFECHAFILTERCONTAINER";
         divAdvancedcontainer_Internalname = "ADVANCEDCONTAINER";
         bttBtntoggle_Internalname = "BTNTOGGLE";
         edtavLinkselection_Internalname = "vLINKSELECTION";
         edtvueloId_Internalname = "VUELOID";
         edtvueloAeropuertoSalidaId_Internalname = "VUELOAEROPUERTOSALIDAID";
         edtvueloAeropuertoLlegadaId_Internalname = "VUELOAEROPUERTOLLEGADAID";
         edtvueloPrecio_Internalname = "VUELOPRECIO";
         edtvueloDescuentoPorcentage_Internalname = "VUELODESCUENTOPORCENTAGE";
         edtaerolineaId_Internalname = "AEROLINEAID";
         edtvueloFecha_Internalname = "VUELOFECHA";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         divGridtable_Internalname = "GRIDTABLE";
         divMain_Internalname = "MAIN";
         Form.Internalname = "FORM";
         subGrid1_Internalname = "GRID1";
      }

      public override void initialize_properties( )
      {
         context.SetDefaultTheme("Carmine");
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtvueloFecha_Jsonclick = "";
         edtaerolineaId_Jsonclick = "";
         edtvueloDescuentoPorcentage_Jsonclick = "";
         edtvueloPrecio_Jsonclick = "";
         edtvueloAeropuertoLlegadaId_Jsonclick = "";
         edtvueloAeropuertoSalidaId_Jsonclick = "";
         edtvueloId_Jsonclick = "";
         subGrid1_Allowcollapsing = 0;
         subGrid1_Allowselection = 0;
         edtvueloFecha_Link = "";
         edtavLinkselection_Link = "";
         subGrid1_Header = "";
         subGrid1_Class = "PromptGrid";
         subGrid1_Backcolorstyle = 0;
         edtavCvuelofecha_Jsonclick = "";
         edtavCvuelofecha_Enabled = 1;
         edtavCaerolineaid_Jsonclick = "";
         edtavCaerolineaid_Enabled = 1;
         edtavCaerolineaid_Visible = 1;
         edtavCvuelodescuentoporcentage_Jsonclick = "";
         edtavCvuelodescuentoporcentage_Enabled = 1;
         edtavCvuelodescuentoporcentage_Visible = 1;
         edtavCvueloprecio_Jsonclick = "";
         edtavCvueloprecio_Enabled = 1;
         edtavCvueloprecio_Visible = 1;
         edtavCvueloaeropuertollegadaid_Jsonclick = "";
         edtavCvueloaeropuertollegadaid_Enabled = 1;
         edtavCvueloaeropuertollegadaid_Visible = 1;
         edtavCvueloaeropuertosalidaid_Jsonclick = "";
         edtavCvueloaeropuertosalidaid_Enabled = 1;
         edtavCvueloaeropuertosalidaid_Visible = 1;
         edtavCvueloid_Jsonclick = "";
         edtavCvueloid_Enabled = 1;
         edtavCvueloid_Visible = 1;
         divVuelofechafiltercontainer_Class = "AdvancedContainerItem";
         divAerolineaidfiltercontainer_Class = "AdvancedContainerItem";
         divVuelodescuentoporcentagefiltercontainer_Class = "AdvancedContainerItem";
         divVuelopreciofiltercontainer_Class = "AdvancedContainerItem";
         divVueloaeropuertollegadaidfiltercontainer_Class = "AdvancedContainerItem";
         divVueloaeropuertosalidaidfiltercontainer_Class = "AdvancedContainerItem";
         divVueloidfiltercontainer_Class = "AdvancedContainerItem";
         bttBtntoggle_Class = "BtnToggle";
         divAdvancedcontainer_Class = "AdvancedContainerVisible";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Selection List vuelo";
         subGrid1_Rows = 10;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{av:'subGrid1_Rows',ctrl:'GRID1',prop:'Rows'},{av:'AV6cvueloId',fld:'vCVUELOID',pic:'ZZZZZ9'},{av:'AV7cvueloAeropuertoSalidaId',fld:'vCVUELOAEROPUERTOSALIDAID',pic:'ZZZZZ9'},{av:'AV8cvueloAeropuertoLlegadaId',fld:'vCVUELOAEROPUERTOLLEGADAID',pic:'ZZZZZ9'},{av:'AV13cvueloPrecio',fld:'vCVUELOPRECIO',pic:'ZZZZZ9.99'},{av:'AV14cvueloDescuentoPorcentage',fld:'vCVUELODESCUENTOPORCENTAGE',pic:'ZZ9'},{av:'AV15caerolineaId',fld:'vCAEROLINEAID',pic:'ZZZZZ9'},{av:'AV16cvueloFecha',fld:'vCVUELOFECHA',pic:''}]");
         setEventMetadata("REFRESH",",oparms:[]}");
         setEventMetadata("'TOGGLE'","{handler:'E180M1',iparms:[{av:'divAdvancedcontainer_Class',ctrl:'ADVANCEDCONTAINER',prop:'Class'},{ctrl:'BTNTOGGLE',prop:'Class'}]");
         setEventMetadata("'TOGGLE'",",oparms:[{av:'divAdvancedcontainer_Class',ctrl:'ADVANCEDCONTAINER',prop:'Class'},{ctrl:'BTNTOGGLE',prop:'Class'}]}");
         setEventMetadata("LBLVUELOIDFILTER.CLICK","{handler:'E110M1',iparms:[{av:'divVueloidfiltercontainer_Class',ctrl:'VUELOIDFILTERCONTAINER',prop:'Class'}]");
         setEventMetadata("LBLVUELOIDFILTER.CLICK",",oparms:[{av:'divVueloidfiltercontainer_Class',ctrl:'VUELOIDFILTERCONTAINER',prop:'Class'},{av:'edtavCvueloid_Visible',ctrl:'vCVUELOID',prop:'Visible'}]}");
         setEventMetadata("LBLVUELOAEROPUERTOSALIDAIDFILTER.CLICK","{handler:'E120M1',iparms:[{av:'divVueloaeropuertosalidaidfiltercontainer_Class',ctrl:'VUELOAEROPUERTOSALIDAIDFILTERCONTAINER',prop:'Class'}]");
         setEventMetadata("LBLVUELOAEROPUERTOSALIDAIDFILTER.CLICK",",oparms:[{av:'divVueloaeropuertosalidaidfiltercontainer_Class',ctrl:'VUELOAEROPUERTOSALIDAIDFILTERCONTAINER',prop:'Class'},{av:'edtavCvueloaeropuertosalidaid_Visible',ctrl:'vCVUELOAEROPUERTOSALIDAID',prop:'Visible'}]}");
         setEventMetadata("LBLVUELOAEROPUERTOLLEGADAIDFILTER.CLICK","{handler:'E130M1',iparms:[{av:'divVueloaeropuertollegadaidfiltercontainer_Class',ctrl:'VUELOAEROPUERTOLLEGADAIDFILTERCONTAINER',prop:'Class'}]");
         setEventMetadata("LBLVUELOAEROPUERTOLLEGADAIDFILTER.CLICK",",oparms:[{av:'divVueloaeropuertollegadaidfiltercontainer_Class',ctrl:'VUELOAEROPUERTOLLEGADAIDFILTERCONTAINER',prop:'Class'},{av:'edtavCvueloaeropuertollegadaid_Visible',ctrl:'vCVUELOAEROPUERTOLLEGADAID',prop:'Visible'}]}");
         setEventMetadata("LBLVUELOPRECIOFILTER.CLICK","{handler:'E140M1',iparms:[{av:'divVuelopreciofiltercontainer_Class',ctrl:'VUELOPRECIOFILTERCONTAINER',prop:'Class'}]");
         setEventMetadata("LBLVUELOPRECIOFILTER.CLICK",",oparms:[{av:'divVuelopreciofiltercontainer_Class',ctrl:'VUELOPRECIOFILTERCONTAINER',prop:'Class'},{av:'edtavCvueloprecio_Visible',ctrl:'vCVUELOPRECIO',prop:'Visible'}]}");
         setEventMetadata("LBLVUELODESCUENTOPORCENTAGEFILTER.CLICK","{handler:'E150M1',iparms:[{av:'divVuelodescuentoporcentagefiltercontainer_Class',ctrl:'VUELODESCUENTOPORCENTAGEFILTERCONTAINER',prop:'Class'}]");
         setEventMetadata("LBLVUELODESCUENTOPORCENTAGEFILTER.CLICK",",oparms:[{av:'divVuelodescuentoporcentagefiltercontainer_Class',ctrl:'VUELODESCUENTOPORCENTAGEFILTERCONTAINER',prop:'Class'},{av:'edtavCvuelodescuentoporcentage_Visible',ctrl:'vCVUELODESCUENTOPORCENTAGE',prop:'Visible'}]}");
         setEventMetadata("LBLAEROLINEAIDFILTER.CLICK","{handler:'E160M1',iparms:[{av:'divAerolineaidfiltercontainer_Class',ctrl:'AEROLINEAIDFILTERCONTAINER',prop:'Class'}]");
         setEventMetadata("LBLAEROLINEAIDFILTER.CLICK",",oparms:[{av:'divAerolineaidfiltercontainer_Class',ctrl:'AEROLINEAIDFILTERCONTAINER',prop:'Class'},{av:'edtavCaerolineaid_Visible',ctrl:'vCAEROLINEAID',prop:'Visible'}]}");
         setEventMetadata("LBLVUELOFECHAFILTER.CLICK","{handler:'E170M1',iparms:[{av:'divVuelofechafiltercontainer_Class',ctrl:'VUELOFECHAFILTERCONTAINER',prop:'Class'}]");
         setEventMetadata("LBLVUELOFECHAFILTER.CLICK",",oparms:[{av:'divVuelofechafiltercontainer_Class',ctrl:'VUELOFECHAFILTERCONTAINER',prop:'Class'}]}");
         setEventMetadata("ENTER","{handler:'E210M2',iparms:[{av:'A18vueloId',fld:'VUELOID',pic:'ZZZZZ9',hsh:true}]");
         setEventMetadata("ENTER",",oparms:[{av:'AV9pvueloId',fld:'vPVUELOID',pic:'ZZZZZ9'}]}");
         setEventMetadata("GRID1_FIRSTPAGE","{handler:'subgrid1_firstpage',iparms:[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{av:'subGrid1_Rows',ctrl:'GRID1',prop:'Rows'},{av:'AV6cvueloId',fld:'vCVUELOID',pic:'ZZZZZ9'},{av:'AV7cvueloAeropuertoSalidaId',fld:'vCVUELOAEROPUERTOSALIDAID',pic:'ZZZZZ9'},{av:'AV8cvueloAeropuertoLlegadaId',fld:'vCVUELOAEROPUERTOLLEGADAID',pic:'ZZZZZ9'},{av:'AV13cvueloPrecio',fld:'vCVUELOPRECIO',pic:'ZZZZZ9.99'},{av:'AV14cvueloDescuentoPorcentage',fld:'vCVUELODESCUENTOPORCENTAGE',pic:'ZZ9'},{av:'AV15caerolineaId',fld:'vCAEROLINEAID',pic:'ZZZZZ9'},{av:'AV16cvueloFecha',fld:'vCVUELOFECHA',pic:''}]");
         setEventMetadata("GRID1_FIRSTPAGE",",oparms:[]}");
         setEventMetadata("GRID1_PREVPAGE","{handler:'subgrid1_previouspage',iparms:[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{av:'subGrid1_Rows',ctrl:'GRID1',prop:'Rows'},{av:'AV6cvueloId',fld:'vCVUELOID',pic:'ZZZZZ9'},{av:'AV7cvueloAeropuertoSalidaId',fld:'vCVUELOAEROPUERTOSALIDAID',pic:'ZZZZZ9'},{av:'AV8cvueloAeropuertoLlegadaId',fld:'vCVUELOAEROPUERTOLLEGADAID',pic:'ZZZZZ9'},{av:'AV13cvueloPrecio',fld:'vCVUELOPRECIO',pic:'ZZZZZ9.99'},{av:'AV14cvueloDescuentoPorcentage',fld:'vCVUELODESCUENTOPORCENTAGE',pic:'ZZ9'},{av:'AV15caerolineaId',fld:'vCAEROLINEAID',pic:'ZZZZZ9'},{av:'AV16cvueloFecha',fld:'vCVUELOFECHA',pic:''}]");
         setEventMetadata("GRID1_PREVPAGE",",oparms:[]}");
         setEventMetadata("GRID1_NEXTPAGE","{handler:'subgrid1_nextpage',iparms:[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{av:'subGrid1_Rows',ctrl:'GRID1',prop:'Rows'},{av:'AV6cvueloId',fld:'vCVUELOID',pic:'ZZZZZ9'},{av:'AV7cvueloAeropuertoSalidaId',fld:'vCVUELOAEROPUERTOSALIDAID',pic:'ZZZZZ9'},{av:'AV8cvueloAeropuertoLlegadaId',fld:'vCVUELOAEROPUERTOLLEGADAID',pic:'ZZZZZ9'},{av:'AV13cvueloPrecio',fld:'vCVUELOPRECIO',pic:'ZZZZZ9.99'},{av:'AV14cvueloDescuentoPorcentage',fld:'vCVUELODESCUENTOPORCENTAGE',pic:'ZZ9'},{av:'AV15caerolineaId',fld:'vCAEROLINEAID',pic:'ZZZZZ9'},{av:'AV16cvueloFecha',fld:'vCVUELOFECHA',pic:''}]");
         setEventMetadata("GRID1_NEXTPAGE",",oparms:[]}");
         setEventMetadata("GRID1_LASTPAGE","{handler:'subgrid1_lastpage',iparms:[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{av:'subGrid1_Rows',ctrl:'GRID1',prop:'Rows'},{av:'AV6cvueloId',fld:'vCVUELOID',pic:'ZZZZZ9'},{av:'AV7cvueloAeropuertoSalidaId',fld:'vCVUELOAEROPUERTOSALIDAID',pic:'ZZZZZ9'},{av:'AV8cvueloAeropuertoLlegadaId',fld:'vCVUELOAEROPUERTOLLEGADAID',pic:'ZZZZZ9'},{av:'AV13cvueloPrecio',fld:'vCVUELOPRECIO',pic:'ZZZZZ9.99'},{av:'AV14cvueloDescuentoPorcentage',fld:'vCVUELODESCUENTOPORCENTAGE',pic:'ZZ9'},{av:'AV15caerolineaId',fld:'vCAEROLINEAID',pic:'ZZZZZ9'},{av:'AV16cvueloFecha',fld:'vCVUELOFECHA',pic:''}]");
         setEventMetadata("GRID1_LASTPAGE",",oparms:[]}");
         setEventMetadata("VALIDV_CVUELOFECHA","{handler:'Validv_Cvuelofecha',iparms:[]");
         setEventMetadata("VALIDV_CVUELOFECHA",",oparms:[]}");
         setEventMetadata("NULL","{handler:'Valid_Vuelofecha',iparms:[]");
         setEventMetadata("NULL",",oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV16cvueloFecha = DateTime.MinValue;
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         lblLblvueloidfilter_Jsonclick = "";
         TempTags = "";
         lblLblvueloaeropuertosalidaidfilter_Jsonclick = "";
         lblLblvueloaeropuertollegadaidfilter_Jsonclick = "";
         lblLblvuelopreciofilter_Jsonclick = "";
         lblLblvuelodescuentoporcentagefilter_Jsonclick = "";
         lblLblaerolineaidfilter_Jsonclick = "";
         lblLblvuelofechafilter_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         bttBtntoggle_Jsonclick = "";
         Grid1Container = new GXWebGrid( context);
         sStyleString = "";
         subGrid1_Linesclass = "";
         Grid1Column = new GXWebColumn();
         AV5LinkSelection = "";
         A58vueloFecha = DateTime.MinValue;
         bttBtn_cancel_Jsonclick = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV19Linkselection_GXI = "";
         scmdbuf = "";
         H000M2_A58vueloFecha = new DateTime[] {DateTime.MinValue} ;
         H000M2_A41aerolineaId = new int[1] ;
         H000M2_n41aerolineaId = new bool[] {false} ;
         H000M2_A39vueloDescuentoPorcentage = new short[1] ;
         H000M2_A38vueloPrecio = new decimal[1] ;
         H000M2_A26vueloAeropuertoLlegadaId = new int[1] ;
         H000M2_A24vueloAeropuertoSalidaId = new int[1] ;
         H000M2_A18vueloId = new int[1] ;
         H000M3_AGRID1_nRecordCount = new long[1] ;
         AV10ADVANCED_LABEL_TEMPLATE = "";
         Grid1Row = new GXWebRow();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sImgUrl = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.gx0060__default(),
            new Object[][] {
                new Object[] {
               H000M2_A58vueloFecha, H000M2_A41aerolineaId, H000M2_n41aerolineaId, H000M2_A39vueloDescuentoPorcentage, H000M2_A38vueloPrecio, H000M2_A26vueloAeropuertoLlegadaId, H000M2_A24vueloAeropuertoSalidaId, H000M2_A18vueloId
               }
               , new Object[] {
               H000M3_AGRID1_nRecordCount
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short GRID1_nEOF ;
      private short AV14cvueloDescuentoPorcentage ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short subGrid1_Backcolorstyle ;
      private short subGrid1_Titlebackstyle ;
      private short A39vueloDescuentoPorcentage ;
      private short subGrid1_Allowselection ;
      private short subGrid1_Allowhovering ;
      private short subGrid1_Allowcollapsing ;
      private short subGrid1_Collapsed ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private short subGrid1_Backstyle ;
      private int nRC_GXsfl_84 ;
      private int nGXsfl_84_idx=1 ;
      private int subGrid1_Rows ;
      private int AV6cvueloId ;
      private int AV7cvueloAeropuertoSalidaId ;
      private int AV8cvueloAeropuertoLlegadaId ;
      private int AV15caerolineaId ;
      private int AV9pvueloId ;
      private int edtavCvueloid_Enabled ;
      private int edtavCvueloid_Visible ;
      private int edtavCvueloaeropuertosalidaid_Enabled ;
      private int edtavCvueloaeropuertosalidaid_Visible ;
      private int edtavCvueloaeropuertollegadaid_Enabled ;
      private int edtavCvueloaeropuertollegadaid_Visible ;
      private int edtavCvueloprecio_Enabled ;
      private int edtavCvueloprecio_Visible ;
      private int edtavCvuelodescuentoporcentage_Enabled ;
      private int edtavCvuelodescuentoporcentage_Visible ;
      private int edtavCaerolineaid_Enabled ;
      private int edtavCaerolineaid_Visible ;
      private int edtavCvuelofecha_Enabled ;
      private int subGrid1_Titlebackcolor ;
      private int subGrid1_Allbackcolor ;
      private int A18vueloId ;
      private int A24vueloAeropuertoSalidaId ;
      private int A26vueloAeropuertoLlegadaId ;
      private int A41aerolineaId ;
      private int subGrid1_Selectedindex ;
      private int subGrid1_Selectioncolor ;
      private int subGrid1_Hoveringcolor ;
      private int subGrid1_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int idxLst ;
      private int subGrid1_Backcolor ;
      private long GRID1_nFirstRecordOnPage ;
      private long GRID1_nCurrentRecord ;
      private long GRID1_nRecordCount ;
      private decimal AV13cvueloPrecio ;
      private decimal A38vueloPrecio ;
      private String divAdvancedcontainer_Class ;
      private String bttBtntoggle_Class ;
      private String divVueloidfiltercontainer_Class ;
      private String divVueloaeropuertosalidaidfiltercontainer_Class ;
      private String divVueloaeropuertollegadaidfiltercontainer_Class ;
      private String divVuelopreciofiltercontainer_Class ;
      private String divVuelodescuentoporcentagefiltercontainer_Class ;
      private String divAerolineaidfiltercontainer_Class ;
      private String divVuelofechafiltercontainer_Class ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_84_idx="0001" ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String divMain_Internalname ;
      private String divAdvancedcontainer_Internalname ;
      private String divVueloidfiltercontainer_Internalname ;
      private String lblLblvueloidfilter_Internalname ;
      private String lblLblvueloidfilter_Jsonclick ;
      private String edtavCvueloid_Internalname ;
      private String TempTags ;
      private String edtavCvueloid_Jsonclick ;
      private String divVueloaeropuertosalidaidfiltercontainer_Internalname ;
      private String lblLblvueloaeropuertosalidaidfilter_Internalname ;
      private String lblLblvueloaeropuertosalidaidfilter_Jsonclick ;
      private String edtavCvueloaeropuertosalidaid_Internalname ;
      private String edtavCvueloaeropuertosalidaid_Jsonclick ;
      private String divVueloaeropuertollegadaidfiltercontainer_Internalname ;
      private String lblLblvueloaeropuertollegadaidfilter_Internalname ;
      private String lblLblvueloaeropuertollegadaidfilter_Jsonclick ;
      private String edtavCvueloaeropuertollegadaid_Internalname ;
      private String edtavCvueloaeropuertollegadaid_Jsonclick ;
      private String divVuelopreciofiltercontainer_Internalname ;
      private String lblLblvuelopreciofilter_Internalname ;
      private String lblLblvuelopreciofilter_Jsonclick ;
      private String edtavCvueloprecio_Internalname ;
      private String edtavCvueloprecio_Jsonclick ;
      private String divVuelodescuentoporcentagefiltercontainer_Internalname ;
      private String lblLblvuelodescuentoporcentagefilter_Internalname ;
      private String lblLblvuelodescuentoporcentagefilter_Jsonclick ;
      private String edtavCvuelodescuentoporcentage_Internalname ;
      private String edtavCvuelodescuentoporcentage_Jsonclick ;
      private String divAerolineaidfiltercontainer_Internalname ;
      private String lblLblaerolineaidfilter_Internalname ;
      private String lblLblaerolineaidfilter_Jsonclick ;
      private String edtavCaerolineaid_Internalname ;
      private String edtavCaerolineaid_Jsonclick ;
      private String divVuelofechafiltercontainer_Internalname ;
      private String lblLblvuelofechafilter_Internalname ;
      private String lblLblvuelofechafilter_Jsonclick ;
      private String edtavCvuelofecha_Internalname ;
      private String edtavCvuelofecha_Jsonclick ;
      private String divGridtable_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtntoggle_Internalname ;
      private String bttBtntoggle_Jsonclick ;
      private String sStyleString ;
      private String subGrid1_Internalname ;
      private String subGrid1_Class ;
      private String subGrid1_Linesclass ;
      private String subGrid1_Header ;
      private String edtavLinkselection_Link ;
      private String edtvueloFecha_Link ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavLinkselection_Internalname ;
      private String edtvueloId_Internalname ;
      private String edtvueloAeropuertoSalidaId_Internalname ;
      private String edtvueloAeropuertoLlegadaId_Internalname ;
      private String edtvueloPrecio_Internalname ;
      private String edtvueloDescuentoPorcentage_Internalname ;
      private String edtaerolineaId_Internalname ;
      private String edtvueloFecha_Internalname ;
      private String scmdbuf ;
      private String AV10ADVANCED_LABEL_TEMPLATE ;
      private String sGXsfl_84_fel_idx="0001" ;
      private String sImgUrl ;
      private String ROClassString ;
      private String edtvueloId_Jsonclick ;
      private String edtvueloAeropuertoSalidaId_Jsonclick ;
      private String edtvueloAeropuertoLlegadaId_Jsonclick ;
      private String edtvueloPrecio_Jsonclick ;
      private String edtvueloDescuentoPorcentage_Jsonclick ;
      private String edtaerolineaId_Jsonclick ;
      private String edtvueloFecha_Jsonclick ;
      private DateTime AV16cvueloFecha ;
      private DateTime A58vueloFecha ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool bGXsfl_84_Refreshing=false ;
      private bool n41aerolineaId ;
      private bool gxdyncontrolsrefreshing ;
      private bool returnInSub ;
      private bool AV5LinkSelection_IsBlob ;
      private String AV19Linkselection_GXI ;
      private String AV5LinkSelection ;
      private GXWebGrid Grid1Container ;
      private GXWebRow Grid1Row ;
      private GXWebColumn Grid1Column ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private DateTime[] H000M2_A58vueloFecha ;
      private int[] H000M2_A41aerolineaId ;
      private bool[] H000M2_n41aerolineaId ;
      private short[] H000M2_A39vueloDescuentoPorcentage ;
      private decimal[] H000M2_A38vueloPrecio ;
      private int[] H000M2_A26vueloAeropuertoLlegadaId ;
      private int[] H000M2_A24vueloAeropuertoSalidaId ;
      private int[] H000M2_A18vueloId ;
      private long[] H000M3_AGRID1_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private int aP0_pvueloId ;
      private GXWebForm Form ;
   }

   public class gx0060__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H000M2( IGxContext context ,
                                             int AV7cvueloAeropuertoSalidaId ,
                                             int AV8cvueloAeropuertoLlegadaId ,
                                             decimal AV13cvueloPrecio ,
                                             short AV14cvueloDescuentoPorcentage ,
                                             int AV15caerolineaId ,
                                             DateTime AV16cvueloFecha ,
                                             int A24vueloAeropuertoSalidaId ,
                                             int A26vueloAeropuertoLlegadaId ,
                                             decimal A38vueloPrecio ,
                                             short A39vueloDescuentoPorcentage ,
                                             int A41aerolineaId ,
                                             DateTime A58vueloFecha ,
                                             int AV6cvueloId )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [10] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [vueloFecha], [aerolineaId], [vueloDescuentoPorcentage], [vueloPrecio], [vueloAeropuertoLlegadaId], [vueloAeropuertoSalidaId], [vueloId]";
         sFromString = " FROM [vuelo]";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([vueloId] >= @AV6cvueloId)";
         if ( ! (0==AV7cvueloAeropuertoSalidaId) )
         {
            sWhereString = sWhereString + " and ([vueloAeropuertoSalidaId] >= @AV7cvueloAeropuertoSalidaId)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! (0==AV8cvueloAeropuertoLlegadaId) )
         {
            sWhereString = sWhereString + " and ([vueloAeropuertoLlegadaId] >= @AV8cvueloAeropuertoLlegadaId)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV13cvueloPrecio) )
         {
            sWhereString = sWhereString + " and ([vueloPrecio] >= @AV13cvueloPrecio)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! (0==AV14cvueloDescuentoPorcentage) )
         {
            sWhereString = sWhereString + " and ([vueloDescuentoPorcentage] >= @AV14cvueloDescuentoPorcentage)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! (0==AV15caerolineaId) )
         {
            sWhereString = sWhereString + " and ([aerolineaId] >= @AV15caerolineaId)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (DateTime.MinValue==AV16cvueloFecha) )
         {
            sWhereString = sWhereString + " and ([vueloFecha] >= @AV16cvueloFecha)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         sOrderString = sOrderString + " ORDER BY [vueloId]";
         scmdbuf = "SELECT " + sSelectString + sFromString + sWhereString + sOrderString + "" + " OFFSET " + "@GXPagingFrom2" + " ROWS FETCH NEXT CAST((SELECT CASE WHEN " + "@GXPagingTo2" + " > 0 THEN " + "@GXPagingTo2" + " ELSE 1e9 END) AS INTEGER) ROWS ONLY";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H000M3( IGxContext context ,
                                             int AV7cvueloAeropuertoSalidaId ,
                                             int AV8cvueloAeropuertoLlegadaId ,
                                             decimal AV13cvueloPrecio ,
                                             short AV14cvueloDescuentoPorcentage ,
                                             int AV15caerolineaId ,
                                             DateTime AV16cvueloFecha ,
                                             int A24vueloAeropuertoSalidaId ,
                                             int A26vueloAeropuertoLlegadaId ,
                                             decimal A38vueloPrecio ,
                                             short A39vueloDescuentoPorcentage ,
                                             int A41aerolineaId ,
                                             DateTime A58vueloFecha ,
                                             int AV6cvueloId )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [7] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [vuelo]";
         scmdbuf = scmdbuf + " WHERE ([vueloId] >= @AV6cvueloId)";
         if ( ! (0==AV7cvueloAeropuertoSalidaId) )
         {
            sWhereString = sWhereString + " and ([vueloAeropuertoSalidaId] >= @AV7cvueloAeropuertoSalidaId)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ! (0==AV8cvueloAeropuertoLlegadaId) )
         {
            sWhereString = sWhereString + " and ([vueloAeropuertoLlegadaId] >= @AV8cvueloAeropuertoLlegadaId)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV13cvueloPrecio) )
         {
            sWhereString = sWhereString + " and ([vueloPrecio] >= @AV13cvueloPrecio)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! (0==AV14cvueloDescuentoPorcentage) )
         {
            sWhereString = sWhereString + " and ([vueloDescuentoPorcentage] >= @AV14cvueloDescuentoPorcentage)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! (0==AV15caerolineaId) )
         {
            sWhereString = sWhereString + " and ([aerolineaId] >= @AV15caerolineaId)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (DateTime.MinValue==AV16cvueloFecha) )
         {
            sWhereString = sWhereString + " and ([vueloFecha] >= @AV16cvueloFecha)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + "";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H000M2(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (decimal)dynConstraints[2] , (short)dynConstraints[3] , (int)dynConstraints[4] , (DateTime)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] , (decimal)dynConstraints[8] , (short)dynConstraints[9] , (int)dynConstraints[10] , (DateTime)dynConstraints[11] , (int)dynConstraints[12] );
               case 1 :
                     return conditional_H000M3(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (decimal)dynConstraints[2] , (short)dynConstraints[3] , (int)dynConstraints[4] , (DateTime)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] , (decimal)dynConstraints[8] , (short)dynConstraints[9] , (int)dynConstraints[10] , (DateTime)dynConstraints[11] , (int)dynConstraints[12] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH000M2 ;
          prmH000M2 = new Object[] {
          new Object[] {"@AV6cvueloId",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7cvueloAeropuertoSalidaId",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8cvueloAeropuertoLlegadaId",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13cvueloPrecio",SqlDbType.Decimal,9,2} ,
          new Object[] {"@AV14cvueloDescuentoPorcentage",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV15caerolineaId",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16cvueloFecha",SqlDbType.DateTime,8,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH000M3 ;
          prmH000M3 = new Object[] {
          new Object[] {"@AV6cvueloId",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7cvueloAeropuertoSalidaId",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8cvueloAeropuertoLlegadaId",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13cvueloPrecio",SqlDbType.Decimal,9,2} ,
          new Object[] {"@AV14cvueloDescuentoPorcentage",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV15caerolineaId",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16cvueloFecha",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H000M2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000M2,11, GxCacheFrequency.OFF ,false,false )
             ,new CursorDef("H000M3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000M3,1, GxCacheFrequency.OFF ,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[13]);
                }
                return;
       }
    }

 }

}
