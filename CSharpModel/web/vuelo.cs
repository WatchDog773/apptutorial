/*
               File: vuelo
        Description: vuelo
             Author: GeneXus C# Generator version 16_0_6-136889
       Generated on: 12/5/2019 12:9:35.78
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class vuelo : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_15") == 0 )
         {
            A18vueloId = (int)(NumberUtil.Val( GetNextPar( ), "."));
            AssignAttri("", false, "A18vueloId", StringUtil.LTrimStr( (decimal)(A18vueloId), 6, 0));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_15( A18vueloId) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_9") == 0 )
         {
            A24vueloAeropuertoSalidaId = (int)(NumberUtil.Val( GetNextPar( ), "."));
            AssignAttri("", false, "A24vueloAeropuertoSalidaId", StringUtil.LTrimStr( (decimal)(A24vueloAeropuertoSalidaId), 6, 0));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_9( A24vueloAeropuertoSalidaId) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_11") == 0 )
         {
            A32vueloAeropuertoSalidaPaisId = (int)(NumberUtil.Val( GetNextPar( ), "."));
            AssignAttri("", false, "A32vueloAeropuertoSalidaPaisId", StringUtil.LTrimStr( (decimal)(A32vueloAeropuertoSalidaPaisId), 6, 0));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_11( A32vueloAeropuertoSalidaPaisId) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_12") == 0 )
         {
            A32vueloAeropuertoSalidaPaisId = (int)(NumberUtil.Val( GetNextPar( ), "."));
            AssignAttri("", false, "A32vueloAeropuertoSalidaPaisId", StringUtil.LTrimStr( (decimal)(A32vueloAeropuertoSalidaPaisId), 6, 0));
            A30vueloAeropuertoSalidaCiudadId = (int)(NumberUtil.Val( GetNextPar( ), "."));
            AssignAttri("", false, "A30vueloAeropuertoSalidaCiudadId", StringUtil.LTrimStr( (decimal)(A30vueloAeropuertoSalidaCiudadId), 6, 0));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_12( A32vueloAeropuertoSalidaPaisId, A30vueloAeropuertoSalidaCiudadId) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_10") == 0 )
         {
            A26vueloAeropuertoLlegadaId = (int)(NumberUtil.Val( GetNextPar( ), "."));
            AssignAttri("", false, "A26vueloAeropuertoLlegadaId", StringUtil.LTrimStr( (decimal)(A26vueloAeropuertoLlegadaId), 6, 0));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_10( A26vueloAeropuertoLlegadaId) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_13") == 0 )
         {
            A34vueloAeropuertoLlegadaPaisId = (int)(NumberUtil.Val( GetNextPar( ), "."));
            AssignAttri("", false, "A34vueloAeropuertoLlegadaPaisId", StringUtil.LTrimStr( (decimal)(A34vueloAeropuertoLlegadaPaisId), 6, 0));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_13( A34vueloAeropuertoLlegadaPaisId) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_14") == 0 )
         {
            A34vueloAeropuertoLlegadaPaisId = (int)(NumberUtil.Val( GetNextPar( ), "."));
            AssignAttri("", false, "A34vueloAeropuertoLlegadaPaisId", StringUtil.LTrimStr( (decimal)(A34vueloAeropuertoLlegadaPaisId), 6, 0));
            A36vueloAeropuertoLlegadaCiudadId = (int)(NumberUtil.Val( GetNextPar( ), "."));
            AssignAttri("", false, "A36vueloAeropuertoLlegadaCiudadId", StringUtil.LTrimStr( (decimal)(A36vueloAeropuertoLlegadaCiudadId), 6, 0));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_14( A34vueloAeropuertoLlegadaPaisId, A36vueloAeropuertoLlegadaCiudadId) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_8") == 0 )
         {
            A41aerolineaId = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n41aerolineaId = false;
            AssignAttri("", false, "A41aerolineaId", StringUtil.LTrimStr( (decimal)(A41aerolineaId), 6, 0));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_8( A41aerolineaId) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridvuelo_asiento") == 0 )
         {
            nRC_GXsfl_143 = (int)(NumberUtil.Val( GetNextPar( ), "."));
            nGXsfl_143_idx = (int)(NumberUtil.Val( GetNextPar( ), "."));
            sGXsfl_143_idx = GetNextPar( );
            Gx_mode = GetNextPar( );
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxnrGridvuelo_asiento_newrow( ) ;
            return  ;
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_web_controls( ) ;
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 16_0_6-136889", 0) ;
            Form.Meta.addItem("description", "vuelo", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtvueloId_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("Carmine");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public vuelo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public vuelo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbvueloAsientoChar = new GXCombobox();
         cmbvueloAsientoLocalizacion = new GXCombobox();
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdmasterpage", "GeneXus.Programs.rwdmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "no-store");
               }
               if ( ! context.WillRedirect( ) )
               {
                  AddString( context.getJSONResponse( )) ;
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            DrawControls( ) ;
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void DrawControls( )
      {
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "WWAdvancedContainer", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divTitlecontainer_Internalname, 1, 0, "px", 0, "px", "TableTop", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         /* Text block */
         GxWebStd.gx_label_ctrl( context, lblTitle_Internalname, "vuelo", "", "", lblTitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         ClassString = "ErrorViewer";
         StyleString = "";
         GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divFormcontainer_Internalname, 1, 0, "px", 0, "px", "FormContainer", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divToolbarcell_Internalname, 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3 ToolbarCellClass", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group ActionGroup", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "btn-group", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
         ClassString = "BtnFirst";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_first_Internalname, "", "", bttBtn_first_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_first_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", TempTags, "", context.GetButtonType( ), "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
         ClassString = "BtnPrevious";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_previous_Internalname, "", "", bttBtn_previous_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_previous_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", TempTags, "", context.GetButtonType( ), "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
         ClassString = "BtnNext";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_next_Internalname, "", "", bttBtn_next_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_next_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", TempTags, "", context.GetButtonType( ), "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
         ClassString = "BtnLast";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_last_Internalname, "", "", bttBtn_last_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_last_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ELAST."+"'", TempTags, "", context.GetButtonType( ), "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
         ClassString = "BtnSelect";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_select_Internalname, "", "Select", bttBtn_select_Jsonclick, 4, "Select", "", StyleString, ClassString, bttBtn_select_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", TempTags, "gx.popup.openPrompt('"+"gx0060.aspx"+"',["+"{Ctrl:gx.dom.el('"+"VUELOID"+"'), id:'"+"VUELOID"+"'"+",IOType:'out',isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");"+"return false;", 2, "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCellAdvanced", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtvueloId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtvueloId_Internalname, "Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtvueloId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A18vueloId), 6, 0, ".", "")), ((edtvueloId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A18vueloId), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A18vueloId), "ZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtvueloId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtvueloId_Enabled, 0, "number", "1", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "", "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtvueloFecha_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtvueloFecha_Internalname, "Fecha", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
         context.WriteHtmlText( "<div id=\""+edtvueloFecha_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
         GxWebStd.gx_single_line_edit( context, edtvueloFecha_Internalname, context.localUtil.Format(A58vueloFecha, "99/99/99"), context.localUtil.Format( A58vueloFecha, "99/99/99"), TempTags+" onchange=\""+"gx.date.valid_date(this, 8,'MDY',0,12,'eng',false,0);"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'MDY',0,12,'eng',false,0);"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtvueloFecha_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtvueloFecha_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "", "HLP_vuelo.htm");
         GxWebStd.gx_bitmap( context, edtvueloFecha_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtvueloFecha_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", "", 1, false, false, "", "HLP_vuelo.htm");
         context.WriteHtmlTextNl( "</div>") ;
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtvueloAeropuertoSalidaId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtvueloAeropuertoSalidaId_Internalname, "Aeropuerto Salida Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtvueloAeropuertoSalidaId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A24vueloAeropuertoSalidaId), 6, 0, ".", "")), ((edtvueloAeropuertoSalidaId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A24vueloAeropuertoSalidaId), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A24vueloAeropuertoSalidaId), "ZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtvueloAeropuertoSalidaId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtvueloAeropuertoSalidaId_Enabled, 0, "number", "1", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "", "HLP_vuelo.htm");
         /* Static images/pictures */
         ClassString = "gx-prompt Image";
         StyleString = "";
         sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
         GxWebStd.gx_bitmap( context, imgprompt_24_Internalname, sImgUrl, imgprompt_24_Link, "", "", context.GetTheme( ), imgprompt_24_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtvueloAeropuertoSalidaNombre_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtvueloAeropuertoSalidaNombre_Internalname, "Aeropuerto Salida Nombre", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtvueloAeropuertoSalidaNombre_Internalname, StringUtil.RTrim( A25vueloAeropuertoSalidaNombre), StringUtil.RTrim( context.localUtil.Format( A25vueloAeropuertoSalidaNombre, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtvueloAeropuertoSalidaNombre_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtvueloAeropuertoSalidaNombre_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "Name", "left", true, "", "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtvueloAeropuertoSalidaPaisId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtvueloAeropuertoSalidaPaisId_Internalname, "Pais de Salida Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtvueloAeropuertoSalidaPaisId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A32vueloAeropuertoSalidaPaisId), 6, 0, ".", "")), ((edtvueloAeropuertoSalidaPaisId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A32vueloAeropuertoSalidaPaisId), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A32vueloAeropuertoSalidaPaisId), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtvueloAeropuertoSalidaPaisId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtvueloAeropuertoSalidaPaisId_Enabled, 0, "number", "1", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "", "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtvueloAeropuertoSalidaPaisNombr_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtvueloAeropuertoSalidaPaisNombr_Internalname, "Pais de Salida Nombre", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtvueloAeropuertoSalidaPaisNombr_Internalname, StringUtil.RTrim( A33vueloAeropuertoSalidaPaisNombr), StringUtil.RTrim( context.localUtil.Format( A33vueloAeropuertoSalidaPaisNombr, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtvueloAeropuertoSalidaPaisNombr_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtvueloAeropuertoSalidaPaisNombr_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "Name", "left", true, "", "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtvueloAeropuertoSalidaCiudadId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtvueloAeropuertoSalidaCiudadId_Internalname, "Ciudad de Salida Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtvueloAeropuertoSalidaCiudadId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A30vueloAeropuertoSalidaCiudadId), 6, 0, ".", "")), ((edtvueloAeropuertoSalidaCiudadId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A30vueloAeropuertoSalidaCiudadId), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A30vueloAeropuertoSalidaCiudadId), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtvueloAeropuertoSalidaCiudadId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtvueloAeropuertoSalidaCiudadId_Enabled, 0, "number", "1", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "", "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtvueloAeropuertoSalidaCiudadNom_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtvueloAeropuertoSalidaCiudadNom_Internalname, "Ciudad de Salida Nombre", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtvueloAeropuertoSalidaCiudadNom_Internalname, StringUtil.RTrim( A31vueloAeropuertoSalidaCiudadNom), StringUtil.RTrim( context.localUtil.Format( A31vueloAeropuertoSalidaCiudadNom, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtvueloAeropuertoSalidaCiudadNom_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtvueloAeropuertoSalidaCiudadNom_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "Name", "left", true, "", "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtvueloAeropuertoLlegadaId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtvueloAeropuertoLlegadaId_Internalname, "Aeropuerto Llegada Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtvueloAeropuertoLlegadaId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A26vueloAeropuertoLlegadaId), 6, 0, ".", "")), ((edtvueloAeropuertoLlegadaId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A26vueloAeropuertoLlegadaId), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A26vueloAeropuertoLlegadaId), "ZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtvueloAeropuertoLlegadaId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtvueloAeropuertoLlegadaId_Enabled, 0, "number", "1", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "", "HLP_vuelo.htm");
         /* Static images/pictures */
         ClassString = "gx-prompt Image";
         StyleString = "";
         sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
         GxWebStd.gx_bitmap( context, imgprompt_26_Internalname, sImgUrl, imgprompt_26_Link, "", "", context.GetTheme( ), imgprompt_26_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtvueloAeropuertoLlegadaNombre_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtvueloAeropuertoLlegadaNombre_Internalname, "Aeropuerto Llegada Nombre", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtvueloAeropuertoLlegadaNombre_Internalname, StringUtil.RTrim( A27vueloAeropuertoLlegadaNombre), StringUtil.RTrim( context.localUtil.Format( A27vueloAeropuertoLlegadaNombre, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtvueloAeropuertoLlegadaNombre_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtvueloAeropuertoLlegadaNombre_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "Name", "left", true, "", "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtvueloAeropuertoLlegadaPaisId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtvueloAeropuertoLlegadaPaisId_Internalname, "Pais Llegada Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtvueloAeropuertoLlegadaPaisId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A34vueloAeropuertoLlegadaPaisId), 6, 0, ".", "")), ((edtvueloAeropuertoLlegadaPaisId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A34vueloAeropuertoLlegadaPaisId), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A34vueloAeropuertoLlegadaPaisId), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtvueloAeropuertoLlegadaPaisId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtvueloAeropuertoLlegadaPaisId_Enabled, 0, "number", "1", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "", "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtvueloAeropuertoLlegadaPaisNomb_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtvueloAeropuertoLlegadaPaisNomb_Internalname, "Pais Llegada Nombre", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtvueloAeropuertoLlegadaPaisNomb_Internalname, StringUtil.RTrim( A35vueloAeropuertoLlegadaPaisNomb), StringUtil.RTrim( context.localUtil.Format( A35vueloAeropuertoLlegadaPaisNomb, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtvueloAeropuertoLlegadaPaisNomb_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtvueloAeropuertoLlegadaPaisNomb_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "Name", "left", true, "", "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtvueloAeropuertoLlegadaCiudadId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtvueloAeropuertoLlegadaCiudadId_Internalname, "Ciudad Llegada Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtvueloAeropuertoLlegadaCiudadId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A36vueloAeropuertoLlegadaCiudadId), 6, 0, ".", "")), ((edtvueloAeropuertoLlegadaCiudadId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A36vueloAeropuertoLlegadaCiudadId), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A36vueloAeropuertoLlegadaCiudadId), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtvueloAeropuertoLlegadaCiudadId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtvueloAeropuertoLlegadaCiudadId_Enabled, 0, "number", "1", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "", "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtvueloAeropuertoLlegadaCiudadNo_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtvueloAeropuertoLlegadaCiudadNo_Internalname, "Ciudad Llegada Nombre", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtvueloAeropuertoLlegadaCiudadNo_Internalname, StringUtil.RTrim( A37vueloAeropuertoLlegadaCiudadNo), StringUtil.RTrim( context.localUtil.Format( A37vueloAeropuertoLlegadaCiudadNo, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtvueloAeropuertoLlegadaCiudadNo_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtvueloAeropuertoLlegadaCiudadNo_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "Name", "left", true, "", "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtvueloPrecio_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtvueloPrecio_Internalname, "Precio", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtvueloPrecio_Internalname, StringUtil.LTrim( StringUtil.NToC( A38vueloPrecio, 9, 2, ".", "")), ((edtvueloPrecio_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A38vueloPrecio, "ZZZZZ9.99")) : context.localUtil.Format( A38vueloPrecio, "ZZZZZ9.99")), TempTags+" onchange=\""+"gx.num.valid_decimal( this, ',','.','2');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_decimal( this, ',','.','2');"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtvueloPrecio_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtvueloPrecio_Enabled, 0, "text", "", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "Price", "right", false, "", "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtvueloDescuentoPorcentage_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtvueloDescuentoPorcentage_Internalname, "Descuento Porcentage", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtvueloDescuentoPorcentage_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A39vueloDescuentoPorcentage), 3, 0, ".", "")), ((edtvueloDescuentoPorcentage_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A39vueloDescuentoPorcentage), "ZZ9")) : context.localUtil.Format( (decimal)(A39vueloDescuentoPorcentage), "ZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtvueloDescuentoPorcentage_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtvueloDescuentoPorcentage_Enabled, 0, "number", "1", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Porcentage", "right", false, "", "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtaerolineaId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtaerolineaId_Internalname, "Aerolinea Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtaerolineaId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A41aerolineaId), 6, 0, ".", "")), ((edtaerolineaId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A41aerolineaId), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A41aerolineaId), "ZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtaerolineaId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtaerolineaId_Enabled, 0, "number", "1", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "", "HLP_vuelo.htm");
         /* Static images/pictures */
         ClassString = "gx-prompt Image";
         StyleString = "";
         sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
         GxWebStd.gx_bitmap( context, imgprompt_41_Internalname, sImgUrl, imgprompt_41_Link, "", "", context.GetTheme( ), imgprompt_41_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtaerolineaNombre_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtaerolineaNombre_Internalname, "Nombre Aerolinea", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtaerolineaNombre_Internalname, StringUtil.RTrim( A42aerolineaNombre), StringUtil.RTrim( context.localUtil.Format( A42aerolineaNombre, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtaerolineaNombre_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtaerolineaNombre_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "Name", "left", true, "", "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtaerolineaDescuentoPorcentage_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtaerolineaDescuentoPorcentage_Internalname, "Aerolinea Descuento Porcentage", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtaerolineaDescuentoPorcentage_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A43aerolineaDescuentoPorcentage), 3, 0, ".", "")), ((edtaerolineaDescuentoPorcentage_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A43aerolineaDescuentoPorcentage), "ZZ9")) : context.localUtil.Format( (decimal)(A43aerolineaDescuentoPorcentage), "ZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtaerolineaDescuentoPorcentage_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtaerolineaDescuentoPorcentage_Enabled, 0, "number", "1", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Porcentage", "right", false, "", "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtvueloPrecioFinal_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtvueloPrecioFinal_Internalname, "Precio Final", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtvueloPrecioFinal_Internalname, StringUtil.LTrim( StringUtil.NToC( A40vueloPrecioFinal, 9, 2, ".", "")), ((edtvueloPrecioFinal_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A40vueloPrecioFinal, "ZZZZZ9.99")) : context.localUtil.Format( A40vueloPrecioFinal, "ZZZZZ9.99")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtvueloPrecioFinal_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtvueloPrecioFinal_Enabled, 0, "text", "", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "Price", "right", false, "", "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtvueloCapacidad_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtvueloCapacidad_Internalname, "Capacidad", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtvueloCapacidad_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A47vueloCapacidad), 4, 0, ".", "")), ((edtvueloCapacidad_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A47vueloCapacidad), "ZZZ9")) : context.localUtil.Format( (decimal)(A47vueloCapacidad), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtvueloCapacidad_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtvueloCapacidad_Enabled, 0, "number", "1", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "", "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 LevelTable", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divAsientotable_Internalname, 1, 0, "px", 0, "px", "LevelTable", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Text block */
         GxWebStd.gx_label_ctrl( context, lblTitleasiento_Internalname, "Asiento", "", "", lblTitleasiento_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         gxdraw_Gridvuelo_asiento( ) ;
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 151,'',false,'',0)\"";
         ClassString = "BtnEnter";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirm", bttBtn_enter_Jsonclick, 5, "Confirm", "", StyleString, ClassString, bttBtn_enter_Visible, bttBtn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 153,'',false,'',0)\"";
         ClassString = "BtnCancel";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Cancel", bttBtn_cancel_Jsonclick, 1, "Cancel", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 155,'',false,'',0)\"";
         ClassString = "BtnDelete";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Delete", bttBtn_delete_Jsonclick, 5, "Delete", "", StyleString, ClassString, bttBtn_delete_Visible, bttBtn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_vuelo.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "Center", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
      }

      protected void gxdraw_Gridvuelo_asiento( )
      {
         /*  Grid Control  */
         Gridvuelo_asientoContainer.AddObjectProperty("GridName", "Gridvuelo_asiento");
         Gridvuelo_asientoContainer.AddObjectProperty("Header", subGridvuelo_asiento_Header);
         Gridvuelo_asientoContainer.AddObjectProperty("Class", "Grid");
         Gridvuelo_asientoContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         Gridvuelo_asientoContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Gridvuelo_asientoContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridvuelo_asiento_Backcolorstyle), 1, 0, ".", "")));
         Gridvuelo_asientoContainer.AddObjectProperty("CmpContext", "");
         Gridvuelo_asientoContainer.AddObjectProperty("InMasterPage", "false");
         Gridvuelo_asientoColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridvuelo_asientoColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A44vueloAsientoId), 6, 0, ".", "")));
         Gridvuelo_asientoColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtvueloAsientoId_Enabled), 5, 0, ".", "")));
         Gridvuelo_asientoContainer.AddColumnProperties(Gridvuelo_asientoColumn);
         Gridvuelo_asientoColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridvuelo_asientoColumn.AddObjectProperty("Value", StringUtil.RTrim( A45vueloAsientoChar));
         Gridvuelo_asientoColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbvueloAsientoChar.Enabled), 5, 0, ".", "")));
         Gridvuelo_asientoContainer.AddColumnProperties(Gridvuelo_asientoColumn);
         Gridvuelo_asientoColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridvuelo_asientoColumn.AddObjectProperty("Value", StringUtil.RTrim( A46vueloAsientoLocalizacion));
         Gridvuelo_asientoColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbvueloAsientoLocalizacion.Enabled), 5, 0, ".", "")));
         Gridvuelo_asientoContainer.AddColumnProperties(Gridvuelo_asientoColumn);
         Gridvuelo_asientoContainer.AddObjectProperty("Selectedindex", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridvuelo_asiento_Selectedindex), 4, 0, ".", "")));
         Gridvuelo_asientoContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridvuelo_asiento_Allowselection), 1, 0, ".", "")));
         Gridvuelo_asientoContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridvuelo_asiento_Selectioncolor), 9, 0, ".", "")));
         Gridvuelo_asientoContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridvuelo_asiento_Allowhovering), 1, 0, ".", "")));
         Gridvuelo_asientoContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridvuelo_asiento_Hoveringcolor), 9, 0, ".", "")));
         Gridvuelo_asientoContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridvuelo_asiento_Allowcollapsing), 1, 0, ".", "")));
         Gridvuelo_asientoContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridvuelo_asiento_Collapsed), 1, 0, ".", "")));
         nGXsfl_143_idx = 0;
         if ( ( nKeyPressed == 1 ) && ( AnyError == 0 ) )
         {
            /* Enter key processing. */
            nBlankRcdCount9 = 5;
            if ( ! IsIns( ) )
            {
               /* Display confirmed (stored) records */
               nRcdExists_9 = 1;
               ScanStart059( ) ;
               while ( RcdFound9 != 0 )
               {
                  init_level_properties9( ) ;
                  getByPrimaryKey059( ) ;
                  AddRow059( ) ;
                  ScanNext059( ) ;
               }
               ScanEnd059( ) ;
               nBlankRcdCount9 = 5;
            }
         }
         else if ( ( nKeyPressed == 3 ) || ( nKeyPressed == 4 ) || ( ( nKeyPressed == 1 ) && ( AnyError != 0 ) ) )
         {
            /* Button check  or addlines. */
            B47vueloCapacidad = A47vueloCapacidad;
            n47vueloCapacidad = false;
            AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
            standaloneNotModal059( ) ;
            standaloneModal059( ) ;
            sMode9 = Gx_mode;
            while ( nGXsfl_143_idx < nRC_GXsfl_143 )
            {
               bGXsfl_143_Refreshing = true;
               ReadRow059( ) ;
               edtvueloAsientoId_Enabled = (int)(context.localUtil.CToN( cgiGet( "VUELOASIENTOID_"+sGXsfl_143_idx+"Enabled"), ".", ","));
               AssignProp("", false, edtvueloAsientoId_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtvueloAsientoId_Enabled), 5, 0), !bGXsfl_143_Refreshing);
               cmbvueloAsientoChar.Enabled = (int)(context.localUtil.CToN( cgiGet( "VUELOASIENTOCHAR_"+sGXsfl_143_idx+"Enabled"), ".", ","));
               AssignProp("", false, cmbvueloAsientoChar_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(cmbvueloAsientoChar.Enabled), 5, 0), !bGXsfl_143_Refreshing);
               cmbvueloAsientoLocalizacion.Enabled = (int)(context.localUtil.CToN( cgiGet( "VUELOASIENTOLOCALIZACION_"+sGXsfl_143_idx+"Enabled"), ".", ","));
               AssignProp("", false, cmbvueloAsientoLocalizacion_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(cmbvueloAsientoLocalizacion.Enabled), 5, 0), !bGXsfl_143_Refreshing);
               if ( ( nRcdExists_9 == 0 ) && ! IsIns( ) )
               {
                  Gx_mode = "INS";
                  AssignAttri("", false, "Gx_mode", Gx_mode);
                  standaloneModal059( ) ;
               }
               SendRow059( ) ;
               bGXsfl_143_Refreshing = false;
            }
            Gx_mode = sMode9;
            AssignAttri("", false, "Gx_mode", Gx_mode);
            A47vueloCapacidad = B47vueloCapacidad;
            n47vueloCapacidad = false;
            AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
         }
         else
         {
            /* Get or get-alike key processing. */
            nBlankRcdCount9 = 5;
            nRcdExists_9 = 1;
            if ( ! IsIns( ) )
            {
               ScanStart059( ) ;
               while ( RcdFound9 != 0 )
               {
                  sGXsfl_143_idx = StringUtil.PadL( StringUtil.LTrimStr( (decimal)(nGXsfl_143_idx+1), 4, 0), 4, "0");
                  SubsflControlProps_1439( ) ;
                  init_level_properties9( ) ;
                  standaloneNotModal059( ) ;
                  getByPrimaryKey059( ) ;
                  standaloneModal059( ) ;
                  AddRow059( ) ;
                  ScanNext059( ) ;
               }
               ScanEnd059( ) ;
            }
         }
         /* Initialize fields for 'new' records and send them. */
         sMode9 = Gx_mode;
         Gx_mode = "INS";
         AssignAttri("", false, "Gx_mode", Gx_mode);
         sGXsfl_143_idx = StringUtil.PadL( StringUtil.LTrimStr( (decimal)(nGXsfl_143_idx+1), 4, 0), 4, "0");
         SubsflControlProps_1439( ) ;
         InitAll059( ) ;
         init_level_properties9( ) ;
         B47vueloCapacidad = A47vueloCapacidad;
         n47vueloCapacidad = false;
         AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
         nRcdExists_9 = 0;
         nIsMod_9 = 0;
         nRcdDeleted_9 = 0;
         nBlankRcdCount9 = (short)(nBlankRcdUsr9+nBlankRcdCount9);
         fRowAdded = 0;
         while ( nBlankRcdCount9 > 0 )
         {
            standaloneNotModal059( ) ;
            standaloneModal059( ) ;
            AddRow059( ) ;
            if ( ( nKeyPressed == 4 ) && ( fRowAdded == 0 ) )
            {
               fRowAdded = 1;
               GX_FocusControl = edtvueloAsientoId_Internalname;
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nBlankRcdCount9 = (short)(nBlankRcdCount9-1);
         }
         Gx_mode = sMode9;
         AssignAttri("", false, "Gx_mode", Gx_mode);
         A47vueloCapacidad = B47vueloCapacidad;
         n47vueloCapacidad = false;
         AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
         sStyleString = "";
         context.WriteHtmlText( "<div id=\""+"Gridvuelo_asientoContainer"+"Div\" "+sStyleString+">"+"</div>") ;
         context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridvuelo_asiento", Gridvuelo_asientoContainer);
         if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
         {
            GxWebStd.gx_hidden_field( context, "Gridvuelo_asientoContainerData", Gridvuelo_asientoContainer.ToJavascriptSource());
         }
         if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
         {
            GxWebStd.gx_hidden_field( context, "Gridvuelo_asientoContainerData"+"V", Gridvuelo_asientoContainer.GridValuesHidden());
         }
         else
         {
            context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Gridvuelo_asientoContainerData"+"V"+"\" value='"+Gridvuelo_asientoContainer.GridValuesHidden()+"'/>") ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read saved values. */
            Z18vueloId = (int)(context.localUtil.CToN( cgiGet( "Z18vueloId"), ".", ","));
            Z58vueloFecha = context.localUtil.CToD( cgiGet( "Z58vueloFecha"), 0);
            Z38vueloPrecio = context.localUtil.CToN( cgiGet( "Z38vueloPrecio"), ".", ",");
            Z39vueloDescuentoPorcentage = (short)(context.localUtil.CToN( cgiGet( "Z39vueloDescuentoPorcentage"), ".", ","));
            Z41aerolineaId = (int)(context.localUtil.CToN( cgiGet( "Z41aerolineaId"), ".", ","));
            Z24vueloAeropuertoSalidaId = (int)(context.localUtil.CToN( cgiGet( "Z24vueloAeropuertoSalidaId"), ".", ","));
            Z26vueloAeropuertoLlegadaId = (int)(context.localUtil.CToN( cgiGet( "Z26vueloAeropuertoLlegadaId"), ".", ","));
            O47vueloCapacidad = (short)(context.localUtil.CToN( cgiGet( "O47vueloCapacidad"), ".", ","));
            IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ".", ","));
            IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ".", ","));
            Gx_mode = cgiGet( "Mode");
            nRC_GXsfl_143 = (int)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_143"), ".", ","));
            Gx_mode = cgiGet( "vMODE");
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtvueloId_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtvueloId_Internalname), ".", ",") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "VUELOID");
               AnyError = 1;
               GX_FocusControl = edtvueloId_Internalname;
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A18vueloId = 0;
               AssignAttri("", false, "A18vueloId", StringUtil.LTrimStr( (decimal)(A18vueloId), 6, 0));
            }
            else
            {
               A18vueloId = (int)(context.localUtil.CToN( cgiGet( edtvueloId_Internalname), ".", ","));
               AssignAttri("", false, "A18vueloId", StringUtil.LTrimStr( (decimal)(A18vueloId), 6, 0));
            }
            if ( context.localUtil.VCDate( cgiGet( edtvueloFecha_Internalname), 1) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"vuelo Fecha"}), 1, "VUELOFECHA");
               AnyError = 1;
               GX_FocusControl = edtvueloFecha_Internalname;
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A58vueloFecha = DateTime.MinValue;
               AssignAttri("", false, "A58vueloFecha", context.localUtil.Format(A58vueloFecha, "99/99/99"));
            }
            else
            {
               A58vueloFecha = context.localUtil.CToD( cgiGet( edtvueloFecha_Internalname), 1);
               AssignAttri("", false, "A58vueloFecha", context.localUtil.Format(A58vueloFecha, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtvueloAeropuertoSalidaId_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtvueloAeropuertoSalidaId_Internalname), ".", ",") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "VUELOAEROPUERTOSALIDAID");
               AnyError = 1;
               GX_FocusControl = edtvueloAeropuertoSalidaId_Internalname;
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A24vueloAeropuertoSalidaId = 0;
               AssignAttri("", false, "A24vueloAeropuertoSalidaId", StringUtil.LTrimStr( (decimal)(A24vueloAeropuertoSalidaId), 6, 0));
            }
            else
            {
               A24vueloAeropuertoSalidaId = (int)(context.localUtil.CToN( cgiGet( edtvueloAeropuertoSalidaId_Internalname), ".", ","));
               AssignAttri("", false, "A24vueloAeropuertoSalidaId", StringUtil.LTrimStr( (decimal)(A24vueloAeropuertoSalidaId), 6, 0));
            }
            A25vueloAeropuertoSalidaNombre = cgiGet( edtvueloAeropuertoSalidaNombre_Internalname);
            AssignAttri("", false, "A25vueloAeropuertoSalidaNombre", A25vueloAeropuertoSalidaNombre);
            A32vueloAeropuertoSalidaPaisId = (int)(context.localUtil.CToN( cgiGet( edtvueloAeropuertoSalidaPaisId_Internalname), ".", ","));
            AssignAttri("", false, "A32vueloAeropuertoSalidaPaisId", StringUtil.LTrimStr( (decimal)(A32vueloAeropuertoSalidaPaisId), 6, 0));
            A33vueloAeropuertoSalidaPaisNombr = cgiGet( edtvueloAeropuertoSalidaPaisNombr_Internalname);
            AssignAttri("", false, "A33vueloAeropuertoSalidaPaisNombr", A33vueloAeropuertoSalidaPaisNombr);
            A30vueloAeropuertoSalidaCiudadId = (int)(context.localUtil.CToN( cgiGet( edtvueloAeropuertoSalidaCiudadId_Internalname), ".", ","));
            AssignAttri("", false, "A30vueloAeropuertoSalidaCiudadId", StringUtil.LTrimStr( (decimal)(A30vueloAeropuertoSalidaCiudadId), 6, 0));
            A31vueloAeropuertoSalidaCiudadNom = cgiGet( edtvueloAeropuertoSalidaCiudadNom_Internalname);
            AssignAttri("", false, "A31vueloAeropuertoSalidaCiudadNom", A31vueloAeropuertoSalidaCiudadNom);
            if ( ( ( context.localUtil.CToN( cgiGet( edtvueloAeropuertoLlegadaId_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtvueloAeropuertoLlegadaId_Internalname), ".", ",") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "VUELOAEROPUERTOLLEGADAID");
               AnyError = 1;
               GX_FocusControl = edtvueloAeropuertoLlegadaId_Internalname;
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A26vueloAeropuertoLlegadaId = 0;
               AssignAttri("", false, "A26vueloAeropuertoLlegadaId", StringUtil.LTrimStr( (decimal)(A26vueloAeropuertoLlegadaId), 6, 0));
            }
            else
            {
               A26vueloAeropuertoLlegadaId = (int)(context.localUtil.CToN( cgiGet( edtvueloAeropuertoLlegadaId_Internalname), ".", ","));
               AssignAttri("", false, "A26vueloAeropuertoLlegadaId", StringUtil.LTrimStr( (decimal)(A26vueloAeropuertoLlegadaId), 6, 0));
            }
            A27vueloAeropuertoLlegadaNombre = cgiGet( edtvueloAeropuertoLlegadaNombre_Internalname);
            AssignAttri("", false, "A27vueloAeropuertoLlegadaNombre", A27vueloAeropuertoLlegadaNombre);
            A34vueloAeropuertoLlegadaPaisId = (int)(context.localUtil.CToN( cgiGet( edtvueloAeropuertoLlegadaPaisId_Internalname), ".", ","));
            AssignAttri("", false, "A34vueloAeropuertoLlegadaPaisId", StringUtil.LTrimStr( (decimal)(A34vueloAeropuertoLlegadaPaisId), 6, 0));
            A35vueloAeropuertoLlegadaPaisNomb = cgiGet( edtvueloAeropuertoLlegadaPaisNomb_Internalname);
            AssignAttri("", false, "A35vueloAeropuertoLlegadaPaisNomb", A35vueloAeropuertoLlegadaPaisNomb);
            A36vueloAeropuertoLlegadaCiudadId = (int)(context.localUtil.CToN( cgiGet( edtvueloAeropuertoLlegadaCiudadId_Internalname), ".", ","));
            AssignAttri("", false, "A36vueloAeropuertoLlegadaCiudadId", StringUtil.LTrimStr( (decimal)(A36vueloAeropuertoLlegadaCiudadId), 6, 0));
            A37vueloAeropuertoLlegadaCiudadNo = cgiGet( edtvueloAeropuertoLlegadaCiudadNo_Internalname);
            AssignAttri("", false, "A37vueloAeropuertoLlegadaCiudadNo", A37vueloAeropuertoLlegadaCiudadNo);
            if ( ( ( context.localUtil.CToN( cgiGet( edtvueloPrecio_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtvueloPrecio_Internalname), ".", ",") > 999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "VUELOPRECIO");
               AnyError = 1;
               GX_FocusControl = edtvueloPrecio_Internalname;
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A38vueloPrecio = 0;
               AssignAttri("", false, "A38vueloPrecio", StringUtil.LTrimStr( A38vueloPrecio, 9, 2));
            }
            else
            {
               A38vueloPrecio = context.localUtil.CToN( cgiGet( edtvueloPrecio_Internalname), ".", ",");
               AssignAttri("", false, "A38vueloPrecio", StringUtil.LTrimStr( A38vueloPrecio, 9, 2));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtvueloDescuentoPorcentage_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtvueloDescuentoPorcentage_Internalname), ".", ",") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "VUELODESCUENTOPORCENTAGE");
               AnyError = 1;
               GX_FocusControl = edtvueloDescuentoPorcentage_Internalname;
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A39vueloDescuentoPorcentage = 0;
               AssignAttri("", false, "A39vueloDescuentoPorcentage", StringUtil.LTrimStr( (decimal)(A39vueloDescuentoPorcentage), 3, 0));
            }
            else
            {
               A39vueloDescuentoPorcentage = (short)(context.localUtil.CToN( cgiGet( edtvueloDescuentoPorcentage_Internalname), ".", ","));
               AssignAttri("", false, "A39vueloDescuentoPorcentage", StringUtil.LTrimStr( (decimal)(A39vueloDescuentoPorcentage), 3, 0));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtaerolineaId_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtaerolineaId_Internalname), ".", ",") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "AEROLINEAID");
               AnyError = 1;
               GX_FocusControl = edtaerolineaId_Internalname;
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A41aerolineaId = 0;
               n41aerolineaId = false;
               AssignAttri("", false, "A41aerolineaId", StringUtil.LTrimStr( (decimal)(A41aerolineaId), 6, 0));
            }
            else
            {
               A41aerolineaId = (int)(context.localUtil.CToN( cgiGet( edtaerolineaId_Internalname), ".", ","));
               n41aerolineaId = false;
               AssignAttri("", false, "A41aerolineaId", StringUtil.LTrimStr( (decimal)(A41aerolineaId), 6, 0));
            }
            n41aerolineaId = ((0==A41aerolineaId) ? true : false);
            A42aerolineaNombre = cgiGet( edtaerolineaNombre_Internalname);
            AssignAttri("", false, "A42aerolineaNombre", A42aerolineaNombre);
            A43aerolineaDescuentoPorcentage = (short)(context.localUtil.CToN( cgiGet( edtaerolineaDescuentoPorcentage_Internalname), ".", ","));
            AssignAttri("", false, "A43aerolineaDescuentoPorcentage", StringUtil.LTrimStr( (decimal)(A43aerolineaDescuentoPorcentage), 3, 0));
            A40vueloPrecioFinal = context.localUtil.CToN( cgiGet( edtvueloPrecioFinal_Internalname), ".", ",");
            AssignAttri("", false, "A40vueloPrecioFinal", StringUtil.LTrimStr( A40vueloPrecioFinal, 9, 2));
            A47vueloCapacidad = (short)(context.localUtil.CToN( cgiGet( edtvueloCapacidad_Internalname), ".", ","));
            n47vueloCapacidad = false;
            AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            standaloneNotModal( ) ;
         }
         else
         {
            standaloneNotModal( ) ;
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
            {
               Gx_mode = "DSP";
               AssignAttri("", false, "Gx_mode", Gx_mode);
               A18vueloId = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AssignAttri("", false, "A18vueloId", StringUtil.LTrimStr( (decimal)(A18vueloId), 6, 0));
               getEqualNoModal( ) ;
               Gx_mode = "DSP";
               AssignAttri("", false, "Gx_mode", Gx_mode);
               disable_std_buttons_dsp( ) ;
               standaloneModal( ) ;
            }
            else
            {
               Gx_mode = "INS";
               AssignAttri("", false, "Gx_mode", Gx_mode);
               standaloneModal( ) ;
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( IsIns( )  )
            {
               /* Clear variables for new insertion. */
               InitAll056( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( IsIns( ) )
         {
            bttBtn_delete_Enabled = 0;
            AssignProp("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Enabled), 5, 0), true);
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         bttBtn_delete_Visible = 0;
         AssignProp("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Visible), 5, 0), true);
         bttBtn_first_Visible = 0;
         AssignProp("", false, bttBtn_first_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_first_Visible), 5, 0), true);
         bttBtn_previous_Visible = 0;
         AssignProp("", false, bttBtn_previous_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_previous_Visible), 5, 0), true);
         bttBtn_next_Visible = 0;
         AssignProp("", false, bttBtn_next_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_next_Visible), 5, 0), true);
         bttBtn_last_Visible = 0;
         AssignProp("", false, bttBtn_last_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_last_Visible), 5, 0), true);
         bttBtn_select_Visible = 0;
         AssignProp("", false, bttBtn_select_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_select_Visible), 5, 0), true);
         bttBtn_delete_Visible = 0;
         AssignProp("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Visible), 5, 0), true);
         if ( IsDsp( ) )
         {
            bttBtn_enter_Visible = 0;
            AssignProp("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_enter_Visible), 5, 0), true);
         }
         DisableAttributes056( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( IsDlt( ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_059( )
      {
         s47vueloCapacidad = O47vueloCapacidad;
         n47vueloCapacidad = false;
         AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
         nGXsfl_143_idx = 0;
         while ( nGXsfl_143_idx < nRC_GXsfl_143 )
         {
            ReadRow059( ) ;
            if ( ( nRcdExists_9 != 0 ) || ( nIsMod_9 != 0 ) )
            {
               GetKey059( ) ;
               if ( ( nRcdExists_9 == 0 ) && ( nRcdDeleted_9 == 0 ) )
               {
                  if ( RcdFound9 == 0 )
                  {
                     Gx_mode = "INS";
                     AssignAttri("", false, "Gx_mode", Gx_mode);
                     BeforeValidate059( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable059( ) ;
                        CloseExtendedTableCursors059( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                           AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
                        }
                        O47vueloCapacidad = A47vueloCapacidad;
                        n47vueloCapacidad = false;
                        AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
                     }
                  }
                  else
                  {
                     GXCCtl = "VUELOASIENTOID_" + sGXsfl_143_idx;
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, GXCCtl);
                     AnyError = 1;
                     GX_FocusControl = edtvueloAsientoId_Internalname;
                     AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( RcdFound9 != 0 )
                  {
                     if ( nRcdDeleted_9 != 0 )
                     {
                        Gx_mode = "DLT";
                        AssignAttri("", false, "Gx_mode", Gx_mode);
                        getByPrimaryKey059( ) ;
                        Load059( ) ;
                        BeforeValidate059( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls059( ) ;
                           O47vueloCapacidad = A47vueloCapacidad;
                           n47vueloCapacidad = false;
                           AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
                        }
                     }
                     else
                     {
                        if ( nIsMod_9 != 0 )
                        {
                           Gx_mode = "UPD";
                           AssignAttri("", false, "Gx_mode", Gx_mode);
                           BeforeValidate059( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable059( ) ;
                              CloseExtendedTableCursors059( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                                 AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
                              }
                              O47vueloCapacidad = A47vueloCapacidad;
                              n47vueloCapacidad = false;
                              AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_9 == 0 )
                     {
                        GXCCtl = "VUELOASIENTOID_" + sGXsfl_143_idx;
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, GXCCtl);
                        AnyError = 1;
                        GX_FocusControl = edtvueloAsientoId_Internalname;
                        AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
            ChangePostValue( edtvueloAsientoId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A44vueloAsientoId), 6, 0, ".", ""))) ;
            ChangePostValue( cmbvueloAsientoChar_Internalname, StringUtil.RTrim( A45vueloAsientoChar)) ;
            ChangePostValue( cmbvueloAsientoLocalizacion_Internalname, StringUtil.RTrim( A46vueloAsientoLocalizacion)) ;
            ChangePostValue( "ZT_"+"Z44vueloAsientoId_"+sGXsfl_143_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z44vueloAsientoId), 6, 0, ".", ""))) ;
            ChangePostValue( "ZT_"+"Z45vueloAsientoChar_"+sGXsfl_143_idx, StringUtil.RTrim( Z45vueloAsientoChar)) ;
            ChangePostValue( "ZT_"+"Z46vueloAsientoLocalizacion_"+sGXsfl_143_idx, StringUtil.RTrim( Z46vueloAsientoLocalizacion)) ;
            ChangePostValue( "nRcdDeleted_9_"+sGXsfl_143_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_9), 4, 0, ".", ""))) ;
            ChangePostValue( "nRcdExists_9_"+sGXsfl_143_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_9), 4, 0, ".", ""))) ;
            ChangePostValue( "nIsMod_9_"+sGXsfl_143_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_9), 4, 0, ".", ""))) ;
            if ( nIsMod_9 != 0 )
            {
               ChangePostValue( "VUELOASIENTOID_"+sGXsfl_143_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtvueloAsientoId_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "VUELOASIENTOCHAR_"+sGXsfl_143_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbvueloAsientoChar.Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "VUELOASIENTOLOCALIZACION_"+sGXsfl_143_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbvueloAsientoLocalizacion.Enabled), 5, 0, ".", ""))) ;
            }
         }
         O47vueloCapacidad = s47vueloCapacidad;
         n47vueloCapacidad = false;
         AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
         /* Start of After( level) rules */
         if ( ( A47vueloCapacidad > 8 ) && ( A47vueloCapacidad < 8 ) )
         {
            GX_msglist.addItem("La cantidad de asientos no puede ser menora 8", 1, "");
            AnyError = 1;
         }
         /* End of After( level) rules */
      }

      protected void ResetCaption050( )
      {
      }

      protected void ZM056( short GX_JID )
      {
         if ( ( GX_JID == 7 ) || ( GX_JID == 0 ) )
         {
            if ( ! IsIns( ) )
            {
               Z58vueloFecha = T00055_A58vueloFecha[0];
               Z38vueloPrecio = T00055_A38vueloPrecio[0];
               Z39vueloDescuentoPorcentage = T00055_A39vueloDescuentoPorcentage[0];
               Z41aerolineaId = T00055_A41aerolineaId[0];
               Z24vueloAeropuertoSalidaId = T00055_A24vueloAeropuertoSalidaId[0];
               Z26vueloAeropuertoLlegadaId = T00055_A26vueloAeropuertoLlegadaId[0];
            }
            else
            {
               Z58vueloFecha = A58vueloFecha;
               Z38vueloPrecio = A38vueloPrecio;
               Z39vueloDescuentoPorcentage = A39vueloDescuentoPorcentage;
               Z41aerolineaId = A41aerolineaId;
               Z24vueloAeropuertoSalidaId = A24vueloAeropuertoSalidaId;
               Z26vueloAeropuertoLlegadaId = A26vueloAeropuertoLlegadaId;
            }
         }
         if ( GX_JID == -7 )
         {
            Z18vueloId = A18vueloId;
            Z58vueloFecha = A58vueloFecha;
            Z38vueloPrecio = A38vueloPrecio;
            Z39vueloDescuentoPorcentage = A39vueloDescuentoPorcentage;
            Z41aerolineaId = A41aerolineaId;
            Z24vueloAeropuertoSalidaId = A24vueloAeropuertoSalidaId;
            Z26vueloAeropuertoLlegadaId = A26vueloAeropuertoLlegadaId;
            Z47vueloCapacidad = A47vueloCapacidad;
            Z25vueloAeropuertoSalidaNombre = A25vueloAeropuertoSalidaNombre;
            Z32vueloAeropuertoSalidaPaisId = A32vueloAeropuertoSalidaPaisId;
            Z30vueloAeropuertoSalidaCiudadId = A30vueloAeropuertoSalidaCiudadId;
            Z33vueloAeropuertoSalidaPaisNombr = A33vueloAeropuertoSalidaPaisNombr;
            Z31vueloAeropuertoSalidaCiudadNom = A31vueloAeropuertoSalidaCiudadNom;
            Z27vueloAeropuertoLlegadaNombre = A27vueloAeropuertoLlegadaNombre;
            Z34vueloAeropuertoLlegadaPaisId = A34vueloAeropuertoLlegadaPaisId;
            Z36vueloAeropuertoLlegadaCiudadId = A36vueloAeropuertoLlegadaCiudadId;
            Z35vueloAeropuertoLlegadaPaisNomb = A35vueloAeropuertoLlegadaPaisNomb;
            Z37vueloAeropuertoLlegadaCiudadNo = A37vueloAeropuertoLlegadaCiudadNo;
            Z42aerolineaNombre = A42aerolineaNombre;
            Z43aerolineaDescuentoPorcentage = A43aerolineaDescuentoPorcentage;
         }
      }

      protected void standaloneNotModal( )
      {
         imgprompt_24_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0070.aspx"+"',["+"{Ctrl:gx.dom.el('"+"VUELOAEROPUERTOSALIDAID"+"'), id:'"+"VUELOAEROPUERTOSALIDAID"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         imgprompt_26_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0070.aspx"+"',["+"{Ctrl:gx.dom.el('"+"VUELOAEROPUERTOLLEGADAID"+"'), id:'"+"VUELOAEROPUERTOLLEGADAID"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         imgprompt_41_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0080.aspx"+"',["+"{Ctrl:gx.dom.el('"+"AEROLINEAID"+"'), id:'"+"AEROLINEAID"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            AssignProp("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Enabled), 5, 0), true);
         }
         else
         {
            bttBtn_delete_Enabled = 1;
            AssignProp("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Enabled), 5, 0), true);
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Enabled = 0;
            AssignProp("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_enter_Enabled), 5, 0), true);
         }
         else
         {
            bttBtn_enter_Enabled = 1;
            AssignProp("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_enter_Enabled), 5, 0), true);
         }
      }

      protected void Load056( )
      {
         /* Using cursor T000516 */
         pr_default.execute(12, new Object[] {A18vueloId});
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound6 = 1;
            A58vueloFecha = T000516_A58vueloFecha[0];
            AssignAttri("", false, "A58vueloFecha", context.localUtil.Format(A58vueloFecha, "99/99/99"));
            A25vueloAeropuertoSalidaNombre = T000516_A25vueloAeropuertoSalidaNombre[0];
            AssignAttri("", false, "A25vueloAeropuertoSalidaNombre", A25vueloAeropuertoSalidaNombre);
            A33vueloAeropuertoSalidaPaisNombr = T000516_A33vueloAeropuertoSalidaPaisNombr[0];
            AssignAttri("", false, "A33vueloAeropuertoSalidaPaisNombr", A33vueloAeropuertoSalidaPaisNombr);
            A31vueloAeropuertoSalidaCiudadNom = T000516_A31vueloAeropuertoSalidaCiudadNom[0];
            AssignAttri("", false, "A31vueloAeropuertoSalidaCiudadNom", A31vueloAeropuertoSalidaCiudadNom);
            A27vueloAeropuertoLlegadaNombre = T000516_A27vueloAeropuertoLlegadaNombre[0];
            AssignAttri("", false, "A27vueloAeropuertoLlegadaNombre", A27vueloAeropuertoLlegadaNombre);
            A35vueloAeropuertoLlegadaPaisNomb = T000516_A35vueloAeropuertoLlegadaPaisNomb[0];
            AssignAttri("", false, "A35vueloAeropuertoLlegadaPaisNomb", A35vueloAeropuertoLlegadaPaisNomb);
            A37vueloAeropuertoLlegadaCiudadNo = T000516_A37vueloAeropuertoLlegadaCiudadNo[0];
            AssignAttri("", false, "A37vueloAeropuertoLlegadaCiudadNo", A37vueloAeropuertoLlegadaCiudadNo);
            A38vueloPrecio = T000516_A38vueloPrecio[0];
            AssignAttri("", false, "A38vueloPrecio", StringUtil.LTrimStr( A38vueloPrecio, 9, 2));
            A39vueloDescuentoPorcentage = T000516_A39vueloDescuentoPorcentage[0];
            AssignAttri("", false, "A39vueloDescuentoPorcentage", StringUtil.LTrimStr( (decimal)(A39vueloDescuentoPorcentage), 3, 0));
            A42aerolineaNombre = T000516_A42aerolineaNombre[0];
            AssignAttri("", false, "A42aerolineaNombre", A42aerolineaNombre);
            A43aerolineaDescuentoPorcentage = T000516_A43aerolineaDescuentoPorcentage[0];
            AssignAttri("", false, "A43aerolineaDescuentoPorcentage", StringUtil.LTrimStr( (decimal)(A43aerolineaDescuentoPorcentage), 3, 0));
            A41aerolineaId = T000516_A41aerolineaId[0];
            AssignAttri("", false, "A41aerolineaId", StringUtil.LTrimStr( (decimal)(A41aerolineaId), 6, 0));
            n41aerolineaId = T000516_n41aerolineaId[0];
            A24vueloAeropuertoSalidaId = T000516_A24vueloAeropuertoSalidaId[0];
            AssignAttri("", false, "A24vueloAeropuertoSalidaId", StringUtil.LTrimStr( (decimal)(A24vueloAeropuertoSalidaId), 6, 0));
            A26vueloAeropuertoLlegadaId = T000516_A26vueloAeropuertoLlegadaId[0];
            AssignAttri("", false, "A26vueloAeropuertoLlegadaId", StringUtil.LTrimStr( (decimal)(A26vueloAeropuertoLlegadaId), 6, 0));
            A32vueloAeropuertoSalidaPaisId = T000516_A32vueloAeropuertoSalidaPaisId[0];
            AssignAttri("", false, "A32vueloAeropuertoSalidaPaisId", StringUtil.LTrimStr( (decimal)(A32vueloAeropuertoSalidaPaisId), 6, 0));
            A30vueloAeropuertoSalidaCiudadId = T000516_A30vueloAeropuertoSalidaCiudadId[0];
            AssignAttri("", false, "A30vueloAeropuertoSalidaCiudadId", StringUtil.LTrimStr( (decimal)(A30vueloAeropuertoSalidaCiudadId), 6, 0));
            A34vueloAeropuertoLlegadaPaisId = T000516_A34vueloAeropuertoLlegadaPaisId[0];
            AssignAttri("", false, "A34vueloAeropuertoLlegadaPaisId", StringUtil.LTrimStr( (decimal)(A34vueloAeropuertoLlegadaPaisId), 6, 0));
            A36vueloAeropuertoLlegadaCiudadId = T000516_A36vueloAeropuertoLlegadaCiudadId[0];
            AssignAttri("", false, "A36vueloAeropuertoLlegadaCiudadId", StringUtil.LTrimStr( (decimal)(A36vueloAeropuertoLlegadaCiudadId), 6, 0));
            A47vueloCapacidad = T000516_A47vueloCapacidad[0];
            AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
            n47vueloCapacidad = T000516_n47vueloCapacidad[0];
            ZM056( -7) ;
         }
         pr_default.close(12);
         OnLoadActions056( ) ;
      }

      protected void OnLoadActions056( )
      {
         O47vueloCapacidad = A47vueloCapacidad;
         n47vueloCapacidad = false;
         AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
         if ( A43aerolineaDescuentoPorcentage > A39vueloDescuentoPorcentage )
         {
            A40vueloPrecioFinal = (decimal)(A38vueloPrecio*(1-A43aerolineaDescuentoPorcentage/ (decimal)(100)));
            AssignAttri("", false, "A40vueloPrecioFinal", StringUtil.LTrimStr( A40vueloPrecioFinal, 9, 2));
         }
         else
         {
            A40vueloPrecioFinal = (decimal)(A38vueloPrecio*(1-A39vueloDescuentoPorcentage/ (decimal)(100)));
            AssignAttri("", false, "A40vueloPrecioFinal", StringUtil.LTrimStr( A40vueloPrecioFinal, 9, 2));
         }
      }

      protected void CheckExtendedTable056( )
      {
         nIsDirty_6 = 0;
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T000514 */
         pr_default.execute(11, new Object[] {A18vueloId});
         if ( (pr_default.getStatus(11) != 101) )
         {
            A47vueloCapacidad = T000514_A47vueloCapacidad[0];
            AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
            n47vueloCapacidad = T000514_n47vueloCapacidad[0];
         }
         else
         {
            nIsDirty_6 = 1;
            A47vueloCapacidad = 0;
            n47vueloCapacidad = false;
            AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
         }
         pr_default.close(11);
         if ( ! ( (DateTime.MinValue==A58vueloFecha) || ( A58vueloFecha >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Field vuelo Fecha is out of range", "OutOfRange", 1, "VUELOFECHA");
            AnyError = 1;
            GX_FocusControl = edtvueloFecha_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T00057 */
         pr_default.execute(5, new Object[] {A24vueloAeropuertoSalidaId});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("No matching 'vuelo Aeropuerto Salida'.", "ForeignKeyNotFound", 1, "VUELOAEROPUERTOSALIDAID");
            AnyError = 1;
            GX_FocusControl = edtvueloAeropuertoSalidaId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A25vueloAeropuertoSalidaNombre = T00057_A25vueloAeropuertoSalidaNombre[0];
         AssignAttri("", false, "A25vueloAeropuertoSalidaNombre", A25vueloAeropuertoSalidaNombre);
         A32vueloAeropuertoSalidaPaisId = T00057_A32vueloAeropuertoSalidaPaisId[0];
         AssignAttri("", false, "A32vueloAeropuertoSalidaPaisId", StringUtil.LTrimStr( (decimal)(A32vueloAeropuertoSalidaPaisId), 6, 0));
         A30vueloAeropuertoSalidaCiudadId = T00057_A30vueloAeropuertoSalidaCiudadId[0];
         AssignAttri("", false, "A30vueloAeropuertoSalidaCiudadId", StringUtil.LTrimStr( (decimal)(A30vueloAeropuertoSalidaCiudadId), 6, 0));
         pr_default.close(5);
         /* Using cursor T00059 */
         pr_default.execute(7, new Object[] {A32vueloAeropuertoSalidaPaisId});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("No matching 'vuelo Aeropuerto Salida'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A33vueloAeropuertoSalidaPaisNombr = T00059_A33vueloAeropuertoSalidaPaisNombr[0];
         AssignAttri("", false, "A33vueloAeropuertoSalidaPaisNombr", A33vueloAeropuertoSalidaPaisNombr);
         pr_default.close(7);
         /* Using cursor T000510 */
         pr_default.execute(8, new Object[] {A32vueloAeropuertoSalidaPaisId, A30vueloAeropuertoSalidaCiudadId});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("No matching 'vuelo Aeropuerto Salida'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A31vueloAeropuertoSalidaCiudadNom = T000510_A31vueloAeropuertoSalidaCiudadNom[0];
         AssignAttri("", false, "A31vueloAeropuertoSalidaCiudadNom", A31vueloAeropuertoSalidaCiudadNom);
         pr_default.close(8);
         /* Using cursor T00058 */
         pr_default.execute(6, new Object[] {A26vueloAeropuertoLlegadaId});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("No matching 'vuelo Aeropuerto Llegada'.", "ForeignKeyNotFound", 1, "VUELOAEROPUERTOLLEGADAID");
            AnyError = 1;
            GX_FocusControl = edtvueloAeropuertoLlegadaId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A27vueloAeropuertoLlegadaNombre = T00058_A27vueloAeropuertoLlegadaNombre[0];
         AssignAttri("", false, "A27vueloAeropuertoLlegadaNombre", A27vueloAeropuertoLlegadaNombre);
         A34vueloAeropuertoLlegadaPaisId = T00058_A34vueloAeropuertoLlegadaPaisId[0];
         AssignAttri("", false, "A34vueloAeropuertoLlegadaPaisId", StringUtil.LTrimStr( (decimal)(A34vueloAeropuertoLlegadaPaisId), 6, 0));
         A36vueloAeropuertoLlegadaCiudadId = T00058_A36vueloAeropuertoLlegadaCiudadId[0];
         AssignAttri("", false, "A36vueloAeropuertoLlegadaCiudadId", StringUtil.LTrimStr( (decimal)(A36vueloAeropuertoLlegadaCiudadId), 6, 0));
         pr_default.close(6);
         /* Using cursor T000511 */
         pr_default.execute(9, new Object[] {A34vueloAeropuertoLlegadaPaisId});
         if ( (pr_default.getStatus(9) == 101) )
         {
            GX_msglist.addItem("No matching 'vuelo Aeropuerto Llegada'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A35vueloAeropuertoLlegadaPaisNomb = T000511_A35vueloAeropuertoLlegadaPaisNomb[0];
         AssignAttri("", false, "A35vueloAeropuertoLlegadaPaisNomb", A35vueloAeropuertoLlegadaPaisNomb);
         pr_default.close(9);
         /* Using cursor T000512 */
         pr_default.execute(10, new Object[] {A34vueloAeropuertoLlegadaPaisId, A36vueloAeropuertoLlegadaCiudadId});
         if ( (pr_default.getStatus(10) == 101) )
         {
            GX_msglist.addItem("No matching 'vuelo Aeropuerto Llegada'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A37vueloAeropuertoLlegadaCiudadNo = T000512_A37vueloAeropuertoLlegadaCiudadNo[0];
         AssignAttri("", false, "A37vueloAeropuertoLlegadaCiudadNo", A37vueloAeropuertoLlegadaCiudadNo);
         pr_default.close(10);
         /* Using cursor T00056 */
         pr_default.execute(4, new Object[] {n41aerolineaId, A41aerolineaId});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A41aerolineaId) ) )
            {
               GX_msglist.addItem("No matching 'aerolinea'.", "ForeignKeyNotFound", 1, "AEROLINEAID");
               AnyError = 1;
               GX_FocusControl = edtaerolineaId_Internalname;
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A42aerolineaNombre = T00056_A42aerolineaNombre[0];
         AssignAttri("", false, "A42aerolineaNombre", A42aerolineaNombre);
         A43aerolineaDescuentoPorcentage = T00056_A43aerolineaDescuentoPorcentage[0];
         AssignAttri("", false, "A43aerolineaDescuentoPorcentage", StringUtil.LTrimStr( (decimal)(A43aerolineaDescuentoPorcentage), 3, 0));
         pr_default.close(4);
         if ( A43aerolineaDescuentoPorcentage > A39vueloDescuentoPorcentage )
         {
            nIsDirty_6 = 1;
            A40vueloPrecioFinal = (decimal)(A38vueloPrecio*(1-A43aerolineaDescuentoPorcentage/ (decimal)(100)));
            AssignAttri("", false, "A40vueloPrecioFinal", StringUtil.LTrimStr( A40vueloPrecioFinal, 9, 2));
         }
         else
         {
            nIsDirty_6 = 1;
            A40vueloPrecioFinal = (decimal)(A38vueloPrecio*(1-A39vueloDescuentoPorcentage/ (decimal)(100)));
            AssignAttri("", false, "A40vueloPrecioFinal", StringUtil.LTrimStr( A40vueloPrecioFinal, 9, 2));
         }
      }

      protected void CloseExtendedTableCursors056( )
      {
         pr_default.close(11);
         pr_default.close(5);
         pr_default.close(7);
         pr_default.close(8);
         pr_default.close(6);
         pr_default.close(9);
         pr_default.close(10);
         pr_default.close(4);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_15( int A18vueloId )
      {
         /* Using cursor T000518 */
         pr_default.execute(13, new Object[] {A18vueloId});
         if ( (pr_default.getStatus(13) != 101) )
         {
            A47vueloCapacidad = T000518_A47vueloCapacidad[0];
            AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
            n47vueloCapacidad = T000518_n47vueloCapacidad[0];
         }
         else
         {
            A47vueloCapacidad = 0;
            n47vueloCapacidad = false;
            AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         AddString( "[[") ;
         AddString( "\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A47vueloCapacidad), 4, 0, ".", "")))+"\"") ;
         AddString( "]") ;
         if ( (pr_default.getStatus(13) == 101) )
         {
            AddString( ",") ;
            AddString( "101") ;
         }
         AddString( "]") ;
         pr_default.close(13);
      }

      protected void gxLoad_9( int A24vueloAeropuertoSalidaId )
      {
         /* Using cursor T000519 */
         pr_default.execute(14, new Object[] {A24vueloAeropuertoSalidaId});
         if ( (pr_default.getStatus(14) == 101) )
         {
            GX_msglist.addItem("No matching 'vuelo Aeropuerto Salida'.", "ForeignKeyNotFound", 1, "VUELOAEROPUERTOSALIDAID");
            AnyError = 1;
            GX_FocusControl = edtvueloAeropuertoSalidaId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A25vueloAeropuertoSalidaNombre = T000519_A25vueloAeropuertoSalidaNombre[0];
         AssignAttri("", false, "A25vueloAeropuertoSalidaNombre", A25vueloAeropuertoSalidaNombre);
         A32vueloAeropuertoSalidaPaisId = T000519_A32vueloAeropuertoSalidaPaisId[0];
         AssignAttri("", false, "A32vueloAeropuertoSalidaPaisId", StringUtil.LTrimStr( (decimal)(A32vueloAeropuertoSalidaPaisId), 6, 0));
         A30vueloAeropuertoSalidaCiudadId = T000519_A30vueloAeropuertoSalidaCiudadId[0];
         AssignAttri("", false, "A30vueloAeropuertoSalidaCiudadId", StringUtil.LTrimStr( (decimal)(A30vueloAeropuertoSalidaCiudadId), 6, 0));
         GxWebStd.set_html_headers( context, 0, "", "");
         AddString( "[[") ;
         AddString( "\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A25vueloAeropuertoSalidaNombre))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A32vueloAeropuertoSalidaPaisId), 6, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A30vueloAeropuertoSalidaCiudadId), 6, 0, ".", "")))+"\"") ;
         AddString( "]") ;
         if ( (pr_default.getStatus(14) == 101) )
         {
            AddString( ",") ;
            AddString( "101") ;
         }
         AddString( "]") ;
         pr_default.close(14);
      }

      protected void gxLoad_11( int A32vueloAeropuertoSalidaPaisId )
      {
         /* Using cursor T000520 */
         pr_default.execute(15, new Object[] {A32vueloAeropuertoSalidaPaisId});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("No matching 'vuelo Aeropuerto Salida'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A33vueloAeropuertoSalidaPaisNombr = T000520_A33vueloAeropuertoSalidaPaisNombr[0];
         AssignAttri("", false, "A33vueloAeropuertoSalidaPaisNombr", A33vueloAeropuertoSalidaPaisNombr);
         GxWebStd.set_html_headers( context, 0, "", "");
         AddString( "[[") ;
         AddString( "\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A33vueloAeropuertoSalidaPaisNombr))+"\"") ;
         AddString( "]") ;
         if ( (pr_default.getStatus(15) == 101) )
         {
            AddString( ",") ;
            AddString( "101") ;
         }
         AddString( "]") ;
         pr_default.close(15);
      }

      protected void gxLoad_12( int A32vueloAeropuertoSalidaPaisId ,
                                int A30vueloAeropuertoSalidaCiudadId )
      {
         /* Using cursor T000521 */
         pr_default.execute(16, new Object[] {A32vueloAeropuertoSalidaPaisId, A30vueloAeropuertoSalidaCiudadId});
         if ( (pr_default.getStatus(16) == 101) )
         {
            GX_msglist.addItem("No matching 'vuelo Aeropuerto Salida'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A31vueloAeropuertoSalidaCiudadNom = T000521_A31vueloAeropuertoSalidaCiudadNom[0];
         AssignAttri("", false, "A31vueloAeropuertoSalidaCiudadNom", A31vueloAeropuertoSalidaCiudadNom);
         GxWebStd.set_html_headers( context, 0, "", "");
         AddString( "[[") ;
         AddString( "\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A31vueloAeropuertoSalidaCiudadNom))+"\"") ;
         AddString( "]") ;
         if ( (pr_default.getStatus(16) == 101) )
         {
            AddString( ",") ;
            AddString( "101") ;
         }
         AddString( "]") ;
         pr_default.close(16);
      }

      protected void gxLoad_10( int A26vueloAeropuertoLlegadaId )
      {
         /* Using cursor T000522 */
         pr_default.execute(17, new Object[] {A26vueloAeropuertoLlegadaId});
         if ( (pr_default.getStatus(17) == 101) )
         {
            GX_msglist.addItem("No matching 'vuelo Aeropuerto Llegada'.", "ForeignKeyNotFound", 1, "VUELOAEROPUERTOLLEGADAID");
            AnyError = 1;
            GX_FocusControl = edtvueloAeropuertoLlegadaId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A27vueloAeropuertoLlegadaNombre = T000522_A27vueloAeropuertoLlegadaNombre[0];
         AssignAttri("", false, "A27vueloAeropuertoLlegadaNombre", A27vueloAeropuertoLlegadaNombre);
         A34vueloAeropuertoLlegadaPaisId = T000522_A34vueloAeropuertoLlegadaPaisId[0];
         AssignAttri("", false, "A34vueloAeropuertoLlegadaPaisId", StringUtil.LTrimStr( (decimal)(A34vueloAeropuertoLlegadaPaisId), 6, 0));
         A36vueloAeropuertoLlegadaCiudadId = T000522_A36vueloAeropuertoLlegadaCiudadId[0];
         AssignAttri("", false, "A36vueloAeropuertoLlegadaCiudadId", StringUtil.LTrimStr( (decimal)(A36vueloAeropuertoLlegadaCiudadId), 6, 0));
         GxWebStd.set_html_headers( context, 0, "", "");
         AddString( "[[") ;
         AddString( "\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A27vueloAeropuertoLlegadaNombre))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A34vueloAeropuertoLlegadaPaisId), 6, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A36vueloAeropuertoLlegadaCiudadId), 6, 0, ".", "")))+"\"") ;
         AddString( "]") ;
         if ( (pr_default.getStatus(17) == 101) )
         {
            AddString( ",") ;
            AddString( "101") ;
         }
         AddString( "]") ;
         pr_default.close(17);
      }

      protected void gxLoad_13( int A34vueloAeropuertoLlegadaPaisId )
      {
         /* Using cursor T000523 */
         pr_default.execute(18, new Object[] {A34vueloAeropuertoLlegadaPaisId});
         if ( (pr_default.getStatus(18) == 101) )
         {
            GX_msglist.addItem("No matching 'vuelo Aeropuerto Llegada'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A35vueloAeropuertoLlegadaPaisNomb = T000523_A35vueloAeropuertoLlegadaPaisNomb[0];
         AssignAttri("", false, "A35vueloAeropuertoLlegadaPaisNomb", A35vueloAeropuertoLlegadaPaisNomb);
         GxWebStd.set_html_headers( context, 0, "", "");
         AddString( "[[") ;
         AddString( "\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A35vueloAeropuertoLlegadaPaisNomb))+"\"") ;
         AddString( "]") ;
         if ( (pr_default.getStatus(18) == 101) )
         {
            AddString( ",") ;
            AddString( "101") ;
         }
         AddString( "]") ;
         pr_default.close(18);
      }

      protected void gxLoad_14( int A34vueloAeropuertoLlegadaPaisId ,
                                int A36vueloAeropuertoLlegadaCiudadId )
      {
         /* Using cursor T000524 */
         pr_default.execute(19, new Object[] {A34vueloAeropuertoLlegadaPaisId, A36vueloAeropuertoLlegadaCiudadId});
         if ( (pr_default.getStatus(19) == 101) )
         {
            GX_msglist.addItem("No matching 'vuelo Aeropuerto Llegada'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A37vueloAeropuertoLlegadaCiudadNo = T000524_A37vueloAeropuertoLlegadaCiudadNo[0];
         AssignAttri("", false, "A37vueloAeropuertoLlegadaCiudadNo", A37vueloAeropuertoLlegadaCiudadNo);
         GxWebStd.set_html_headers( context, 0, "", "");
         AddString( "[[") ;
         AddString( "\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A37vueloAeropuertoLlegadaCiudadNo))+"\"") ;
         AddString( "]") ;
         if ( (pr_default.getStatus(19) == 101) )
         {
            AddString( ",") ;
            AddString( "101") ;
         }
         AddString( "]") ;
         pr_default.close(19);
      }

      protected void gxLoad_8( int A41aerolineaId )
      {
         /* Using cursor T000525 */
         pr_default.execute(20, new Object[] {n41aerolineaId, A41aerolineaId});
         if ( (pr_default.getStatus(20) == 101) )
         {
            if ( ! ( (0==A41aerolineaId) ) )
            {
               GX_msglist.addItem("No matching 'aerolinea'.", "ForeignKeyNotFound", 1, "AEROLINEAID");
               AnyError = 1;
               GX_FocusControl = edtaerolineaId_Internalname;
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A42aerolineaNombre = T000525_A42aerolineaNombre[0];
         AssignAttri("", false, "A42aerolineaNombre", A42aerolineaNombre);
         A43aerolineaDescuentoPorcentage = T000525_A43aerolineaDescuentoPorcentage[0];
         AssignAttri("", false, "A43aerolineaDescuentoPorcentage", StringUtil.LTrimStr( (decimal)(A43aerolineaDescuentoPorcentage), 3, 0));
         GxWebStd.set_html_headers( context, 0, "", "");
         AddString( "[[") ;
         AddString( "\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A42aerolineaNombre))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A43aerolineaDescuentoPorcentage), 3, 0, ".", "")))+"\"") ;
         AddString( "]") ;
         if ( (pr_default.getStatus(20) == 101) )
         {
            AddString( ",") ;
            AddString( "101") ;
         }
         AddString( "]") ;
         pr_default.close(20);
      }

      protected void GetKey056( )
      {
         /* Using cursor T000526 */
         pr_default.execute(21, new Object[] {A18vueloId});
         if ( (pr_default.getStatus(21) != 101) )
         {
            RcdFound6 = 1;
         }
         else
         {
            RcdFound6 = 0;
         }
         pr_default.close(21);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00055 */
         pr_default.execute(3, new Object[] {A18vueloId});
         if ( (pr_default.getStatus(3) != 101) )
         {
            ZM056( 7) ;
            RcdFound6 = 1;
            A18vueloId = T00055_A18vueloId[0];
            AssignAttri("", false, "A18vueloId", StringUtil.LTrimStr( (decimal)(A18vueloId), 6, 0));
            A58vueloFecha = T00055_A58vueloFecha[0];
            AssignAttri("", false, "A58vueloFecha", context.localUtil.Format(A58vueloFecha, "99/99/99"));
            A38vueloPrecio = T00055_A38vueloPrecio[0];
            AssignAttri("", false, "A38vueloPrecio", StringUtil.LTrimStr( A38vueloPrecio, 9, 2));
            A39vueloDescuentoPorcentage = T00055_A39vueloDescuentoPorcentage[0];
            AssignAttri("", false, "A39vueloDescuentoPorcentage", StringUtil.LTrimStr( (decimal)(A39vueloDescuentoPorcentage), 3, 0));
            A41aerolineaId = T00055_A41aerolineaId[0];
            AssignAttri("", false, "A41aerolineaId", StringUtil.LTrimStr( (decimal)(A41aerolineaId), 6, 0));
            n41aerolineaId = T00055_n41aerolineaId[0];
            A24vueloAeropuertoSalidaId = T00055_A24vueloAeropuertoSalidaId[0];
            AssignAttri("", false, "A24vueloAeropuertoSalidaId", StringUtil.LTrimStr( (decimal)(A24vueloAeropuertoSalidaId), 6, 0));
            A26vueloAeropuertoLlegadaId = T00055_A26vueloAeropuertoLlegadaId[0];
            AssignAttri("", false, "A26vueloAeropuertoLlegadaId", StringUtil.LTrimStr( (decimal)(A26vueloAeropuertoLlegadaId), 6, 0));
            Z18vueloId = A18vueloId;
            sMode6 = Gx_mode;
            Gx_mode = "DSP";
            AssignAttri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load056( ) ;
            if ( AnyError == 1 )
            {
               RcdFound6 = 0;
               InitializeNonKey056( ) ;
            }
            Gx_mode = sMode6;
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound6 = 0;
            InitializeNonKey056( ) ;
            sMode6 = Gx_mode;
            Gx_mode = "DSP";
            AssignAttri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode6;
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(3);
      }

      protected void getEqualNoModal( )
      {
         GetKey056( ) ;
         if ( RcdFound6 == 0 )
         {
            Gx_mode = "INS";
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound6 = 0;
         /* Using cursor T000527 */
         pr_default.execute(22, new Object[] {A18vueloId});
         if ( (pr_default.getStatus(22) != 101) )
         {
            while ( (pr_default.getStatus(22) != 101) && ( ( T000527_A18vueloId[0] < A18vueloId ) ) )
            {
               pr_default.readNext(22);
            }
            if ( (pr_default.getStatus(22) != 101) && ( ( T000527_A18vueloId[0] > A18vueloId ) ) )
            {
               A18vueloId = T000527_A18vueloId[0];
               AssignAttri("", false, "A18vueloId", StringUtil.LTrimStr( (decimal)(A18vueloId), 6, 0));
               RcdFound6 = 1;
            }
         }
         pr_default.close(22);
      }

      protected void move_previous( )
      {
         RcdFound6 = 0;
         /* Using cursor T000528 */
         pr_default.execute(23, new Object[] {A18vueloId});
         if ( (pr_default.getStatus(23) != 101) )
         {
            while ( (pr_default.getStatus(23) != 101) && ( ( T000528_A18vueloId[0] > A18vueloId ) ) )
            {
               pr_default.readNext(23);
            }
            if ( (pr_default.getStatus(23) != 101) && ( ( T000528_A18vueloId[0] < A18vueloId ) ) )
            {
               A18vueloId = T000528_A18vueloId[0];
               AssignAttri("", false, "A18vueloId", StringUtil.LTrimStr( (decimal)(A18vueloId), 6, 0));
               RcdFound6 = 1;
            }
         }
         pr_default.close(23);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey056( ) ;
         if ( IsIns( ) )
         {
            /* Insert record */
            A47vueloCapacidad = O47vueloCapacidad;
            n47vueloCapacidad = false;
            AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
            GX_FocusControl = edtvueloId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
            Insert056( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound6 == 1 )
            {
               if ( A18vueloId != Z18vueloId )
               {
                  A18vueloId = Z18vueloId;
                  AssignAttri("", false, "A18vueloId", StringUtil.LTrimStr( (decimal)(A18vueloId), 6, 0));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "VUELOID");
                  AnyError = 1;
                  GX_FocusControl = edtvueloId_Internalname;
                  AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( IsDlt( ) )
               {
                  A47vueloCapacidad = O47vueloCapacidad;
                  n47vueloCapacidad = false;
                  AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtvueloId_Internalname;
                  AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  AssignAttri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  A47vueloCapacidad = O47vueloCapacidad;
                  n47vueloCapacidad = false;
                  AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
                  Update056( ) ;
                  GX_FocusControl = edtvueloId_Internalname;
                  AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A18vueloId != Z18vueloId )
               {
                  Gx_mode = "INS";
                  AssignAttri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  A47vueloCapacidad = O47vueloCapacidad;
                  n47vueloCapacidad = false;
                  AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
                  GX_FocusControl = edtvueloId_Internalname;
                  AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert056( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "VUELOID");
                     AnyError = 1;
                     GX_FocusControl = edtvueloId_Internalname;
                     AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     AssignAttri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     A47vueloCapacidad = O47vueloCapacidad;
                     n47vueloCapacidad = false;
                     AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
                     GX_FocusControl = edtvueloId_Internalname;
                     AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert056( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A18vueloId != Z18vueloId )
         {
            A18vueloId = Z18vueloId;
            AssignAttri("", false, "A18vueloId", StringUtil.LTrimStr( (decimal)(A18vueloId), 6, 0));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "VUELOID");
            AnyError = 1;
            GX_FocusControl = edtvueloId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            A47vueloCapacidad = O47vueloCapacidad;
            n47vueloCapacidad = false;
            AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtvueloId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         getEqualNoModal( ) ;
         if ( RcdFound6 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "VUELOID");
            AnyError = 1;
            GX_FocusControl = edtvueloId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtvueloFecha_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         ScanStart056( ) ;
         if ( RcdFound6 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtvueloFecha_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd056( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         move_previous( ) ;
         if ( RcdFound6 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtvueloFecha_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         move_next( ) ;
         if ( RcdFound6 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtvueloFecha_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         ScanStart056( ) ;
         if ( RcdFound6 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound6 != 0 )
            {
               ScanNext056( ) ;
            }
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtvueloFecha_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd056( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency056( )
      {
         if ( ! IsIns( ) )
         {
            /* Using cursor T00054 */
            pr_default.execute(2, new Object[] {A18vueloId});
            if ( (pr_default.getStatus(2) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"vuelo"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(2) == 101) || ( Z58vueloFecha != T00054_A58vueloFecha[0] ) || ( Z38vueloPrecio != T00054_A38vueloPrecio[0] ) || ( Z39vueloDescuentoPorcentage != T00054_A39vueloDescuentoPorcentage[0] ) || ( Z41aerolineaId != T00054_A41aerolineaId[0] ) || ( Z24vueloAeropuertoSalidaId != T00054_A24vueloAeropuertoSalidaId[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z26vueloAeropuertoLlegadaId != T00054_A26vueloAeropuertoLlegadaId[0] ) )
            {
               if ( Z58vueloFecha != T00054_A58vueloFecha[0] )
               {
                  GXUtil.WriteLog("vuelo:[seudo value changed for attri]"+"vueloFecha");
                  GXUtil.WriteLogRaw("Old: ",Z58vueloFecha);
                  GXUtil.WriteLogRaw("Current: ",T00054_A58vueloFecha[0]);
               }
               if ( Z38vueloPrecio != T00054_A38vueloPrecio[0] )
               {
                  GXUtil.WriteLog("vuelo:[seudo value changed for attri]"+"vueloPrecio");
                  GXUtil.WriteLogRaw("Old: ",Z38vueloPrecio);
                  GXUtil.WriteLogRaw("Current: ",T00054_A38vueloPrecio[0]);
               }
               if ( Z39vueloDescuentoPorcentage != T00054_A39vueloDescuentoPorcentage[0] )
               {
                  GXUtil.WriteLog("vuelo:[seudo value changed for attri]"+"vueloDescuentoPorcentage");
                  GXUtil.WriteLogRaw("Old: ",Z39vueloDescuentoPorcentage);
                  GXUtil.WriteLogRaw("Current: ",T00054_A39vueloDescuentoPorcentage[0]);
               }
               if ( Z41aerolineaId != T00054_A41aerolineaId[0] )
               {
                  GXUtil.WriteLog("vuelo:[seudo value changed for attri]"+"aerolineaId");
                  GXUtil.WriteLogRaw("Old: ",Z41aerolineaId);
                  GXUtil.WriteLogRaw("Current: ",T00054_A41aerolineaId[0]);
               }
               if ( Z24vueloAeropuertoSalidaId != T00054_A24vueloAeropuertoSalidaId[0] )
               {
                  GXUtil.WriteLog("vuelo:[seudo value changed for attri]"+"vueloAeropuertoSalidaId");
                  GXUtil.WriteLogRaw("Old: ",Z24vueloAeropuertoSalidaId);
                  GXUtil.WriteLogRaw("Current: ",T00054_A24vueloAeropuertoSalidaId[0]);
               }
               if ( Z26vueloAeropuertoLlegadaId != T00054_A26vueloAeropuertoLlegadaId[0] )
               {
                  GXUtil.WriteLog("vuelo:[seudo value changed for attri]"+"vueloAeropuertoLlegadaId");
                  GXUtil.WriteLogRaw("Old: ",Z26vueloAeropuertoLlegadaId);
                  GXUtil.WriteLogRaw("Current: ",T00054_A26vueloAeropuertoLlegadaId[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"vuelo"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert056( )
      {
         BeforeValidate056( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable056( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM056( 0) ;
            CheckOptimisticConcurrency056( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm056( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert056( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000529 */
                     pr_default.execute(24, new Object[] {A58vueloFecha, A38vueloPrecio, A39vueloDescuentoPorcentage, n41aerolineaId, A41aerolineaId, A24vueloAeropuertoSalidaId, A26vueloAeropuertoLlegadaId});
                     A18vueloId = T000529_A18vueloId[0];
                     AssignAttri("", false, "A18vueloId", StringUtil.LTrimStr( (decimal)(A18vueloId), 6, 0));
                     pr_default.close(24);
                     dsDefault.SmartCacheProvider.SetUpdated("vuelo") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel056( ) ;
                           if ( AnyError == 0 )
                           {
                              /* Save values for previous() function. */
                              GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                              ResetCaption050( ) ;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load056( ) ;
            }
            EndLevel056( ) ;
         }
         CloseExtendedTableCursors056( ) ;
      }

      protected void Update056( )
      {
         BeforeValidate056( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable056( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency056( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm056( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate056( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000530 */
                     pr_default.execute(25, new Object[] {A58vueloFecha, A38vueloPrecio, A39vueloDescuentoPorcentage, n41aerolineaId, A41aerolineaId, A24vueloAeropuertoSalidaId, A26vueloAeropuertoLlegadaId, A18vueloId});
                     pr_default.close(25);
                     dsDefault.SmartCacheProvider.SetUpdated("vuelo") ;
                     if ( (pr_default.getStatus(25) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"vuelo"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate056( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel056( ) ;
                           if ( AnyError == 0 )
                           {
                              getByPrimaryKey( ) ;
                              GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), "SuccessfullyUpdated", 0, "", true);
                              ResetCaption050( ) ;
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel056( ) ;
         }
         CloseExtendedTableCursors056( ) ;
      }

      protected void DeferredUpdate056( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         AssignAttri("", false, "Gx_mode", Gx_mode);
         BeforeValidate056( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency056( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls056( ) ;
            AfterConfirm056( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete056( ) ;
               if ( AnyError == 0 )
               {
                  A47vueloCapacidad = O47vueloCapacidad;
                  n47vueloCapacidad = false;
                  AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
                  ScanStart059( ) ;
                  while ( RcdFound9 != 0 )
                  {
                     getByPrimaryKey059( ) ;
                     Delete059( ) ;
                     ScanNext059( ) ;
                     O47vueloCapacidad = A47vueloCapacidad;
                     n47vueloCapacidad = false;
                     AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
                  }
                  ScanEnd059( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000531 */
                     pr_default.execute(26, new Object[] {A18vueloId});
                     pr_default.close(26);
                     dsDefault.SmartCacheProvider.SetUpdated("vuelo") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( delete) rules */
                        /* End of After( delete) rules */
                        if ( AnyError == 0 )
                        {
                           move_next( ) ;
                           if ( RcdFound6 == 0 )
                           {
                              InitAll056( ) ;
                              Gx_mode = "INS";
                              AssignAttri("", false, "Gx_mode", Gx_mode);
                           }
                           else
                           {
                              getByPrimaryKey( ) ;
                              Gx_mode = "UPD";
                              AssignAttri("", false, "Gx_mode", Gx_mode);
                           }
                           GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), "SuccessfullyDeleted", 0, "", true);
                           ResetCaption050( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
         sMode6 = Gx_mode;
         Gx_mode = "DLT";
         AssignAttri("", false, "Gx_mode", Gx_mode);
         EndLevel056( ) ;
         Gx_mode = sMode6;
         AssignAttri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls056( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T000533 */
            pr_default.execute(27, new Object[] {A18vueloId});
            if ( (pr_default.getStatus(27) != 101) )
            {
               A47vueloCapacidad = T000533_A47vueloCapacidad[0];
               AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
               n47vueloCapacidad = T000533_n47vueloCapacidad[0];
            }
            else
            {
               A47vueloCapacidad = 0;
               n47vueloCapacidad = false;
               AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
            }
            pr_default.close(27);
            /* Using cursor T000534 */
            pr_default.execute(28, new Object[] {A24vueloAeropuertoSalidaId});
            A25vueloAeropuertoSalidaNombre = T000534_A25vueloAeropuertoSalidaNombre[0];
            AssignAttri("", false, "A25vueloAeropuertoSalidaNombre", A25vueloAeropuertoSalidaNombre);
            A32vueloAeropuertoSalidaPaisId = T000534_A32vueloAeropuertoSalidaPaisId[0];
            AssignAttri("", false, "A32vueloAeropuertoSalidaPaisId", StringUtil.LTrimStr( (decimal)(A32vueloAeropuertoSalidaPaisId), 6, 0));
            A30vueloAeropuertoSalidaCiudadId = T000534_A30vueloAeropuertoSalidaCiudadId[0];
            AssignAttri("", false, "A30vueloAeropuertoSalidaCiudadId", StringUtil.LTrimStr( (decimal)(A30vueloAeropuertoSalidaCiudadId), 6, 0));
            pr_default.close(28);
            /* Using cursor T000535 */
            pr_default.execute(29, new Object[] {A32vueloAeropuertoSalidaPaisId});
            A33vueloAeropuertoSalidaPaisNombr = T000535_A33vueloAeropuertoSalidaPaisNombr[0];
            AssignAttri("", false, "A33vueloAeropuertoSalidaPaisNombr", A33vueloAeropuertoSalidaPaisNombr);
            pr_default.close(29);
            /* Using cursor T000536 */
            pr_default.execute(30, new Object[] {A32vueloAeropuertoSalidaPaisId, A30vueloAeropuertoSalidaCiudadId});
            A31vueloAeropuertoSalidaCiudadNom = T000536_A31vueloAeropuertoSalidaCiudadNom[0];
            AssignAttri("", false, "A31vueloAeropuertoSalidaCiudadNom", A31vueloAeropuertoSalidaCiudadNom);
            pr_default.close(30);
            /* Using cursor T000537 */
            pr_default.execute(31, new Object[] {A26vueloAeropuertoLlegadaId});
            A27vueloAeropuertoLlegadaNombre = T000537_A27vueloAeropuertoLlegadaNombre[0];
            AssignAttri("", false, "A27vueloAeropuertoLlegadaNombre", A27vueloAeropuertoLlegadaNombre);
            A34vueloAeropuertoLlegadaPaisId = T000537_A34vueloAeropuertoLlegadaPaisId[0];
            AssignAttri("", false, "A34vueloAeropuertoLlegadaPaisId", StringUtil.LTrimStr( (decimal)(A34vueloAeropuertoLlegadaPaisId), 6, 0));
            A36vueloAeropuertoLlegadaCiudadId = T000537_A36vueloAeropuertoLlegadaCiudadId[0];
            AssignAttri("", false, "A36vueloAeropuertoLlegadaCiudadId", StringUtil.LTrimStr( (decimal)(A36vueloAeropuertoLlegadaCiudadId), 6, 0));
            pr_default.close(31);
            /* Using cursor T000538 */
            pr_default.execute(32, new Object[] {A34vueloAeropuertoLlegadaPaisId});
            A35vueloAeropuertoLlegadaPaisNomb = T000538_A35vueloAeropuertoLlegadaPaisNomb[0];
            AssignAttri("", false, "A35vueloAeropuertoLlegadaPaisNomb", A35vueloAeropuertoLlegadaPaisNomb);
            pr_default.close(32);
            /* Using cursor T000539 */
            pr_default.execute(33, new Object[] {A34vueloAeropuertoLlegadaPaisId, A36vueloAeropuertoLlegadaCiudadId});
            A37vueloAeropuertoLlegadaCiudadNo = T000539_A37vueloAeropuertoLlegadaCiudadNo[0];
            AssignAttri("", false, "A37vueloAeropuertoLlegadaCiudadNo", A37vueloAeropuertoLlegadaCiudadNo);
            pr_default.close(33);
            /* Using cursor T000540 */
            pr_default.execute(34, new Object[] {n41aerolineaId, A41aerolineaId});
            A42aerolineaNombre = T000540_A42aerolineaNombre[0];
            AssignAttri("", false, "A42aerolineaNombre", A42aerolineaNombre);
            A43aerolineaDescuentoPorcentage = T000540_A43aerolineaDescuentoPorcentage[0];
            AssignAttri("", false, "A43aerolineaDescuentoPorcentage", StringUtil.LTrimStr( (decimal)(A43aerolineaDescuentoPorcentage), 3, 0));
            pr_default.close(34);
            if ( A43aerolineaDescuentoPorcentage > A39vueloDescuentoPorcentage )
            {
               A40vueloPrecioFinal = (decimal)(A38vueloPrecio*(1-A43aerolineaDescuentoPorcentage/ (decimal)(100)));
               AssignAttri("", false, "A40vueloPrecioFinal", StringUtil.LTrimStr( A40vueloPrecioFinal, 9, 2));
            }
            else
            {
               A40vueloPrecioFinal = (decimal)(A38vueloPrecio*(1-A39vueloDescuentoPorcentage/ (decimal)(100)));
               AssignAttri("", false, "A40vueloPrecioFinal", StringUtil.LTrimStr( A40vueloPrecioFinal, 9, 2));
            }
         }
      }

      protected void ProcessNestedLevel059( )
      {
         s47vueloCapacidad = O47vueloCapacidad;
         n47vueloCapacidad = false;
         AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
         nGXsfl_143_idx = 0;
         while ( nGXsfl_143_idx < nRC_GXsfl_143 )
         {
            ReadRow059( ) ;
            if ( ( nRcdExists_9 != 0 ) || ( nIsMod_9 != 0 ) )
            {
               standaloneNotModal059( ) ;
               GetKey059( ) ;
               if ( ( nRcdExists_9 == 0 ) && ( nRcdDeleted_9 == 0 ) )
               {
                  Gx_mode = "INS";
                  AssignAttri("", false, "Gx_mode", Gx_mode);
                  Insert059( ) ;
               }
               else
               {
                  if ( RcdFound9 != 0 )
                  {
                     if ( ( nRcdDeleted_9 != 0 ) && ( nRcdExists_9 != 0 ) )
                     {
                        Gx_mode = "DLT";
                        AssignAttri("", false, "Gx_mode", Gx_mode);
                        Delete059( ) ;
                     }
                     else
                     {
                        if ( nRcdExists_9 != 0 )
                        {
                           Gx_mode = "UPD";
                           AssignAttri("", false, "Gx_mode", Gx_mode);
                           Update059( ) ;
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_9 == 0 )
                     {
                        GXCCtl = "VUELOASIENTOID_" + sGXsfl_143_idx;
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, GXCCtl);
                        AnyError = 1;
                        GX_FocusControl = edtvueloAsientoId_Internalname;
                        AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
               O47vueloCapacidad = A47vueloCapacidad;
               n47vueloCapacidad = false;
               AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
            }
            ChangePostValue( edtvueloAsientoId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A44vueloAsientoId), 6, 0, ".", ""))) ;
            ChangePostValue( cmbvueloAsientoChar_Internalname, StringUtil.RTrim( A45vueloAsientoChar)) ;
            ChangePostValue( cmbvueloAsientoLocalizacion_Internalname, StringUtil.RTrim( A46vueloAsientoLocalizacion)) ;
            ChangePostValue( "ZT_"+"Z44vueloAsientoId_"+sGXsfl_143_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z44vueloAsientoId), 6, 0, ".", ""))) ;
            ChangePostValue( "ZT_"+"Z45vueloAsientoChar_"+sGXsfl_143_idx, StringUtil.RTrim( Z45vueloAsientoChar)) ;
            ChangePostValue( "ZT_"+"Z46vueloAsientoLocalizacion_"+sGXsfl_143_idx, StringUtil.RTrim( Z46vueloAsientoLocalizacion)) ;
            ChangePostValue( "nRcdDeleted_9_"+sGXsfl_143_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_9), 4, 0, ".", ""))) ;
            ChangePostValue( "nRcdExists_9_"+sGXsfl_143_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_9), 4, 0, ".", ""))) ;
            ChangePostValue( "nIsMod_9_"+sGXsfl_143_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_9), 4, 0, ".", ""))) ;
            if ( nIsMod_9 != 0 )
            {
               ChangePostValue( "VUELOASIENTOID_"+sGXsfl_143_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtvueloAsientoId_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "VUELOASIENTOCHAR_"+sGXsfl_143_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbvueloAsientoChar.Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "VUELOASIENTOLOCALIZACION_"+sGXsfl_143_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbvueloAsientoLocalizacion.Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         if ( ( A47vueloCapacidad > 8 ) && ( A47vueloCapacidad < 8 ) )
         {
            GX_msglist.addItem("La cantidad de asientos no puede ser menora 8", 1, "");
            AnyError = 1;
         }
         /* End of After( level) rules */
         InitAll059( ) ;
         if ( AnyError != 0 )
         {
            O47vueloCapacidad = s47vueloCapacidad;
            n47vueloCapacidad = false;
            AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
         }
         nRcdExists_9 = 0;
         nIsMod_9 = 0;
         nRcdDeleted_9 = 0;
      }

      protected void ProcessLevel056( )
      {
         /* Save parent mode. */
         sMode6 = Gx_mode;
         ProcessNestedLevel059( ) ;
         if ( AnyError != 0 )
         {
            O47vueloCapacidad = s47vueloCapacidad;
            n47vueloCapacidad = false;
            AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
         }
         /* Restore parent mode. */
         Gx_mode = sMode6;
         AssignAttri("", false, "Gx_mode", Gx_mode);
         /* ' Update level parameters */
      }

      protected void EndLevel056( )
      {
         if ( ! IsIns( ) )
         {
            pr_default.close(2);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete056( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(3);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(34);
            pr_default.close(28);
            pr_default.close(31);
            pr_default.close(29);
            pr_default.close(30);
            pr_default.close(32);
            pr_default.close(33);
            pr_default.close(27);
            context.CommitDataStores("vuelo",pr_default);
            if ( AnyError == 0 )
            {
               ConfirmValues050( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(3);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(34);
            pr_default.close(28);
            pr_default.close(31);
            pr_default.close(29);
            pr_default.close(30);
            pr_default.close(32);
            pr_default.close(33);
            pr_default.close(27);
            context.RollbackDataStores("vuelo",pr_default);
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart056( )
      {
         /* Using cursor T000541 */
         pr_default.execute(35);
         RcdFound6 = 0;
         if ( (pr_default.getStatus(35) != 101) )
         {
            RcdFound6 = 1;
            A18vueloId = T000541_A18vueloId[0];
            AssignAttri("", false, "A18vueloId", StringUtil.LTrimStr( (decimal)(A18vueloId), 6, 0));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext056( )
      {
         /* Scan next routine */
         pr_default.readNext(35);
         RcdFound6 = 0;
         if ( (pr_default.getStatus(35) != 101) )
         {
            RcdFound6 = 1;
            A18vueloId = T000541_A18vueloId[0];
            AssignAttri("", false, "A18vueloId", StringUtil.LTrimStr( (decimal)(A18vueloId), 6, 0));
         }
      }

      protected void ScanEnd056( )
      {
         pr_default.close(35);
      }

      protected void AfterConfirm056( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert056( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate056( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete056( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete056( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate056( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes056( )
      {
         edtvueloId_Enabled = 0;
         AssignProp("", false, edtvueloId_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtvueloId_Enabled), 5, 0), true);
         edtvueloFecha_Enabled = 0;
         AssignProp("", false, edtvueloFecha_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtvueloFecha_Enabled), 5, 0), true);
         edtvueloAeropuertoSalidaId_Enabled = 0;
         AssignProp("", false, edtvueloAeropuertoSalidaId_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtvueloAeropuertoSalidaId_Enabled), 5, 0), true);
         edtvueloAeropuertoSalidaNombre_Enabled = 0;
         AssignProp("", false, edtvueloAeropuertoSalidaNombre_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtvueloAeropuertoSalidaNombre_Enabled), 5, 0), true);
         edtvueloAeropuertoSalidaPaisId_Enabled = 0;
         AssignProp("", false, edtvueloAeropuertoSalidaPaisId_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtvueloAeropuertoSalidaPaisId_Enabled), 5, 0), true);
         edtvueloAeropuertoSalidaPaisNombr_Enabled = 0;
         AssignProp("", false, edtvueloAeropuertoSalidaPaisNombr_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtvueloAeropuertoSalidaPaisNombr_Enabled), 5, 0), true);
         edtvueloAeropuertoSalidaCiudadId_Enabled = 0;
         AssignProp("", false, edtvueloAeropuertoSalidaCiudadId_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtvueloAeropuertoSalidaCiudadId_Enabled), 5, 0), true);
         edtvueloAeropuertoSalidaCiudadNom_Enabled = 0;
         AssignProp("", false, edtvueloAeropuertoSalidaCiudadNom_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtvueloAeropuertoSalidaCiudadNom_Enabled), 5, 0), true);
         edtvueloAeropuertoLlegadaId_Enabled = 0;
         AssignProp("", false, edtvueloAeropuertoLlegadaId_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtvueloAeropuertoLlegadaId_Enabled), 5, 0), true);
         edtvueloAeropuertoLlegadaNombre_Enabled = 0;
         AssignProp("", false, edtvueloAeropuertoLlegadaNombre_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtvueloAeropuertoLlegadaNombre_Enabled), 5, 0), true);
         edtvueloAeropuertoLlegadaPaisId_Enabled = 0;
         AssignProp("", false, edtvueloAeropuertoLlegadaPaisId_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtvueloAeropuertoLlegadaPaisId_Enabled), 5, 0), true);
         edtvueloAeropuertoLlegadaPaisNomb_Enabled = 0;
         AssignProp("", false, edtvueloAeropuertoLlegadaPaisNomb_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtvueloAeropuertoLlegadaPaisNomb_Enabled), 5, 0), true);
         edtvueloAeropuertoLlegadaCiudadId_Enabled = 0;
         AssignProp("", false, edtvueloAeropuertoLlegadaCiudadId_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtvueloAeropuertoLlegadaCiudadId_Enabled), 5, 0), true);
         edtvueloAeropuertoLlegadaCiudadNo_Enabled = 0;
         AssignProp("", false, edtvueloAeropuertoLlegadaCiudadNo_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtvueloAeropuertoLlegadaCiudadNo_Enabled), 5, 0), true);
         edtvueloPrecio_Enabled = 0;
         AssignProp("", false, edtvueloPrecio_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtvueloPrecio_Enabled), 5, 0), true);
         edtvueloDescuentoPorcentage_Enabled = 0;
         AssignProp("", false, edtvueloDescuentoPorcentage_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtvueloDescuentoPorcentage_Enabled), 5, 0), true);
         edtaerolineaId_Enabled = 0;
         AssignProp("", false, edtaerolineaId_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtaerolineaId_Enabled), 5, 0), true);
         edtaerolineaNombre_Enabled = 0;
         AssignProp("", false, edtaerolineaNombre_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtaerolineaNombre_Enabled), 5, 0), true);
         edtaerolineaDescuentoPorcentage_Enabled = 0;
         AssignProp("", false, edtaerolineaDescuentoPorcentage_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtaerolineaDescuentoPorcentage_Enabled), 5, 0), true);
         edtvueloPrecioFinal_Enabled = 0;
         AssignProp("", false, edtvueloPrecioFinal_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtvueloPrecioFinal_Enabled), 5, 0), true);
         edtvueloCapacidad_Enabled = 0;
         AssignProp("", false, edtvueloCapacidad_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtvueloCapacidad_Enabled), 5, 0), true);
      }

      protected void ZM059( short GX_JID )
      {
         if ( ( GX_JID == 16 ) || ( GX_JID == 0 ) )
         {
            if ( ! IsIns( ) )
            {
               Z46vueloAsientoLocalizacion = T00053_A46vueloAsientoLocalizacion[0];
            }
            else
            {
               Z46vueloAsientoLocalizacion = A46vueloAsientoLocalizacion;
            }
         }
         if ( GX_JID == -16 )
         {
            Z18vueloId = A18vueloId;
            Z44vueloAsientoId = A44vueloAsientoId;
            Z45vueloAsientoChar = A45vueloAsientoChar;
            Z46vueloAsientoLocalizacion = A46vueloAsientoLocalizacion;
         }
      }

      protected void standaloneNotModal059( )
      {
      }

      protected void standaloneModal059( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            edtvueloAsientoId_Enabled = 0;
            AssignProp("", false, edtvueloAsientoId_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtvueloAsientoId_Enabled), 5, 0), !bGXsfl_143_Refreshing);
         }
         else
         {
            edtvueloAsientoId_Enabled = 1;
            AssignProp("", false, edtvueloAsientoId_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtvueloAsientoId_Enabled), 5, 0), !bGXsfl_143_Refreshing);
         }
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            cmbvueloAsientoChar.Enabled = 0;
            AssignProp("", false, cmbvueloAsientoChar_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(cmbvueloAsientoChar.Enabled), 5, 0), !bGXsfl_143_Refreshing);
         }
         else
         {
            cmbvueloAsientoChar.Enabled = 1;
            AssignProp("", false, cmbvueloAsientoChar_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(cmbvueloAsientoChar.Enabled), 5, 0), !bGXsfl_143_Refreshing);
         }
      }

      protected void Load059( )
      {
         /* Using cursor T000542 */
         pr_default.execute(36, new Object[] {A18vueloId, A44vueloAsientoId, A45vueloAsientoChar});
         if ( (pr_default.getStatus(36) != 101) )
         {
            RcdFound9 = 1;
            A46vueloAsientoLocalizacion = T000542_A46vueloAsientoLocalizacion[0];
            ZM059( -16) ;
         }
         pr_default.close(36);
         OnLoadActions059( ) ;
      }

      protected void OnLoadActions059( )
      {
         if ( IsIns( )  )
         {
            A47vueloCapacidad = (short)(O47vueloCapacidad+1);
            n47vueloCapacidad = false;
            AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
         }
         else
         {
            if ( IsUpd( )  )
            {
               A47vueloCapacidad = O47vueloCapacidad;
               n47vueloCapacidad = false;
               AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
            }
            else
            {
               if ( IsDlt( )  )
               {
                  A47vueloCapacidad = (short)(O47vueloCapacidad-1);
                  n47vueloCapacidad = false;
                  AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
               }
            }
         }
      }

      protected void CheckExtendedTable059( )
      {
         nIsDirty_9 = 0;
         Gx_BScreen = 1;
         standaloneModal059( ) ;
         if ( ! ( ( StringUtil.StrCmp(A45vueloAsientoChar, "A") == 0 ) || ( StringUtil.StrCmp(A45vueloAsientoChar, "B") == 0 ) || ( StringUtil.StrCmp(A45vueloAsientoChar, "C") == 0 ) || ( StringUtil.StrCmp(A45vueloAsientoChar, "D") == 0 ) || ( StringUtil.StrCmp(A45vueloAsientoChar, "E") == 0 ) || ( StringUtil.StrCmp(A45vueloAsientoChar, "F") == 0 ) ) )
         {
            GXCCtl = "VUELOASIENTOCHAR_" + sGXsfl_143_idx;
            GX_msglist.addItem("Field vuelo Asiento Char is out of range", "OutOfRange", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = cmbvueloAsientoChar_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( StringUtil.StrCmp(A46vueloAsientoLocalizacion, "V") == 0 ) || ( StringUtil.StrCmp(A46vueloAsientoLocalizacion, "M") == 0 ) || ( StringUtil.StrCmp(A46vueloAsientoLocalizacion, "P") == 0 ) ) )
         {
            GXCCtl = "VUELOASIENTOLOCALIZACION_" + sGXsfl_143_idx;
            GX_msglist.addItem("Field vuelo Asiento Localizacion is out of range", "OutOfRange", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = cmbvueloAsientoLocalizacion_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( IsIns( )  )
         {
            nIsDirty_9 = 1;
            A47vueloCapacidad = (short)(O47vueloCapacidad+1);
            n47vueloCapacidad = false;
            AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
         }
         else
         {
            if ( IsUpd( )  )
            {
               nIsDirty_9 = 1;
               A47vueloCapacidad = O47vueloCapacidad;
               n47vueloCapacidad = false;
               AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
            }
            else
            {
               if ( IsDlt( )  )
               {
                  nIsDirty_9 = 1;
                  A47vueloCapacidad = (short)(O47vueloCapacidad-1);
                  n47vueloCapacidad = false;
                  AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
               }
            }
         }
      }

      protected void CloseExtendedTableCursors059( )
      {
      }

      protected void enableDisable059( )
      {
      }

      protected void GetKey059( )
      {
         /* Using cursor T000543 */
         pr_default.execute(37, new Object[] {A18vueloId, A44vueloAsientoId, A45vueloAsientoChar});
         if ( (pr_default.getStatus(37) != 101) )
         {
            RcdFound9 = 1;
         }
         else
         {
            RcdFound9 = 0;
         }
         pr_default.close(37);
      }

      protected void getByPrimaryKey059( )
      {
         /* Using cursor T00053 */
         pr_default.execute(1, new Object[] {A18vueloId, A44vueloAsientoId, A45vueloAsientoChar});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM059( 16) ;
            RcdFound9 = 1;
            InitializeNonKey059( ) ;
            A44vueloAsientoId = T00053_A44vueloAsientoId[0];
            A45vueloAsientoChar = T00053_A45vueloAsientoChar[0];
            A46vueloAsientoLocalizacion = T00053_A46vueloAsientoLocalizacion[0];
            Z18vueloId = A18vueloId;
            Z44vueloAsientoId = A44vueloAsientoId;
            Z45vueloAsientoChar = A45vueloAsientoChar;
            sMode9 = Gx_mode;
            Gx_mode = "DSP";
            AssignAttri("", false, "Gx_mode", Gx_mode);
            standaloneModal059( ) ;
            Load059( ) ;
            Gx_mode = sMode9;
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound9 = 0;
            InitializeNonKey059( ) ;
            sMode9 = Gx_mode;
            Gx_mode = "DSP";
            AssignAttri("", false, "Gx_mode", Gx_mode);
            standaloneModal059( ) ;
            Gx_mode = sMode9;
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         if ( IsDsp( ) || IsDlt( ) )
         {
            DisableAttributes059( ) ;
         }
         pr_default.close(1);
      }

      protected void CheckOptimisticConcurrency059( )
      {
         if ( ! IsIns( ) )
         {
            /* Using cursor T00052 */
            pr_default.execute(0, new Object[] {A18vueloId, A44vueloAsientoId, A45vueloAsientoChar});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"vueloAsiento"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z46vueloAsientoLocalizacion, T00052_A46vueloAsientoLocalizacion[0]) != 0 ) )
            {
               if ( StringUtil.StrCmp(Z46vueloAsientoLocalizacion, T00052_A46vueloAsientoLocalizacion[0]) != 0 )
               {
                  GXUtil.WriteLog("vuelo:[seudo value changed for attri]"+"vueloAsientoLocalizacion");
                  GXUtil.WriteLogRaw("Old: ",Z46vueloAsientoLocalizacion);
                  GXUtil.WriteLogRaw("Current: ",T00052_A46vueloAsientoLocalizacion[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"vueloAsiento"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert059( )
      {
         BeforeValidate059( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable059( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM059( 0) ;
            CheckOptimisticConcurrency059( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm059( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert059( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000544 */
                     pr_default.execute(38, new Object[] {A18vueloId, A44vueloAsientoId, A45vueloAsientoChar, A46vueloAsientoLocalizacion});
                     pr_default.close(38);
                     dsDefault.SmartCacheProvider.SetUpdated("vueloAsiento") ;
                     if ( (pr_default.getStatus(38) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load059( ) ;
            }
            EndLevel059( ) ;
         }
         CloseExtendedTableCursors059( ) ;
      }

      protected void Update059( )
      {
         BeforeValidate059( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable059( ) ;
         }
         if ( ( nIsMod_9 != 0 ) || ( nIsDirty_9 != 0 ) )
         {
            if ( AnyError == 0 )
            {
               CheckOptimisticConcurrency059( ) ;
               if ( AnyError == 0 )
               {
                  AfterConfirm059( ) ;
                  if ( AnyError == 0 )
                  {
                     BeforeUpdate059( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Using cursor T000545 */
                        pr_default.execute(39, new Object[] {A46vueloAsientoLocalizacion, A18vueloId, A44vueloAsientoId, A45vueloAsientoChar});
                        pr_default.close(39);
                        dsDefault.SmartCacheProvider.SetUpdated("vueloAsiento") ;
                        if ( (pr_default.getStatus(39) == 103) )
                        {
                           GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"vueloAsiento"}), "RecordIsLocked", 1, "");
                           AnyError = 1;
                        }
                        DeferredUpdate059( ) ;
                        if ( AnyError == 0 )
                        {
                           /* Start of After( update) rules */
                           /* End of After( update) rules */
                           if ( AnyError == 0 )
                           {
                              getByPrimaryKey059( ) ;
                           }
                        }
                        else
                        {
                           GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                           AnyError = 1;
                        }
                     }
                  }
               }
               EndLevel059( ) ;
            }
         }
         CloseExtendedTableCursors059( ) ;
      }

      protected void DeferredUpdate059( )
      {
      }

      protected void Delete059( )
      {
         Gx_mode = "DLT";
         AssignAttri("", false, "Gx_mode", Gx_mode);
         BeforeValidate059( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency059( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls059( ) ;
            AfterConfirm059( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete059( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000546 */
                  pr_default.execute(40, new Object[] {A18vueloId, A44vueloAsientoId, A45vueloAsientoChar});
                  pr_default.close(40);
                  dsDefault.SmartCacheProvider.SetUpdated("vueloAsiento") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode9 = Gx_mode;
         Gx_mode = "DLT";
         AssignAttri("", false, "Gx_mode", Gx_mode);
         EndLevel059( ) ;
         Gx_mode = sMode9;
         AssignAttri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls059( )
      {
         standaloneModal059( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            if ( IsIns( )  )
            {
               A47vueloCapacidad = (short)(O47vueloCapacidad+1);
               n47vueloCapacidad = false;
               AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
            }
            else
            {
               if ( IsUpd( )  )
               {
                  A47vueloCapacidad = O47vueloCapacidad;
                  n47vueloCapacidad = false;
                  AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
               }
               else
               {
                  if ( IsDlt( )  )
                  {
                     A47vueloCapacidad = (short)(O47vueloCapacidad-1);
                     n47vueloCapacidad = false;
                     AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
                  }
               }
            }
         }
      }

      protected void EndLevel059( )
      {
         if ( ! IsIns( ) )
         {
            pr_default.close(0);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart059( )
      {
         /* Scan By routine */
         /* Using cursor T000547 */
         pr_default.execute(41, new Object[] {A18vueloId});
         RcdFound9 = 0;
         if ( (pr_default.getStatus(41) != 101) )
         {
            RcdFound9 = 1;
            A44vueloAsientoId = T000547_A44vueloAsientoId[0];
            A45vueloAsientoChar = T000547_A45vueloAsientoChar[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext059( )
      {
         /* Scan next routine */
         pr_default.readNext(41);
         RcdFound9 = 0;
         if ( (pr_default.getStatus(41) != 101) )
         {
            RcdFound9 = 1;
            A44vueloAsientoId = T000547_A44vueloAsientoId[0];
            A45vueloAsientoChar = T000547_A45vueloAsientoChar[0];
         }
      }

      protected void ScanEnd059( )
      {
         pr_default.close(41);
      }

      protected void AfterConfirm059( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert059( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate059( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete059( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete059( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate059( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes059( )
      {
         edtvueloAsientoId_Enabled = 0;
         AssignProp("", false, edtvueloAsientoId_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtvueloAsientoId_Enabled), 5, 0), !bGXsfl_143_Refreshing);
         cmbvueloAsientoChar.Enabled = 0;
         AssignProp("", false, cmbvueloAsientoChar_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(cmbvueloAsientoChar.Enabled), 5, 0), !bGXsfl_143_Refreshing);
         cmbvueloAsientoLocalizacion.Enabled = 0;
         AssignProp("", false, cmbvueloAsientoLocalizacion_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(cmbvueloAsientoLocalizacion.Enabled), 5, 0), !bGXsfl_143_Refreshing);
      }

      protected void send_integrity_lvl_hashes059( )
      {
      }

      protected void send_integrity_lvl_hashes056( )
      {
      }

      protected void SubsflControlProps_1439( )
      {
         edtvueloAsientoId_Internalname = "VUELOASIENTOID_"+sGXsfl_143_idx;
         cmbvueloAsientoChar_Internalname = "VUELOASIENTOCHAR_"+sGXsfl_143_idx;
         cmbvueloAsientoLocalizacion_Internalname = "VUELOASIENTOLOCALIZACION_"+sGXsfl_143_idx;
      }

      protected void SubsflControlProps_fel_1439( )
      {
         edtvueloAsientoId_Internalname = "VUELOASIENTOID_"+sGXsfl_143_fel_idx;
         cmbvueloAsientoChar_Internalname = "VUELOASIENTOCHAR_"+sGXsfl_143_fel_idx;
         cmbvueloAsientoLocalizacion_Internalname = "VUELOASIENTOLOCALIZACION_"+sGXsfl_143_fel_idx;
      }

      protected void AddRow059( )
      {
         nGXsfl_143_idx = (int)(nGXsfl_143_idx+1);
         sGXsfl_143_idx = StringUtil.PadL( StringUtil.LTrimStr( (decimal)(nGXsfl_143_idx), 4, 0), 4, "0");
         SubsflControlProps_1439( ) ;
         SendRow059( ) ;
      }

      protected void SendRow059( )
      {
         Gridvuelo_asientoRow = GXWebRow.GetNew(context);
         if ( subGridvuelo_asiento_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGridvuelo_asiento_Backstyle = 0;
            if ( StringUtil.StrCmp(subGridvuelo_asiento_Class, "") != 0 )
            {
               subGridvuelo_asiento_Linesclass = subGridvuelo_asiento_Class+"Odd";
            }
         }
         else if ( subGridvuelo_asiento_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGridvuelo_asiento_Backstyle = 0;
            subGridvuelo_asiento_Backcolor = subGridvuelo_asiento_Allbackcolor;
            if ( StringUtil.StrCmp(subGridvuelo_asiento_Class, "") != 0 )
            {
               subGridvuelo_asiento_Linesclass = subGridvuelo_asiento_Class+"Uniform";
            }
         }
         else if ( subGridvuelo_asiento_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGridvuelo_asiento_Backstyle = 1;
            if ( StringUtil.StrCmp(subGridvuelo_asiento_Class, "") != 0 )
            {
               subGridvuelo_asiento_Linesclass = subGridvuelo_asiento_Class+"Odd";
            }
            subGridvuelo_asiento_Backcolor = (int)(0x0);
         }
         else if ( subGridvuelo_asiento_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGridvuelo_asiento_Backstyle = 1;
            if ( ((int)((nGXsfl_143_idx) % (2))) == 0 )
            {
               subGridvuelo_asiento_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridvuelo_asiento_Class, "") != 0 )
               {
                  subGridvuelo_asiento_Linesclass = subGridvuelo_asiento_Class+"Even";
               }
            }
            else
            {
               subGridvuelo_asiento_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridvuelo_asiento_Class, "") != 0 )
               {
                  subGridvuelo_asiento_Linesclass = subGridvuelo_asiento_Class+"Odd";
               }
            }
         }
         /* Subfile cell */
         /* Single line edit */
         TempTags = " data-gxoch1=\"gx.fn.setControlValue('nIsMod_9_" + sGXsfl_143_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 144,'',false,'" + sGXsfl_143_idx + "',143)\"";
         ROClassString = "Attribute";
         Gridvuelo_asientoRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtvueloAsientoId_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A44vueloAsientoId), 6, 0, ".", "")),StringUtil.LTrim( context.localUtil.Format( (decimal)(A44vueloAsientoId), "ZZZZZ9")),TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,144);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtvueloAsientoId_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(int)edtvueloAsientoId_Enabled,(short)1,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)143,(short)1,(short)-1,(short)0,(bool)true,(String)"Id",(String)"right",(bool)false,(String)""});
         /* Subfile cell */
         TempTags = " data-gxoch1=\"gx.fn.setControlValue('nIsMod_9_" + sGXsfl_143_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 145,'',false,'" + sGXsfl_143_idx + "',143)\"";
         GXCCtl = "VUELOASIENTOCHAR_" + sGXsfl_143_idx;
         cmbvueloAsientoChar.Name = GXCCtl;
         cmbvueloAsientoChar.WebTags = "";
         cmbvueloAsientoChar.addItem("A", "A", 0);
         cmbvueloAsientoChar.addItem("B", "B", 0);
         cmbvueloAsientoChar.addItem("C", "C", 0);
         cmbvueloAsientoChar.addItem("D", "D", 0);
         cmbvueloAsientoChar.addItem("E", "E", 0);
         cmbvueloAsientoChar.addItem("F", "F", 0);
         if ( cmbvueloAsientoChar.ItemCount > 0 )
         {
            A45vueloAsientoChar = cmbvueloAsientoChar.getValidValue(A45vueloAsientoChar);
         }
         /* ComboBox */
         Gridvuelo_asientoRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbvueloAsientoChar,(String)cmbvueloAsientoChar_Internalname,StringUtil.RTrim( A45vueloAsientoChar),(short)1,(String)cmbvueloAsientoChar_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,cmbvueloAsientoChar.Enabled,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",(String)"",TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,145);\"",(String)"",(bool)true});
         cmbvueloAsientoChar.CurrentValue = StringUtil.RTrim( A45vueloAsientoChar);
         AssignProp("", false, cmbvueloAsientoChar_Internalname, "Values", (String)(cmbvueloAsientoChar.ToJavascriptSource()), !bGXsfl_143_Refreshing);
         /* Subfile cell */
         TempTags = " data-gxoch1=\"gx.fn.setControlValue('nIsMod_9_" + sGXsfl_143_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 146,'',false,'" + sGXsfl_143_idx + "',143)\"";
         if ( ( cmbvueloAsientoLocalizacion.ItemCount == 0 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "VUELOASIENTOLOCALIZACION_" + sGXsfl_143_idx;
            cmbvueloAsientoLocalizacion.Name = GXCCtl;
            cmbvueloAsientoLocalizacion.WebTags = "";
            cmbvueloAsientoLocalizacion.addItem("V", "Ventana", 0);
            cmbvueloAsientoLocalizacion.addItem("M", "Medio", 0);
            cmbvueloAsientoLocalizacion.addItem("P", "Pasillo", 0);
            if ( cmbvueloAsientoLocalizacion.ItemCount > 0 )
            {
               A46vueloAsientoLocalizacion = cmbvueloAsientoLocalizacion.getValidValue(A46vueloAsientoLocalizacion);
            }
         }
         /* ComboBox */
         Gridvuelo_asientoRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbvueloAsientoLocalizacion,(String)cmbvueloAsientoLocalizacion_Internalname,StringUtil.RTrim( A46vueloAsientoLocalizacion),(short)1,(String)cmbvueloAsientoLocalizacion_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,cmbvueloAsientoLocalizacion.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",(String)"",TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,146);\"",(String)"",(bool)true});
         cmbvueloAsientoLocalizacion.CurrentValue = StringUtil.RTrim( A46vueloAsientoLocalizacion);
         AssignProp("", false, cmbvueloAsientoLocalizacion_Internalname, "Values", (String)(cmbvueloAsientoLocalizacion.ToJavascriptSource()), !bGXsfl_143_Refreshing);
         context.httpAjaxContext.ajax_sending_grid_row(Gridvuelo_asientoRow);
         send_integrity_lvl_hashes059( ) ;
         GXCCtl = "Z44vueloAsientoId_" + sGXsfl_143_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z44vueloAsientoId), 6, 0, ".", "")));
         GXCCtl = "Z45vueloAsientoChar_" + sGXsfl_143_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.RTrim( Z45vueloAsientoChar));
         GXCCtl = "Z46vueloAsientoLocalizacion_" + sGXsfl_143_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.RTrim( Z46vueloAsientoLocalizacion));
         GXCCtl = "nRcdDeleted_9_" + sGXsfl_143_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_9), 4, 0, ".", "")));
         GXCCtl = "nRcdExists_9_" + sGXsfl_143_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_9), 4, 0, ".", "")));
         GXCCtl = "nIsMod_9_" + sGXsfl_143_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_9), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "VUELOASIENTOID_"+sGXsfl_143_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtvueloAsientoId_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "VUELOASIENTOCHAR_"+sGXsfl_143_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbvueloAsientoChar.Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "VUELOASIENTOLOCALIZACION_"+sGXsfl_143_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbvueloAsientoLocalizacion.Enabled), 5, 0, ".", "")));
         context.httpAjaxContext.ajax_sending_grid_row(null);
         Gridvuelo_asientoContainer.AddRow(Gridvuelo_asientoRow);
      }

      protected void ReadRow059( )
      {
         nGXsfl_143_idx = (int)(nGXsfl_143_idx+1);
         sGXsfl_143_idx = StringUtil.PadL( StringUtil.LTrimStr( (decimal)(nGXsfl_143_idx), 4, 0), 4, "0");
         SubsflControlProps_1439( ) ;
         edtvueloAsientoId_Enabled = (int)(context.localUtil.CToN( cgiGet( "VUELOASIENTOID_"+sGXsfl_143_idx+"Enabled"), ".", ","));
         cmbvueloAsientoChar.Enabled = (int)(context.localUtil.CToN( cgiGet( "VUELOASIENTOCHAR_"+sGXsfl_143_idx+"Enabled"), ".", ","));
         cmbvueloAsientoLocalizacion.Enabled = (int)(context.localUtil.CToN( cgiGet( "VUELOASIENTOLOCALIZACION_"+sGXsfl_143_idx+"Enabled"), ".", ","));
         if ( ( ( context.localUtil.CToN( cgiGet( edtvueloAsientoId_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtvueloAsientoId_Internalname), ".", ",") > Convert.ToDecimal( 999999 )) ) )
         {
            GXCCtl = "VUELOASIENTOID_" + sGXsfl_143_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtvueloAsientoId_Internalname;
            wbErr = true;
            A44vueloAsientoId = 0;
         }
         else
         {
            A44vueloAsientoId = (int)(context.localUtil.CToN( cgiGet( edtvueloAsientoId_Internalname), ".", ","));
         }
         cmbvueloAsientoChar.Name = cmbvueloAsientoChar_Internalname;
         cmbvueloAsientoChar.CurrentValue = cgiGet( cmbvueloAsientoChar_Internalname);
         A45vueloAsientoChar = cgiGet( cmbvueloAsientoChar_Internalname);
         cmbvueloAsientoLocalizacion.Name = cmbvueloAsientoLocalizacion_Internalname;
         cmbvueloAsientoLocalizacion.CurrentValue = cgiGet( cmbvueloAsientoLocalizacion_Internalname);
         A46vueloAsientoLocalizacion = cgiGet( cmbvueloAsientoLocalizacion_Internalname);
         GXCCtl = "Z44vueloAsientoId_" + sGXsfl_143_idx;
         Z44vueloAsientoId = (int)(context.localUtil.CToN( cgiGet( GXCCtl), ".", ","));
         GXCCtl = "Z45vueloAsientoChar_" + sGXsfl_143_idx;
         Z45vueloAsientoChar = cgiGet( GXCCtl);
         GXCCtl = "Z46vueloAsientoLocalizacion_" + sGXsfl_143_idx;
         Z46vueloAsientoLocalizacion = cgiGet( GXCCtl);
         GXCCtl = "nRcdDeleted_9_" + sGXsfl_143_idx;
         nRcdDeleted_9 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ".", ","));
         GXCCtl = "nRcdExists_9_" + sGXsfl_143_idx;
         nRcdExists_9 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ".", ","));
         GXCCtl = "nIsMod_9_" + sGXsfl_143_idx;
         nIsMod_9 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ".", ","));
      }

      protected void assign_properties_default( )
      {
         defcmbvueloAsientoChar_Enabled = cmbvueloAsientoChar.Enabled;
         defedtvueloAsientoId_Enabled = edtvueloAsientoId_Enabled;
      }

      protected void ConfirmValues050( )
      {
         nGXsfl_143_idx = 0;
         sGXsfl_143_idx = StringUtil.PadL( StringUtil.LTrimStr( (decimal)(nGXsfl_143_idx), 4, 0), 4, "0");
         SubsflControlProps_1439( ) ;
         while ( nGXsfl_143_idx < nRC_GXsfl_143 )
         {
            nGXsfl_143_idx = (int)(nGXsfl_143_idx+1);
            sGXsfl_143_idx = StringUtil.PadL( StringUtil.LTrimStr( (decimal)(nGXsfl_143_idx), 4, 0), 4, "0");
            SubsflControlProps_1439( ) ;
            ChangePostValue( "Z44vueloAsientoId_"+sGXsfl_143_idx, cgiGet( "ZT_"+"Z44vueloAsientoId_"+sGXsfl_143_idx)) ;
            DeletePostValue( "ZT_"+"Z44vueloAsientoId_"+sGXsfl_143_idx) ;
            ChangePostValue( "Z45vueloAsientoChar_"+sGXsfl_143_idx, cgiGet( "ZT_"+"Z45vueloAsientoChar_"+sGXsfl_143_idx)) ;
            DeletePostValue( "ZT_"+"Z45vueloAsientoChar_"+sGXsfl_143_idx) ;
            ChangePostValue( "Z46vueloAsientoLocalizacion_"+sGXsfl_143_idx, cgiGet( "ZT_"+"Z46vueloAsientoLocalizacion_"+sGXsfl_143_idx)) ;
            DeletePostValue( "ZT_"+"Z46vueloAsientoLocalizacion_"+sGXsfl_143_idx) ;
         }
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 136889), false, true);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 136889), false, true);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 136889), false, true);
         context.AddJavascriptSource("gxcfg.js", "?20191251293848", false, true);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 136889), false, true);
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 136889), false, true);
         context.AddJavascriptSource("calendar-en.js", "?"+context.GetBuildNumber( 136889), false, true);
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + "-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("vuelo.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" title=\"submit\" style=\"display:none\" disabled>") ;
         AssignProp("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "Z18vueloId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z18vueloId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z58vueloFecha", context.localUtil.DToC( Z58vueloFecha, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z38vueloPrecio", StringUtil.LTrim( StringUtil.NToC( Z38vueloPrecio, 9, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z39vueloDescuentoPorcentage", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z39vueloDescuentoPorcentage), 3, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z41aerolineaId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z41aerolineaId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z24vueloAeropuertoSalidaId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z24vueloAeropuertoSalidaId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z26vueloAeropuertoLlegadaId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z26vueloAeropuertoLlegadaId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "O47vueloCapacidad", StringUtil.LTrim( StringUtil.NToC( (decimal)(O47vueloCapacidad), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_143", StringUtil.LTrim( StringUtil.NToC( (decimal)(nGXsfl_143_idx), 8, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendSecurityToken(sPrefix);
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("vuelo.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "vuelo" ;
      }

      public override String GetPgmdesc( )
      {
         return "vuelo" ;
      }

      protected void InitializeNonKey056( )
      {
         A40vueloPrecioFinal = 0;
         AssignAttri("", false, "A40vueloPrecioFinal", StringUtil.LTrimStr( A40vueloPrecioFinal, 9, 2));
         A58vueloFecha = DateTime.MinValue;
         AssignAttri("", false, "A58vueloFecha", context.localUtil.Format(A58vueloFecha, "99/99/99"));
         A24vueloAeropuertoSalidaId = 0;
         AssignAttri("", false, "A24vueloAeropuertoSalidaId", StringUtil.LTrimStr( (decimal)(A24vueloAeropuertoSalidaId), 6, 0));
         A25vueloAeropuertoSalidaNombre = "";
         AssignAttri("", false, "A25vueloAeropuertoSalidaNombre", A25vueloAeropuertoSalidaNombre);
         A32vueloAeropuertoSalidaPaisId = 0;
         AssignAttri("", false, "A32vueloAeropuertoSalidaPaisId", StringUtil.LTrimStr( (decimal)(A32vueloAeropuertoSalidaPaisId), 6, 0));
         A33vueloAeropuertoSalidaPaisNombr = "";
         AssignAttri("", false, "A33vueloAeropuertoSalidaPaisNombr", A33vueloAeropuertoSalidaPaisNombr);
         A30vueloAeropuertoSalidaCiudadId = 0;
         AssignAttri("", false, "A30vueloAeropuertoSalidaCiudadId", StringUtil.LTrimStr( (decimal)(A30vueloAeropuertoSalidaCiudadId), 6, 0));
         A31vueloAeropuertoSalidaCiudadNom = "";
         AssignAttri("", false, "A31vueloAeropuertoSalidaCiudadNom", A31vueloAeropuertoSalidaCiudadNom);
         A26vueloAeropuertoLlegadaId = 0;
         AssignAttri("", false, "A26vueloAeropuertoLlegadaId", StringUtil.LTrimStr( (decimal)(A26vueloAeropuertoLlegadaId), 6, 0));
         A27vueloAeropuertoLlegadaNombre = "";
         AssignAttri("", false, "A27vueloAeropuertoLlegadaNombre", A27vueloAeropuertoLlegadaNombre);
         A34vueloAeropuertoLlegadaPaisId = 0;
         AssignAttri("", false, "A34vueloAeropuertoLlegadaPaisId", StringUtil.LTrimStr( (decimal)(A34vueloAeropuertoLlegadaPaisId), 6, 0));
         A35vueloAeropuertoLlegadaPaisNomb = "";
         AssignAttri("", false, "A35vueloAeropuertoLlegadaPaisNomb", A35vueloAeropuertoLlegadaPaisNomb);
         A36vueloAeropuertoLlegadaCiudadId = 0;
         AssignAttri("", false, "A36vueloAeropuertoLlegadaCiudadId", StringUtil.LTrimStr( (decimal)(A36vueloAeropuertoLlegadaCiudadId), 6, 0));
         A37vueloAeropuertoLlegadaCiudadNo = "";
         AssignAttri("", false, "A37vueloAeropuertoLlegadaCiudadNo", A37vueloAeropuertoLlegadaCiudadNo);
         A38vueloPrecio = 0;
         AssignAttri("", false, "A38vueloPrecio", StringUtil.LTrimStr( A38vueloPrecio, 9, 2));
         A39vueloDescuentoPorcentage = 0;
         AssignAttri("", false, "A39vueloDescuentoPorcentage", StringUtil.LTrimStr( (decimal)(A39vueloDescuentoPorcentage), 3, 0));
         A41aerolineaId = 0;
         n41aerolineaId = false;
         AssignAttri("", false, "A41aerolineaId", StringUtil.LTrimStr( (decimal)(A41aerolineaId), 6, 0));
         n41aerolineaId = ((0==A41aerolineaId) ? true : false);
         A42aerolineaNombre = "";
         AssignAttri("", false, "A42aerolineaNombre", A42aerolineaNombre);
         A43aerolineaDescuentoPorcentage = 0;
         AssignAttri("", false, "A43aerolineaDescuentoPorcentage", StringUtil.LTrimStr( (decimal)(A43aerolineaDescuentoPorcentage), 3, 0));
         A47vueloCapacidad = 0;
         n47vueloCapacidad = false;
         AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
         O47vueloCapacidad = A47vueloCapacidad;
         n47vueloCapacidad = false;
         AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrimStr( (decimal)(A47vueloCapacidad), 4, 0));
         Z58vueloFecha = DateTime.MinValue;
         Z38vueloPrecio = 0;
         Z39vueloDescuentoPorcentage = 0;
         Z41aerolineaId = 0;
         Z24vueloAeropuertoSalidaId = 0;
         Z26vueloAeropuertoLlegadaId = 0;
      }

      protected void InitAll056( )
      {
         A18vueloId = 0;
         AssignAttri("", false, "A18vueloId", StringUtil.LTrimStr( (decimal)(A18vueloId), 6, 0));
         InitializeNonKey056( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void InitializeNonKey059( )
      {
         A46vueloAsientoLocalizacion = "";
         Z46vueloAsientoLocalizacion = "";
      }

      protected void InitAll059( )
      {
         A44vueloAsientoId = 0;
         A45vueloAsientoChar = "";
         InitializeNonKey059( ) ;
      }

      protected void StandaloneModalInsert059( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ));
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20191251293857", true, true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.eng.js", "?"+GetCacheInvalidationToken( ), false, true);
         context.AddJavascriptSource("vuelo.js", "?20191251293858", false, true);
         /* End function include_jscripts */
      }

      protected void init_level_properties9( )
      {
         cmbvueloAsientoChar.Enabled = defcmbvueloAsientoChar_Enabled;
         AssignProp("", false, cmbvueloAsientoChar_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(cmbvueloAsientoChar.Enabled), 5, 0), !bGXsfl_143_Refreshing);
         edtvueloAsientoId_Enabled = defedtvueloAsientoId_Enabled;
         AssignProp("", false, edtvueloAsientoId_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtvueloAsientoId_Enabled), 5, 0), !bGXsfl_143_Refreshing);
      }

      protected void init_default_properties( )
      {
         lblTitle_Internalname = "TITLE";
         divTitlecontainer_Internalname = "TITLECONTAINER";
         bttBtn_first_Internalname = "BTN_FIRST";
         bttBtn_previous_Internalname = "BTN_PREVIOUS";
         bttBtn_next_Internalname = "BTN_NEXT";
         bttBtn_last_Internalname = "BTN_LAST";
         bttBtn_select_Internalname = "BTN_SELECT";
         divToolbarcell_Internalname = "TOOLBARCELL";
         edtvueloId_Internalname = "VUELOID";
         edtvueloFecha_Internalname = "VUELOFECHA";
         edtvueloAeropuertoSalidaId_Internalname = "VUELOAEROPUERTOSALIDAID";
         edtvueloAeropuertoSalidaNombre_Internalname = "VUELOAEROPUERTOSALIDANOMBRE";
         edtvueloAeropuertoSalidaPaisId_Internalname = "VUELOAEROPUERTOSALIDAPAISID";
         edtvueloAeropuertoSalidaPaisNombr_Internalname = "VUELOAEROPUERTOSALIDAPAISNOMBR";
         edtvueloAeropuertoSalidaCiudadId_Internalname = "VUELOAEROPUERTOSALIDACIUDADID";
         edtvueloAeropuertoSalidaCiudadNom_Internalname = "VUELOAEROPUERTOSALIDACIUDADNOM";
         edtvueloAeropuertoLlegadaId_Internalname = "VUELOAEROPUERTOLLEGADAID";
         edtvueloAeropuertoLlegadaNombre_Internalname = "VUELOAEROPUERTOLLEGADANOMBRE";
         edtvueloAeropuertoLlegadaPaisId_Internalname = "VUELOAEROPUERTOLLEGADAPAISID";
         edtvueloAeropuertoLlegadaPaisNomb_Internalname = "VUELOAEROPUERTOLLEGADAPAISNOMB";
         edtvueloAeropuertoLlegadaCiudadId_Internalname = "VUELOAEROPUERTOLLEGADACIUDADID";
         edtvueloAeropuertoLlegadaCiudadNo_Internalname = "VUELOAEROPUERTOLLEGADACIUDADNO";
         edtvueloPrecio_Internalname = "VUELOPRECIO";
         edtvueloDescuentoPorcentage_Internalname = "VUELODESCUENTOPORCENTAGE";
         edtaerolineaId_Internalname = "AEROLINEAID";
         edtaerolineaNombre_Internalname = "AEROLINEANOMBRE";
         edtaerolineaDescuentoPorcentage_Internalname = "AEROLINEADESCUENTOPORCENTAGE";
         edtvueloPrecioFinal_Internalname = "VUELOPRECIOFINAL";
         edtvueloCapacidad_Internalname = "VUELOCAPACIDAD";
         lblTitleasiento_Internalname = "TITLEASIENTO";
         edtvueloAsientoId_Internalname = "VUELOASIENTOID";
         cmbvueloAsientoChar_Internalname = "VUELOASIENTOCHAR";
         cmbvueloAsientoLocalizacion_Internalname = "VUELOASIENTOLOCALIZACION";
         divAsientotable_Internalname = "ASIENTOTABLE";
         divFormcontainer_Internalname = "FORMCONTAINER";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
         imgprompt_24_Internalname = "PROMPT_24";
         imgprompt_26_Internalname = "PROMPT_26";
         imgprompt_41_Internalname = "PROMPT_41";
         subGridvuelo_asiento_Internalname = "GRIDVUELO_ASIENTO";
      }

      public override void initialize_properties( )
      {
         context.SetDefaultTheme("Carmine");
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "vuelo";
         cmbvueloAsientoLocalizacion_Jsonclick = "";
         cmbvueloAsientoChar_Jsonclick = "";
         edtvueloAsientoId_Jsonclick = "";
         subGridvuelo_asiento_Class = "Grid";
         subGridvuelo_asiento_Backcolorstyle = 0;
         subGridvuelo_asiento_Allowcollapsing = 0;
         subGridvuelo_asiento_Allowselection = 0;
         cmbvueloAsientoLocalizacion.Enabled = 1;
         cmbvueloAsientoChar.Enabled = 1;
         edtvueloAsientoId_Enabled = 1;
         subGridvuelo_asiento_Header = "";
         bttBtn_delete_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Enabled = 1;
         bttBtn_enter_Visible = 1;
         edtvueloCapacidad_Jsonclick = "";
         edtvueloCapacidad_Enabled = 0;
         edtvueloPrecioFinal_Jsonclick = "";
         edtvueloPrecioFinal_Enabled = 0;
         edtaerolineaDescuentoPorcentage_Jsonclick = "";
         edtaerolineaDescuentoPorcentage_Enabled = 0;
         edtaerolineaNombre_Jsonclick = "";
         edtaerolineaNombre_Enabled = 0;
         imgprompt_41_Visible = 1;
         imgprompt_41_Link = "";
         edtaerolineaId_Jsonclick = "";
         edtaerolineaId_Enabled = 1;
         edtvueloDescuentoPorcentage_Jsonclick = "";
         edtvueloDescuentoPorcentage_Enabled = 1;
         edtvueloPrecio_Jsonclick = "";
         edtvueloPrecio_Enabled = 1;
         edtvueloAeropuertoLlegadaCiudadNo_Jsonclick = "";
         edtvueloAeropuertoLlegadaCiudadNo_Enabled = 0;
         edtvueloAeropuertoLlegadaCiudadId_Jsonclick = "";
         edtvueloAeropuertoLlegadaCiudadId_Enabled = 0;
         edtvueloAeropuertoLlegadaPaisNomb_Jsonclick = "";
         edtvueloAeropuertoLlegadaPaisNomb_Enabled = 0;
         edtvueloAeropuertoLlegadaPaisId_Jsonclick = "";
         edtvueloAeropuertoLlegadaPaisId_Enabled = 0;
         edtvueloAeropuertoLlegadaNombre_Jsonclick = "";
         edtvueloAeropuertoLlegadaNombre_Enabled = 0;
         imgprompt_26_Visible = 1;
         imgprompt_26_Link = "";
         edtvueloAeropuertoLlegadaId_Jsonclick = "";
         edtvueloAeropuertoLlegadaId_Enabled = 1;
         edtvueloAeropuertoSalidaCiudadNom_Jsonclick = "";
         edtvueloAeropuertoSalidaCiudadNom_Enabled = 0;
         edtvueloAeropuertoSalidaCiudadId_Jsonclick = "";
         edtvueloAeropuertoSalidaCiudadId_Enabled = 0;
         edtvueloAeropuertoSalidaPaisNombr_Jsonclick = "";
         edtvueloAeropuertoSalidaPaisNombr_Enabled = 0;
         edtvueloAeropuertoSalidaPaisId_Jsonclick = "";
         edtvueloAeropuertoSalidaPaisId_Enabled = 0;
         edtvueloAeropuertoSalidaNombre_Jsonclick = "";
         edtvueloAeropuertoSalidaNombre_Enabled = 0;
         imgprompt_24_Visible = 1;
         imgprompt_24_Link = "";
         edtvueloAeropuertoSalidaId_Jsonclick = "";
         edtvueloAeropuertoSalidaId_Enabled = 1;
         edtvueloFecha_Jsonclick = "";
         edtvueloFecha_Enabled = 1;
         edtvueloId_Jsonclick = "";
         edtvueloId_Enabled = 1;
         bttBtn_select_Visible = 1;
         bttBtn_last_Visible = 1;
         bttBtn_next_Visible = 1;
         bttBtn_previous_Visible = 1;
         bttBtn_first_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGridvuelo_asiento_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         Gx_mode = "INS";
         AssignAttri("", false, "Gx_mode", Gx_mode);
         SubsflControlProps_1439( ) ;
         while ( nGXsfl_143_idx <= nRC_GXsfl_143 )
         {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            standaloneNotModal059( ) ;
            standaloneModal059( ) ;
            init_web_controls( ) ;
            dynload_actions( ) ;
            SendRow059( ) ;
            nGXsfl_143_idx = (int)(nGXsfl_143_idx+1);
            sGXsfl_143_idx = StringUtil.PadL( StringUtil.LTrimStr( (decimal)(nGXsfl_143_idx), 4, 0), 4, "0");
            SubsflControlProps_1439( ) ;
         }
         AddString( context.httpAjaxContext.getJSONContainerResponse( Gridvuelo_asientoContainer)) ;
         /* End function gxnrGridvuelo_asiento_newrow */
      }

      protected void init_web_controls( )
      {
         GXCCtl = "VUELOASIENTOCHAR_" + sGXsfl_143_idx;
         cmbvueloAsientoChar.Name = GXCCtl;
         cmbvueloAsientoChar.WebTags = "";
         cmbvueloAsientoChar.addItem("A", "A", 0);
         cmbvueloAsientoChar.addItem("B", "B", 0);
         cmbvueloAsientoChar.addItem("C", "C", 0);
         cmbvueloAsientoChar.addItem("D", "D", 0);
         cmbvueloAsientoChar.addItem("E", "E", 0);
         cmbvueloAsientoChar.addItem("F", "F", 0);
         if ( cmbvueloAsientoChar.ItemCount > 0 )
         {
            A45vueloAsientoChar = cmbvueloAsientoChar.getValidValue(A45vueloAsientoChar);
         }
         GXCCtl = "VUELOASIENTOLOCALIZACION_" + sGXsfl_143_idx;
         cmbvueloAsientoLocalizacion.Name = GXCCtl;
         cmbvueloAsientoLocalizacion.WebTags = "";
         cmbvueloAsientoLocalizacion.addItem("V", "Ventana", 0);
         cmbvueloAsientoLocalizacion.addItem("M", "Medio", 0);
         cmbvueloAsientoLocalizacion.addItem("P", "Pasillo", 0);
         if ( cmbvueloAsientoLocalizacion.ItemCount > 0 )
         {
            A46vueloAsientoLocalizacion = cmbvueloAsientoLocalizacion.getValidValue(A46vueloAsientoLocalizacion);
         }
         /* End function init_web_controls */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         getEqualNoModal( ) ;
         GX_FocusControl = edtvueloFecha_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      protected bool IsIns( )
      {
         return ((StringUtil.StrCmp(Gx_mode, "INS")==0) ? true : false) ;
      }

      protected bool IsDlt( )
      {
         return ((StringUtil.StrCmp(Gx_mode, "DLT")==0) ? true : false) ;
      }

      protected bool IsUpd( )
      {
         return ((StringUtil.StrCmp(Gx_mode, "UPD")==0) ? true : false) ;
      }

      protected bool IsDsp( )
      {
         return ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? true : false) ;
      }

      public void Valid_Vueloid( )
      {
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         send_integrity_footer_hashes( ) ;
         /* Using cursor T000533 */
         pr_default.execute(27, new Object[] {A18vueloId});
         if ( (pr_default.getStatus(27) != 101) )
         {
            A47vueloCapacidad = T000533_A47vueloCapacidad[0];
            n47vueloCapacidad = T000533_n47vueloCapacidad[0];
         }
         else
         {
            A47vueloCapacidad = 0;
            n47vueloCapacidad = false;
         }
         pr_default.close(27);
         dynload_actions( ) ;
         /*  Sending validation outputs */
         AssignAttri("", false, "A58vueloFecha", context.localUtil.Format(A58vueloFecha, "99/99/99"));
         AssignAttri("", false, "A24vueloAeropuertoSalidaId", StringUtil.LTrim( StringUtil.NToC( (decimal)(A24vueloAeropuertoSalidaId), 6, 0, ".", "")));
         AssignAttri("", false, "A26vueloAeropuertoLlegadaId", StringUtil.LTrim( StringUtil.NToC( (decimal)(A26vueloAeropuertoLlegadaId), 6, 0, ".", "")));
         AssignAttri("", false, "A38vueloPrecio", StringUtil.LTrim( StringUtil.NToC( A38vueloPrecio, 9, 2, ".", "")));
         AssignAttri("", false, "A39vueloDescuentoPorcentage", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39vueloDescuentoPorcentage), 3, 0, ".", "")));
         AssignAttri("", false, "A41aerolineaId", StringUtil.LTrim( StringUtil.NToC( (decimal)(A41aerolineaId), 6, 0, ".", "")));
         AssignAttri("", false, "A47vueloCapacidad", StringUtil.LTrim( StringUtil.NToC( (decimal)(A47vueloCapacidad), 4, 0, ".", "")));
         AssignAttri("", false, "A25vueloAeropuertoSalidaNombre", StringUtil.RTrim( A25vueloAeropuertoSalidaNombre));
         AssignAttri("", false, "A32vueloAeropuertoSalidaPaisId", StringUtil.LTrim( StringUtil.NToC( (decimal)(A32vueloAeropuertoSalidaPaisId), 6, 0, ".", "")));
         AssignAttri("", false, "A30vueloAeropuertoSalidaCiudadId", StringUtil.LTrim( StringUtil.NToC( (decimal)(A30vueloAeropuertoSalidaCiudadId), 6, 0, ".", "")));
         AssignAttri("", false, "A33vueloAeropuertoSalidaPaisNombr", StringUtil.RTrim( A33vueloAeropuertoSalidaPaisNombr));
         AssignAttri("", false, "A31vueloAeropuertoSalidaCiudadNom", StringUtil.RTrim( A31vueloAeropuertoSalidaCiudadNom));
         AssignAttri("", false, "A27vueloAeropuertoLlegadaNombre", StringUtil.RTrim( A27vueloAeropuertoLlegadaNombre));
         AssignAttri("", false, "A34vueloAeropuertoLlegadaPaisId", StringUtil.LTrim( StringUtil.NToC( (decimal)(A34vueloAeropuertoLlegadaPaisId), 6, 0, ".", "")));
         AssignAttri("", false, "A36vueloAeropuertoLlegadaCiudadId", StringUtil.LTrim( StringUtil.NToC( (decimal)(A36vueloAeropuertoLlegadaCiudadId), 6, 0, ".", "")));
         AssignAttri("", false, "A35vueloAeropuertoLlegadaPaisNomb", StringUtil.RTrim( A35vueloAeropuertoLlegadaPaisNomb));
         AssignAttri("", false, "A37vueloAeropuertoLlegadaCiudadNo", StringUtil.RTrim( A37vueloAeropuertoLlegadaCiudadNo));
         AssignAttri("", false, "A42aerolineaNombre", StringUtil.RTrim( A42aerolineaNombre));
         AssignAttri("", false, "A43aerolineaDescuentoPorcentage", StringUtil.LTrim( StringUtil.NToC( (decimal)(A43aerolineaDescuentoPorcentage), 3, 0, ".", "")));
         AssignAttri("", false, "A40vueloPrecioFinal", StringUtil.LTrim( StringUtil.NToC( A40vueloPrecioFinal, 9, 2, ".", "")));
         AssignAttri("", false, "Gx_mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "Z18vueloId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z18vueloId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z58vueloFecha", context.localUtil.Format(Z58vueloFecha, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "Z24vueloAeropuertoSalidaId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z24vueloAeropuertoSalidaId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z26vueloAeropuertoLlegadaId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z26vueloAeropuertoLlegadaId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z38vueloPrecio", StringUtil.LTrim( StringUtil.NToC( Z38vueloPrecio, 9, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z39vueloDescuentoPorcentage", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z39vueloDescuentoPorcentage), 3, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z41aerolineaId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z41aerolineaId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z47vueloCapacidad", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z47vueloCapacidad), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z25vueloAeropuertoSalidaNombre", StringUtil.RTrim( Z25vueloAeropuertoSalidaNombre));
         GxWebStd.gx_hidden_field( context, "Z32vueloAeropuertoSalidaPaisId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z32vueloAeropuertoSalidaPaisId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z30vueloAeropuertoSalidaCiudadId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z30vueloAeropuertoSalidaCiudadId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z33vueloAeropuertoSalidaPaisNombr", StringUtil.RTrim( Z33vueloAeropuertoSalidaPaisNombr));
         GxWebStd.gx_hidden_field( context, "Z31vueloAeropuertoSalidaCiudadNom", StringUtil.RTrim( Z31vueloAeropuertoSalidaCiudadNom));
         GxWebStd.gx_hidden_field( context, "Z27vueloAeropuertoLlegadaNombre", StringUtil.RTrim( Z27vueloAeropuertoLlegadaNombre));
         GxWebStd.gx_hidden_field( context, "Z34vueloAeropuertoLlegadaPaisId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z34vueloAeropuertoLlegadaPaisId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z36vueloAeropuertoLlegadaCiudadId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z36vueloAeropuertoLlegadaCiudadId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z35vueloAeropuertoLlegadaPaisNomb", StringUtil.RTrim( Z35vueloAeropuertoLlegadaPaisNomb));
         GxWebStd.gx_hidden_field( context, "Z37vueloAeropuertoLlegadaCiudadNo", StringUtil.RTrim( Z37vueloAeropuertoLlegadaCiudadNo));
         GxWebStd.gx_hidden_field( context, "Z42aerolineaNombre", StringUtil.RTrim( Z42aerolineaNombre));
         GxWebStd.gx_hidden_field( context, "Z43aerolineaDescuentoPorcentage", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z43aerolineaDescuentoPorcentage), 3, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z40vueloPrecioFinal", StringUtil.LTrim( StringUtil.NToC( Z40vueloPrecioFinal, 9, 2, ".", "")));
         AssignAttri("", false, "O47vueloCapacidad", StringUtil.LTrim( StringUtil.NToC( (decimal)(O47vueloCapacidad), 4, 0, ".", "")));
         AssignProp("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Enabled), 5, 0), true);
         AssignProp("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_enter_Enabled), 5, 0), true);
         SendCloseFormHiddens( ) ;
      }

      public void Valid_Vueloaeropuertosalidaid( )
      {
         /* Using cursor T000534 */
         pr_default.execute(28, new Object[] {A24vueloAeropuertoSalidaId});
         if ( (pr_default.getStatus(28) == 101) )
         {
            GX_msglist.addItem("No matching 'vuelo Aeropuerto Salida'.", "ForeignKeyNotFound", 1, "VUELOAEROPUERTOSALIDAID");
            AnyError = 1;
            GX_FocusControl = edtvueloAeropuertoSalidaId_Internalname;
         }
         A25vueloAeropuertoSalidaNombre = T000534_A25vueloAeropuertoSalidaNombre[0];
         A32vueloAeropuertoSalidaPaisId = T000534_A32vueloAeropuertoSalidaPaisId[0];
         A30vueloAeropuertoSalidaCiudadId = T000534_A30vueloAeropuertoSalidaCiudadId[0];
         pr_default.close(28);
         /* Using cursor T000535 */
         pr_default.execute(29, new Object[] {A32vueloAeropuertoSalidaPaisId});
         if ( (pr_default.getStatus(29) == 101) )
         {
            GX_msglist.addItem("No matching 'vuelo Aeropuerto Salida'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A33vueloAeropuertoSalidaPaisNombr = T000535_A33vueloAeropuertoSalidaPaisNombr[0];
         pr_default.close(29);
         /* Using cursor T000536 */
         pr_default.execute(30, new Object[] {A32vueloAeropuertoSalidaPaisId, A30vueloAeropuertoSalidaCiudadId});
         if ( (pr_default.getStatus(30) == 101) )
         {
            GX_msglist.addItem("No matching 'vuelo Aeropuerto Salida'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A31vueloAeropuertoSalidaCiudadNom = T000536_A31vueloAeropuertoSalidaCiudadNom[0];
         pr_default.close(30);
         dynload_actions( ) ;
         /*  Sending validation outputs */
         AssignAttri("", false, "A25vueloAeropuertoSalidaNombre", StringUtil.RTrim( A25vueloAeropuertoSalidaNombre));
         AssignAttri("", false, "A32vueloAeropuertoSalidaPaisId", StringUtil.LTrim( StringUtil.NToC( (decimal)(A32vueloAeropuertoSalidaPaisId), 6, 0, ".", "")));
         AssignAttri("", false, "A30vueloAeropuertoSalidaCiudadId", StringUtil.LTrim( StringUtil.NToC( (decimal)(A30vueloAeropuertoSalidaCiudadId), 6, 0, ".", "")));
         AssignAttri("", false, "A33vueloAeropuertoSalidaPaisNombr", StringUtil.RTrim( A33vueloAeropuertoSalidaPaisNombr));
         AssignAttri("", false, "A31vueloAeropuertoSalidaCiudadNom", StringUtil.RTrim( A31vueloAeropuertoSalidaCiudadNom));
      }

      public void Valid_Vueloaeropuertollegadaid( )
      {
         /* Using cursor T000537 */
         pr_default.execute(31, new Object[] {A26vueloAeropuertoLlegadaId});
         if ( (pr_default.getStatus(31) == 101) )
         {
            GX_msglist.addItem("No matching 'vuelo Aeropuerto Llegada'.", "ForeignKeyNotFound", 1, "VUELOAEROPUERTOLLEGADAID");
            AnyError = 1;
            GX_FocusControl = edtvueloAeropuertoLlegadaId_Internalname;
         }
         A27vueloAeropuertoLlegadaNombre = T000537_A27vueloAeropuertoLlegadaNombre[0];
         A34vueloAeropuertoLlegadaPaisId = T000537_A34vueloAeropuertoLlegadaPaisId[0];
         A36vueloAeropuertoLlegadaCiudadId = T000537_A36vueloAeropuertoLlegadaCiudadId[0];
         pr_default.close(31);
         /* Using cursor T000538 */
         pr_default.execute(32, new Object[] {A34vueloAeropuertoLlegadaPaisId});
         if ( (pr_default.getStatus(32) == 101) )
         {
            GX_msglist.addItem("No matching 'vuelo Aeropuerto Llegada'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A35vueloAeropuertoLlegadaPaisNomb = T000538_A35vueloAeropuertoLlegadaPaisNomb[0];
         pr_default.close(32);
         /* Using cursor T000539 */
         pr_default.execute(33, new Object[] {A34vueloAeropuertoLlegadaPaisId, A36vueloAeropuertoLlegadaCiudadId});
         if ( (pr_default.getStatus(33) == 101) )
         {
            GX_msglist.addItem("No matching 'vuelo Aeropuerto Llegada'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A37vueloAeropuertoLlegadaCiudadNo = T000539_A37vueloAeropuertoLlegadaCiudadNo[0];
         pr_default.close(33);
         dynload_actions( ) ;
         /*  Sending validation outputs */
         AssignAttri("", false, "A27vueloAeropuertoLlegadaNombre", StringUtil.RTrim( A27vueloAeropuertoLlegadaNombre));
         AssignAttri("", false, "A34vueloAeropuertoLlegadaPaisId", StringUtil.LTrim( StringUtil.NToC( (decimal)(A34vueloAeropuertoLlegadaPaisId), 6, 0, ".", "")));
         AssignAttri("", false, "A36vueloAeropuertoLlegadaCiudadId", StringUtil.LTrim( StringUtil.NToC( (decimal)(A36vueloAeropuertoLlegadaCiudadId), 6, 0, ".", "")));
         AssignAttri("", false, "A35vueloAeropuertoLlegadaPaisNomb", StringUtil.RTrim( A35vueloAeropuertoLlegadaPaisNomb));
         AssignAttri("", false, "A37vueloAeropuertoLlegadaCiudadNo", StringUtil.RTrim( A37vueloAeropuertoLlegadaCiudadNo));
      }

      public void Valid_Aerolineaid( )
      {
         n41aerolineaId = false;
         /* Using cursor T000540 */
         pr_default.execute(34, new Object[] {n41aerolineaId, A41aerolineaId});
         if ( (pr_default.getStatus(34) == 101) )
         {
            if ( ! ( (0==A41aerolineaId) ) )
            {
               GX_msglist.addItem("No matching 'aerolinea'.", "ForeignKeyNotFound", 1, "AEROLINEAID");
               AnyError = 1;
               GX_FocusControl = edtaerolineaId_Internalname;
            }
         }
         A42aerolineaNombre = T000540_A42aerolineaNombre[0];
         A43aerolineaDescuentoPorcentage = T000540_A43aerolineaDescuentoPorcentage[0];
         pr_default.close(34);
         if ( A43aerolineaDescuentoPorcentage > A39vueloDescuentoPorcentage )
         {
            A40vueloPrecioFinal = (decimal)(A38vueloPrecio*(1-A43aerolineaDescuentoPorcentage/ (decimal)(100)));
         }
         else
         {
            A40vueloPrecioFinal = (decimal)(A38vueloPrecio*(1-A39vueloDescuentoPorcentage/ (decimal)(100)));
         }
         dynload_actions( ) ;
         /*  Sending validation outputs */
         AssignAttri("", false, "A42aerolineaNombre", StringUtil.RTrim( A42aerolineaNombre));
         AssignAttri("", false, "A43aerolineaDescuentoPorcentage", StringUtil.LTrim( StringUtil.NToC( (decimal)(A43aerolineaDescuentoPorcentage), 3, 0, ".", "")));
         AssignAttri("", false, "A40vueloPrecioFinal", StringUtil.LTrim( StringUtil.NToC( A40vueloPrecioFinal, 9, 2, ".", "")));
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}]");
         setEventMetadata("ENTER",",oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[]");
         setEventMetadata("REFRESH",",oparms:[]}");
         setEventMetadata("VALID_VUELOID","{handler:'Valid_Vueloid',iparms:[{av:'A18vueloId',fld:'VUELOID',pic:'ZZZZZ9'},{av:'Gx_mode',fld:'vMODE',pic:'@!'}]");
         setEventMetadata("VALID_VUELOID",",oparms:[{av:'A58vueloFecha',fld:'VUELOFECHA',pic:''},{av:'A24vueloAeropuertoSalidaId',fld:'VUELOAEROPUERTOSALIDAID',pic:'ZZZZZ9'},{av:'A26vueloAeropuertoLlegadaId',fld:'VUELOAEROPUERTOLLEGADAID',pic:'ZZZZZ9'},{av:'A38vueloPrecio',fld:'VUELOPRECIO',pic:'ZZZZZ9.99'},{av:'A39vueloDescuentoPorcentage',fld:'VUELODESCUENTOPORCENTAGE',pic:'ZZ9'},{av:'A41aerolineaId',fld:'AEROLINEAID',pic:'ZZZZZ9'},{av:'A47vueloCapacidad',fld:'VUELOCAPACIDAD',pic:'ZZZ9'},{av:'A25vueloAeropuertoSalidaNombre',fld:'VUELOAEROPUERTOSALIDANOMBRE',pic:''},{av:'A32vueloAeropuertoSalidaPaisId',fld:'VUELOAEROPUERTOSALIDAPAISID',pic:'ZZZZZ9'},{av:'A30vueloAeropuertoSalidaCiudadId',fld:'VUELOAEROPUERTOSALIDACIUDADID',pic:'ZZZZZ9'},{av:'A33vueloAeropuertoSalidaPaisNombr',fld:'VUELOAEROPUERTOSALIDAPAISNOMBR',pic:''},{av:'A31vueloAeropuertoSalidaCiudadNom',fld:'VUELOAEROPUERTOSALIDACIUDADNOM',pic:''},{av:'A27vueloAeropuertoLlegadaNombre',fld:'VUELOAEROPUERTOLLEGADANOMBRE',pic:''},{av:'A34vueloAeropuertoLlegadaPaisId',fld:'VUELOAEROPUERTOLLEGADAPAISID',pic:'ZZZZZ9'},{av:'A36vueloAeropuertoLlegadaCiudadId',fld:'VUELOAEROPUERTOLLEGADACIUDADID',pic:'ZZZZZ9'},{av:'A35vueloAeropuertoLlegadaPaisNomb',fld:'VUELOAEROPUERTOLLEGADAPAISNOMB',pic:''},{av:'A37vueloAeropuertoLlegadaCiudadNo',fld:'VUELOAEROPUERTOLLEGADACIUDADNO',pic:''},{av:'A42aerolineaNombre',fld:'AEROLINEANOMBRE',pic:''},{av:'A43aerolineaDescuentoPorcentage',fld:'AEROLINEADESCUENTOPORCENTAGE',pic:'ZZ9'},{av:'A40vueloPrecioFinal',fld:'VUELOPRECIOFINAL',pic:'ZZZZZ9.99'},{av:'Gx_mode',fld:'vMODE',pic:'@!'},{av:'Z18vueloId'},{av:'Z58vueloFecha'},{av:'Z24vueloAeropuertoSalidaId'},{av:'Z26vueloAeropuertoLlegadaId'},{av:'Z38vueloPrecio'},{av:'Z39vueloDescuentoPorcentage'},{av:'Z41aerolineaId'},{av:'Z47vueloCapacidad'},{av:'Z25vueloAeropuertoSalidaNombre'},{av:'Z32vueloAeropuertoSalidaPaisId'},{av:'Z30vueloAeropuertoSalidaCiudadId'},{av:'Z33vueloAeropuertoSalidaPaisNombr'},{av:'Z31vueloAeropuertoSalidaCiudadNom'},{av:'Z27vueloAeropuertoLlegadaNombre'},{av:'Z34vueloAeropuertoLlegadaPaisId'},{av:'Z36vueloAeropuertoLlegadaCiudadId'},{av:'Z35vueloAeropuertoLlegadaPaisNomb'},{av:'Z37vueloAeropuertoLlegadaCiudadNo'},{av:'Z42aerolineaNombre'},{av:'Z43aerolineaDescuentoPorcentage'},{av:'Z40vueloPrecioFinal'},{av:'O47vueloCapacidad'},{ctrl:'BTN_DELETE',prop:'Enabled'},{ctrl:'BTN_ENTER',prop:'Enabled'}]}");
         setEventMetadata("VALID_VUELOFECHA","{handler:'Valid_Vuelofecha',iparms:[]");
         setEventMetadata("VALID_VUELOFECHA",",oparms:[]}");
         setEventMetadata("VALID_VUELOAEROPUERTOSALIDAID","{handler:'Valid_Vueloaeropuertosalidaid',iparms:[{av:'A24vueloAeropuertoSalidaId',fld:'VUELOAEROPUERTOSALIDAID',pic:'ZZZZZ9'},{av:'A32vueloAeropuertoSalidaPaisId',fld:'VUELOAEROPUERTOSALIDAPAISID',pic:'ZZZZZ9'},{av:'A30vueloAeropuertoSalidaCiudadId',fld:'VUELOAEROPUERTOSALIDACIUDADID',pic:'ZZZZZ9'},{av:'A25vueloAeropuertoSalidaNombre',fld:'VUELOAEROPUERTOSALIDANOMBRE',pic:''},{av:'A33vueloAeropuertoSalidaPaisNombr',fld:'VUELOAEROPUERTOSALIDAPAISNOMBR',pic:''},{av:'A31vueloAeropuertoSalidaCiudadNom',fld:'VUELOAEROPUERTOSALIDACIUDADNOM',pic:''}]");
         setEventMetadata("VALID_VUELOAEROPUERTOSALIDAID",",oparms:[{av:'A25vueloAeropuertoSalidaNombre',fld:'VUELOAEROPUERTOSALIDANOMBRE',pic:''},{av:'A32vueloAeropuertoSalidaPaisId',fld:'VUELOAEROPUERTOSALIDAPAISID',pic:'ZZZZZ9'},{av:'A30vueloAeropuertoSalidaCiudadId',fld:'VUELOAEROPUERTOSALIDACIUDADID',pic:'ZZZZZ9'},{av:'A33vueloAeropuertoSalidaPaisNombr',fld:'VUELOAEROPUERTOSALIDAPAISNOMBR',pic:''},{av:'A31vueloAeropuertoSalidaCiudadNom',fld:'VUELOAEROPUERTOSALIDACIUDADNOM',pic:''}]}");
         setEventMetadata("VALID_VUELOAEROPUERTOSALIDAPAISID","{handler:'Valid_Vueloaeropuertosalidapaisid',iparms:[]");
         setEventMetadata("VALID_VUELOAEROPUERTOSALIDAPAISID",",oparms:[]}");
         setEventMetadata("VALID_VUELOAEROPUERTOSALIDACIUDADID","{handler:'Valid_Vueloaeropuertosalidaciudadid',iparms:[]");
         setEventMetadata("VALID_VUELOAEROPUERTOSALIDACIUDADID",",oparms:[]}");
         setEventMetadata("VALID_VUELOAEROPUERTOLLEGADAID","{handler:'Valid_Vueloaeropuertollegadaid',iparms:[{av:'A26vueloAeropuertoLlegadaId',fld:'VUELOAEROPUERTOLLEGADAID',pic:'ZZZZZ9'},{av:'A34vueloAeropuertoLlegadaPaisId',fld:'VUELOAEROPUERTOLLEGADAPAISID',pic:'ZZZZZ9'},{av:'A36vueloAeropuertoLlegadaCiudadId',fld:'VUELOAEROPUERTOLLEGADACIUDADID',pic:'ZZZZZ9'},{av:'A27vueloAeropuertoLlegadaNombre',fld:'VUELOAEROPUERTOLLEGADANOMBRE',pic:''},{av:'A35vueloAeropuertoLlegadaPaisNomb',fld:'VUELOAEROPUERTOLLEGADAPAISNOMB',pic:''},{av:'A37vueloAeropuertoLlegadaCiudadNo',fld:'VUELOAEROPUERTOLLEGADACIUDADNO',pic:''}]");
         setEventMetadata("VALID_VUELOAEROPUERTOLLEGADAID",",oparms:[{av:'A27vueloAeropuertoLlegadaNombre',fld:'VUELOAEROPUERTOLLEGADANOMBRE',pic:''},{av:'A34vueloAeropuertoLlegadaPaisId',fld:'VUELOAEROPUERTOLLEGADAPAISID',pic:'ZZZZZ9'},{av:'A36vueloAeropuertoLlegadaCiudadId',fld:'VUELOAEROPUERTOLLEGADACIUDADID',pic:'ZZZZZ9'},{av:'A35vueloAeropuertoLlegadaPaisNomb',fld:'VUELOAEROPUERTOLLEGADAPAISNOMB',pic:''},{av:'A37vueloAeropuertoLlegadaCiudadNo',fld:'VUELOAEROPUERTOLLEGADACIUDADNO',pic:''}]}");
         setEventMetadata("VALID_VUELOAEROPUERTOLLEGADAPAISID","{handler:'Valid_Vueloaeropuertollegadapaisid',iparms:[]");
         setEventMetadata("VALID_VUELOAEROPUERTOLLEGADAPAISID",",oparms:[]}");
         setEventMetadata("VALID_VUELOAEROPUERTOLLEGADACIUDADID","{handler:'Valid_Vueloaeropuertollegadaciudadid',iparms:[]");
         setEventMetadata("VALID_VUELOAEROPUERTOLLEGADACIUDADID",",oparms:[]}");
         setEventMetadata("VALID_VUELOPRECIO","{handler:'Valid_Vueloprecio',iparms:[]");
         setEventMetadata("VALID_VUELOPRECIO",",oparms:[]}");
         setEventMetadata("VALID_VUELODESCUENTOPORCENTAGE","{handler:'Valid_Vuelodescuentoporcentage',iparms:[]");
         setEventMetadata("VALID_VUELODESCUENTOPORCENTAGE",",oparms:[]}");
         setEventMetadata("VALID_AEROLINEAID","{handler:'Valid_Aerolineaid',iparms:[{av:'A41aerolineaId',fld:'AEROLINEAID',pic:'ZZZZZ9'},{av:'A38vueloPrecio',fld:'VUELOPRECIO',pic:'ZZZZZ9.99'},{av:'A43aerolineaDescuentoPorcentage',fld:'AEROLINEADESCUENTOPORCENTAGE',pic:'ZZ9'},{av:'A39vueloDescuentoPorcentage',fld:'VUELODESCUENTOPORCENTAGE',pic:'ZZ9'},{av:'A42aerolineaNombre',fld:'AEROLINEANOMBRE',pic:''},{av:'A40vueloPrecioFinal',fld:'VUELOPRECIOFINAL',pic:'ZZZZZ9.99'}]");
         setEventMetadata("VALID_AEROLINEAID",",oparms:[{av:'A42aerolineaNombre',fld:'AEROLINEANOMBRE',pic:''},{av:'A43aerolineaDescuentoPorcentage',fld:'AEROLINEADESCUENTOPORCENTAGE',pic:'ZZ9'},{av:'A40vueloPrecioFinal',fld:'VUELOPRECIOFINAL',pic:'ZZZZZ9.99'}]}");
         setEventMetadata("VALID_AEROLINEADESCUENTOPORCENTAGE","{handler:'Valid_Aerolineadescuentoporcentage',iparms:[]");
         setEventMetadata("VALID_AEROLINEADESCUENTOPORCENTAGE",",oparms:[]}");
         setEventMetadata("VALID_VUELOCAPACIDAD","{handler:'Valid_Vuelocapacidad',iparms:[]");
         setEventMetadata("VALID_VUELOCAPACIDAD",",oparms:[]}");
         setEventMetadata("VALID_VUELOASIENTOID","{handler:'Valid_Vueloasientoid',iparms:[]");
         setEventMetadata("VALID_VUELOASIENTOID",",oparms:[]}");
         setEventMetadata("VALID_VUELOASIENTOCHAR","{handler:'Valid_Vueloasientochar',iparms:[]");
         setEventMetadata("VALID_VUELOASIENTOCHAR",",oparms:[]}");
         setEventMetadata("VALID_VUELOASIENTOLOCALIZACION","{handler:'Valid_Vueloasientolocalizacion',iparms:[]");
         setEventMetadata("VALID_VUELOASIENTOLOCALIZACION",",oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(3);
         pr_default.close(34);
         pr_default.close(28);
         pr_default.close(31);
         pr_default.close(29);
         pr_default.close(30);
         pr_default.close(32);
         pr_default.close(33);
         pr_default.close(27);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z58vueloFecha = DateTime.MinValue;
         Z45vueloAsientoChar = "";
         Z46vueloAsientoLocalizacion = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         Gx_mode = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         lblTitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_first_Jsonclick = "";
         bttBtn_previous_Jsonclick = "";
         bttBtn_next_Jsonclick = "";
         bttBtn_last_Jsonclick = "";
         bttBtn_select_Jsonclick = "";
         A58vueloFecha = DateTime.MinValue;
         sImgUrl = "";
         A25vueloAeropuertoSalidaNombre = "";
         A33vueloAeropuertoSalidaPaisNombr = "";
         A31vueloAeropuertoSalidaCiudadNom = "";
         A27vueloAeropuertoLlegadaNombre = "";
         A35vueloAeropuertoLlegadaPaisNomb = "";
         A37vueloAeropuertoLlegadaCiudadNo = "";
         A42aerolineaNombre = "";
         lblTitleasiento_Jsonclick = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         Gridvuelo_asientoContainer = new GXWebGrid( context);
         Gridvuelo_asientoColumn = new GXWebColumn();
         A45vueloAsientoChar = "";
         A46vueloAsientoLocalizacion = "";
         sMode9 = "";
         sStyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         Z25vueloAeropuertoSalidaNombre = "";
         Z33vueloAeropuertoSalidaPaisNombr = "";
         Z31vueloAeropuertoSalidaCiudadNom = "";
         Z27vueloAeropuertoLlegadaNombre = "";
         Z35vueloAeropuertoLlegadaPaisNomb = "";
         Z37vueloAeropuertoLlegadaCiudadNo = "";
         Z42aerolineaNombre = "";
         T000516_A18vueloId = new int[1] ;
         T000516_A58vueloFecha = new DateTime[] {DateTime.MinValue} ;
         T000516_A25vueloAeropuertoSalidaNombre = new String[] {""} ;
         T000516_A33vueloAeropuertoSalidaPaisNombr = new String[] {""} ;
         T000516_A31vueloAeropuertoSalidaCiudadNom = new String[] {""} ;
         T000516_A27vueloAeropuertoLlegadaNombre = new String[] {""} ;
         T000516_A35vueloAeropuertoLlegadaPaisNomb = new String[] {""} ;
         T000516_A37vueloAeropuertoLlegadaCiudadNo = new String[] {""} ;
         T000516_A38vueloPrecio = new decimal[1] ;
         T000516_A39vueloDescuentoPorcentage = new short[1] ;
         T000516_A42aerolineaNombre = new String[] {""} ;
         T000516_A43aerolineaDescuentoPorcentage = new short[1] ;
         T000516_A41aerolineaId = new int[1] ;
         T000516_n41aerolineaId = new bool[] {false} ;
         T000516_A24vueloAeropuertoSalidaId = new int[1] ;
         T000516_A26vueloAeropuertoLlegadaId = new int[1] ;
         T000516_A32vueloAeropuertoSalidaPaisId = new int[1] ;
         T000516_A30vueloAeropuertoSalidaCiudadId = new int[1] ;
         T000516_A34vueloAeropuertoLlegadaPaisId = new int[1] ;
         T000516_A36vueloAeropuertoLlegadaCiudadId = new int[1] ;
         T000516_A47vueloCapacidad = new short[1] ;
         T000516_n47vueloCapacidad = new bool[] {false} ;
         T000514_A47vueloCapacidad = new short[1] ;
         T000514_n47vueloCapacidad = new bool[] {false} ;
         T00057_A25vueloAeropuertoSalidaNombre = new String[] {""} ;
         T00057_A32vueloAeropuertoSalidaPaisId = new int[1] ;
         T00057_A30vueloAeropuertoSalidaCiudadId = new int[1] ;
         T00059_A33vueloAeropuertoSalidaPaisNombr = new String[] {""} ;
         T000510_A31vueloAeropuertoSalidaCiudadNom = new String[] {""} ;
         T00058_A27vueloAeropuertoLlegadaNombre = new String[] {""} ;
         T00058_A34vueloAeropuertoLlegadaPaisId = new int[1] ;
         T00058_A36vueloAeropuertoLlegadaCiudadId = new int[1] ;
         T000511_A35vueloAeropuertoLlegadaPaisNomb = new String[] {""} ;
         T000512_A37vueloAeropuertoLlegadaCiudadNo = new String[] {""} ;
         T00056_A42aerolineaNombre = new String[] {""} ;
         T00056_A43aerolineaDescuentoPorcentage = new short[1] ;
         T000518_A47vueloCapacidad = new short[1] ;
         T000518_n47vueloCapacidad = new bool[] {false} ;
         T000519_A25vueloAeropuertoSalidaNombre = new String[] {""} ;
         T000519_A32vueloAeropuertoSalidaPaisId = new int[1] ;
         T000519_A30vueloAeropuertoSalidaCiudadId = new int[1] ;
         T000520_A33vueloAeropuertoSalidaPaisNombr = new String[] {""} ;
         T000521_A31vueloAeropuertoSalidaCiudadNom = new String[] {""} ;
         T000522_A27vueloAeropuertoLlegadaNombre = new String[] {""} ;
         T000522_A34vueloAeropuertoLlegadaPaisId = new int[1] ;
         T000522_A36vueloAeropuertoLlegadaCiudadId = new int[1] ;
         T000523_A35vueloAeropuertoLlegadaPaisNomb = new String[] {""} ;
         T000524_A37vueloAeropuertoLlegadaCiudadNo = new String[] {""} ;
         T000525_A42aerolineaNombre = new String[] {""} ;
         T000525_A43aerolineaDescuentoPorcentage = new short[1] ;
         T000526_A18vueloId = new int[1] ;
         T00055_A18vueloId = new int[1] ;
         T00055_A58vueloFecha = new DateTime[] {DateTime.MinValue} ;
         T00055_A38vueloPrecio = new decimal[1] ;
         T00055_A39vueloDescuentoPorcentage = new short[1] ;
         T00055_A41aerolineaId = new int[1] ;
         T00055_n41aerolineaId = new bool[] {false} ;
         T00055_A24vueloAeropuertoSalidaId = new int[1] ;
         T00055_A26vueloAeropuertoLlegadaId = new int[1] ;
         sMode6 = "";
         T000527_A18vueloId = new int[1] ;
         T000528_A18vueloId = new int[1] ;
         T00054_A18vueloId = new int[1] ;
         T00054_A58vueloFecha = new DateTime[] {DateTime.MinValue} ;
         T00054_A38vueloPrecio = new decimal[1] ;
         T00054_A39vueloDescuentoPorcentage = new short[1] ;
         T00054_A41aerolineaId = new int[1] ;
         T00054_n41aerolineaId = new bool[] {false} ;
         T00054_A24vueloAeropuertoSalidaId = new int[1] ;
         T00054_A26vueloAeropuertoLlegadaId = new int[1] ;
         T000529_A18vueloId = new int[1] ;
         T000533_A47vueloCapacidad = new short[1] ;
         T000533_n47vueloCapacidad = new bool[] {false} ;
         T000534_A25vueloAeropuertoSalidaNombre = new String[] {""} ;
         T000534_A32vueloAeropuertoSalidaPaisId = new int[1] ;
         T000534_A30vueloAeropuertoSalidaCiudadId = new int[1] ;
         T000535_A33vueloAeropuertoSalidaPaisNombr = new String[] {""} ;
         T000536_A31vueloAeropuertoSalidaCiudadNom = new String[] {""} ;
         T000537_A27vueloAeropuertoLlegadaNombre = new String[] {""} ;
         T000537_A34vueloAeropuertoLlegadaPaisId = new int[1] ;
         T000537_A36vueloAeropuertoLlegadaCiudadId = new int[1] ;
         T000538_A35vueloAeropuertoLlegadaPaisNomb = new String[] {""} ;
         T000539_A37vueloAeropuertoLlegadaCiudadNo = new String[] {""} ;
         T000540_A42aerolineaNombre = new String[] {""} ;
         T000540_A43aerolineaDescuentoPorcentage = new short[1] ;
         T000541_A18vueloId = new int[1] ;
         T000542_A18vueloId = new int[1] ;
         T000542_A44vueloAsientoId = new int[1] ;
         T000542_A45vueloAsientoChar = new String[] {""} ;
         T000542_A46vueloAsientoLocalizacion = new String[] {""} ;
         T000543_A18vueloId = new int[1] ;
         T000543_A44vueloAsientoId = new int[1] ;
         T000543_A45vueloAsientoChar = new String[] {""} ;
         T00053_A18vueloId = new int[1] ;
         T00053_A44vueloAsientoId = new int[1] ;
         T00053_A45vueloAsientoChar = new String[] {""} ;
         T00053_A46vueloAsientoLocalizacion = new String[] {""} ;
         T00052_A18vueloId = new int[1] ;
         T00052_A44vueloAsientoId = new int[1] ;
         T00052_A45vueloAsientoChar = new String[] {""} ;
         T00052_A46vueloAsientoLocalizacion = new String[] {""} ;
         T000547_A18vueloId = new int[1] ;
         T000547_A44vueloAsientoId = new int[1] ;
         T000547_A45vueloAsientoChar = new String[] {""} ;
         Gridvuelo_asientoRow = new GXWebRow();
         subGridvuelo_asiento_Linesclass = "";
         ROClassString = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         ZZ58vueloFecha = DateTime.MinValue;
         ZZ25vueloAeropuertoSalidaNombre = "";
         ZZ33vueloAeropuertoSalidaPaisNombr = "";
         ZZ31vueloAeropuertoSalidaCiudadNom = "";
         ZZ27vueloAeropuertoLlegadaNombre = "";
         ZZ35vueloAeropuertoLlegadaPaisNomb = "";
         ZZ37vueloAeropuertoLlegadaCiudadNo = "";
         ZZ42aerolineaNombre = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.vuelo__default(),
            new Object[][] {
                new Object[] {
               T00052_A18vueloId, T00052_A44vueloAsientoId, T00052_A45vueloAsientoChar, T00052_A46vueloAsientoLocalizacion
               }
               , new Object[] {
               T00053_A18vueloId, T00053_A44vueloAsientoId, T00053_A45vueloAsientoChar, T00053_A46vueloAsientoLocalizacion
               }
               , new Object[] {
               T00054_A18vueloId, T00054_A58vueloFecha, T00054_A38vueloPrecio, T00054_A39vueloDescuentoPorcentage, T00054_A41aerolineaId, T00054_n41aerolineaId, T00054_A24vueloAeropuertoSalidaId, T00054_A26vueloAeropuertoLlegadaId
               }
               , new Object[] {
               T00055_A18vueloId, T00055_A58vueloFecha, T00055_A38vueloPrecio, T00055_A39vueloDescuentoPorcentage, T00055_A41aerolineaId, T00055_n41aerolineaId, T00055_A24vueloAeropuertoSalidaId, T00055_A26vueloAeropuertoLlegadaId
               }
               , new Object[] {
               T00056_A42aerolineaNombre, T00056_A43aerolineaDescuentoPorcentage
               }
               , new Object[] {
               T00057_A25vueloAeropuertoSalidaNombre, T00057_A32vueloAeropuertoSalidaPaisId, T00057_A30vueloAeropuertoSalidaCiudadId
               }
               , new Object[] {
               T00058_A27vueloAeropuertoLlegadaNombre, T00058_A34vueloAeropuertoLlegadaPaisId, T00058_A36vueloAeropuertoLlegadaCiudadId
               }
               , new Object[] {
               T00059_A33vueloAeropuertoSalidaPaisNombr
               }
               , new Object[] {
               T000510_A31vueloAeropuertoSalidaCiudadNom
               }
               , new Object[] {
               T000511_A35vueloAeropuertoLlegadaPaisNomb
               }
               , new Object[] {
               T000512_A37vueloAeropuertoLlegadaCiudadNo
               }
               , new Object[] {
               T000514_A47vueloCapacidad, T000514_n47vueloCapacidad
               }
               , new Object[] {
               T000516_A18vueloId, T000516_A58vueloFecha, T000516_A25vueloAeropuertoSalidaNombre, T000516_A33vueloAeropuertoSalidaPaisNombr, T000516_A31vueloAeropuertoSalidaCiudadNom, T000516_A27vueloAeropuertoLlegadaNombre, T000516_A35vueloAeropuertoLlegadaPaisNomb, T000516_A37vueloAeropuertoLlegadaCiudadNo, T000516_A38vueloPrecio, T000516_A39vueloDescuentoPorcentage,
               T000516_A42aerolineaNombre, T000516_A43aerolineaDescuentoPorcentage, T000516_A41aerolineaId, T000516_n41aerolineaId, T000516_A24vueloAeropuertoSalidaId, T000516_A26vueloAeropuertoLlegadaId, T000516_A32vueloAeropuertoSalidaPaisId, T000516_A30vueloAeropuertoSalidaCiudadId, T000516_A34vueloAeropuertoLlegadaPaisId, T000516_A36vueloAeropuertoLlegadaCiudadId,
               T000516_A47vueloCapacidad, T000516_n47vueloCapacidad
               }
               , new Object[] {
               T000518_A47vueloCapacidad, T000518_n47vueloCapacidad
               }
               , new Object[] {
               T000519_A25vueloAeropuertoSalidaNombre, T000519_A32vueloAeropuertoSalidaPaisId, T000519_A30vueloAeropuertoSalidaCiudadId
               }
               , new Object[] {
               T000520_A33vueloAeropuertoSalidaPaisNombr
               }
               , new Object[] {
               T000521_A31vueloAeropuertoSalidaCiudadNom
               }
               , new Object[] {
               T000522_A27vueloAeropuertoLlegadaNombre, T000522_A34vueloAeropuertoLlegadaPaisId, T000522_A36vueloAeropuertoLlegadaCiudadId
               }
               , new Object[] {
               T000523_A35vueloAeropuertoLlegadaPaisNomb
               }
               , new Object[] {
               T000524_A37vueloAeropuertoLlegadaCiudadNo
               }
               , new Object[] {
               T000525_A42aerolineaNombre, T000525_A43aerolineaDescuentoPorcentage
               }
               , new Object[] {
               T000526_A18vueloId
               }
               , new Object[] {
               T000527_A18vueloId
               }
               , new Object[] {
               T000528_A18vueloId
               }
               , new Object[] {
               T000529_A18vueloId
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000533_A47vueloCapacidad, T000533_n47vueloCapacidad
               }
               , new Object[] {
               T000534_A25vueloAeropuertoSalidaNombre, T000534_A32vueloAeropuertoSalidaPaisId, T000534_A30vueloAeropuertoSalidaCiudadId
               }
               , new Object[] {
               T000535_A33vueloAeropuertoSalidaPaisNombr
               }
               , new Object[] {
               T000536_A31vueloAeropuertoSalidaCiudadNom
               }
               , new Object[] {
               T000537_A27vueloAeropuertoLlegadaNombre, T000537_A34vueloAeropuertoLlegadaPaisId, T000537_A36vueloAeropuertoLlegadaCiudadId
               }
               , new Object[] {
               T000538_A35vueloAeropuertoLlegadaPaisNomb
               }
               , new Object[] {
               T000539_A37vueloAeropuertoLlegadaCiudadNo
               }
               , new Object[] {
               T000540_A42aerolineaNombre, T000540_A43aerolineaDescuentoPorcentage
               }
               , new Object[] {
               T000541_A18vueloId
               }
               , new Object[] {
               T000542_A18vueloId, T000542_A44vueloAsientoId, T000542_A45vueloAsientoChar, T000542_A46vueloAsientoLocalizacion
               }
               , new Object[] {
               T000543_A18vueloId, T000543_A44vueloAsientoId, T000543_A45vueloAsientoChar
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000547_A18vueloId, T000547_A44vueloAsientoId, T000547_A45vueloAsientoChar
               }
            }
         );
      }

      private short Z39vueloDescuentoPorcentage ;
      private short O47vueloCapacidad ;
      private short nRcdDeleted_9 ;
      private short nRcdExists_9 ;
      private short nIsMod_9 ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A39vueloDescuentoPorcentage ;
      private short A43aerolineaDescuentoPorcentage ;
      private short A47vueloCapacidad ;
      private short subGridvuelo_asiento_Backcolorstyle ;
      private short subGridvuelo_asiento_Allowselection ;
      private short subGridvuelo_asiento_Allowhovering ;
      private short subGridvuelo_asiento_Allowcollapsing ;
      private short subGridvuelo_asiento_Collapsed ;
      private short nBlankRcdCount9 ;
      private short RcdFound9 ;
      private short B47vueloCapacidad ;
      private short nBlankRcdUsr9 ;
      private short s47vueloCapacidad ;
      private short GX_JID ;
      private short Z47vueloCapacidad ;
      private short Z43aerolineaDescuentoPorcentage ;
      private short RcdFound6 ;
      private short nIsDirty_6 ;
      private short Gx_BScreen ;
      private short nIsDirty_9 ;
      private short subGridvuelo_asiento_Backstyle ;
      private short gxajaxcallmode ;
      private short ZZ39vueloDescuentoPorcentage ;
      private short ZZ47vueloCapacidad ;
      private short ZZ43aerolineaDescuentoPorcentage ;
      private short ZO47vueloCapacidad ;
      private int Z18vueloId ;
      private int Z41aerolineaId ;
      private int Z24vueloAeropuertoSalidaId ;
      private int Z26vueloAeropuertoLlegadaId ;
      private int nRC_GXsfl_143 ;
      private int nGXsfl_143_idx=1 ;
      private int Z44vueloAsientoId ;
      private int A18vueloId ;
      private int A24vueloAeropuertoSalidaId ;
      private int A32vueloAeropuertoSalidaPaisId ;
      private int A30vueloAeropuertoSalidaCiudadId ;
      private int A26vueloAeropuertoLlegadaId ;
      private int A34vueloAeropuertoLlegadaPaisId ;
      private int A36vueloAeropuertoLlegadaCiudadId ;
      private int A41aerolineaId ;
      private int trnEnded ;
      private int bttBtn_first_Visible ;
      private int bttBtn_previous_Visible ;
      private int bttBtn_next_Visible ;
      private int bttBtn_last_Visible ;
      private int bttBtn_select_Visible ;
      private int edtvueloId_Enabled ;
      private int edtvueloFecha_Enabled ;
      private int edtvueloAeropuertoSalidaId_Enabled ;
      private int imgprompt_24_Visible ;
      private int edtvueloAeropuertoSalidaNombre_Enabled ;
      private int edtvueloAeropuertoSalidaPaisId_Enabled ;
      private int edtvueloAeropuertoSalidaPaisNombr_Enabled ;
      private int edtvueloAeropuertoSalidaCiudadId_Enabled ;
      private int edtvueloAeropuertoSalidaCiudadNom_Enabled ;
      private int edtvueloAeropuertoLlegadaId_Enabled ;
      private int imgprompt_26_Visible ;
      private int edtvueloAeropuertoLlegadaNombre_Enabled ;
      private int edtvueloAeropuertoLlegadaPaisId_Enabled ;
      private int edtvueloAeropuertoLlegadaPaisNomb_Enabled ;
      private int edtvueloAeropuertoLlegadaCiudadId_Enabled ;
      private int edtvueloAeropuertoLlegadaCiudadNo_Enabled ;
      private int edtvueloPrecio_Enabled ;
      private int edtvueloDescuentoPorcentage_Enabled ;
      private int edtaerolineaId_Enabled ;
      private int imgprompt_41_Visible ;
      private int edtaerolineaNombre_Enabled ;
      private int edtaerolineaDescuentoPorcentage_Enabled ;
      private int edtvueloPrecioFinal_Enabled ;
      private int edtvueloCapacidad_Enabled ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_enter_Enabled ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int bttBtn_delete_Enabled ;
      private int A44vueloAsientoId ;
      private int edtvueloAsientoId_Enabled ;
      private int subGridvuelo_asiento_Selectedindex ;
      private int subGridvuelo_asiento_Selectioncolor ;
      private int subGridvuelo_asiento_Hoveringcolor ;
      private int fRowAdded ;
      private int Z32vueloAeropuertoSalidaPaisId ;
      private int Z30vueloAeropuertoSalidaCiudadId ;
      private int Z34vueloAeropuertoLlegadaPaisId ;
      private int Z36vueloAeropuertoLlegadaCiudadId ;
      private int subGridvuelo_asiento_Backcolor ;
      private int subGridvuelo_asiento_Allbackcolor ;
      private int defcmbvueloAsientoChar_Enabled ;
      private int defedtvueloAsientoId_Enabled ;
      private int idxLst ;
      private int ZZ18vueloId ;
      private int ZZ24vueloAeropuertoSalidaId ;
      private int ZZ26vueloAeropuertoLlegadaId ;
      private int ZZ41aerolineaId ;
      private int ZZ32vueloAeropuertoSalidaPaisId ;
      private int ZZ30vueloAeropuertoSalidaCiudadId ;
      private int ZZ34vueloAeropuertoLlegadaPaisId ;
      private int ZZ36vueloAeropuertoLlegadaCiudadId ;
      private long GRIDVUELO_ASIENTO_nFirstRecordOnPage ;
      private decimal Z38vueloPrecio ;
      private decimal A38vueloPrecio ;
      private decimal A40vueloPrecioFinal ;
      private decimal Z40vueloPrecioFinal ;
      private decimal ZZ38vueloPrecio ;
      private decimal ZZ40vueloPrecioFinal ;
      private String sPrefix ;
      private String Z45vueloAsientoChar ;
      private String Z46vueloAsientoLocalizacion ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_143_idx="0001" ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtvueloId_Internalname ;
      private String divMaintable_Internalname ;
      private String divTitlecontainer_Internalname ;
      private String lblTitle_Internalname ;
      private String lblTitle_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String divFormcontainer_Internalname ;
      private String divToolbarcell_Internalname ;
      private String TempTags ;
      private String bttBtn_first_Internalname ;
      private String bttBtn_first_Jsonclick ;
      private String bttBtn_previous_Internalname ;
      private String bttBtn_previous_Jsonclick ;
      private String bttBtn_next_Internalname ;
      private String bttBtn_next_Jsonclick ;
      private String bttBtn_last_Internalname ;
      private String bttBtn_last_Jsonclick ;
      private String bttBtn_select_Internalname ;
      private String bttBtn_select_Jsonclick ;
      private String edtvueloId_Jsonclick ;
      private String edtvueloFecha_Internalname ;
      private String edtvueloFecha_Jsonclick ;
      private String edtvueloAeropuertoSalidaId_Internalname ;
      private String edtvueloAeropuertoSalidaId_Jsonclick ;
      private String sImgUrl ;
      private String imgprompt_24_Internalname ;
      private String imgprompt_24_Link ;
      private String edtvueloAeropuertoSalidaNombre_Internalname ;
      private String A25vueloAeropuertoSalidaNombre ;
      private String edtvueloAeropuertoSalidaNombre_Jsonclick ;
      private String edtvueloAeropuertoSalidaPaisId_Internalname ;
      private String edtvueloAeropuertoSalidaPaisId_Jsonclick ;
      private String edtvueloAeropuertoSalidaPaisNombr_Internalname ;
      private String A33vueloAeropuertoSalidaPaisNombr ;
      private String edtvueloAeropuertoSalidaPaisNombr_Jsonclick ;
      private String edtvueloAeropuertoSalidaCiudadId_Internalname ;
      private String edtvueloAeropuertoSalidaCiudadId_Jsonclick ;
      private String edtvueloAeropuertoSalidaCiudadNom_Internalname ;
      private String A31vueloAeropuertoSalidaCiudadNom ;
      private String edtvueloAeropuertoSalidaCiudadNom_Jsonclick ;
      private String edtvueloAeropuertoLlegadaId_Internalname ;
      private String edtvueloAeropuertoLlegadaId_Jsonclick ;
      private String imgprompt_26_Internalname ;
      private String imgprompt_26_Link ;
      private String edtvueloAeropuertoLlegadaNombre_Internalname ;
      private String A27vueloAeropuertoLlegadaNombre ;
      private String edtvueloAeropuertoLlegadaNombre_Jsonclick ;
      private String edtvueloAeropuertoLlegadaPaisId_Internalname ;
      private String edtvueloAeropuertoLlegadaPaisId_Jsonclick ;
      private String edtvueloAeropuertoLlegadaPaisNomb_Internalname ;
      private String A35vueloAeropuertoLlegadaPaisNomb ;
      private String edtvueloAeropuertoLlegadaPaisNomb_Jsonclick ;
      private String edtvueloAeropuertoLlegadaCiudadId_Internalname ;
      private String edtvueloAeropuertoLlegadaCiudadId_Jsonclick ;
      private String edtvueloAeropuertoLlegadaCiudadNo_Internalname ;
      private String A37vueloAeropuertoLlegadaCiudadNo ;
      private String edtvueloAeropuertoLlegadaCiudadNo_Jsonclick ;
      private String edtvueloPrecio_Internalname ;
      private String edtvueloPrecio_Jsonclick ;
      private String edtvueloDescuentoPorcentage_Internalname ;
      private String edtvueloDescuentoPorcentage_Jsonclick ;
      private String edtaerolineaId_Internalname ;
      private String edtaerolineaId_Jsonclick ;
      private String imgprompt_41_Internalname ;
      private String imgprompt_41_Link ;
      private String edtaerolineaNombre_Internalname ;
      private String A42aerolineaNombre ;
      private String edtaerolineaNombre_Jsonclick ;
      private String edtaerolineaDescuentoPorcentage_Internalname ;
      private String edtaerolineaDescuentoPorcentage_Jsonclick ;
      private String edtvueloPrecioFinal_Internalname ;
      private String edtvueloPrecioFinal_Jsonclick ;
      private String edtvueloCapacidad_Internalname ;
      private String edtvueloCapacidad_Jsonclick ;
      private String divAsientotable_Internalname ;
      private String lblTitleasiento_Internalname ;
      private String lblTitleasiento_Jsonclick ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String subGridvuelo_asiento_Header ;
      private String A45vueloAsientoChar ;
      private String A46vueloAsientoLocalizacion ;
      private String sMode9 ;
      private String edtvueloAsientoId_Internalname ;
      private String cmbvueloAsientoChar_Internalname ;
      private String cmbvueloAsientoLocalizacion_Internalname ;
      private String sStyleString ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GXCCtl ;
      private String Z25vueloAeropuertoSalidaNombre ;
      private String Z33vueloAeropuertoSalidaPaisNombr ;
      private String Z31vueloAeropuertoSalidaCiudadNom ;
      private String Z27vueloAeropuertoLlegadaNombre ;
      private String Z35vueloAeropuertoLlegadaPaisNomb ;
      private String Z37vueloAeropuertoLlegadaCiudadNo ;
      private String Z42aerolineaNombre ;
      private String sMode6 ;
      private String sGXsfl_143_fel_idx="0001" ;
      private String subGridvuelo_asiento_Class ;
      private String subGridvuelo_asiento_Linesclass ;
      private String ROClassString ;
      private String edtvueloAsientoId_Jsonclick ;
      private String cmbvueloAsientoChar_Jsonclick ;
      private String cmbvueloAsientoLocalizacion_Jsonclick ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String subGridvuelo_asiento_Internalname ;
      private String ZZ25vueloAeropuertoSalidaNombre ;
      private String ZZ33vueloAeropuertoSalidaPaisNombr ;
      private String ZZ31vueloAeropuertoSalidaCiudadNom ;
      private String ZZ27vueloAeropuertoLlegadaNombre ;
      private String ZZ35vueloAeropuertoLlegadaPaisNomb ;
      private String ZZ37vueloAeropuertoLlegadaCiudadNo ;
      private String ZZ42aerolineaNombre ;
      private DateTime Z58vueloFecha ;
      private DateTime A58vueloFecha ;
      private DateTime ZZ58vueloFecha ;
      private bool entryPointCalled ;
      private bool n41aerolineaId ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n47vueloCapacidad ;
      private bool bGXsfl_143_Refreshing=false ;
      private bool Gx_longc ;
      private GXWebGrid Gridvuelo_asientoContainer ;
      private GXWebRow Gridvuelo_asientoRow ;
      private GXWebColumn Gridvuelo_asientoColumn ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbvueloAsientoChar ;
      private GXCombobox cmbvueloAsientoLocalizacion ;
      private IDataStoreProvider pr_default ;
      private int[] T000516_A18vueloId ;
      private DateTime[] T000516_A58vueloFecha ;
      private String[] T000516_A25vueloAeropuertoSalidaNombre ;
      private String[] T000516_A33vueloAeropuertoSalidaPaisNombr ;
      private String[] T000516_A31vueloAeropuertoSalidaCiudadNom ;
      private String[] T000516_A27vueloAeropuertoLlegadaNombre ;
      private String[] T000516_A35vueloAeropuertoLlegadaPaisNomb ;
      private String[] T000516_A37vueloAeropuertoLlegadaCiudadNo ;
      private decimal[] T000516_A38vueloPrecio ;
      private short[] T000516_A39vueloDescuentoPorcentage ;
      private String[] T000516_A42aerolineaNombre ;
      private short[] T000516_A43aerolineaDescuentoPorcentage ;
      private int[] T000516_A41aerolineaId ;
      private bool[] T000516_n41aerolineaId ;
      private int[] T000516_A24vueloAeropuertoSalidaId ;
      private int[] T000516_A26vueloAeropuertoLlegadaId ;
      private int[] T000516_A32vueloAeropuertoSalidaPaisId ;
      private int[] T000516_A30vueloAeropuertoSalidaCiudadId ;
      private int[] T000516_A34vueloAeropuertoLlegadaPaisId ;
      private int[] T000516_A36vueloAeropuertoLlegadaCiudadId ;
      private short[] T000516_A47vueloCapacidad ;
      private bool[] T000516_n47vueloCapacidad ;
      private short[] T000514_A47vueloCapacidad ;
      private bool[] T000514_n47vueloCapacidad ;
      private String[] T00057_A25vueloAeropuertoSalidaNombre ;
      private int[] T00057_A32vueloAeropuertoSalidaPaisId ;
      private int[] T00057_A30vueloAeropuertoSalidaCiudadId ;
      private String[] T00059_A33vueloAeropuertoSalidaPaisNombr ;
      private String[] T000510_A31vueloAeropuertoSalidaCiudadNom ;
      private String[] T00058_A27vueloAeropuertoLlegadaNombre ;
      private int[] T00058_A34vueloAeropuertoLlegadaPaisId ;
      private int[] T00058_A36vueloAeropuertoLlegadaCiudadId ;
      private String[] T000511_A35vueloAeropuertoLlegadaPaisNomb ;
      private String[] T000512_A37vueloAeropuertoLlegadaCiudadNo ;
      private String[] T00056_A42aerolineaNombre ;
      private short[] T00056_A43aerolineaDescuentoPorcentage ;
      private short[] T000518_A47vueloCapacidad ;
      private bool[] T000518_n47vueloCapacidad ;
      private String[] T000519_A25vueloAeropuertoSalidaNombre ;
      private int[] T000519_A32vueloAeropuertoSalidaPaisId ;
      private int[] T000519_A30vueloAeropuertoSalidaCiudadId ;
      private String[] T000520_A33vueloAeropuertoSalidaPaisNombr ;
      private String[] T000521_A31vueloAeropuertoSalidaCiudadNom ;
      private String[] T000522_A27vueloAeropuertoLlegadaNombre ;
      private int[] T000522_A34vueloAeropuertoLlegadaPaisId ;
      private int[] T000522_A36vueloAeropuertoLlegadaCiudadId ;
      private String[] T000523_A35vueloAeropuertoLlegadaPaisNomb ;
      private String[] T000524_A37vueloAeropuertoLlegadaCiudadNo ;
      private String[] T000525_A42aerolineaNombre ;
      private short[] T000525_A43aerolineaDescuentoPorcentage ;
      private int[] T000526_A18vueloId ;
      private int[] T00055_A18vueloId ;
      private DateTime[] T00055_A58vueloFecha ;
      private decimal[] T00055_A38vueloPrecio ;
      private short[] T00055_A39vueloDescuentoPorcentage ;
      private int[] T00055_A41aerolineaId ;
      private bool[] T00055_n41aerolineaId ;
      private int[] T00055_A24vueloAeropuertoSalidaId ;
      private int[] T00055_A26vueloAeropuertoLlegadaId ;
      private int[] T000527_A18vueloId ;
      private int[] T000528_A18vueloId ;
      private int[] T00054_A18vueloId ;
      private DateTime[] T00054_A58vueloFecha ;
      private decimal[] T00054_A38vueloPrecio ;
      private short[] T00054_A39vueloDescuentoPorcentage ;
      private int[] T00054_A41aerolineaId ;
      private bool[] T00054_n41aerolineaId ;
      private int[] T00054_A24vueloAeropuertoSalidaId ;
      private int[] T00054_A26vueloAeropuertoLlegadaId ;
      private int[] T000529_A18vueloId ;
      private short[] T000533_A47vueloCapacidad ;
      private bool[] T000533_n47vueloCapacidad ;
      private String[] T000534_A25vueloAeropuertoSalidaNombre ;
      private int[] T000534_A32vueloAeropuertoSalidaPaisId ;
      private int[] T000534_A30vueloAeropuertoSalidaCiudadId ;
      private String[] T000535_A33vueloAeropuertoSalidaPaisNombr ;
      private String[] T000536_A31vueloAeropuertoSalidaCiudadNom ;
      private String[] T000537_A27vueloAeropuertoLlegadaNombre ;
      private int[] T000537_A34vueloAeropuertoLlegadaPaisId ;
      private int[] T000537_A36vueloAeropuertoLlegadaCiudadId ;
      private String[] T000538_A35vueloAeropuertoLlegadaPaisNomb ;
      private String[] T000539_A37vueloAeropuertoLlegadaCiudadNo ;
      private String[] T000540_A42aerolineaNombre ;
      private short[] T000540_A43aerolineaDescuentoPorcentage ;
      private int[] T000541_A18vueloId ;
      private int[] T000542_A18vueloId ;
      private int[] T000542_A44vueloAsientoId ;
      private String[] T000542_A45vueloAsientoChar ;
      private String[] T000542_A46vueloAsientoLocalizacion ;
      private int[] T000543_A18vueloId ;
      private int[] T000543_A44vueloAsientoId ;
      private String[] T000543_A45vueloAsientoChar ;
      private int[] T00053_A18vueloId ;
      private int[] T00053_A44vueloAsientoId ;
      private String[] T00053_A45vueloAsientoChar ;
      private String[] T00053_A46vueloAsientoLocalizacion ;
      private int[] T00052_A18vueloId ;
      private int[] T00052_A44vueloAsientoId ;
      private String[] T00052_A45vueloAsientoChar ;
      private String[] T00052_A46vueloAsientoLocalizacion ;
      private int[] T000547_A18vueloId ;
      private int[] T000547_A44vueloAsientoId ;
      private String[] T000547_A45vueloAsientoChar ;
      private GXWebForm Form ;
   }

   public class vuelo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new UpdateCursor(def[25])
         ,new UpdateCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new ForEachCursor(def[33])
         ,new ForEachCursor(def[34])
         ,new ForEachCursor(def[35])
         ,new ForEachCursor(def[36])
         ,new ForEachCursor(def[37])
         ,new UpdateCursor(def[38])
         ,new UpdateCursor(def[39])
         ,new UpdateCursor(def[40])
         ,new ForEachCursor(def[41])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000516 ;
          prmT000516 = new Object[] {
          new Object[] {"@vueloId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000514 ;
          prmT000514 = new Object[] {
          new Object[] {"@vueloId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00057 ;
          prmT00057 = new Object[] {
          new Object[] {"@vueloAeropuertoSalidaId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00059 ;
          prmT00059 = new Object[] {
          new Object[] {"@vueloAeropuertoSalidaPaisId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000510 ;
          prmT000510 = new Object[] {
          new Object[] {"@vueloAeropuertoSalidaPaisId",SqlDbType.Int,6,0} ,
          new Object[] {"@vueloAeropuertoSalidaCiudadId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00058 ;
          prmT00058 = new Object[] {
          new Object[] {"@vueloAeropuertoLlegadaId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000511 ;
          prmT000511 = new Object[] {
          new Object[] {"@vueloAeropuertoLlegadaPaisId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000512 ;
          prmT000512 = new Object[] {
          new Object[] {"@vueloAeropuertoLlegadaPaisId",SqlDbType.Int,6,0} ,
          new Object[] {"@vueloAeropuertoLlegadaCiudadId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00056 ;
          prmT00056 = new Object[] {
          new Object[] {"@aerolineaId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000518 ;
          prmT000518 = new Object[] {
          new Object[] {"@vueloId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000519 ;
          prmT000519 = new Object[] {
          new Object[] {"@vueloAeropuertoSalidaId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000520 ;
          prmT000520 = new Object[] {
          new Object[] {"@vueloAeropuertoSalidaPaisId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000521 ;
          prmT000521 = new Object[] {
          new Object[] {"@vueloAeropuertoSalidaPaisId",SqlDbType.Int,6,0} ,
          new Object[] {"@vueloAeropuertoSalidaCiudadId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000522 ;
          prmT000522 = new Object[] {
          new Object[] {"@vueloAeropuertoLlegadaId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000523 ;
          prmT000523 = new Object[] {
          new Object[] {"@vueloAeropuertoLlegadaPaisId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000524 ;
          prmT000524 = new Object[] {
          new Object[] {"@vueloAeropuertoLlegadaPaisId",SqlDbType.Int,6,0} ,
          new Object[] {"@vueloAeropuertoLlegadaCiudadId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000525 ;
          prmT000525 = new Object[] {
          new Object[] {"@aerolineaId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000526 ;
          prmT000526 = new Object[] {
          new Object[] {"@vueloId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00055 ;
          prmT00055 = new Object[] {
          new Object[] {"@vueloId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000527 ;
          prmT000527 = new Object[] {
          new Object[] {"@vueloId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000528 ;
          prmT000528 = new Object[] {
          new Object[] {"@vueloId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00054 ;
          prmT00054 = new Object[] {
          new Object[] {"@vueloId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000529 ;
          prmT000529 = new Object[] {
          new Object[] {"@vueloFecha",SqlDbType.DateTime,8,0} ,
          new Object[] {"@vueloPrecio",SqlDbType.Decimal,9,2} ,
          new Object[] {"@vueloDescuentoPorcentage",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@aerolineaId",SqlDbType.Int,6,0} ,
          new Object[] {"@vueloAeropuertoSalidaId",SqlDbType.Int,6,0} ,
          new Object[] {"@vueloAeropuertoLlegadaId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000530 ;
          prmT000530 = new Object[] {
          new Object[] {"@vueloFecha",SqlDbType.DateTime,8,0} ,
          new Object[] {"@vueloPrecio",SqlDbType.Decimal,9,2} ,
          new Object[] {"@vueloDescuentoPorcentage",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@aerolineaId",SqlDbType.Int,6,0} ,
          new Object[] {"@vueloAeropuertoSalidaId",SqlDbType.Int,6,0} ,
          new Object[] {"@vueloAeropuertoLlegadaId",SqlDbType.Int,6,0} ,
          new Object[] {"@vueloId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000531 ;
          prmT000531 = new Object[] {
          new Object[] {"@vueloId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000541 ;
          prmT000541 = new Object[] {
          } ;
          Object[] prmT000542 ;
          prmT000542 = new Object[] {
          new Object[] {"@vueloId",SqlDbType.Int,6,0} ,
          new Object[] {"@vueloAsientoId",SqlDbType.Int,6,0} ,
          new Object[] {"@vueloAsientoChar",SqlDbType.NChar,1,0}
          } ;
          Object[] prmT000543 ;
          prmT000543 = new Object[] {
          new Object[] {"@vueloId",SqlDbType.Int,6,0} ,
          new Object[] {"@vueloAsientoId",SqlDbType.Int,6,0} ,
          new Object[] {"@vueloAsientoChar",SqlDbType.NChar,1,0}
          } ;
          Object[] prmT00053 ;
          prmT00053 = new Object[] {
          new Object[] {"@vueloId",SqlDbType.Int,6,0} ,
          new Object[] {"@vueloAsientoId",SqlDbType.Int,6,0} ,
          new Object[] {"@vueloAsientoChar",SqlDbType.NChar,1,0}
          } ;
          Object[] prmT00052 ;
          prmT00052 = new Object[] {
          new Object[] {"@vueloId",SqlDbType.Int,6,0} ,
          new Object[] {"@vueloAsientoId",SqlDbType.Int,6,0} ,
          new Object[] {"@vueloAsientoChar",SqlDbType.NChar,1,0}
          } ;
          Object[] prmT000544 ;
          prmT000544 = new Object[] {
          new Object[] {"@vueloId",SqlDbType.Int,6,0} ,
          new Object[] {"@vueloAsientoId",SqlDbType.Int,6,0} ,
          new Object[] {"@vueloAsientoChar",SqlDbType.NChar,1,0} ,
          new Object[] {"@vueloAsientoLocalizacion",SqlDbType.NChar,1,0}
          } ;
          Object[] prmT000545 ;
          prmT000545 = new Object[] {
          new Object[] {"@vueloAsientoLocalizacion",SqlDbType.NChar,1,0} ,
          new Object[] {"@vueloId",SqlDbType.Int,6,0} ,
          new Object[] {"@vueloAsientoId",SqlDbType.Int,6,0} ,
          new Object[] {"@vueloAsientoChar",SqlDbType.NChar,1,0}
          } ;
          Object[] prmT000546 ;
          prmT000546 = new Object[] {
          new Object[] {"@vueloId",SqlDbType.Int,6,0} ,
          new Object[] {"@vueloAsientoId",SqlDbType.Int,6,0} ,
          new Object[] {"@vueloAsientoChar",SqlDbType.NChar,1,0}
          } ;
          Object[] prmT000547 ;
          prmT000547 = new Object[] {
          new Object[] {"@vueloId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000533 ;
          prmT000533 = new Object[] {
          new Object[] {"@vueloId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000534 ;
          prmT000534 = new Object[] {
          new Object[] {"@vueloAeropuertoSalidaId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000535 ;
          prmT000535 = new Object[] {
          new Object[] {"@vueloAeropuertoSalidaPaisId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000536 ;
          prmT000536 = new Object[] {
          new Object[] {"@vueloAeropuertoSalidaPaisId",SqlDbType.Int,6,0} ,
          new Object[] {"@vueloAeropuertoSalidaCiudadId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000537 ;
          prmT000537 = new Object[] {
          new Object[] {"@vueloAeropuertoLlegadaId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000538 ;
          prmT000538 = new Object[] {
          new Object[] {"@vueloAeropuertoLlegadaPaisId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000539 ;
          prmT000539 = new Object[] {
          new Object[] {"@vueloAeropuertoLlegadaPaisId",SqlDbType.Int,6,0} ,
          new Object[] {"@vueloAeropuertoLlegadaCiudadId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000540 ;
          prmT000540 = new Object[] {
          new Object[] {"@aerolineaId",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00052", "SELECT [vueloId], [vueloAsientoId], [vueloAsientoChar], [vueloAsientoLocalizacion] FROM [vueloAsiento] WITH (UPDLOCK) WHERE [vueloId] = @vueloId AND [vueloAsientoId] = @vueloAsientoId AND [vueloAsientoChar] = @vueloAsientoChar ",true, GxErrorMask.GX_NOMASK, false, this,prmT00052,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00053", "SELECT [vueloId], [vueloAsientoId], [vueloAsientoChar], [vueloAsientoLocalizacion] FROM [vueloAsiento] WHERE [vueloId] = @vueloId AND [vueloAsientoId] = @vueloAsientoId AND [vueloAsientoChar] = @vueloAsientoChar ",true, GxErrorMask.GX_NOMASK, false, this,prmT00053,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00054", "SELECT [vueloId], [vueloFecha], [vueloPrecio], [vueloDescuentoPorcentage], [aerolineaId], [vueloAeropuertoSalidaId] AS vueloAeropuertoSalidaId, [vueloAeropuertoLlegadaId] AS vueloAeropuertoLlegadaId FROM [vuelo] WITH (UPDLOCK) WHERE [vueloId] = @vueloId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00054,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00055", "SELECT [vueloId], [vueloFecha], [vueloPrecio], [vueloDescuentoPorcentage], [aerolineaId], [vueloAeropuertoSalidaId] AS vueloAeropuertoSalidaId, [vueloAeropuertoLlegadaId] AS vueloAeropuertoLlegadaId FROM [vuelo] WHERE [vueloId] = @vueloId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00055,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00056", "SELECT [aerolineaNombre], [aerolineaDescuentoPorcentage] FROM [aerolinea] WHERE [aerolineaId] = @aerolineaId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00056,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00057", "SELECT [aeropuertoNombre] AS vueloAeropuertoSalidaNombre, [paisId] AS vueloAeropuertoSalidaPaisId, [ciudadId] AS vueloAeropuertoSalidaCiudadId FROM [aeropuerto] WHERE [aeropuertoId] = @vueloAeropuertoSalidaId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00057,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00058", "SELECT [aeropuertoNombre] AS vueloAeropuertoLlegadaNombre, [paisId] AS vueloAeropuertoLlegadaPaisId, [ciudadId] AS vueloAeropuertoLlegadaCiudadId FROM [aeropuerto] WHERE [aeropuertoId] = @vueloAeropuertoLlegadaId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00058,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00059", "SELECT [paisNombre] AS vueloAeropuertoSalidaPaisNombr FROM [pais] WHERE [paisId] = @vueloAeropuertoSalidaPaisId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00059,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000510", "SELECT [ciudadNombre] AS vueloAeropuertoSalidaCiudadNom FROM [paisciudad] WHERE [paisId] = @vueloAeropuertoSalidaPaisId AND [ciudadId] = @vueloAeropuertoSalidaCiudadId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000510,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000511", "SELECT [paisNombre] AS vueloAeropuertoLlegadaPaisNomb FROM [pais] WHERE [paisId] = @vueloAeropuertoLlegadaPaisId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000511,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000512", "SELECT [ciudadNombre] AS vueloAeropuertoLlegadaCiudadNo FROM [paisciudad] WHERE [paisId] = @vueloAeropuertoLlegadaPaisId AND [ciudadId] = @vueloAeropuertoLlegadaCiudadId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000512,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000514", "SELECT COALESCE( T1.[vueloCapacidad], 0) AS vueloCapacidad FROM (SELECT COUNT(*) AS vueloCapacidad, [vueloId] FROM [vueloAsiento] WITH (UPDLOCK) GROUP BY [vueloId] ) T1 WHERE T1.[vueloId] = @vueloId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000514,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000516", "SELECT TM1.[vueloId], TM1.[vueloFecha], T3.[aeropuertoNombre] AS vueloAeropuertoSalidaNombre, T4.[paisNombre] AS vueloAeropuertoSalidaPaisNombr, T5.[ciudadNombre] AS vueloAeropuertoSalidaCiudadNom, T6.[aeropuertoNombre] AS vueloAeropuertoLlegadaNombre, T7.[paisNombre] AS vueloAeropuertoLlegadaPaisNomb, T8.[ciudadNombre] AS vueloAeropuertoLlegadaCiudadNo, TM1.[vueloPrecio], TM1.[vueloDescuentoPorcentage], T9.[aerolineaNombre], T9.[aerolineaDescuentoPorcentage], TM1.[aerolineaId], TM1.[vueloAeropuertoSalidaId] AS vueloAeropuertoSalidaId, TM1.[vueloAeropuertoLlegadaId] AS vueloAeropuertoLlegadaId, T3.[paisId] AS vueloAeropuertoSalidaPaisId, T3.[ciudadId] AS vueloAeropuertoSalidaCiudadId, T6.[paisId] AS vueloAeropuertoLlegadaPaisId, T6.[ciudadId] AS vueloAeropuertoLlegadaCiudadId, COALESCE( T2.[vueloCapacidad], 0) AS vueloCapacidad FROM (((((((([vuelo] TM1 LEFT JOIN (SELECT COUNT(*) AS vueloCapacidad, [vueloId] FROM [vueloAsiento] GROUP BY [vueloId] ) T2 ON T2.[vueloId] = TM1.[vueloId]) INNER JOIN [aeropuerto] T3 ON T3.[aeropuertoId] = TM1.[vueloAeropuertoSalidaId]) INNER JOIN [pais] T4 ON T4.[paisId] = T3.[paisId]) INNER JOIN [paisciudad] T5 ON T5.[paisId] = T3.[paisId] AND T5.[ciudadId] = T3.[ciudadId]) INNER JOIN [aeropuerto] T6 ON T6.[aeropuertoId] = TM1.[vueloAeropuertoLlegadaId]) INNER JOIN [pais] T7 ON T7.[paisId] = T6.[paisId]) INNER JOIN [paisciudad] T8 ON T8.[paisId] = T6.[paisId] AND T8.[ciudadId] = T6.[ciudadId]) LEFT JOIN [aerolinea] T9 ON T9.[aerolineaId] = TM1.[aerolineaId]) WHERE TM1.[vueloId] = @vueloId ORDER BY TM1.[vueloId]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000516,100, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000518", "SELECT COALESCE( T1.[vueloCapacidad], 0) AS vueloCapacidad FROM (SELECT COUNT(*) AS vueloCapacidad, [vueloId] FROM [vueloAsiento] WITH (UPDLOCK) GROUP BY [vueloId] ) T1 WHERE T1.[vueloId] = @vueloId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000518,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000519", "SELECT [aeropuertoNombre] AS vueloAeropuertoSalidaNombre, [paisId] AS vueloAeropuertoSalidaPaisId, [ciudadId] AS vueloAeropuertoSalidaCiudadId FROM [aeropuerto] WHERE [aeropuertoId] = @vueloAeropuertoSalidaId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000519,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000520", "SELECT [paisNombre] AS vueloAeropuertoSalidaPaisNombr FROM [pais] WHERE [paisId] = @vueloAeropuertoSalidaPaisId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000520,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000521", "SELECT [ciudadNombre] AS vueloAeropuertoSalidaCiudadNom FROM [paisciudad] WHERE [paisId] = @vueloAeropuertoSalidaPaisId AND [ciudadId] = @vueloAeropuertoSalidaCiudadId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000521,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000522", "SELECT [aeropuertoNombre] AS vueloAeropuertoLlegadaNombre, [paisId] AS vueloAeropuertoLlegadaPaisId, [ciudadId] AS vueloAeropuertoLlegadaCiudadId FROM [aeropuerto] WHERE [aeropuertoId] = @vueloAeropuertoLlegadaId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000522,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000523", "SELECT [paisNombre] AS vueloAeropuertoLlegadaPaisNomb FROM [pais] WHERE [paisId] = @vueloAeropuertoLlegadaPaisId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000523,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000524", "SELECT [ciudadNombre] AS vueloAeropuertoLlegadaCiudadNo FROM [paisciudad] WHERE [paisId] = @vueloAeropuertoLlegadaPaisId AND [ciudadId] = @vueloAeropuertoLlegadaCiudadId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000524,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000525", "SELECT [aerolineaNombre], [aerolineaDescuentoPorcentage] FROM [aerolinea] WHERE [aerolineaId] = @aerolineaId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000525,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000526", "SELECT [vueloId] FROM [vuelo] WHERE [vueloId] = @vueloId  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000526,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000527", "SELECT TOP 1 [vueloId] FROM [vuelo] WHERE ( [vueloId] > @vueloId) ORDER BY [vueloId]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000527,1, GxCacheFrequency.OFF ,true,true )
             ,new CursorDef("T000528", "SELECT TOP 1 [vueloId] FROM [vuelo] WHERE ( [vueloId] < @vueloId) ORDER BY [vueloId] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000528,1, GxCacheFrequency.OFF ,true,true )
             ,new CursorDef("T000529", "INSERT INTO [vuelo]([vueloFecha], [vueloPrecio], [vueloDescuentoPorcentage], [aerolineaId], [vueloAeropuertoSalidaId], [vueloAeropuertoLlegadaId]) VALUES(@vueloFecha, @vueloPrecio, @vueloDescuentoPorcentage, @aerolineaId, @vueloAeropuertoSalidaId, @vueloAeropuertoLlegadaId); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000529)
             ,new CursorDef("T000530", "UPDATE [vuelo] SET [vueloFecha]=@vueloFecha, [vueloPrecio]=@vueloPrecio, [vueloDescuentoPorcentage]=@vueloDescuentoPorcentage, [aerolineaId]=@aerolineaId, [vueloAeropuertoSalidaId]=@vueloAeropuertoSalidaId, [vueloAeropuertoLlegadaId]=@vueloAeropuertoLlegadaId  WHERE [vueloId] = @vueloId", GxErrorMask.GX_NOMASK,prmT000530)
             ,new CursorDef("T000531", "DELETE FROM [vuelo]  WHERE [vueloId] = @vueloId", GxErrorMask.GX_NOMASK,prmT000531)
             ,new CursorDef("T000533", "SELECT COALESCE( T1.[vueloCapacidad], 0) AS vueloCapacidad FROM (SELECT COUNT(*) AS vueloCapacidad, [vueloId] FROM [vueloAsiento] WITH (UPDLOCK) GROUP BY [vueloId] ) T1 WHERE T1.[vueloId] = @vueloId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000533,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000534", "SELECT [aeropuertoNombre] AS vueloAeropuertoSalidaNombre, [paisId] AS vueloAeropuertoSalidaPaisId, [ciudadId] AS vueloAeropuertoSalidaCiudadId FROM [aeropuerto] WHERE [aeropuertoId] = @vueloAeropuertoSalidaId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000534,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000535", "SELECT [paisNombre] AS vueloAeropuertoSalidaPaisNombr FROM [pais] WHERE [paisId] = @vueloAeropuertoSalidaPaisId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000535,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000536", "SELECT [ciudadNombre] AS vueloAeropuertoSalidaCiudadNom FROM [paisciudad] WHERE [paisId] = @vueloAeropuertoSalidaPaisId AND [ciudadId] = @vueloAeropuertoSalidaCiudadId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000536,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000537", "SELECT [aeropuertoNombre] AS vueloAeropuertoLlegadaNombre, [paisId] AS vueloAeropuertoLlegadaPaisId, [ciudadId] AS vueloAeropuertoLlegadaCiudadId FROM [aeropuerto] WHERE [aeropuertoId] = @vueloAeropuertoLlegadaId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000537,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000538", "SELECT [paisNombre] AS vueloAeropuertoLlegadaPaisNomb FROM [pais] WHERE [paisId] = @vueloAeropuertoLlegadaPaisId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000538,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000539", "SELECT [ciudadNombre] AS vueloAeropuertoLlegadaCiudadNo FROM [paisciudad] WHERE [paisId] = @vueloAeropuertoLlegadaPaisId AND [ciudadId] = @vueloAeropuertoLlegadaCiudadId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000539,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000540", "SELECT [aerolineaNombre], [aerolineaDescuentoPorcentage] FROM [aerolinea] WHERE [aerolineaId] = @aerolineaId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000540,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000541", "SELECT [vueloId] FROM [vuelo] ORDER BY [vueloId]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000541,100, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000542", "SELECT [vueloId], [vueloAsientoId], [vueloAsientoChar], [vueloAsientoLocalizacion] FROM [vueloAsiento] WHERE [vueloId] = @vueloId and [vueloAsientoId] = @vueloAsientoId and [vueloAsientoChar] = @vueloAsientoChar ORDER BY [vueloId], [vueloAsientoId], [vueloAsientoChar] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000542,11, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000543", "SELECT [vueloId], [vueloAsientoId], [vueloAsientoChar] FROM [vueloAsiento] WHERE [vueloId] = @vueloId AND [vueloAsientoId] = @vueloAsientoId AND [vueloAsientoChar] = @vueloAsientoChar ",true, GxErrorMask.GX_NOMASK, false, this,prmT000543,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000544", "INSERT INTO [vueloAsiento]([vueloId], [vueloAsientoId], [vueloAsientoChar], [vueloAsientoLocalizacion]) VALUES(@vueloId, @vueloAsientoId, @vueloAsientoChar, @vueloAsientoLocalizacion)", GxErrorMask.GX_NOMASK,prmT000544)
             ,new CursorDef("T000545", "UPDATE [vueloAsiento] SET [vueloAsientoLocalizacion]=@vueloAsientoLocalizacion  WHERE [vueloId] = @vueloId AND [vueloAsientoId] = @vueloAsientoId AND [vueloAsientoChar] = @vueloAsientoChar", GxErrorMask.GX_NOMASK,prmT000545)
             ,new CursorDef("T000546", "DELETE FROM [vueloAsiento]  WHERE [vueloId] = @vueloId AND [vueloAsientoId] = @vueloAsientoId AND [vueloAsientoChar] = @vueloAsientoChar", GxErrorMask.GX_NOMASK,prmT000546)
             ,new CursorDef("T000547", "SELECT [vueloId], [vueloAsientoId], [vueloAsientoChar] FROM [vueloAsiento] WHERE [vueloId] = @vueloId ORDER BY [vueloId], [vueloAsientoId], [vueloAsientoChar] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000547,11, GxCacheFrequency.OFF ,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 11 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 20) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 20) ;
                ((String[]) buf[7])[0] = rslt.getString(8, 20) ;
                ((decimal[]) buf[8])[0] = rslt.getDecimal(9) ;
                ((short[]) buf[9])[0] = rslt.getShort(10) ;
                ((String[]) buf[10])[0] = rslt.getString(11, 20) ;
                ((short[]) buf[11])[0] = rslt.getShort(12) ;
                ((int[]) buf[12])[0] = rslt.getInt(13) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(13);
                ((int[]) buf[14])[0] = rslt.getInt(14) ;
                ((int[]) buf[15])[0] = rslt.getInt(15) ;
                ((int[]) buf[16])[0] = rslt.getInt(16) ;
                ((int[]) buf[17])[0] = rslt.getInt(17) ;
                ((int[]) buf[18])[0] = rslt.getInt(18) ;
                ((int[]) buf[19])[0] = rslt.getInt(19) ;
                ((short[]) buf[20])[0] = rslt.getShort(20) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(20);
                return;
             case 13 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 19 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 20 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 27 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 28 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 29 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 31 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 32 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 33 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 34 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 35 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 36 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
                return;
             case 37 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                return;
             case 41 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 20 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 23 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 24 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (decimal)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[4]);
                }
                stmt.SetParameter(5, (int)parms[5]);
                stmt.SetParameter(6, (int)parms[6]);
                return;
             case 25 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (decimal)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[4]);
                }
                stmt.SetParameter(5, (int)parms[5]);
                stmt.SetParameter(6, (int)parms[6]);
                stmt.SetParameter(7, (int)parms[7]);
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 27 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 28 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 29 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 31 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 32 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 33 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 34 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 36 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 37 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 38 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 39 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 40 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 41 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
