/*
               File: categoria_DataProvider
        Description: categoria_Data Provider
             Author: GeneXus C# Generator version 16_0_6-136889
       Generated on: 12/4/2019 23:7:41.31
       Program type: Main program
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class acategoria_dataprovider : GXProcedure
   {
      public acategoria_dataprovider( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public acategoria_dataprovider( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( out GXBCCollection<Sdtcategoria> aP0_Gxm1rootcol )
      {
         this.Gxm1rootcol = new GXBCCollection<Sdtcategoria>( context, "categoria", "TravelAgency") ;
         initialize();
         executePrivate();
         aP0_Gxm1rootcol=this.Gxm1rootcol;
      }

      public GXBCCollection<Sdtcategoria> executeUdp( )
      {
         this.Gxm1rootcol = new GXBCCollection<Sdtcategoria>( context, "categoria", "TravelAgency") ;
         initialize();
         executePrivate();
         aP0_Gxm1rootcol=this.Gxm1rootcol;
         return Gxm1rootcol ;
      }

      public void executeSubmit( out GXBCCollection<Sdtcategoria> aP0_Gxm1rootcol )
      {
         acategoria_dataprovider objacategoria_dataprovider;
         objacategoria_dataprovider = new acategoria_dataprovider();
         objacategoria_dataprovider.Gxm1rootcol = new GXBCCollection<Sdtcategoria>( context, "categoria", "TravelAgency") ;
         objacategoria_dataprovider.context.SetSubmitInitialConfig(context);
         objacategoria_dataprovider.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objacategoria_dataprovider);
         aP0_Gxm1rootcol=this.Gxm1rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((acategoria_dataprovider)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private GXBCCollection<Sdtcategoria> aP0_Gxm1rootcol ;
      private GXBCCollection<Sdtcategoria> Gxm1rootcol ;
   }

}
