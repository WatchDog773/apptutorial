/*
               File: atraccionPorNombre
        Description: atraccion Por Nombre
             Author: GeneXus C# Generator version 16_0_6-136889
       Generated on: 12/2/2019 23:24:8.80
       Program type: Main program
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aatraccionpornombre : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      public override void webExecute( )
      {
         context.SetDefaultTheme("Carmine");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV9nombreDe = gxfirstwebparm;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV10nombrePara = GetNextPar( );
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public aatraccionpornombre( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public aatraccionpornombre( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_nombreDe ,
                           String aP1_nombrePara )
      {
         this.AV9nombreDe = aP0_nombreDe;
         this.AV10nombrePara = aP1_nombrePara;
         initialize();
         executePrivate();
      }

      public void executeSubmit( String aP0_nombreDe ,
                                 String aP1_nombrePara )
      {
         aatraccionpornombre objaatraccionpornombre;
         objaatraccionpornombre = new aatraccionpornombre();
         objaatraccionpornombre.AV9nombreDe = aP0_nombreDe;
         objaatraccionpornombre.AV10nombrePara = aP1_nombrePara;
         objaatraccionpornombre.context.SetSubmitInitialConfig(context);
         objaatraccionpornombre.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaatraccionpornombre);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aatraccionpornombre)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 6;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 256, 16834, 11909, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*6));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            H0G0( false, 28) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+28);
            H0G0( false, 81) ;
            getPrinter().GxDrawLine(93, Gx_line+80, 733, Gx_line+80, 1, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Id", 107, Gx_line+67, 117, Gx_line+81, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Nombre", 286, Gx_line+67, 328, Gx_line+81, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Pais", 510, Gx_line+67, 534, Gx_line+81, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawLine(100, Gx_line+80, 720, Gx_line+80, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Foto", 716, Gx_line+67, 740, Gx_line+81, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Id", 107, Gx_line+67, 117, Gx_line+81, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+81);
            /* Using cursor P000G2 */
            pr_default.execute(0, new Object[] {AV9nombreDe, AV10nombrePara});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A9paisId = P000G2_A9paisId[0];
               A15ciudadId = P000G2_A15ciudadId[0];
               A8atraccionNombre = P000G2_A8atraccionNombre[0];
               A40000atraccionFoto_GXI = P000G2_A40000atraccionFoto_GXI[0];
               A17ciudadNombre = P000G2_A17ciudadNombre[0];
               A7atraccionId = P000G2_A7atraccionId[0];
               A13atraccionFoto = P000G2_A13atraccionFoto[0];
               A17ciudadNombre = P000G2_A17ciudadNombre[0];
               H0G0( false, 63) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A7atraccionId), "ZZZZZ9")), 81, Gx_line+13, 117, Gx_line+27, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A8atraccionNombre, "")), 227, Gx_line+13, 328, Gx_line+27, 0+256, 0, 0, 0) ;
               sImgUrl = (String.IsNullOrEmpty(StringUtil.RTrim( A13atraccionFoto)) ? A40000atraccionFoto_GXI : A13atraccionFoto);
               getPrinter().GxDrawBitMap(sImgUrl, 687, Gx_line+13, 740, Gx_line+53) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A17ciudadNombre, "")), 433, Gx_line+13, 534, Gx_line+27, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+63);
               pr_default.readNext(0);
            }
            pr_default.close(0);
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H0G0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( context.WillRedirect( ) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void H0G0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         scmdbuf = "";
         P000G2_A9paisId = new int[1] ;
         P000G2_A15ciudadId = new int[1] ;
         P000G2_A8atraccionNombre = new String[] {""} ;
         P000G2_A40000atraccionFoto_GXI = new String[] {""} ;
         P000G2_A17ciudadNombre = new String[] {""} ;
         P000G2_A7atraccionId = new int[1] ;
         P000G2_A13atraccionFoto = new String[] {""} ;
         A8atraccionNombre = "";
         A40000atraccionFoto_GXI = "";
         A17ciudadNombre = "";
         A13atraccionFoto = "";
         sImgUrl = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aatraccionpornombre__default(),
            new Object[][] {
                new Object[] {
               P000G2_A9paisId, P000G2_A15ciudadId, P000G2_A8atraccionNombre, P000G2_A40000atraccionFoto_GXI, P000G2_A17ciudadNombre, P000G2_A7atraccionId, P000G2_A13atraccionFoto
               }
            }
         );
         /* GeneXus formulas. */
         Gx_line = 0;
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short GxWebError ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int Gx_OldLine ;
      private int A9paisId ;
      private int A15ciudadId ;
      private int A7atraccionId ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV9nombreDe ;
      private String AV10nombrePara ;
      private String scmdbuf ;
      private String A8atraccionNombre ;
      private String A17ciudadNombre ;
      private String sImgUrl ;
      private bool entryPointCalled ;
      private String A40000atraccionFoto_GXI ;
      private String A13atraccionFoto ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P000G2_A9paisId ;
      private int[] P000G2_A15ciudadId ;
      private String[] P000G2_A8atraccionNombre ;
      private String[] P000G2_A40000atraccionFoto_GXI ;
      private String[] P000G2_A17ciudadNombre ;
      private int[] P000G2_A7atraccionId ;
      private String[] P000G2_A13atraccionFoto ;
   }

   public class aatraccionpornombre__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP000G2 ;
          prmP000G2 = new Object[] {
          new Object[] {"@AV9nombreDe",SqlDbType.NChar,20,0} ,
          new Object[] {"@AV10nombrePara",SqlDbType.NChar,20,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P000G2", "SELECT T1.[paisId], T1.[ciudadId], T1.[atraccionNombre], T1.[atraccionFoto_GXI], T2.[ciudadNombre], T1.[atraccionId], T1.[atraccionFoto] FROM ([atraccion] T1 INNER JOIN [paisciudad] T2 ON T2.[paisId] = T1.[paisId] AND T2.[ciudadId] = T1.[ciudadId]) WHERE (T1.[atraccionNombre] >= @AV9nombreDe) AND (T1.[atraccionNombre] >= @AV10nombrePara) ORDER BY T1.[atraccionId] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000G2,100, GxCacheFrequency.OFF ,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((String[]) buf[3])[0] = rslt.getMultimediaUri(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 20) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((String[]) buf[6])[0] = rslt.getMultimediaFile(7, rslt.getVarchar(4)) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
       }
    }

 }

}
