/*
               File: type_Sdtatraccion
        Description: atraccion
             Author: GeneXus C# Generator version 16_0_6-136889
       Generated on: 12/3/2019 21:45:46.46
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "atraccion" )]
   [XmlType(TypeName =  "atraccion" , Namespace = "TravelAgency" )]
   [Serializable]
   public class Sdtatraccion : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public Sdtatraccion( )
      {
      }

      public Sdtatraccion( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV7atraccionId )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV7atraccionId});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"atraccionId", typeof(int)}}) ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "atraccion");
         metadata.Set("BT", "atraccion");
         metadata.Set("PK", "[ \"atraccionId\" ]");
         metadata.Set("PKAssigned", "[ \"atraccionId\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"categoriaId\" ],\"FKMap\":[  ] },{ \"FK\":[ \"paisId\",\"ciudadId\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override GeneXus.Utils.GxStringCollection StateAttributes( )
      {
         GeneXus.Utils.GxStringCollection state = new GeneXus.Utils.GxStringCollection() ;
         state.Add("gxTpr_Atraccionfoto_gxi");
         state.Add("gxTpr_Mode");
         state.Add("gxTpr_Initialized");
         state.Add("gxTpr_Atraccionid_Z");
         state.Add("gxTpr_Atraccionnombre_Z");
         state.Add("gxTpr_Paisid_Z");
         state.Add("gxTpr_Paisnombre_Z");
         state.Add("gxTpr_Categoriaid_Z");
         state.Add("gxTpr_Categorianombre_Z");
         state.Add("gxTpr_Ciudadid_Z");
         state.Add("gxTpr_Ciudadnombre_Z");
         state.Add("gxTpr_Atraccionfoto_gxi_Z");
         state.Add("gxTpr_Categoriaid_N");
         return state ;
      }

      public override void Copy( GxUserType source )
      {
         Sdtatraccion sdt ;
         sdt = (Sdtatraccion)(source);
         gxTv_Sdtatraccion_Atraccionid = sdt.gxTv_Sdtatraccion_Atraccionid ;
         gxTv_Sdtatraccion_Atraccionnombre = sdt.gxTv_Sdtatraccion_Atraccionnombre ;
         gxTv_Sdtatraccion_Paisid = sdt.gxTv_Sdtatraccion_Paisid ;
         gxTv_Sdtatraccion_Paisnombre = sdt.gxTv_Sdtatraccion_Paisnombre ;
         gxTv_Sdtatraccion_Categoriaid = sdt.gxTv_Sdtatraccion_Categoriaid ;
         gxTv_Sdtatraccion_Categorianombre = sdt.gxTv_Sdtatraccion_Categorianombre ;
         gxTv_Sdtatraccion_Atraccionfoto = sdt.gxTv_Sdtatraccion_Atraccionfoto ;
         gxTv_Sdtatraccion_Atraccionfoto_gxi = sdt.gxTv_Sdtatraccion_Atraccionfoto_gxi ;
         gxTv_Sdtatraccion_Ciudadid = sdt.gxTv_Sdtatraccion_Ciudadid ;
         gxTv_Sdtatraccion_Ciudadnombre = sdt.gxTv_Sdtatraccion_Ciudadnombre ;
         gxTv_Sdtatraccion_Mode = sdt.gxTv_Sdtatraccion_Mode ;
         gxTv_Sdtatraccion_Initialized = sdt.gxTv_Sdtatraccion_Initialized ;
         gxTv_Sdtatraccion_Atraccionid_Z = sdt.gxTv_Sdtatraccion_Atraccionid_Z ;
         gxTv_Sdtatraccion_Atraccionnombre_Z = sdt.gxTv_Sdtatraccion_Atraccionnombre_Z ;
         gxTv_Sdtatraccion_Paisid_Z = sdt.gxTv_Sdtatraccion_Paisid_Z ;
         gxTv_Sdtatraccion_Paisnombre_Z = sdt.gxTv_Sdtatraccion_Paisnombre_Z ;
         gxTv_Sdtatraccion_Categoriaid_Z = sdt.gxTv_Sdtatraccion_Categoriaid_Z ;
         gxTv_Sdtatraccion_Categorianombre_Z = sdt.gxTv_Sdtatraccion_Categorianombre_Z ;
         gxTv_Sdtatraccion_Ciudadid_Z = sdt.gxTv_Sdtatraccion_Ciudadid_Z ;
         gxTv_Sdtatraccion_Ciudadnombre_Z = sdt.gxTv_Sdtatraccion_Ciudadnombre_Z ;
         gxTv_Sdtatraccion_Atraccionfoto_gxi_Z = sdt.gxTv_Sdtatraccion_Atraccionfoto_gxi_Z ;
         gxTv_Sdtatraccion_Categoriaid_N = sdt.gxTv_Sdtatraccion_Categoriaid_N ;
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         ToJSON( includeState, true) ;
         return  ;
      }

      public override void ToJSON( bool includeState ,
                                   bool includeNonInitialized )
      {
         AddObjectProperty("atraccionId", gxTv_Sdtatraccion_Atraccionid, false, includeNonInitialized);
         AddObjectProperty("atraccionNombre", gxTv_Sdtatraccion_Atraccionnombre, false, includeNonInitialized);
         AddObjectProperty("paisId", gxTv_Sdtatraccion_Paisid, false, includeNonInitialized);
         AddObjectProperty("paisNombre", gxTv_Sdtatraccion_Paisnombre, false, includeNonInitialized);
         AddObjectProperty("categoriaId", gxTv_Sdtatraccion_Categoriaid, false, includeNonInitialized);
         AddObjectProperty("categoriaId_N", gxTv_Sdtatraccion_Categoriaid_N, false, includeNonInitialized);
         AddObjectProperty("categoriaNombre", gxTv_Sdtatraccion_Categorianombre, false, includeNonInitialized);
         AddObjectProperty("atraccionFoto", gxTv_Sdtatraccion_Atraccionfoto, false, includeNonInitialized);
         AddObjectProperty("ciudadId", gxTv_Sdtatraccion_Ciudadid, false, includeNonInitialized);
         AddObjectProperty("ciudadNombre", gxTv_Sdtatraccion_Ciudadnombre, false, includeNonInitialized);
         if ( includeState )
         {
            AddObjectProperty("atraccionFoto_GXI", gxTv_Sdtatraccion_Atraccionfoto_gxi, false, includeNonInitialized);
            AddObjectProperty("Mode", gxTv_Sdtatraccion_Mode, false, includeNonInitialized);
            AddObjectProperty("Initialized", gxTv_Sdtatraccion_Initialized, false, includeNonInitialized);
            AddObjectProperty("atraccionId_Z", gxTv_Sdtatraccion_Atraccionid_Z, false, includeNonInitialized);
            AddObjectProperty("atraccionNombre_Z", gxTv_Sdtatraccion_Atraccionnombre_Z, false, includeNonInitialized);
            AddObjectProperty("paisId_Z", gxTv_Sdtatraccion_Paisid_Z, false, includeNonInitialized);
            AddObjectProperty("paisNombre_Z", gxTv_Sdtatraccion_Paisnombre_Z, false, includeNonInitialized);
            AddObjectProperty("categoriaId_Z", gxTv_Sdtatraccion_Categoriaid_Z, false, includeNonInitialized);
            AddObjectProperty("categoriaNombre_Z", gxTv_Sdtatraccion_Categorianombre_Z, false, includeNonInitialized);
            AddObjectProperty("ciudadId_Z", gxTv_Sdtatraccion_Ciudadid_Z, false, includeNonInitialized);
            AddObjectProperty("ciudadNombre_Z", gxTv_Sdtatraccion_Ciudadnombre_Z, false, includeNonInitialized);
            AddObjectProperty("atraccionFoto_GXI_Z", gxTv_Sdtatraccion_Atraccionfoto_gxi_Z, false, includeNonInitialized);
            AddObjectProperty("categoriaId_N", gxTv_Sdtatraccion_Categoriaid_N, false, includeNonInitialized);
         }
         return  ;
      }

      public void UpdateDirties( Sdtatraccion sdt )
      {
         if ( sdt.IsDirty("atraccionId") )
         {
            gxTv_Sdtatraccion_Atraccionid = sdt.gxTv_Sdtatraccion_Atraccionid ;
         }
         if ( sdt.IsDirty("atraccionNombre") )
         {
            gxTv_Sdtatraccion_Atraccionnombre = sdt.gxTv_Sdtatraccion_Atraccionnombre ;
         }
         if ( sdt.IsDirty("paisId") )
         {
            gxTv_Sdtatraccion_Paisid = sdt.gxTv_Sdtatraccion_Paisid ;
         }
         if ( sdt.IsDirty("paisNombre") )
         {
            gxTv_Sdtatraccion_Paisnombre = sdt.gxTv_Sdtatraccion_Paisnombre ;
         }
         if ( sdt.IsDirty("categoriaId") )
         {
            gxTv_Sdtatraccion_Categoriaid = sdt.gxTv_Sdtatraccion_Categoriaid ;
         }
         if ( sdt.IsDirty("categoriaNombre") )
         {
            gxTv_Sdtatraccion_Categorianombre = sdt.gxTv_Sdtatraccion_Categorianombre ;
         }
         if ( sdt.IsDirty("atraccionFoto") )
         {
            gxTv_Sdtatraccion_Atraccionfoto = sdt.gxTv_Sdtatraccion_Atraccionfoto ;
         }
         if ( sdt.IsDirty("atraccionFoto") )
         {
            gxTv_Sdtatraccion_Atraccionfoto_gxi = sdt.gxTv_Sdtatraccion_Atraccionfoto_gxi ;
         }
         if ( sdt.IsDirty("ciudadId") )
         {
            gxTv_Sdtatraccion_Ciudadid = sdt.gxTv_Sdtatraccion_Ciudadid ;
         }
         if ( sdt.IsDirty("ciudadNombre") )
         {
            gxTv_Sdtatraccion_Ciudadnombre = sdt.gxTv_Sdtatraccion_Ciudadnombre ;
         }
         return  ;
      }

      [  SoapElement( ElementName = "atraccionId" )]
      [  XmlElement( ElementName = "atraccionId"   )]
      public int gxTpr_Atraccionid
      {
         get {
            return gxTv_Sdtatraccion_Atraccionid ;
         }

         set {
            if ( gxTv_Sdtatraccion_Atraccionid != value )
            {
               gxTv_Sdtatraccion_Mode = "INS";
               this.gxTv_Sdtatraccion_Atraccionid_Z_SetNull( );
               this.gxTv_Sdtatraccion_Atraccionnombre_Z_SetNull( );
               this.gxTv_Sdtatraccion_Paisid_Z_SetNull( );
               this.gxTv_Sdtatraccion_Paisnombre_Z_SetNull( );
               this.gxTv_Sdtatraccion_Categoriaid_Z_SetNull( );
               this.gxTv_Sdtatraccion_Categorianombre_Z_SetNull( );
               this.gxTv_Sdtatraccion_Ciudadid_Z_SetNull( );
               this.gxTv_Sdtatraccion_Ciudadnombre_Z_SetNull( );
               this.gxTv_Sdtatraccion_Atraccionfoto_gxi_Z_SetNull( );
            }
            gxTv_Sdtatraccion_Atraccionid = value;
            SetDirty("Atraccionid");
         }

      }

      [  SoapElement( ElementName = "atraccionNombre" )]
      [  XmlElement( ElementName = "atraccionNombre"   )]
      public String gxTpr_Atraccionnombre
      {
         get {
            return gxTv_Sdtatraccion_Atraccionnombre ;
         }

         set {
            gxTv_Sdtatraccion_Atraccionnombre = value;
            SetDirty("Atraccionnombre");
         }

      }

      [  SoapElement( ElementName = "paisId" )]
      [  XmlElement( ElementName = "paisId"   )]
      public int gxTpr_Paisid
      {
         get {
            return gxTv_Sdtatraccion_Paisid ;
         }

         set {
            gxTv_Sdtatraccion_Paisid = value;
            SetDirty("Paisid");
         }

      }

      [  SoapElement( ElementName = "paisNombre" )]
      [  XmlElement( ElementName = "paisNombre"   )]
      public String gxTpr_Paisnombre
      {
         get {
            return gxTv_Sdtatraccion_Paisnombre ;
         }

         set {
            gxTv_Sdtatraccion_Paisnombre = value;
            SetDirty("Paisnombre");
         }

      }

      [  SoapElement( ElementName = "categoriaId" )]
      [  XmlElement( ElementName = "categoriaId"   )]
      public int gxTpr_Categoriaid
      {
         get {
            return gxTv_Sdtatraccion_Categoriaid ;
         }

         set {
            gxTv_Sdtatraccion_Categoriaid_N = 0;
            gxTv_Sdtatraccion_Categoriaid = value;
            SetDirty("Categoriaid");
         }

      }

      public void gxTv_Sdtatraccion_Categoriaid_SetNull( )
      {
         gxTv_Sdtatraccion_Categoriaid_N = 1;
         gxTv_Sdtatraccion_Categoriaid = 0;
         return  ;
      }

      public bool gxTv_Sdtatraccion_Categoriaid_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "categoriaNombre" )]
      [  XmlElement( ElementName = "categoriaNombre"   )]
      public String gxTpr_Categorianombre
      {
         get {
            return gxTv_Sdtatraccion_Categorianombre ;
         }

         set {
            gxTv_Sdtatraccion_Categorianombre = value;
            SetDirty("Categorianombre");
         }

      }

      [  SoapElement( ElementName = "atraccionFoto" )]
      [  XmlElement( ElementName = "atraccionFoto"   )]
      [GxUpload()]
      public String gxTpr_Atraccionfoto
      {
         get {
            return gxTv_Sdtatraccion_Atraccionfoto ;
         }

         set {
            gxTv_Sdtatraccion_Atraccionfoto = value;
            SetDirty("Atraccionfoto");
         }

      }

      [  SoapElement( ElementName = "atraccionFoto_GXI" )]
      [  XmlElement( ElementName = "atraccionFoto_GXI"   )]
      public String gxTpr_Atraccionfoto_gxi
      {
         get {
            return gxTv_Sdtatraccion_Atraccionfoto_gxi ;
         }

         set {
            gxTv_Sdtatraccion_Atraccionfoto_gxi = value;
            SetDirty("Atraccionfoto_gxi");
         }

      }

      [  SoapElement( ElementName = "ciudadId" )]
      [  XmlElement( ElementName = "ciudadId"   )]
      public int gxTpr_Ciudadid
      {
         get {
            return gxTv_Sdtatraccion_Ciudadid ;
         }

         set {
            gxTv_Sdtatraccion_Ciudadid = value;
            SetDirty("Ciudadid");
         }

      }

      [  SoapElement( ElementName = "ciudadNombre" )]
      [  XmlElement( ElementName = "ciudadNombre"   )]
      public String gxTpr_Ciudadnombre
      {
         get {
            return gxTv_Sdtatraccion_Ciudadnombre ;
         }

         set {
            gxTv_Sdtatraccion_Ciudadnombre = value;
            SetDirty("Ciudadnombre");
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_Sdtatraccion_Mode ;
         }

         set {
            gxTv_Sdtatraccion_Mode = value;
            SetDirty("Mode");
         }

      }

      public void gxTv_Sdtatraccion_Mode_SetNull( )
      {
         gxTv_Sdtatraccion_Mode = "";
         return  ;
      }

      public bool gxTv_Sdtatraccion_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_Sdtatraccion_Initialized ;
         }

         set {
            gxTv_Sdtatraccion_Initialized = value;
            SetDirty("Initialized");
         }

      }

      public void gxTv_Sdtatraccion_Initialized_SetNull( )
      {
         gxTv_Sdtatraccion_Initialized = 0;
         return  ;
      }

      public bool gxTv_Sdtatraccion_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "atraccionId_Z" )]
      [  XmlElement( ElementName = "atraccionId_Z"   )]
      public int gxTpr_Atraccionid_Z
      {
         get {
            return gxTv_Sdtatraccion_Atraccionid_Z ;
         }

         set {
            gxTv_Sdtatraccion_Atraccionid_Z = value;
            SetDirty("Atraccionid_Z");
         }

      }

      public void gxTv_Sdtatraccion_Atraccionid_Z_SetNull( )
      {
         gxTv_Sdtatraccion_Atraccionid_Z = 0;
         return  ;
      }

      public bool gxTv_Sdtatraccion_Atraccionid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "atraccionNombre_Z" )]
      [  XmlElement( ElementName = "atraccionNombre_Z"   )]
      public String gxTpr_Atraccionnombre_Z
      {
         get {
            return gxTv_Sdtatraccion_Atraccionnombre_Z ;
         }

         set {
            gxTv_Sdtatraccion_Atraccionnombre_Z = value;
            SetDirty("Atraccionnombre_Z");
         }

      }

      public void gxTv_Sdtatraccion_Atraccionnombre_Z_SetNull( )
      {
         gxTv_Sdtatraccion_Atraccionnombre_Z = "";
         return  ;
      }

      public bool gxTv_Sdtatraccion_Atraccionnombre_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "paisId_Z" )]
      [  XmlElement( ElementName = "paisId_Z"   )]
      public int gxTpr_Paisid_Z
      {
         get {
            return gxTv_Sdtatraccion_Paisid_Z ;
         }

         set {
            gxTv_Sdtatraccion_Paisid_Z = value;
            SetDirty("Paisid_Z");
         }

      }

      public void gxTv_Sdtatraccion_Paisid_Z_SetNull( )
      {
         gxTv_Sdtatraccion_Paisid_Z = 0;
         return  ;
      }

      public bool gxTv_Sdtatraccion_Paisid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "paisNombre_Z" )]
      [  XmlElement( ElementName = "paisNombre_Z"   )]
      public String gxTpr_Paisnombre_Z
      {
         get {
            return gxTv_Sdtatraccion_Paisnombre_Z ;
         }

         set {
            gxTv_Sdtatraccion_Paisnombre_Z = value;
            SetDirty("Paisnombre_Z");
         }

      }

      public void gxTv_Sdtatraccion_Paisnombre_Z_SetNull( )
      {
         gxTv_Sdtatraccion_Paisnombre_Z = "";
         return  ;
      }

      public bool gxTv_Sdtatraccion_Paisnombre_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "categoriaId_Z" )]
      [  XmlElement( ElementName = "categoriaId_Z"   )]
      public int gxTpr_Categoriaid_Z
      {
         get {
            return gxTv_Sdtatraccion_Categoriaid_Z ;
         }

         set {
            gxTv_Sdtatraccion_Categoriaid_Z = value;
            SetDirty("Categoriaid_Z");
         }

      }

      public void gxTv_Sdtatraccion_Categoriaid_Z_SetNull( )
      {
         gxTv_Sdtatraccion_Categoriaid_Z = 0;
         return  ;
      }

      public bool gxTv_Sdtatraccion_Categoriaid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "categoriaNombre_Z" )]
      [  XmlElement( ElementName = "categoriaNombre_Z"   )]
      public String gxTpr_Categorianombre_Z
      {
         get {
            return gxTv_Sdtatraccion_Categorianombre_Z ;
         }

         set {
            gxTv_Sdtatraccion_Categorianombre_Z = value;
            SetDirty("Categorianombre_Z");
         }

      }

      public void gxTv_Sdtatraccion_Categorianombre_Z_SetNull( )
      {
         gxTv_Sdtatraccion_Categorianombre_Z = "";
         return  ;
      }

      public bool gxTv_Sdtatraccion_Categorianombre_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ciudadId_Z" )]
      [  XmlElement( ElementName = "ciudadId_Z"   )]
      public int gxTpr_Ciudadid_Z
      {
         get {
            return gxTv_Sdtatraccion_Ciudadid_Z ;
         }

         set {
            gxTv_Sdtatraccion_Ciudadid_Z = value;
            SetDirty("Ciudadid_Z");
         }

      }

      public void gxTv_Sdtatraccion_Ciudadid_Z_SetNull( )
      {
         gxTv_Sdtatraccion_Ciudadid_Z = 0;
         return  ;
      }

      public bool gxTv_Sdtatraccion_Ciudadid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ciudadNombre_Z" )]
      [  XmlElement( ElementName = "ciudadNombre_Z"   )]
      public String gxTpr_Ciudadnombre_Z
      {
         get {
            return gxTv_Sdtatraccion_Ciudadnombre_Z ;
         }

         set {
            gxTv_Sdtatraccion_Ciudadnombre_Z = value;
            SetDirty("Ciudadnombre_Z");
         }

      }

      public void gxTv_Sdtatraccion_Ciudadnombre_Z_SetNull( )
      {
         gxTv_Sdtatraccion_Ciudadnombre_Z = "";
         return  ;
      }

      public bool gxTv_Sdtatraccion_Ciudadnombre_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "atraccionFoto_GXI_Z" )]
      [  XmlElement( ElementName = "atraccionFoto_GXI_Z"   )]
      public String gxTpr_Atraccionfoto_gxi_Z
      {
         get {
            return gxTv_Sdtatraccion_Atraccionfoto_gxi_Z ;
         }

         set {
            gxTv_Sdtatraccion_Atraccionfoto_gxi_Z = value;
            SetDirty("Atraccionfoto_gxi_Z");
         }

      }

      public void gxTv_Sdtatraccion_Atraccionfoto_gxi_Z_SetNull( )
      {
         gxTv_Sdtatraccion_Atraccionfoto_gxi_Z = "";
         return  ;
      }

      public bool gxTv_Sdtatraccion_Atraccionfoto_gxi_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "categoriaId_N" )]
      [  XmlElement( ElementName = "categoriaId_N"   )]
      public short gxTpr_Categoriaid_N
      {
         get {
            return gxTv_Sdtatraccion_Categoriaid_N ;
         }

         set {
            gxTv_Sdtatraccion_Categoriaid_N = value;
            SetDirty("Categoriaid_N");
         }

      }

      public void gxTv_Sdtatraccion_Categoriaid_N_SetNull( )
      {
         gxTv_Sdtatraccion_Categoriaid_N = 0;
         return  ;
      }

      public bool gxTv_Sdtatraccion_Categoriaid_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_Sdtatraccion_Atraccionnombre = "";
         gxTv_Sdtatraccion_Paisnombre = "";
         gxTv_Sdtatraccion_Categorianombre = "";
         gxTv_Sdtatraccion_Atraccionfoto = "";
         gxTv_Sdtatraccion_Atraccionfoto_gxi = "";
         gxTv_Sdtatraccion_Ciudadnombre = "";
         gxTv_Sdtatraccion_Mode = "";
         gxTv_Sdtatraccion_Atraccionnombre_Z = "";
         gxTv_Sdtatraccion_Paisnombre_Z = "";
         gxTv_Sdtatraccion_Categorianombre_Z = "";
         gxTv_Sdtatraccion_Ciudadnombre_Z = "";
         gxTv_Sdtatraccion_Atraccionfoto_gxi_Z = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "atraccion", "GeneXus.Programs.atraccion_bc", new Object[] {context}, constructorCallingAssembly);;
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_Sdtatraccion_Initialized ;
      private short gxTv_Sdtatraccion_Categoriaid_N ;
      private int gxTv_Sdtatraccion_Atraccionid ;
      private int gxTv_Sdtatraccion_Paisid ;
      private int gxTv_Sdtatraccion_Categoriaid ;
      private int gxTv_Sdtatraccion_Ciudadid ;
      private int gxTv_Sdtatraccion_Atraccionid_Z ;
      private int gxTv_Sdtatraccion_Paisid_Z ;
      private int gxTv_Sdtatraccion_Categoriaid_Z ;
      private int gxTv_Sdtatraccion_Ciudadid_Z ;
      private String gxTv_Sdtatraccion_Atraccionnombre ;
      private String gxTv_Sdtatraccion_Paisnombre ;
      private String gxTv_Sdtatraccion_Categorianombre ;
      private String gxTv_Sdtatraccion_Ciudadnombre ;
      private String gxTv_Sdtatraccion_Mode ;
      private String gxTv_Sdtatraccion_Atraccionnombre_Z ;
      private String gxTv_Sdtatraccion_Paisnombre_Z ;
      private String gxTv_Sdtatraccion_Categorianombre_Z ;
      private String gxTv_Sdtatraccion_Ciudadnombre_Z ;
      private String gxTv_Sdtatraccion_Atraccionfoto_gxi ;
      private String gxTv_Sdtatraccion_Atraccionfoto_gxi_Z ;
      private String gxTv_Sdtatraccion_Atraccionfoto ;
   }

   [DataContract(Name = @"atraccion", Namespace = "TravelAgency")]
   public class Sdtatraccion_RESTInterface : GxGenericCollectionItem<Sdtatraccion>, System.Web.SessionState.IRequiresSessionState
   {
      public Sdtatraccion_RESTInterface( ) : base()
      {
      }

      public Sdtatraccion_RESTInterface( Sdtatraccion psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "atraccionId" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Atraccionid
      {
         get {
            return sdt.gxTpr_Atraccionid ;
         }

         set {
            sdt.gxTpr_Atraccionid = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "atraccionNombre" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Atraccionnombre
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Atraccionnombre) ;
         }

         set {
            sdt.gxTpr_Atraccionnombre = value;
         }

      }

      [DataMember( Name = "paisId" , Order = 2 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Paisid
      {
         get {
            return sdt.gxTpr_Paisid ;
         }

         set {
            sdt.gxTpr_Paisid = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "paisNombre" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Paisnombre
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Paisnombre) ;
         }

         set {
            sdt.gxTpr_Paisnombre = value;
         }

      }

      [DataMember( Name = "categoriaId" , Order = 4 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Categoriaid
      {
         get {
            return sdt.gxTpr_Categoriaid ;
         }

         set {
            sdt.gxTpr_Categoriaid = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "categoriaNombre" , Order = 5 )]
      [GxSeudo()]
      public String gxTpr_Categorianombre
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Categorianombre) ;
         }

         set {
            sdt.gxTpr_Categorianombre = value;
         }

      }

      [DataMember( Name = "atraccionFoto" , Order = 6 )]
      [GxUpload()]
      public String gxTpr_Atraccionfoto
      {
         get {
            return (!String.IsNullOrEmpty(StringUtil.RTrim( sdt.gxTpr_Atraccionfoto)) ? PathUtil.RelativeURL( sdt.gxTpr_Atraccionfoto) : StringUtil.RTrim( sdt.gxTpr_Atraccionfoto_gxi)) ;
         }

         set {
            sdt.gxTpr_Atraccionfoto = value;
         }

      }

      [DataMember( Name = "ciudadId" , Order = 7 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Ciudadid
      {
         get {
            return sdt.gxTpr_Ciudadid ;
         }

         set {
            sdt.gxTpr_Ciudadid = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ciudadNombre" , Order = 8 )]
      [GxSeudo()]
      public String gxTpr_Ciudadnombre
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Ciudadnombre) ;
         }

         set {
            sdt.gxTpr_Ciudadnombre = value;
         }

      }

      public Sdtatraccion sdt
      {
         get {
            return (Sdtatraccion)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new Sdtatraccion() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 9 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
