/*
               File: atraccionPorNombre
        Description: Stub for atraccionPorNombre
             Author: GeneXus C# Generator version 16_0_6-136889
       Generated on: 12/2/2019 23:24:8.76
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class atraccionpornombre : GXProcedure
   {
      public atraccionpornombre( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public atraccionpornombre( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_nombreDe ,
                           String aP1_nombrePara )
      {
         this.AV2nombreDe = aP0_nombreDe;
         this.AV3nombrePara = aP1_nombrePara;
         initialize();
         executePrivate();
      }

      public void executeSubmit( String aP0_nombreDe ,
                                 String aP1_nombrePara )
      {
         atraccionpornombre objatraccionpornombre;
         objatraccionpornombre = new atraccionpornombre();
         objatraccionpornombre.AV2nombreDe = aP0_nombreDe;
         objatraccionpornombre.AV3nombrePara = aP1_nombrePara;
         objatraccionpornombre.context.SetSubmitInitialConfig(context);
         objatraccionpornombre.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objatraccionpornombre);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((atraccionpornombre)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(String)AV2nombreDe,(String)AV3nombrePara} ;
         ClassLoader.Execute("aatraccionpornombre","GeneXus.Programs","aatraccionpornombre", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 2 ) )
         {
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String AV2nombreDe ;
      private String AV3nombrePara ;
      private IGxDataStore dsDefault ;
      private Object[] args ;
   }

}
