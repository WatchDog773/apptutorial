/*
               File: categoriasYAtracciones
        Description: categorias YAtracciones
             Author: GeneXus C# Generator version 16_0_6-136889
       Generated on: 12/3/2019 22:12:6.90
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class categoriasyatracciones : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public categoriasyatracciones( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public categoriasyatracciones( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdmasterpage", "GeneXus.Programs.rwdmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "no-store");
               }
               if ( ! context.WillRedirect( ) )
               {
                  AddString( context.getJSONResponse( )) ;
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA0W2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START0W2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 136889), false, true);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 136889), false, true);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 136889), false, true);
         context.AddJavascriptSource("gxcfg.js", "?20191232212695", false, true);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + "-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("categoriasyatracciones.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" title=\"submit\" style=\"display:none\" disabled>") ;
         AssignProp("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_ATRACCIONID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A7atraccionId), "ZZZZZ9"), context));
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCATEGORIA", AV5categoria);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCATEGORIA", AV5categoria);
         }
         GxWebStd.gx_hidden_field( context, "CIUDADNOMBRE", StringUtil.RTrim( A17ciudadNombre));
         GxWebStd.gx_hidden_field( context, "CATEGORIANOMBRE", StringUtil.RTrim( A12categoriaNombre));
         GxWebStd.gx_hidden_field( context, "ATRACCIONID", StringUtil.LTrim( StringUtil.NToC( (decimal)(A7atraccionId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_ATRACCIONID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A7atraccionId), "ZZZZZ9"), context));
         GxWebStd.gx_hidden_field( context, "CATEGORIAID", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40000categoriaId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CATEGORIAID", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40001categoriaId), 6, 0, ".", "")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendSecurityToken((String)(sPrefix));
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE0W2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT0W2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("categoriasyatracciones.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "categoriasYAtracciones" ;
      }

      public override String GetPgmdesc( )
      {
         return "categorias YAtracciones" ;
      }

      protected void WB0W0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Crear la nueva categoria \"Sitio Turistico\" y asignarlos a todas atraciones de Beijin con categoria \"Monumento\"", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_categoriasYAtracciones.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-6", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttHacer_Internalname, "", "Hacer", bttHacer_Jsonclick, 5, "Hacer", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'HACER\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_categoriasYAtracciones.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-6", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttDeshacer_Internalname, "", "Deshacer", bttDeshacer_Jsonclick, 5, "Deshacer", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DESHACER\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_categoriasYAtracciones.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
         }
         wbLoad = true;
      }

      protected void START0W2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 16_0_6-136889", 0) ;
            Form.Meta.addItem("description", "categorias YAtracciones", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP0W0( ) ;
      }

      protected void WS0W2( )
      {
         START0W2( ) ;
         EVT0W2( ) ;
      }

      protected void EVT0W2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'HACER'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: 'Hacer' */
                              E110W2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DESHACER'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: 'Deshacer' */
                              E120W2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: Load */
                              E130W2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE0W2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA0W2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            init_web_controls( ) ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void clear_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
            dynload_actions( ) ;
         }
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF0W2( ) ;
         if ( isFullAjaxMode( ) )
         {
            send_integrity_footer_hashes( ) ;
         }
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF0W2( )
      {
         initialize_formulas( ) ;
         clear_multi_value_controls( ) ;
         gxdyncontrolsrefreshing = true;
         fix_multi_value_controls( ) ;
         gxdyncontrolsrefreshing = false;
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: Load */
            E130W2 ();
            WB0W0( ) ;
         }
      }

      protected void send_integrity_lvl_hashes0W2( )
      {
         GxWebStd.gx_hidden_field( context, "ATRACCIONID", StringUtil.LTrim( StringUtil.NToC( (decimal)(A7atraccionId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_ATRACCIONID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A7atraccionId), "ZZZZZ9"), context));
      }

      protected void STRUP0W0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Using cursor H000W3 */
         pr_default.execute(0);
         if ( (pr_default.getStatus(0) != 101) )
         {
            A40000categoriaId = H000W3_A40000categoriaId[0];
            n40000categoriaId = H000W3_n40000categoriaId[0];
         }
         else
         {
            A40000categoriaId = 0;
            n40000categoriaId = false;
            AssignAttri("", false, "A40000categoriaId", StringUtil.LTrimStr( (decimal)(A40000categoriaId), 6, 0));
         }
         pr_default.close(0);
         /* Using cursor H000W5 */
         pr_default.execute(1);
         if ( (pr_default.getStatus(1) != 101) )
         {
            A40001categoriaId = H000W5_A40001categoriaId[0];
            n40001categoriaId = H000W5_n40001categoriaId[0];
         }
         else
         {
            A40001categoriaId = 0;
            n40001categoriaId = false;
            AssignAttri("", false, "A40001categoriaId", StringUtil.LTrimStr( (decimal)(A40001categoriaId), 6, 0));
         }
         pr_default.close(1);
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read saved values. */
            /* Read variables values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void E110W2( )
      {
         /* 'Hacer' Routine */
         AV5categoria.gxTpr_Categorianombre = "Sitio Turistico";
         AV5categoria.Save();
         if ( AV5categoria.Success() )
         {
            /* Using cursor H000W6 */
            pr_default.execute(2);
            while ( (pr_default.getStatus(2) != 101) )
            {
               A9paisId = H000W6_A9paisId[0];
               A15ciudadId = H000W6_A15ciudadId[0];
               A11categoriaId = H000W6_A11categoriaId[0];
               n11categoriaId = H000W6_n11categoriaId[0];
               A12categoriaNombre = H000W6_A12categoriaNombre[0];
               A17ciudadNombre = H000W6_A17ciudadNombre[0];
               A7atraccionId = H000W6_A7atraccionId[0];
               A17ciudadNombre = H000W6_A17ciudadNombre[0];
               A12categoriaNombre = H000W6_A12categoriaNombre[0];
               AV7atraccion.Load(A7atraccionId);
               AV7atraccion.gxTpr_Categoriaid = AV5categoria.gxTpr_Categoriaid;
               AV7atraccion.Save();
               pr_default.readNext(2);
            }
            pr_default.close(2);
            context.CommitDataStores("categoriasyatracciones",pr_default);
         }
         /*  Sending Event outputs  */
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5categoria", AV5categoria);
      }

      protected void E120W2( )
      {
         /* 'Deshacer' Routine */
         /* Using cursor H000W8 */
         pr_default.execute(3);
         if ( (pr_default.getStatus(3) != 101) )
         {
            A40000categoriaId = H000W8_A40000categoriaId[0];
            n40000categoriaId = H000W8_n40000categoriaId[0];
         }
         else
         {
            A40000categoriaId = 0;
            n40000categoriaId = false;
            AssignAttri("", false, "A40000categoriaId", StringUtil.LTrimStr( (decimal)(A40000categoriaId), 6, 0));
         }
         pr_default.close(3);
         AV6categoriaId = A40000categoriaId;
         /* Using cursor H000W9 */
         pr_default.execute(4);
         while ( (pr_default.getStatus(4) != 101) )
         {
            A9paisId = H000W9_A9paisId[0];
            A15ciudadId = H000W9_A15ciudadId[0];
            A11categoriaId = H000W9_A11categoriaId[0];
            n11categoriaId = H000W9_n11categoriaId[0];
            A12categoriaNombre = H000W9_A12categoriaNombre[0];
            A17ciudadNombre = H000W9_A17ciudadNombre[0];
            A7atraccionId = H000W9_A7atraccionId[0];
            A17ciudadNombre = H000W9_A17ciudadNombre[0];
            A12categoriaNombre = H000W9_A12categoriaNombre[0];
            AV7atraccion.Load(A7atraccionId);
            AV7atraccion.gxTpr_Categoriaid = AV6categoriaId;
            AV7atraccion.Save();
            pr_default.readNext(4);
         }
         pr_default.close(4);
         /* Using cursor H000W11 */
         pr_default.execute(5);
         if ( (pr_default.getStatus(5) != 101) )
         {
            A40001categoriaId = H000W11_A40001categoriaId[0];
            n40001categoriaId = H000W11_n40001categoriaId[0];
         }
         else
         {
            A40001categoriaId = 0;
            n40001categoriaId = false;
            AssignAttri("", false, "A40001categoriaId", StringUtil.LTrimStr( (decimal)(A40001categoriaId), 6, 0));
         }
         pr_default.close(5);
         AV6categoriaId = A40001categoriaId;
         AV5categoria.Load(AV6categoriaId);
         AV5categoria.Delete();
         if ( AV5categoria.Success() )
         {
            context.CommitDataStores("categoriasyatracciones",pr_default);
         }
         else
         {
            context.RollbackDataStores("categoriasyatracciones",pr_default);
         }
         /*  Sending Event outputs  */
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5categoria", AV5categoria);
      }

      protected void nextLoad( )
      {
      }

      protected void E130W2( )
      {
         /* Load Routine */
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA0W2( ) ;
         WS0W2( ) ;
         WE0W2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ));
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20191232212713", true, true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.eng.js", "?"+GetCacheInvalidationToken( ), false, true);
         context.AddJavascriptSource("categoriasyatracciones.js", "?20191232212713", false, true);
         /* End function include_jscripts */
      }

      protected void init_web_controls( )
      {
         /* End function init_web_controls */
      }

      protected void init_default_properties( )
      {
         lblTextblock1_Internalname = "TEXTBLOCK1";
         bttHacer_Internalname = "HACER";
         bttDeshacer_Internalname = "DESHACER";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         context.SetDefaultTheme("Carmine");
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "categorias YAtracciones";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'A7atraccionId',fld:'ATRACCIONID',pic:'ZZZZZ9',hsh:true}]");
         setEventMetadata("REFRESH",",oparms:[]}");
         setEventMetadata("'HACER'","{handler:'E110W2',iparms:[{av:'AV5categoria',fld:'vCATEGORIA',pic:''},{av:'A17ciudadNombre',fld:'CIUDADNOMBRE',pic:''},{av:'A12categoriaNombre',fld:'CATEGORIANOMBRE',pic:''},{av:'A7atraccionId',fld:'ATRACCIONID',pic:'ZZZZZ9',hsh:true}]");
         setEventMetadata("'HACER'",",oparms:[{av:'AV5categoria',fld:'vCATEGORIA',pic:''}]}");
         setEventMetadata("'DESHACER'","{handler:'E120W2',iparms:[{av:'A17ciudadNombre',fld:'CIUDADNOMBRE',pic:''},{av:'A12categoriaNombre',fld:'CATEGORIANOMBRE',pic:''},{av:'A7atraccionId',fld:'ATRACCIONID',pic:'ZZZZZ9',hsh:true}]");
         setEventMetadata("'DESHACER'",",oparms:[{av:'A40000categoriaId',fld:'CATEGORIAID',pic:'999999'},{av:'A40001categoriaId',fld:'CATEGORIAID',pic:'999999'},{av:'AV5categoria',fld:'vCATEGORIA',pic:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         AV5categoria = new Sdtcategoria(context);
         A17ciudadNombre = "";
         A12categoriaNombre = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         lblTextblock1_Jsonclick = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttHacer_Jsonclick = "";
         bttDeshacer_Jsonclick = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H000W3_A40000categoriaId = new int[1] ;
         H000W3_n40000categoriaId = new bool[] {false} ;
         H000W5_A40001categoriaId = new int[1] ;
         H000W5_n40001categoriaId = new bool[] {false} ;
         H000W6_A9paisId = new int[1] ;
         H000W6_A15ciudadId = new int[1] ;
         H000W6_A11categoriaId = new int[1] ;
         H000W6_n11categoriaId = new bool[] {false} ;
         H000W6_A12categoriaNombre = new String[] {""} ;
         H000W6_A17ciudadNombre = new String[] {""} ;
         H000W6_A7atraccionId = new int[1] ;
         AV7atraccion = new Sdtatraccion(context);
         H000W8_A40000categoriaId = new int[1] ;
         H000W8_n40000categoriaId = new bool[] {false} ;
         H000W9_A9paisId = new int[1] ;
         H000W9_A15ciudadId = new int[1] ;
         H000W9_A11categoriaId = new int[1] ;
         H000W9_n11categoriaId = new bool[] {false} ;
         H000W9_A12categoriaNombre = new String[] {""} ;
         H000W9_A17ciudadNombre = new String[] {""} ;
         H000W9_A7atraccionId = new int[1] ;
         H000W11_A40001categoriaId = new int[1] ;
         H000W11_n40001categoriaId = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.categoriasyatracciones__default(),
            new Object[][] {
                new Object[] {
               H000W3_A40000categoriaId, H000W3_n40000categoriaId
               }
               , new Object[] {
               H000W5_A40001categoriaId, H000W5_n40001categoriaId
               }
               , new Object[] {
               H000W6_A9paisId, H000W6_A15ciudadId, H000W6_A11categoriaId, H000W6_n11categoriaId, H000W6_A12categoriaNombre, H000W6_A17ciudadNombre, H000W6_A7atraccionId
               }
               , new Object[] {
               H000W8_A40000categoriaId, H000W8_n40000categoriaId
               }
               , new Object[] {
               H000W9_A9paisId, H000W9_A15ciudadId, H000W9_A11categoriaId, H000W9_n11categoriaId, H000W9_A12categoriaNombre, H000W9_A17ciudadNombre, H000W9_A7atraccionId
               }
               , new Object[] {
               H000W11_A40001categoriaId, H000W11_n40001categoriaId
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A7atraccionId ;
      private int A40000categoriaId ;
      private int A40001categoriaId ;
      private int A9paisId ;
      private int A15ciudadId ;
      private int A11categoriaId ;
      private int AV6categoriaId ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String A17ciudadNombre ;
      private String A12categoriaNombre ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String divMaintable_Internalname ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttHacer_Internalname ;
      private String bttHacer_Jsonclick ;
      private String bttDeshacer_Internalname ;
      private String bttDeshacer_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool gxdyncontrolsrefreshing ;
      private bool n40000categoriaId ;
      private bool n40001categoriaId ;
      private bool n11categoriaId ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H000W3_A40000categoriaId ;
      private bool[] H000W3_n40000categoriaId ;
      private int[] H000W5_A40001categoriaId ;
      private bool[] H000W5_n40001categoriaId ;
      private int[] H000W6_A9paisId ;
      private int[] H000W6_A15ciudadId ;
      private int[] H000W6_A11categoriaId ;
      private bool[] H000W6_n11categoriaId ;
      private String[] H000W6_A12categoriaNombre ;
      private String[] H000W6_A17ciudadNombre ;
      private int[] H000W6_A7atraccionId ;
      private int[] H000W8_A40000categoriaId ;
      private bool[] H000W8_n40000categoriaId ;
      private int[] H000W9_A9paisId ;
      private int[] H000W9_A15ciudadId ;
      private int[] H000W9_A11categoriaId ;
      private bool[] H000W9_n11categoriaId ;
      private String[] H000W9_A12categoriaNombre ;
      private String[] H000W9_A17ciudadNombre ;
      private int[] H000W9_A7atraccionId ;
      private int[] H000W11_A40001categoriaId ;
      private bool[] H000W11_n40001categoriaId ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
      private Sdtcategoria AV5categoria ;
      private Sdtatraccion AV7atraccion ;
   }

   public class categoriasyatracciones__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH000W3 ;
          prmH000W3 = new Object[] {
          } ;
          Object[] prmH000W5 ;
          prmH000W5 = new Object[] {
          } ;
          Object[] prmH000W6 ;
          prmH000W6 = new Object[] {
          } ;
          Object[] prmH000W8 ;
          prmH000W8 = new Object[] {
          } ;
          Object[] prmH000W9 ;
          prmH000W9 = new Object[] {
          } ;
          Object[] prmH000W11 ;
          prmH000W11 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("H000W3", "SELECT COALESCE( T1.[categoriaId], 0) AS categoriaId FROM (SELECT MIN([categoriaId]) AS categoriaId FROM [categoria] WHERE [categoriaNombre] = 'Monumento' ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000W3,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("H000W5", "SELECT COALESCE( T1.[categoriaId], 0) AS categoriaId FROM (SELECT MIN([categoriaId]) AS categoriaId FROM [categoria] WHERE [categoriaNombre] = 'Tourist site' ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000W5,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("H000W6", "SELECT T1.[paisId], T1.[ciudadId], T1.[categoriaId], T3.[categoriaNombre], T2.[ciudadNombre], T1.[atraccionId] FROM (([atraccion] T1 INNER JOIN [paisciudad] T2 ON T2.[paisId] = T1.[paisId] AND T2.[ciudadId] = T1.[ciudadId]) LEFT JOIN [categoria] T3 ON T3.[categoriaId] = T1.[categoriaId]) WHERE (T2.[ciudadNombre] = 'Beigin') AND (T3.[categoriaNombre] = 'Monumento') ORDER BY T1.[atraccionId] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000W6,100, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("H000W8", "SELECT COALESCE( T1.[categoriaId], 0) AS categoriaId FROM (SELECT MIN([categoriaId]) AS categoriaId FROM [categoria] WHERE [categoriaNombre] = 'Monumento' ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000W8,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("H000W9", "SELECT T1.[paisId], T1.[ciudadId], T1.[categoriaId], T3.[categoriaNombre], T2.[ciudadNombre], T1.[atraccionId] FROM (([atraccion] T1 INNER JOIN [paisciudad] T2 ON T2.[paisId] = T1.[paisId] AND T2.[ciudadId] = T1.[ciudadId]) LEFT JOIN [categoria] T3 ON T3.[categoriaId] = T1.[categoriaId]) WHERE (T2.[ciudadNombre] = 'Beijin') AND (T3.[categoriaNombre] = 'Sitio Turistico') ORDER BY T1.[atraccionId] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000W9,100, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("H000W11", "SELECT COALESCE( T1.[categoriaId], 0) AS categoriaId FROM (SELECT MIN([categoriaId]) AS categoriaId FROM [categoria] WHERE [categoriaNombre] = 'Tourist site' ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000W11,1, GxCacheFrequency.OFF ,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 20) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 20) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 20) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 20) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
