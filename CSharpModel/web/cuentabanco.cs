/*
               File: cuentaBanco
        Description: cuenta Banco
             Author: GeneXus C# Generator version 16_0_6-136889
       Generated on: 12/2/2019 0:46:12.12
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class cuentabanco : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_2") == 0 )
         {
            A1clienteId = (int)(NumberUtil.Val( GetNextPar( ), "."));
            AssignAttri("", false, "A1clienteId", StringUtil.LTrimStr( (decimal)(A1clienteId), 6, 0));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_2( A1clienteId) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_web_controls( ) ;
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 16_0_6-136889", 0) ;
            Form.Meta.addItem("description", "cuenta Banco", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtcuentaBancoId_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("Carmine");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public cuentabanco( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public cuentabanco( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdmasterpage", "GeneXus.Programs.rwdmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "no-store");
               }
               if ( ! context.WillRedirect( ) )
               {
                  AddString( context.getJSONResponse( )) ;
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            DrawControls( ) ;
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void DrawControls( )
      {
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "WWAdvancedContainer", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divTitlecontainer_Internalname, 1, 0, "px", 0, "px", "TableTop", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         /* Text block */
         GxWebStd.gx_label_ctrl( context, lblTitle_Internalname, "cuenta Banco", "", "", lblTitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_cuentaBanco.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         ClassString = "ErrorViewer";
         StyleString = "";
         GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divFormcontainer_Internalname, 1, 0, "px", 0, "px", "FormContainer", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divToolbarcell_Internalname, 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3 ToolbarCellClass", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group ActionGroup", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "btn-group", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
         ClassString = "BtnFirst";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_first_Internalname, "", "", bttBtn_first_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_first_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", TempTags, "", context.GetButtonType( ), "HLP_cuentaBanco.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
         ClassString = "BtnPrevious";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_previous_Internalname, "", "", bttBtn_previous_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_previous_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", TempTags, "", context.GetButtonType( ), "HLP_cuentaBanco.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
         ClassString = "BtnNext";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_next_Internalname, "", "", bttBtn_next_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_next_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", TempTags, "", context.GetButtonType( ), "HLP_cuentaBanco.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
         ClassString = "BtnLast";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_last_Internalname, "", "", bttBtn_last_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_last_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ELAST."+"'", TempTags, "", context.GetButtonType( ), "HLP_cuentaBanco.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
         ClassString = "BtnSelect";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_select_Internalname, "", "Select", bttBtn_select_Jsonclick, 4, "Select", "", StyleString, ClassString, bttBtn_select_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", TempTags, "gx.popup.openPrompt('"+"gx00e0.aspx"+"',["+"{Ctrl:gx.dom.el('"+"CUENTABANCOID"+"'), id:'"+"CUENTABANCOID"+"'"+",IOType:'out',isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");"+"return false;", 2, "HLP_cuentaBanco.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCellAdvanced", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtcuentaBancoId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtcuentaBancoId_Internalname, "Banco Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtcuentaBancoId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A56cuentaBancoId), 6, 0, ".", "")), ((edtcuentaBancoId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A56cuentaBancoId), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A56cuentaBancoId), "ZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtcuentaBancoId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtcuentaBancoId_Enabled, 0, "number", "1", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "", "HLP_cuentaBanco.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtcuentaBancoNumero_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtcuentaBancoNumero_Internalname, "Banco Numero", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtcuentaBancoNumero_Internalname, StringUtil.RTrim( A57cuentaBancoNumero), StringUtil.RTrim( context.localUtil.Format( A57cuentaBancoNumero, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtcuentaBancoNumero_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtcuentaBancoNumero_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "", "HLP_cuentaBanco.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtclienteId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtclienteId_Internalname, "cliente Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtclienteId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1clienteId), 6, 0, ".", "")), ((edtclienteId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1clienteId), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1clienteId), "ZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtclienteId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtclienteId_Enabled, 0, "number", "1", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "", "HLP_cuentaBanco.htm");
         /* Static images/pictures */
         ClassString = "gx-prompt Image";
         StyleString = "";
         sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
         GxWebStd.gx_bitmap( context, imgprompt_1_Internalname, sImgUrl, imgprompt_1_Link, "", "", context.GetTheme( ), imgprompt_1_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_cuentaBanco.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtclienteNombre_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtclienteNombre_Internalname, "cliente Nombre", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtclienteNombre_Internalname, StringUtil.RTrim( A2clienteNombre), StringUtil.RTrim( context.localUtil.Format( A2clienteNombre, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtclienteNombre_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtclienteNombre_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "Name", "left", true, "", "HLP_cuentaBanco.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtclienteApellido_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtclienteApellido_Internalname, "cliente Apellido", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtclienteApellido_Internalname, StringUtil.RTrim( A3clienteApellido), StringUtil.RTrim( context.localUtil.Format( A3clienteApellido, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtclienteApellido_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtclienteApellido_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "Name", "left", true, "", "HLP_cuentaBanco.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtbancoMontoCuenta_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtbancoMontoCuenta_Internalname, "Monto Cuenta", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtbancoMontoCuenta_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A53bancoMontoCuenta), 4, 0, ".", "")), ((edtbancoMontoCuenta_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A53bancoMontoCuenta), "ZZZ9")) : context.localUtil.Format( (decimal)(A53bancoMontoCuenta), "ZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtbancoMontoCuenta_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtbancoMontoCuenta_Enabled, 0, "number", "1", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "", "HLP_cuentaBanco.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
         ClassString = "BtnEnter";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirm", bttBtn_enter_Jsonclick, 5, "Confirm", "", StyleString, ClassString, bttBtn_enter_Visible, bttBtn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_cuentaBanco.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
         ClassString = "BtnCancel";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Cancel", bttBtn_cancel_Jsonclick, 1, "Cancel", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_cuentaBanco.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
         ClassString = "BtnDelete";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Delete", bttBtn_delete_Jsonclick, 5, "Delete", "", StyleString, ClassString, bttBtn_delete_Visible, bttBtn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_cuentaBanco.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "Center", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read saved values. */
            Z56cuentaBancoId = (int)(context.localUtil.CToN( cgiGet( "Z56cuentaBancoId"), ".", ","));
            Z57cuentaBancoNumero = cgiGet( "Z57cuentaBancoNumero");
            Z53bancoMontoCuenta = (short)(context.localUtil.CToN( cgiGet( "Z53bancoMontoCuenta"), ".", ","));
            Z1clienteId = (int)(context.localUtil.CToN( cgiGet( "Z1clienteId"), ".", ","));
            IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ".", ","));
            IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ".", ","));
            Gx_mode = cgiGet( "Mode");
            Gx_mode = cgiGet( "vMODE");
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtcuentaBancoId_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtcuentaBancoId_Internalname), ".", ",") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CUENTABANCOID");
               AnyError = 1;
               GX_FocusControl = edtcuentaBancoId_Internalname;
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A56cuentaBancoId = 0;
               AssignAttri("", false, "A56cuentaBancoId", StringUtil.LTrimStr( (decimal)(A56cuentaBancoId), 6, 0));
            }
            else
            {
               A56cuentaBancoId = (int)(context.localUtil.CToN( cgiGet( edtcuentaBancoId_Internalname), ".", ","));
               AssignAttri("", false, "A56cuentaBancoId", StringUtil.LTrimStr( (decimal)(A56cuentaBancoId), 6, 0));
            }
            A57cuentaBancoNumero = cgiGet( edtcuentaBancoNumero_Internalname);
            AssignAttri("", false, "A57cuentaBancoNumero", A57cuentaBancoNumero);
            if ( ( ( context.localUtil.CToN( cgiGet( edtclienteId_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtclienteId_Internalname), ".", ",") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CLIENTEID");
               AnyError = 1;
               GX_FocusControl = edtclienteId_Internalname;
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A1clienteId = 0;
               AssignAttri("", false, "A1clienteId", StringUtil.LTrimStr( (decimal)(A1clienteId), 6, 0));
            }
            else
            {
               A1clienteId = (int)(context.localUtil.CToN( cgiGet( edtclienteId_Internalname), ".", ","));
               AssignAttri("", false, "A1clienteId", StringUtil.LTrimStr( (decimal)(A1clienteId), 6, 0));
            }
            A2clienteNombre = cgiGet( edtclienteNombre_Internalname);
            AssignAttri("", false, "A2clienteNombre", A2clienteNombre);
            A3clienteApellido = cgiGet( edtclienteApellido_Internalname);
            AssignAttri("", false, "A3clienteApellido", A3clienteApellido);
            if ( ( ( context.localUtil.CToN( cgiGet( edtbancoMontoCuenta_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtbancoMontoCuenta_Internalname), ".", ",") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "BANCOMONTOCUENTA");
               AnyError = 1;
               GX_FocusControl = edtbancoMontoCuenta_Internalname;
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A53bancoMontoCuenta = 0;
               AssignAttri("", false, "A53bancoMontoCuenta", StringUtil.LTrimStr( (decimal)(A53bancoMontoCuenta), 4, 0));
            }
            else
            {
               A53bancoMontoCuenta = (short)(context.localUtil.CToN( cgiGet( edtbancoMontoCuenta_Internalname), ".", ","));
               AssignAttri("", false, "A53bancoMontoCuenta", StringUtil.LTrimStr( (decimal)(A53bancoMontoCuenta), 4, 0));
            }
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            standaloneNotModal( ) ;
         }
         else
         {
            standaloneNotModal( ) ;
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
            {
               Gx_mode = "DSP";
               AssignAttri("", false, "Gx_mode", Gx_mode);
               A56cuentaBancoId = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AssignAttri("", false, "A56cuentaBancoId", StringUtil.LTrimStr( (decimal)(A56cuentaBancoId), 6, 0));
               getEqualNoModal( ) ;
               Gx_mode = "DSP";
               AssignAttri("", false, "Gx_mode", Gx_mode);
               disable_std_buttons_dsp( ) ;
               standaloneModal( ) ;
            }
            else
            {
               Gx_mode = "INS";
               AssignAttri("", false, "Gx_mode", Gx_mode);
               standaloneModal( ) ;
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( IsIns( )  )
            {
               /* Clear variables for new insertion. */
               InitAll0914( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( IsIns( ) )
         {
            bttBtn_delete_Enabled = 0;
            AssignProp("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Enabled), 5, 0), true);
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         bttBtn_delete_Visible = 0;
         AssignProp("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Visible), 5, 0), true);
         bttBtn_first_Visible = 0;
         AssignProp("", false, bttBtn_first_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_first_Visible), 5, 0), true);
         bttBtn_previous_Visible = 0;
         AssignProp("", false, bttBtn_previous_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_previous_Visible), 5, 0), true);
         bttBtn_next_Visible = 0;
         AssignProp("", false, bttBtn_next_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_next_Visible), 5, 0), true);
         bttBtn_last_Visible = 0;
         AssignProp("", false, bttBtn_last_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_last_Visible), 5, 0), true);
         bttBtn_select_Visible = 0;
         AssignProp("", false, bttBtn_select_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_select_Visible), 5, 0), true);
         bttBtn_delete_Visible = 0;
         AssignProp("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Visible), 5, 0), true);
         if ( IsDsp( ) )
         {
            bttBtn_enter_Visible = 0;
            AssignProp("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_enter_Visible), 5, 0), true);
         }
         DisableAttributes0914( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( IsDlt( ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption090( )
      {
      }

      protected void ZM0914( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            if ( ! IsIns( ) )
            {
               Z57cuentaBancoNumero = T00093_A57cuentaBancoNumero[0];
               Z53bancoMontoCuenta = T00093_A53bancoMontoCuenta[0];
               Z1clienteId = T00093_A1clienteId[0];
            }
            else
            {
               Z57cuentaBancoNumero = A57cuentaBancoNumero;
               Z53bancoMontoCuenta = A53bancoMontoCuenta;
               Z1clienteId = A1clienteId;
            }
         }
         if ( GX_JID == -1 )
         {
            Z56cuentaBancoId = A56cuentaBancoId;
            Z57cuentaBancoNumero = A57cuentaBancoNumero;
            Z53bancoMontoCuenta = A53bancoMontoCuenta;
            Z1clienteId = A1clienteId;
            Z2clienteNombre = A2clienteNombre;
            Z3clienteApellido = A3clienteApellido;
         }
      }

      protected void standaloneNotModal( )
      {
         imgprompt_1_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx00d0.aspx"+"',["+"{Ctrl:gx.dom.el('"+"CLIENTEID"+"'), id:'"+"CLIENTEID"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            AssignProp("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Enabled), 5, 0), true);
         }
         else
         {
            bttBtn_delete_Enabled = 1;
            AssignProp("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Enabled), 5, 0), true);
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Enabled = 0;
            AssignProp("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_enter_Enabled), 5, 0), true);
         }
         else
         {
            bttBtn_enter_Enabled = 1;
            AssignProp("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_enter_Enabled), 5, 0), true);
         }
      }

      protected void Load0914( )
      {
         /* Using cursor T00095 */
         pr_default.execute(3, new Object[] {A56cuentaBancoId});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound14 = 1;
            A57cuentaBancoNumero = T00095_A57cuentaBancoNumero[0];
            AssignAttri("", false, "A57cuentaBancoNumero", A57cuentaBancoNumero);
            A2clienteNombre = T00095_A2clienteNombre[0];
            AssignAttri("", false, "A2clienteNombre", A2clienteNombre);
            A3clienteApellido = T00095_A3clienteApellido[0];
            AssignAttri("", false, "A3clienteApellido", A3clienteApellido);
            A53bancoMontoCuenta = T00095_A53bancoMontoCuenta[0];
            AssignAttri("", false, "A53bancoMontoCuenta", StringUtil.LTrimStr( (decimal)(A53bancoMontoCuenta), 4, 0));
            A1clienteId = T00095_A1clienteId[0];
            AssignAttri("", false, "A1clienteId", StringUtil.LTrimStr( (decimal)(A1clienteId), 6, 0));
            ZM0914( -1) ;
         }
         pr_default.close(3);
         OnLoadActions0914( ) ;
      }

      protected void OnLoadActions0914( )
      {
      }

      protected void CheckExtendedTable0914( )
      {
         nIsDirty_14 = 0;
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T00094 */
         pr_default.execute(2, new Object[] {A1clienteId});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("No matching 'cliente'.", "ForeignKeyNotFound", 1, "CLIENTEID");
            AnyError = 1;
            GX_FocusControl = edtclienteId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A2clienteNombre = T00094_A2clienteNombre[0];
         AssignAttri("", false, "A2clienteNombre", A2clienteNombre);
         A3clienteApellido = T00094_A3clienteApellido[0];
         AssignAttri("", false, "A3clienteApellido", A3clienteApellido);
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors0914( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_2( int A1clienteId )
      {
         /* Using cursor T00096 */
         pr_default.execute(4, new Object[] {A1clienteId});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("No matching 'cliente'.", "ForeignKeyNotFound", 1, "CLIENTEID");
            AnyError = 1;
            GX_FocusControl = edtclienteId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A2clienteNombre = T00096_A2clienteNombre[0];
         AssignAttri("", false, "A2clienteNombre", A2clienteNombre);
         A3clienteApellido = T00096_A3clienteApellido[0];
         AssignAttri("", false, "A3clienteApellido", A3clienteApellido);
         GxWebStd.set_html_headers( context, 0, "", "");
         AddString( "[[") ;
         AddString( "\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A2clienteNombre))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A3clienteApellido))+"\"") ;
         AddString( "]") ;
         if ( (pr_default.getStatus(4) == 101) )
         {
            AddString( ",") ;
            AddString( "101") ;
         }
         AddString( "]") ;
         pr_default.close(4);
      }

      protected void GetKey0914( )
      {
         /* Using cursor T00097 */
         pr_default.execute(5, new Object[] {A56cuentaBancoId});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound14 = 1;
         }
         else
         {
            RcdFound14 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00093 */
         pr_default.execute(1, new Object[] {A56cuentaBancoId});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0914( 1) ;
            RcdFound14 = 1;
            A56cuentaBancoId = T00093_A56cuentaBancoId[0];
            AssignAttri("", false, "A56cuentaBancoId", StringUtil.LTrimStr( (decimal)(A56cuentaBancoId), 6, 0));
            A57cuentaBancoNumero = T00093_A57cuentaBancoNumero[0];
            AssignAttri("", false, "A57cuentaBancoNumero", A57cuentaBancoNumero);
            A53bancoMontoCuenta = T00093_A53bancoMontoCuenta[0];
            AssignAttri("", false, "A53bancoMontoCuenta", StringUtil.LTrimStr( (decimal)(A53bancoMontoCuenta), 4, 0));
            A1clienteId = T00093_A1clienteId[0];
            AssignAttri("", false, "A1clienteId", StringUtil.LTrimStr( (decimal)(A1clienteId), 6, 0));
            Z56cuentaBancoId = A56cuentaBancoId;
            sMode14 = Gx_mode;
            Gx_mode = "DSP";
            AssignAttri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load0914( ) ;
            if ( AnyError == 1 )
            {
               RcdFound14 = 0;
               InitializeNonKey0914( ) ;
            }
            Gx_mode = sMode14;
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound14 = 0;
            InitializeNonKey0914( ) ;
            sMode14 = Gx_mode;
            Gx_mode = "DSP";
            AssignAttri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode14;
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0914( ) ;
         if ( RcdFound14 == 0 )
         {
            Gx_mode = "INS";
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound14 = 0;
         /* Using cursor T00098 */
         pr_default.execute(6, new Object[] {A56cuentaBancoId});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T00098_A56cuentaBancoId[0] < A56cuentaBancoId ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T00098_A56cuentaBancoId[0] > A56cuentaBancoId ) ) )
            {
               A56cuentaBancoId = T00098_A56cuentaBancoId[0];
               AssignAttri("", false, "A56cuentaBancoId", StringUtil.LTrimStr( (decimal)(A56cuentaBancoId), 6, 0));
               RcdFound14 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound14 = 0;
         /* Using cursor T00099 */
         pr_default.execute(7, new Object[] {A56cuentaBancoId});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T00099_A56cuentaBancoId[0] > A56cuentaBancoId ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T00099_A56cuentaBancoId[0] < A56cuentaBancoId ) ) )
            {
               A56cuentaBancoId = T00099_A56cuentaBancoId[0];
               AssignAttri("", false, "A56cuentaBancoId", StringUtil.LTrimStr( (decimal)(A56cuentaBancoId), 6, 0));
               RcdFound14 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0914( ) ;
         if ( IsIns( ) )
         {
            /* Insert record */
            GX_FocusControl = edtcuentaBancoId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0914( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound14 == 1 )
            {
               if ( A56cuentaBancoId != Z56cuentaBancoId )
               {
                  A56cuentaBancoId = Z56cuentaBancoId;
                  AssignAttri("", false, "A56cuentaBancoId", StringUtil.LTrimStr( (decimal)(A56cuentaBancoId), 6, 0));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CUENTABANCOID");
                  AnyError = 1;
                  GX_FocusControl = edtcuentaBancoId_Internalname;
                  AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( IsDlt( ) )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtcuentaBancoId_Internalname;
                  AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  AssignAttri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update0914( ) ;
                  GX_FocusControl = edtcuentaBancoId_Internalname;
                  AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A56cuentaBancoId != Z56cuentaBancoId )
               {
                  Gx_mode = "INS";
                  AssignAttri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtcuentaBancoId_Internalname;
                  AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0914( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CUENTABANCOID");
                     AnyError = 1;
                     GX_FocusControl = edtcuentaBancoId_Internalname;
                     AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     AssignAttri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtcuentaBancoId_Internalname;
                     AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0914( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A56cuentaBancoId != Z56cuentaBancoId )
         {
            A56cuentaBancoId = Z56cuentaBancoId;
            AssignAttri("", false, "A56cuentaBancoId", StringUtil.LTrimStr( (decimal)(A56cuentaBancoId), 6, 0));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CUENTABANCOID");
            AnyError = 1;
            GX_FocusControl = edtcuentaBancoId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtcuentaBancoId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         getEqualNoModal( ) ;
         if ( RcdFound14 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "CUENTABANCOID");
            AnyError = 1;
            GX_FocusControl = edtcuentaBancoId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtcuentaBancoNumero_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         ScanStart0914( ) ;
         if ( RcdFound14 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtcuentaBancoNumero_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd0914( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         move_previous( ) ;
         if ( RcdFound14 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtcuentaBancoNumero_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         move_next( ) ;
         if ( RcdFound14 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtcuentaBancoNumero_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         ScanStart0914( ) ;
         if ( RcdFound14 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound14 != 0 )
            {
               ScanNext0914( ) ;
            }
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtcuentaBancoNumero_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd0914( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency0914( )
      {
         if ( ! IsIns( ) )
         {
            /* Using cursor T00092 */
            pr_default.execute(0, new Object[] {A56cuentaBancoId});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"cuentaBanco"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z57cuentaBancoNumero, T00092_A57cuentaBancoNumero[0]) != 0 ) || ( Z53bancoMontoCuenta != T00092_A53bancoMontoCuenta[0] ) || ( Z1clienteId != T00092_A1clienteId[0] ) )
            {
               if ( StringUtil.StrCmp(Z57cuentaBancoNumero, T00092_A57cuentaBancoNumero[0]) != 0 )
               {
                  GXUtil.WriteLog("cuentabanco:[seudo value changed for attri]"+"cuentaBancoNumero");
                  GXUtil.WriteLogRaw("Old: ",Z57cuentaBancoNumero);
                  GXUtil.WriteLogRaw("Current: ",T00092_A57cuentaBancoNumero[0]);
               }
               if ( Z53bancoMontoCuenta != T00092_A53bancoMontoCuenta[0] )
               {
                  GXUtil.WriteLog("cuentabanco:[seudo value changed for attri]"+"bancoMontoCuenta");
                  GXUtil.WriteLogRaw("Old: ",Z53bancoMontoCuenta);
                  GXUtil.WriteLogRaw("Current: ",T00092_A53bancoMontoCuenta[0]);
               }
               if ( Z1clienteId != T00092_A1clienteId[0] )
               {
                  GXUtil.WriteLog("cuentabanco:[seudo value changed for attri]"+"clienteId");
                  GXUtil.WriteLogRaw("Old: ",Z1clienteId);
                  GXUtil.WriteLogRaw("Current: ",T00092_A1clienteId[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"cuentaBanco"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0914( )
      {
         BeforeValidate0914( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0914( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0914( 0) ;
            CheckOptimisticConcurrency0914( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0914( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0914( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000910 */
                     pr_default.execute(8, new Object[] {A57cuentaBancoNumero, A53bancoMontoCuenta, A1clienteId});
                     A56cuentaBancoId = T000910_A56cuentaBancoId[0];
                     AssignAttri("", false, "A56cuentaBancoId", StringUtil.LTrimStr( (decimal)(A56cuentaBancoId), 6, 0));
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("cuentaBanco") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                           ResetCaption090( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0914( ) ;
            }
            EndLevel0914( ) ;
         }
         CloseExtendedTableCursors0914( ) ;
      }

      protected void Update0914( )
      {
         BeforeValidate0914( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0914( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0914( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0914( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0914( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000911 */
                     pr_default.execute(9, new Object[] {A57cuentaBancoNumero, A53bancoMontoCuenta, A1clienteId, A56cuentaBancoId});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("cuentaBanco") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"cuentaBanco"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0914( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), "SuccessfullyUpdated", 0, "", true);
                           ResetCaption090( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0914( ) ;
         }
         CloseExtendedTableCursors0914( ) ;
      }

      protected void DeferredUpdate0914( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         AssignAttri("", false, "Gx_mode", Gx_mode);
         BeforeValidate0914( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0914( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0914( ) ;
            AfterConfirm0914( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0914( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000912 */
                  pr_default.execute(10, new Object[] {A56cuentaBancoId});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("cuentaBanco") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound14 == 0 )
                        {
                           InitAll0914( ) ;
                           Gx_mode = "INS";
                           AssignAttri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           AssignAttri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), "SuccessfullyDeleted", 0, "", true);
                        ResetCaption090( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode14 = Gx_mode;
         Gx_mode = "DLT";
         AssignAttri("", false, "Gx_mode", Gx_mode);
         EndLevel0914( ) ;
         Gx_mode = sMode14;
         AssignAttri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls0914( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T000913 */
            pr_default.execute(11, new Object[] {A1clienteId});
            A2clienteNombre = T000913_A2clienteNombre[0];
            AssignAttri("", false, "A2clienteNombre", A2clienteNombre);
            A3clienteApellido = T000913_A3clienteApellido[0];
            AssignAttri("", false, "A3clienteApellido", A3clienteApellido);
            pr_default.close(11);
         }
      }

      protected void EndLevel0914( )
      {
         if ( ! IsIns( ) )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0914( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(11);
            context.CommitDataStores("cuentabanco",pr_default);
            if ( AnyError == 0 )
            {
               ConfirmValues090( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(11);
            context.RollbackDataStores("cuentabanco",pr_default);
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0914( )
      {
         /* Using cursor T000914 */
         pr_default.execute(12);
         RcdFound14 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound14 = 1;
            A56cuentaBancoId = T000914_A56cuentaBancoId[0];
            AssignAttri("", false, "A56cuentaBancoId", StringUtil.LTrimStr( (decimal)(A56cuentaBancoId), 6, 0));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0914( )
      {
         /* Scan next routine */
         pr_default.readNext(12);
         RcdFound14 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound14 = 1;
            A56cuentaBancoId = T000914_A56cuentaBancoId[0];
            AssignAttri("", false, "A56cuentaBancoId", StringUtil.LTrimStr( (decimal)(A56cuentaBancoId), 6, 0));
         }
      }

      protected void ScanEnd0914( )
      {
         pr_default.close(12);
      }

      protected void AfterConfirm0914( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0914( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0914( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0914( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0914( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0914( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0914( )
      {
         edtcuentaBancoId_Enabled = 0;
         AssignProp("", false, edtcuentaBancoId_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtcuentaBancoId_Enabled), 5, 0), true);
         edtcuentaBancoNumero_Enabled = 0;
         AssignProp("", false, edtcuentaBancoNumero_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtcuentaBancoNumero_Enabled), 5, 0), true);
         edtclienteId_Enabled = 0;
         AssignProp("", false, edtclienteId_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtclienteId_Enabled), 5, 0), true);
         edtclienteNombre_Enabled = 0;
         AssignProp("", false, edtclienteNombre_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtclienteNombre_Enabled), 5, 0), true);
         edtclienteApellido_Enabled = 0;
         AssignProp("", false, edtclienteApellido_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtclienteApellido_Enabled), 5, 0), true);
         edtbancoMontoCuenta_Enabled = 0;
         AssignProp("", false, edtbancoMontoCuenta_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtbancoMontoCuenta_Enabled), 5, 0), true);
      }

      protected void send_integrity_lvl_hashes0914( )
      {
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues090( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 136889), false, true);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 136889), false, true);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 136889), false, true);
         context.AddJavascriptSource("gxcfg.js", "?20191220461284", false, true);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + "-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("cuentabanco.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" title=\"submit\" style=\"display:none\" disabled>") ;
         AssignProp("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "Z56cuentaBancoId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z56cuentaBancoId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z57cuentaBancoNumero", StringUtil.RTrim( Z57cuentaBancoNumero));
         GxWebStd.gx_hidden_field( context, "Z53bancoMontoCuenta", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z53bancoMontoCuenta), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z1clienteId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1clienteId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendSecurityToken(sPrefix);
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("cuentabanco.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "cuentaBanco" ;
      }

      public override String GetPgmdesc( )
      {
         return "cuenta Banco" ;
      }

      protected void InitializeNonKey0914( )
      {
         A57cuentaBancoNumero = "";
         AssignAttri("", false, "A57cuentaBancoNumero", A57cuentaBancoNumero);
         A1clienteId = 0;
         AssignAttri("", false, "A1clienteId", StringUtil.LTrimStr( (decimal)(A1clienteId), 6, 0));
         A2clienteNombre = "";
         AssignAttri("", false, "A2clienteNombre", A2clienteNombre);
         A3clienteApellido = "";
         AssignAttri("", false, "A3clienteApellido", A3clienteApellido);
         A53bancoMontoCuenta = 0;
         AssignAttri("", false, "A53bancoMontoCuenta", StringUtil.LTrimStr( (decimal)(A53bancoMontoCuenta), 4, 0));
         Z57cuentaBancoNumero = "";
         Z53bancoMontoCuenta = 0;
         Z1clienteId = 0;
      }

      protected void InitAll0914( )
      {
         A56cuentaBancoId = 0;
         AssignAttri("", false, "A56cuentaBancoId", StringUtil.LTrimStr( (decimal)(A56cuentaBancoId), 6, 0));
         InitializeNonKey0914( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ));
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20191220461289", true, true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.eng.js", "?"+GetCacheInvalidationToken( ), false, true);
         context.AddJavascriptSource("cuentabanco.js", "?20191220461289", false, true);
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTitle_Internalname = "TITLE";
         divTitlecontainer_Internalname = "TITLECONTAINER";
         bttBtn_first_Internalname = "BTN_FIRST";
         bttBtn_previous_Internalname = "BTN_PREVIOUS";
         bttBtn_next_Internalname = "BTN_NEXT";
         bttBtn_last_Internalname = "BTN_LAST";
         bttBtn_select_Internalname = "BTN_SELECT";
         divToolbarcell_Internalname = "TOOLBARCELL";
         edtcuentaBancoId_Internalname = "CUENTABANCOID";
         edtcuentaBancoNumero_Internalname = "CUENTABANCONUMERO";
         edtclienteId_Internalname = "CLIENTEID";
         edtclienteNombre_Internalname = "CLIENTENOMBRE";
         edtclienteApellido_Internalname = "CLIENTEAPELLIDO";
         edtbancoMontoCuenta_Internalname = "BANCOMONTOCUENTA";
         divFormcontainer_Internalname = "FORMCONTAINER";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
         imgprompt_1_Internalname = "PROMPT_1";
      }

      public override void initialize_properties( )
      {
         context.SetDefaultTheme("Carmine");
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "cuenta Banco";
         bttBtn_delete_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Enabled = 1;
         bttBtn_enter_Visible = 1;
         edtbancoMontoCuenta_Jsonclick = "";
         edtbancoMontoCuenta_Enabled = 1;
         edtclienteApellido_Jsonclick = "";
         edtclienteApellido_Enabled = 0;
         edtclienteNombre_Jsonclick = "";
         edtclienteNombre_Enabled = 0;
         imgprompt_1_Visible = 1;
         imgprompt_1_Link = "";
         edtclienteId_Jsonclick = "";
         edtclienteId_Enabled = 1;
         edtcuentaBancoNumero_Jsonclick = "";
         edtcuentaBancoNumero_Enabled = 1;
         edtcuentaBancoId_Jsonclick = "";
         edtcuentaBancoId_Enabled = 1;
         bttBtn_select_Visible = 1;
         bttBtn_last_Visible = 1;
         bttBtn_next_Visible = 1;
         bttBtn_previous_Visible = 1;
         bttBtn_first_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void init_web_controls( )
      {
         /* End function init_web_controls */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         getEqualNoModal( ) ;
         GX_FocusControl = edtcuentaBancoNumero_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      protected bool IsIns( )
      {
         return ((StringUtil.StrCmp(Gx_mode, "INS")==0) ? true : false) ;
      }

      protected bool IsDlt( )
      {
         return ((StringUtil.StrCmp(Gx_mode, "DLT")==0) ? true : false) ;
      }

      protected bool IsUpd( )
      {
         return ((StringUtil.StrCmp(Gx_mode, "UPD")==0) ? true : false) ;
      }

      protected bool IsDsp( )
      {
         return ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? true : false) ;
      }

      public void Valid_Cuentabancoid( )
      {
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         send_integrity_footer_hashes( ) ;
         dynload_actions( ) ;
         /*  Sending validation outputs */
         AssignAttri("", false, "A57cuentaBancoNumero", StringUtil.RTrim( A57cuentaBancoNumero));
         AssignAttri("", false, "A1clienteId", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1clienteId), 6, 0, ".", "")));
         AssignAttri("", false, "A53bancoMontoCuenta", StringUtil.LTrim( StringUtil.NToC( (decimal)(A53bancoMontoCuenta), 4, 0, ".", "")));
         AssignAttri("", false, "A2clienteNombre", StringUtil.RTrim( A2clienteNombre));
         AssignAttri("", false, "A3clienteApellido", StringUtil.RTrim( A3clienteApellido));
         AssignAttri("", false, "Gx_mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "Z56cuentaBancoId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z56cuentaBancoId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z57cuentaBancoNumero", StringUtil.RTrim( Z57cuentaBancoNumero));
         GxWebStd.gx_hidden_field( context, "Z1clienteId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1clienteId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z53bancoMontoCuenta", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z53bancoMontoCuenta), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z2clienteNombre", StringUtil.RTrim( Z2clienteNombre));
         GxWebStd.gx_hidden_field( context, "Z3clienteApellido", StringUtil.RTrim( Z3clienteApellido));
         AssignProp("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Enabled), 5, 0), true);
         AssignProp("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_enter_Enabled), 5, 0), true);
         SendCloseFormHiddens( ) ;
      }

      public void Valid_Clienteid( )
      {
         /* Using cursor T000913 */
         pr_default.execute(11, new Object[] {A1clienteId});
         if ( (pr_default.getStatus(11) == 101) )
         {
            GX_msglist.addItem("No matching 'cliente'.", "ForeignKeyNotFound", 1, "CLIENTEID");
            AnyError = 1;
            GX_FocusControl = edtclienteId_Internalname;
         }
         A2clienteNombre = T000913_A2clienteNombre[0];
         A3clienteApellido = T000913_A3clienteApellido[0];
         pr_default.close(11);
         dynload_actions( ) ;
         /*  Sending validation outputs */
         AssignAttri("", false, "A2clienteNombre", StringUtil.RTrim( A2clienteNombre));
         AssignAttri("", false, "A3clienteApellido", StringUtil.RTrim( A3clienteApellido));
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}]");
         setEventMetadata("ENTER",",oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[]");
         setEventMetadata("REFRESH",",oparms:[]}");
         setEventMetadata("VALID_CUENTABANCOID","{handler:'Valid_Cuentabancoid',iparms:[{av:'A56cuentaBancoId',fld:'CUENTABANCOID',pic:'ZZZZZ9'},{av:'Gx_mode',fld:'vMODE',pic:'@!'}]");
         setEventMetadata("VALID_CUENTABANCOID",",oparms:[{av:'A57cuentaBancoNumero',fld:'CUENTABANCONUMERO',pic:''},{av:'A1clienteId',fld:'CLIENTEID',pic:'ZZZZZ9'},{av:'A53bancoMontoCuenta',fld:'BANCOMONTOCUENTA',pic:'ZZZ9'},{av:'A2clienteNombre',fld:'CLIENTENOMBRE',pic:''},{av:'A3clienteApellido',fld:'CLIENTEAPELLIDO',pic:''},{av:'Gx_mode',fld:'vMODE',pic:'@!'},{av:'Z56cuentaBancoId'},{av:'Z57cuentaBancoNumero'},{av:'Z1clienteId'},{av:'Z53bancoMontoCuenta'},{av:'Z2clienteNombre'},{av:'Z3clienteApellido'},{ctrl:'BTN_DELETE',prop:'Enabled'},{ctrl:'BTN_ENTER',prop:'Enabled'}]}");
         setEventMetadata("VALID_CLIENTEID","{handler:'Valid_Clienteid',iparms:[{av:'A1clienteId',fld:'CLIENTEID',pic:'ZZZZZ9'},{av:'A2clienteNombre',fld:'CLIENTENOMBRE',pic:''},{av:'A3clienteApellido',fld:'CLIENTEAPELLIDO',pic:''}]");
         setEventMetadata("VALID_CLIENTEID",",oparms:[{av:'A2clienteNombre',fld:'CLIENTENOMBRE',pic:''},{av:'A3clienteApellido',fld:'CLIENTEAPELLIDO',pic:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(11);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z57cuentaBancoNumero = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         lblTitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_first_Jsonclick = "";
         bttBtn_previous_Jsonclick = "";
         bttBtn_next_Jsonclick = "";
         bttBtn_last_Jsonclick = "";
         bttBtn_select_Jsonclick = "";
         A57cuentaBancoNumero = "";
         sImgUrl = "";
         A2clienteNombre = "";
         A3clienteApellido = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z2clienteNombre = "";
         Z3clienteApellido = "";
         T00095_A56cuentaBancoId = new int[1] ;
         T00095_A57cuentaBancoNumero = new String[] {""} ;
         T00095_A2clienteNombre = new String[] {""} ;
         T00095_A3clienteApellido = new String[] {""} ;
         T00095_A53bancoMontoCuenta = new short[1] ;
         T00095_A1clienteId = new int[1] ;
         T00094_A2clienteNombre = new String[] {""} ;
         T00094_A3clienteApellido = new String[] {""} ;
         T00096_A2clienteNombre = new String[] {""} ;
         T00096_A3clienteApellido = new String[] {""} ;
         T00097_A56cuentaBancoId = new int[1] ;
         T00093_A56cuentaBancoId = new int[1] ;
         T00093_A57cuentaBancoNumero = new String[] {""} ;
         T00093_A53bancoMontoCuenta = new short[1] ;
         T00093_A1clienteId = new int[1] ;
         sMode14 = "";
         T00098_A56cuentaBancoId = new int[1] ;
         T00099_A56cuentaBancoId = new int[1] ;
         T00092_A56cuentaBancoId = new int[1] ;
         T00092_A57cuentaBancoNumero = new String[] {""} ;
         T00092_A53bancoMontoCuenta = new short[1] ;
         T00092_A1clienteId = new int[1] ;
         T000910_A56cuentaBancoId = new int[1] ;
         T000913_A2clienteNombre = new String[] {""} ;
         T000913_A3clienteApellido = new String[] {""} ;
         T000914_A56cuentaBancoId = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         ZZ57cuentaBancoNumero = "";
         ZZ2clienteNombre = "";
         ZZ3clienteApellido = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.cuentabanco__default(),
            new Object[][] {
                new Object[] {
               T00092_A56cuentaBancoId, T00092_A57cuentaBancoNumero, T00092_A53bancoMontoCuenta, T00092_A1clienteId
               }
               , new Object[] {
               T00093_A56cuentaBancoId, T00093_A57cuentaBancoNumero, T00093_A53bancoMontoCuenta, T00093_A1clienteId
               }
               , new Object[] {
               T00094_A2clienteNombre, T00094_A3clienteApellido
               }
               , new Object[] {
               T00095_A56cuentaBancoId, T00095_A57cuentaBancoNumero, T00095_A2clienteNombre, T00095_A3clienteApellido, T00095_A53bancoMontoCuenta, T00095_A1clienteId
               }
               , new Object[] {
               T00096_A2clienteNombre, T00096_A3clienteApellido
               }
               , new Object[] {
               T00097_A56cuentaBancoId
               }
               , new Object[] {
               T00098_A56cuentaBancoId
               }
               , new Object[] {
               T00099_A56cuentaBancoId
               }
               , new Object[] {
               T000910_A56cuentaBancoId
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000913_A2clienteNombre, T000913_A3clienteApellido
               }
               , new Object[] {
               T000914_A56cuentaBancoId
               }
            }
         );
      }

      private short Z53bancoMontoCuenta ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A53bancoMontoCuenta ;
      private short GX_JID ;
      private short RcdFound14 ;
      private short nIsDirty_14 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short ZZ53bancoMontoCuenta ;
      private int Z56cuentaBancoId ;
      private int Z1clienteId ;
      private int A1clienteId ;
      private int trnEnded ;
      private int bttBtn_first_Visible ;
      private int bttBtn_previous_Visible ;
      private int bttBtn_next_Visible ;
      private int bttBtn_last_Visible ;
      private int bttBtn_select_Visible ;
      private int A56cuentaBancoId ;
      private int edtcuentaBancoId_Enabled ;
      private int edtcuentaBancoNumero_Enabled ;
      private int edtclienteId_Enabled ;
      private int imgprompt_1_Visible ;
      private int edtclienteNombre_Enabled ;
      private int edtclienteApellido_Enabled ;
      private int edtbancoMontoCuenta_Enabled ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_enter_Enabled ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int bttBtn_delete_Enabled ;
      private int idxLst ;
      private int ZZ56cuentaBancoId ;
      private int ZZ1clienteId ;
      private String sPrefix ;
      private String Z57cuentaBancoNumero ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtcuentaBancoId_Internalname ;
      private String divMaintable_Internalname ;
      private String divTitlecontainer_Internalname ;
      private String lblTitle_Internalname ;
      private String lblTitle_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String divFormcontainer_Internalname ;
      private String divToolbarcell_Internalname ;
      private String TempTags ;
      private String bttBtn_first_Internalname ;
      private String bttBtn_first_Jsonclick ;
      private String bttBtn_previous_Internalname ;
      private String bttBtn_previous_Jsonclick ;
      private String bttBtn_next_Internalname ;
      private String bttBtn_next_Jsonclick ;
      private String bttBtn_last_Internalname ;
      private String bttBtn_last_Jsonclick ;
      private String bttBtn_select_Internalname ;
      private String bttBtn_select_Jsonclick ;
      private String edtcuentaBancoId_Jsonclick ;
      private String edtcuentaBancoNumero_Internalname ;
      private String A57cuentaBancoNumero ;
      private String edtcuentaBancoNumero_Jsonclick ;
      private String edtclienteId_Internalname ;
      private String edtclienteId_Jsonclick ;
      private String sImgUrl ;
      private String imgprompt_1_Internalname ;
      private String imgprompt_1_Link ;
      private String edtclienteNombre_Internalname ;
      private String A2clienteNombre ;
      private String edtclienteNombre_Jsonclick ;
      private String edtclienteApellido_Internalname ;
      private String A3clienteApellido ;
      private String edtclienteApellido_Jsonclick ;
      private String edtbancoMontoCuenta_Internalname ;
      private String edtbancoMontoCuenta_Jsonclick ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z2clienteNombre ;
      private String Z3clienteApellido ;
      private String sMode14 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String ZZ57cuentaBancoNumero ;
      private String ZZ2clienteNombre ;
      private String ZZ3clienteApellido ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T00095_A56cuentaBancoId ;
      private String[] T00095_A57cuentaBancoNumero ;
      private String[] T00095_A2clienteNombre ;
      private String[] T00095_A3clienteApellido ;
      private short[] T00095_A53bancoMontoCuenta ;
      private int[] T00095_A1clienteId ;
      private String[] T00094_A2clienteNombre ;
      private String[] T00094_A3clienteApellido ;
      private String[] T00096_A2clienteNombre ;
      private String[] T00096_A3clienteApellido ;
      private int[] T00097_A56cuentaBancoId ;
      private int[] T00093_A56cuentaBancoId ;
      private String[] T00093_A57cuentaBancoNumero ;
      private short[] T00093_A53bancoMontoCuenta ;
      private int[] T00093_A1clienteId ;
      private int[] T00098_A56cuentaBancoId ;
      private int[] T00099_A56cuentaBancoId ;
      private int[] T00092_A56cuentaBancoId ;
      private String[] T00092_A57cuentaBancoNumero ;
      private short[] T00092_A53bancoMontoCuenta ;
      private int[] T00092_A1clienteId ;
      private int[] T000910_A56cuentaBancoId ;
      private String[] T000913_A2clienteNombre ;
      private String[] T000913_A3clienteApellido ;
      private int[] T000914_A56cuentaBancoId ;
      private GXWebForm Form ;
   }

   public class cuentabanco__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00095 ;
          prmT00095 = new Object[] {
          new Object[] {"@cuentaBancoId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00094 ;
          prmT00094 = new Object[] {
          new Object[] {"@clienteId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00096 ;
          prmT00096 = new Object[] {
          new Object[] {"@clienteId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00097 ;
          prmT00097 = new Object[] {
          new Object[] {"@cuentaBancoId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00093 ;
          prmT00093 = new Object[] {
          new Object[] {"@cuentaBancoId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00098 ;
          prmT00098 = new Object[] {
          new Object[] {"@cuentaBancoId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00099 ;
          prmT00099 = new Object[] {
          new Object[] {"@cuentaBancoId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00092 ;
          prmT00092 = new Object[] {
          new Object[] {"@cuentaBancoId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000910 ;
          prmT000910 = new Object[] {
          new Object[] {"@cuentaBancoNumero",SqlDbType.NChar,20,0} ,
          new Object[] {"@bancoMontoCuenta",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@clienteId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000911 ;
          prmT000911 = new Object[] {
          new Object[] {"@cuentaBancoNumero",SqlDbType.NChar,20,0} ,
          new Object[] {"@bancoMontoCuenta",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@clienteId",SqlDbType.Int,6,0} ,
          new Object[] {"@cuentaBancoId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000912 ;
          prmT000912 = new Object[] {
          new Object[] {"@cuentaBancoId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000914 ;
          prmT000914 = new Object[] {
          } ;
          Object[] prmT000913 ;
          prmT000913 = new Object[] {
          new Object[] {"@clienteId",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00092", "SELECT [cuentaBancoId], [cuentaBancoNumero], [bancoMontoCuenta], [clienteId] FROM [cuentaBanco] WITH (UPDLOCK) WHERE [cuentaBancoId] = @cuentaBancoId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00092,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00093", "SELECT [cuentaBancoId], [cuentaBancoNumero], [bancoMontoCuenta], [clienteId] FROM [cuentaBanco] WHERE [cuentaBancoId] = @cuentaBancoId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00093,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00094", "SELECT [clienteNombre], [clienteApellido] FROM [cliente] WHERE [clienteId] = @clienteId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00094,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00095", "SELECT TM1.[cuentaBancoId], TM1.[cuentaBancoNumero], T2.[clienteNombre], T2.[clienteApellido], TM1.[bancoMontoCuenta], TM1.[clienteId] FROM ([cuentaBanco] TM1 INNER JOIN [cliente] T2 ON T2.[clienteId] = TM1.[clienteId]) WHERE TM1.[cuentaBancoId] = @cuentaBancoId ORDER BY TM1.[cuentaBancoId]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00095,100, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00096", "SELECT [clienteNombre], [clienteApellido] FROM [cliente] WHERE [clienteId] = @clienteId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00096,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00097", "SELECT [cuentaBancoId] FROM [cuentaBanco] WHERE [cuentaBancoId] = @cuentaBancoId  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00097,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00098", "SELECT TOP 1 [cuentaBancoId] FROM [cuentaBanco] WHERE ( [cuentaBancoId] > @cuentaBancoId) ORDER BY [cuentaBancoId]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00098,1, GxCacheFrequency.OFF ,true,true )
             ,new CursorDef("T00099", "SELECT TOP 1 [cuentaBancoId] FROM [cuentaBanco] WHERE ( [cuentaBancoId] < @cuentaBancoId) ORDER BY [cuentaBancoId] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00099,1, GxCacheFrequency.OFF ,true,true )
             ,new CursorDef("T000910", "INSERT INTO [cuentaBanco]([cuentaBancoNumero], [bancoMontoCuenta], [clienteId]) VALUES(@cuentaBancoNumero, @bancoMontoCuenta, @clienteId); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000910)
             ,new CursorDef("T000911", "UPDATE [cuentaBanco] SET [cuentaBancoNumero]=@cuentaBancoNumero, [bancoMontoCuenta]=@bancoMontoCuenta, [clienteId]=@clienteId  WHERE [cuentaBancoId] = @cuentaBancoId", GxErrorMask.GX_NOMASK,prmT000911)
             ,new CursorDef("T000912", "DELETE FROM [cuentaBanco]  WHERE [cuentaBancoId] = @cuentaBancoId", GxErrorMask.GX_NOMASK,prmT000912)
             ,new CursorDef("T000913", "SELECT [clienteNombre], [clienteApellido] FROM [cliente] WHERE [clienteId] = @clienteId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000913,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000914", "SELECT [cuentaBancoId] FROM [cuentaBanco] ORDER BY [cuentaBancoId]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000914,100, GxCacheFrequency.OFF ,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
