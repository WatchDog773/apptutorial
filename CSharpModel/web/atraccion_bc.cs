/*
               File: atraccion_BC
        Description: atraccion
             Author: GeneXus C# Generator version 16_0_6-136889
       Generated on: 12/3/2019 21:45:45.56
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class atraccion_bc : GXHttpHandler, IGxSilentTrn
   {
      public atraccion_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public atraccion_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow022( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey022( ) ;
         standaloneModal( ) ;
         AddRow022( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: After Trn */
            E11022 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( IsIns( )  )
            {
               Z7atraccionId = A7atraccionId;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_020( )
      {
         BeforeValidate022( ) ;
         if ( AnyError == 0 )
         {
            if ( IsDlt( ) )
            {
               OnDeleteControls022( ) ;
            }
            else
            {
               CheckExtendedTable022( ) ;
               if ( AnyError == 0 )
               {
                  ZM022( 3) ;
                  ZM022( 4) ;
                  ZM022( 5) ;
               }
               CloseExtendedTableCursors022( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E12022( )
      {
         /* Start Routine */
      }

      protected void E11022( )
      {
         /* After Trn Routine */
      }

      protected void ZM022( short GX_JID )
      {
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            Z8atraccionNombre = A8atraccionNombre;
            Z9paisId = A9paisId;
            Z15ciudadId = A15ciudadId;
            Z11categoriaId = A11categoriaId;
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            Z10paisNombre = A10paisNombre;
         }
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
            Z17ciudadNombre = A17ciudadNombre;
         }
         if ( ( GX_JID == 5 ) || ( GX_JID == 0 ) )
         {
            Z12categoriaNombre = A12categoriaNombre;
         }
         if ( GX_JID == -2 )
         {
            Z7atraccionId = A7atraccionId;
            Z8atraccionNombre = A8atraccionNombre;
            Z13atraccionFoto = A13atraccionFoto;
            Z40000atraccionFoto_GXI = A40000atraccionFoto_GXI;
            Z9paisId = A9paisId;
            Z15ciudadId = A15ciudadId;
            Z11categoriaId = A11categoriaId;
            Z10paisNombre = A10paisNombre;
            Z12categoriaNombre = A12categoriaNombre;
            Z17ciudadNombre = A17ciudadNombre;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load022( )
      {
         /* Using cursor BC00027 */
         pr_default.execute(5, new Object[] {A7atraccionId});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound2 = 1;
            A8atraccionNombre = BC00027_A8atraccionNombre[0];
            A10paisNombre = BC00027_A10paisNombre[0];
            A12categoriaNombre = BC00027_A12categoriaNombre[0];
            A40000atraccionFoto_GXI = BC00027_A40000atraccionFoto_GXI[0];
            A17ciudadNombre = BC00027_A17ciudadNombre[0];
            A9paisId = BC00027_A9paisId[0];
            A15ciudadId = BC00027_A15ciudadId[0];
            A11categoriaId = BC00027_A11categoriaId[0];
            n11categoriaId = BC00027_n11categoriaId[0];
            A13atraccionFoto = BC00027_A13atraccionFoto[0];
            ZM022( -2) ;
         }
         pr_default.close(5);
         OnLoadActions022( ) ;
      }

      protected void OnLoadActions022( )
      {
      }

      protected void CheckExtendedTable022( )
      {
         nIsDirty_2 = 0;
         standaloneModal( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A8atraccionNombre)) )
         {
            GX_msglist.addItem("Ingrese el nombre de la atraccion", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC00024 */
         pr_default.execute(2, new Object[] {A9paisId});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("No matching 'pais'.", "ForeignKeyNotFound", 1, "PAISID");
            AnyError = 1;
         }
         A10paisNombre = BC00024_A10paisNombre[0];
         pr_default.close(2);
         /* Using cursor BC00025 */
         pr_default.execute(3, new Object[] {A9paisId, A15ciudadId});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("No matching 'ciudad'.", "ForeignKeyNotFound", 1, "PAISID");
            AnyError = 1;
         }
         A17ciudadNombre = BC00025_A17ciudadNombre[0];
         pr_default.close(3);
         /* Using cursor BC00026 */
         pr_default.execute(4, new Object[] {n11categoriaId, A11categoriaId});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A11categoriaId) ) )
            {
               GX_msglist.addItem("No matching 'categoria'.", "ForeignKeyNotFound", 1, "CATEGORIAID");
               AnyError = 1;
            }
         }
         A12categoriaNombre = BC00026_A12categoriaNombre[0];
         pr_default.close(4);
      }

      protected void CloseExtendedTableCursors022( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(4);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey022( )
      {
         /* Using cursor BC00028 */
         pr_default.execute(6, new Object[] {A7atraccionId});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound2 = 1;
         }
         else
         {
            RcdFound2 = 0;
         }
         pr_default.close(6);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00023 */
         pr_default.execute(1, new Object[] {A7atraccionId});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM022( 2) ;
            RcdFound2 = 1;
            A7atraccionId = BC00023_A7atraccionId[0];
            A8atraccionNombre = BC00023_A8atraccionNombre[0];
            A40000atraccionFoto_GXI = BC00023_A40000atraccionFoto_GXI[0];
            A9paisId = BC00023_A9paisId[0];
            A15ciudadId = BC00023_A15ciudadId[0];
            A11categoriaId = BC00023_A11categoriaId[0];
            n11categoriaId = BC00023_n11categoriaId[0];
            A13atraccionFoto = BC00023_A13atraccionFoto[0];
            Z7atraccionId = A7atraccionId;
            sMode2 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load022( ) ;
            if ( AnyError == 1 )
            {
               RcdFound2 = 0;
               InitializeNonKey022( ) ;
            }
            Gx_mode = sMode2;
         }
         else
         {
            RcdFound2 = 0;
            InitializeNonKey022( ) ;
            sMode2 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode2;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey022( ) ;
         if ( RcdFound2 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_020( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency022( )
      {
         if ( ! IsIns( ) )
         {
            /* Using cursor BC00022 */
            pr_default.execute(0, new Object[] {A7atraccionId});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"atraccion"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z8atraccionNombre, BC00022_A8atraccionNombre[0]) != 0 ) || ( Z9paisId != BC00022_A9paisId[0] ) || ( Z15ciudadId != BC00022_A15ciudadId[0] ) || ( Z11categoriaId != BC00022_A11categoriaId[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"atraccion"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert022( )
      {
         BeforeValidate022( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable022( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM022( 0) ;
            CheckOptimisticConcurrency022( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm022( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert022( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00029 */
                     pr_default.execute(7, new Object[] {A8atraccionNombre, A13atraccionFoto, A40000atraccionFoto_GXI, A9paisId, A15ciudadId, n11categoriaId, A11categoriaId});
                     A7atraccionId = BC00029_A7atraccionId[0];
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("atraccion") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load022( ) ;
            }
            EndLevel022( ) ;
         }
         CloseExtendedTableCursors022( ) ;
      }

      protected void Update022( )
      {
         BeforeValidate022( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable022( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency022( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm022( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate022( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000210 */
                     pr_default.execute(8, new Object[] {A8atraccionNombre, A9paisId, A15ciudadId, n11categoriaId, A11categoriaId, A7atraccionId});
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("atraccion") ;
                     if ( (pr_default.getStatus(8) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"atraccion"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate022( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), "SuccessfullyUpdated", 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel022( ) ;
         }
         CloseExtendedTableCursors022( ) ;
      }

      protected void DeferredUpdate022( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor BC000211 */
            pr_default.execute(9, new Object[] {A13atraccionFoto, A40000atraccionFoto_GXI, A7atraccionId});
            pr_default.close(9);
            dsDefault.SmartCacheProvider.SetUpdated("atraccion") ;
         }
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate022( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency022( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls022( ) ;
            AfterConfirm022( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete022( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC000212 */
                  pr_default.execute(10, new Object[] {A7atraccionId});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("atraccion") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), "SuccessfullyDeleted", 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode2 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel022( ) ;
         Gx_mode = sMode2;
      }

      protected void OnDeleteControls022( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC000213 */
            pr_default.execute(11, new Object[] {A9paisId});
            A10paisNombre = BC000213_A10paisNombre[0];
            pr_default.close(11);
            /* Using cursor BC000214 */
            pr_default.execute(12, new Object[] {n11categoriaId, A11categoriaId});
            A12categoriaNombre = BC000214_A12categoriaNombre[0];
            pr_default.close(12);
            /* Using cursor BC000215 */
            pr_default.execute(13, new Object[] {A9paisId, A15ciudadId});
            A17ciudadNombre = BC000215_A17ciudadNombre[0];
            pr_default.close(13);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor BC000216 */
            pr_default.execute(14, new Object[] {A7atraccionId});
            if ( (pr_default.getStatus(14) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"atraccion"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(14);
         }
      }

      protected void EndLevel022( )
      {
         if ( ! IsIns( ) )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete022( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart022( )
      {
         /* Scan By routine */
         /* Using cursor BC000217 */
         pr_default.execute(15, new Object[] {A7atraccionId});
         RcdFound2 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound2 = 1;
            A7atraccionId = BC000217_A7atraccionId[0];
            A8atraccionNombre = BC000217_A8atraccionNombre[0];
            A10paisNombre = BC000217_A10paisNombre[0];
            A12categoriaNombre = BC000217_A12categoriaNombre[0];
            A40000atraccionFoto_GXI = BC000217_A40000atraccionFoto_GXI[0];
            A17ciudadNombre = BC000217_A17ciudadNombre[0];
            A9paisId = BC000217_A9paisId[0];
            A15ciudadId = BC000217_A15ciudadId[0];
            A11categoriaId = BC000217_A11categoriaId[0];
            n11categoriaId = BC000217_n11categoriaId[0];
            A13atraccionFoto = BC000217_A13atraccionFoto[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext022( )
      {
         /* Scan next routine */
         pr_default.readNext(15);
         RcdFound2 = 0;
         ScanKeyLoad022( ) ;
      }

      protected void ScanKeyLoad022( )
      {
         sMode2 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound2 = 1;
            A7atraccionId = BC000217_A7atraccionId[0];
            A8atraccionNombre = BC000217_A8atraccionNombre[0];
            A10paisNombre = BC000217_A10paisNombre[0];
            A12categoriaNombre = BC000217_A12categoriaNombre[0];
            A40000atraccionFoto_GXI = BC000217_A40000atraccionFoto_GXI[0];
            A17ciudadNombre = BC000217_A17ciudadNombre[0];
            A9paisId = BC000217_A9paisId[0];
            A15ciudadId = BC000217_A15ciudadId[0];
            A11categoriaId = BC000217_A11categoriaId[0];
            n11categoriaId = BC000217_n11categoriaId[0];
            A13atraccionFoto = BC000217_A13atraccionFoto[0];
         }
         Gx_mode = sMode2;
      }

      protected void ScanKeyEnd022( )
      {
         pr_default.close(15);
      }

      protected void AfterConfirm022( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert022( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate022( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete022( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete022( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate022( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes022( )
      {
      }

      protected void send_integrity_lvl_hashes022( )
      {
      }

      protected void AddRow022( )
      {
         VarsToRow2( bcatraccion) ;
      }

      protected void ReadRow022( )
      {
         RowToVars2( bcatraccion, 1) ;
      }

      protected void InitializeNonKey022( )
      {
         A8atraccionNombre = "";
         A9paisId = 0;
         A10paisNombre = "";
         A11categoriaId = 0;
         n11categoriaId = false;
         A12categoriaNombre = "";
         A13atraccionFoto = "";
         A40000atraccionFoto_GXI = "";
         A15ciudadId = 0;
         A17ciudadNombre = "";
         Z8atraccionNombre = "";
         Z9paisId = 0;
         Z15ciudadId = 0;
         Z11categoriaId = 0;
      }

      protected void InitAll022( )
      {
         A7atraccionId = 0;
         InitializeNonKey022( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected bool IsIns( )
      {
         return ((StringUtil.StrCmp(Gx_mode, "INS")==0) ? true : false) ;
      }

      protected bool IsDlt( )
      {
         return ((StringUtil.StrCmp(Gx_mode, "DLT")==0) ? true : false) ;
      }

      protected bool IsUpd( )
      {
         return ((StringUtil.StrCmp(Gx_mode, "UPD")==0) ? true : false) ;
      }

      protected bool IsDsp( )
      {
         return ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? true : false) ;
      }

      public void VarsToRow2( Sdtatraccion obj2 )
      {
         obj2.gxTpr_Mode = Gx_mode;
         obj2.gxTpr_Atraccionnombre = A8atraccionNombre;
         obj2.gxTpr_Paisid = A9paisId;
         obj2.gxTpr_Paisnombre = A10paisNombre;
         obj2.gxTpr_Categoriaid = A11categoriaId;
         obj2.gxTpr_Categorianombre = A12categoriaNombre;
         obj2.gxTpr_Atraccionfoto = A13atraccionFoto;
         obj2.gxTpr_Atraccionfoto_gxi = A40000atraccionFoto_GXI;
         obj2.gxTpr_Ciudadid = A15ciudadId;
         obj2.gxTpr_Ciudadnombre = A17ciudadNombre;
         obj2.gxTpr_Atraccionid = A7atraccionId;
         obj2.gxTpr_Atraccionid_Z = Z7atraccionId;
         obj2.gxTpr_Atraccionnombre_Z = Z8atraccionNombre;
         obj2.gxTpr_Paisid_Z = Z9paisId;
         obj2.gxTpr_Paisnombre_Z = Z10paisNombre;
         obj2.gxTpr_Categoriaid_Z = Z11categoriaId;
         obj2.gxTpr_Categorianombre_Z = Z12categoriaNombre;
         obj2.gxTpr_Ciudadid_Z = Z15ciudadId;
         obj2.gxTpr_Ciudadnombre_Z = Z17ciudadNombre;
         obj2.gxTpr_Atraccionfoto_gxi_Z = Z40000atraccionFoto_GXI;
         obj2.gxTpr_Categoriaid_N = (short)(Convert.ToInt16(n11categoriaId));
         obj2.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow2( Sdtatraccion obj2 )
      {
         obj2.gxTpr_Atraccionid = A7atraccionId;
         return  ;
      }

      public void RowToVars2( Sdtatraccion obj2 ,
                              int forceLoad )
      {
         Gx_mode = obj2.gxTpr_Mode;
         A8atraccionNombre = obj2.gxTpr_Atraccionnombre;
         A9paisId = obj2.gxTpr_Paisid;
         A10paisNombre = obj2.gxTpr_Paisnombre;
         A11categoriaId = obj2.gxTpr_Categoriaid;
         n11categoriaId = false;
         A12categoriaNombre = obj2.gxTpr_Categorianombre;
         A13atraccionFoto = obj2.gxTpr_Atraccionfoto;
         A40000atraccionFoto_GXI = obj2.gxTpr_Atraccionfoto_gxi;
         A15ciudadId = obj2.gxTpr_Ciudadid;
         A17ciudadNombre = obj2.gxTpr_Ciudadnombre;
         A7atraccionId = obj2.gxTpr_Atraccionid;
         Z7atraccionId = obj2.gxTpr_Atraccionid_Z;
         Z8atraccionNombre = obj2.gxTpr_Atraccionnombre_Z;
         Z9paisId = obj2.gxTpr_Paisid_Z;
         Z10paisNombre = obj2.gxTpr_Paisnombre_Z;
         Z11categoriaId = obj2.gxTpr_Categoriaid_Z;
         Z12categoriaNombre = obj2.gxTpr_Categorianombre_Z;
         Z15ciudadId = obj2.gxTpr_Ciudadid_Z;
         Z17ciudadNombre = obj2.gxTpr_Ciudadnombre_Z;
         Z40000atraccionFoto_GXI = obj2.gxTpr_Atraccionfoto_gxi_Z;
         n11categoriaId = (bool)(Convert.ToBoolean(obj2.gxTpr_Categoriaid_N));
         Gx_mode = obj2.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A7atraccionId = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey022( ) ;
         ScanKeyStart022( ) ;
         if ( RcdFound2 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z7atraccionId = A7atraccionId;
         }
         ZM022( -2) ;
         OnLoadActions022( ) ;
         AddRow022( ) ;
         ScanKeyEnd022( ) ;
         if ( RcdFound2 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars2( bcatraccion, 0) ;
         ScanKeyStart022( ) ;
         if ( RcdFound2 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z7atraccionId = A7atraccionId;
         }
         ZM022( -2) ;
         OnLoadActions022( ) ;
         AddRow022( ) ;
         ScanKeyEnd022( ) ;
         if ( RcdFound2 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      protected void SaveImpl( )
      {
         nKeyPressed = 1;
         GetKey022( ) ;
         if ( IsIns( ) )
         {
            /* Insert record */
            Insert022( ) ;
         }
         else
         {
            if ( RcdFound2 == 1 )
            {
               if ( A7atraccionId != Z7atraccionId )
               {
                  A7atraccionId = Z7atraccionId;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( IsDlt( ) )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update022( ) ;
               }
            }
            else
            {
               if ( IsDlt( ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A7atraccionId != Z7atraccionId )
                  {
                     if ( IsUpd( ) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert022( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert022( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars2( bcatraccion, 0) ;
         SaveImpl( ) ;
         VarsToRow2( bcatraccion) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public bool Insert( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars2( bcatraccion, 0) ;
         Gx_mode = "INS";
         /* Insert record */
         Insert022( ) ;
         AfterTrn( ) ;
         VarsToRow2( bcatraccion) ;
         context.GX_msglist = BackMsgLst;
         return (AnyError==0) ;
      }

      protected void UpdateImpl( )
      {
         if ( IsUpd( ) )
         {
            SaveImpl( ) ;
         }
         else
         {
            Sdtatraccion auxBC = new Sdtatraccion(context) ;
            IGxSilentTrn auxTrn = auxBC.getTransaction() ;
            auxBC.Load(A7atraccionId);
            if ( auxTrn.Errors() == 0 )
            {
               auxBC.UpdateDirties(bcatraccion);
               auxBC.Save();
            }
            LclMsgLst = (msglist)(auxTrn.GetMessages());
            AnyError = (short)(auxTrn.Errors());
            context.GX_msglist = LclMsgLst;
            if ( auxTrn.Errors() == 0 )
            {
               Gx_mode = auxTrn.GetMode();
               AfterTrn( ) ;
            }
         }
      }

      public bool Update( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars2( bcatraccion, 0) ;
         UpdateImpl( ) ;
         VarsToRow2( bcatraccion) ;
         context.GX_msglist = BackMsgLst;
         return (AnyError==0) ;
      }

      public bool InsertOrUpdate( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars2( bcatraccion, 0) ;
         Gx_mode = "INS";
         /* Insert record */
         Insert022( ) ;
         if ( AnyError == 1 )
         {
            if ( StringUtil.StrCmp(context.GX_msglist.getItemValue(1), "DuplicatePrimaryKey") == 0 )
            {
               AnyError = 0;
               context.GX_msglist.removeAllItems();
               UpdateImpl( ) ;
            }
         }
         else
         {
            AfterTrn( ) ;
         }
         VarsToRow2( bcatraccion) ;
         context.GX_msglist = BackMsgLst;
         return (AnyError==0) ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars2( bcatraccion, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey022( ) ;
         if ( RcdFound2 == 1 )
         {
            if ( IsIns( ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A7atraccionId != Z7atraccionId )
            {
               A7atraccionId = Z7atraccionId;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( IsDlt( ) )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A7atraccionId != Z7atraccionId )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( IsUpd( ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(11);
         pr_default.close(13);
         pr_default.close(12);
         context.RollbackDataStores("atraccion_bc",pr_default);
         VarsToRow2( bcatraccion) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcatraccion.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcatraccion.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcatraccion )
         {
            bcatraccion = (Sdtatraccion)(sdt);
            if ( StringUtil.StrCmp(bcatraccion.gxTpr_Mode, "") == 0 )
            {
               bcatraccion.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow2( bcatraccion) ;
            }
            else
            {
               RowToVars2( bcatraccion, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcatraccion.gxTpr_Mode, "") == 0 )
            {
               bcatraccion.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars2( bcatraccion, 1) ;
         return  ;
      }

      public void ForceCommitOnExit( )
      {
         mustCommit = true;
         return  ;
      }

      public Sdtatraccion atraccion_BC
      {
         get {
            return bcatraccion ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(11);
         pr_default.close(13);
         pr_default.close(12);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z8atraccionNombre = "";
         A8atraccionNombre = "";
         Z10paisNombre = "";
         A10paisNombre = "";
         Z17ciudadNombre = "";
         A17ciudadNombre = "";
         Z12categoriaNombre = "";
         A12categoriaNombre = "";
         Z13atraccionFoto = "";
         A13atraccionFoto = "";
         Z40000atraccionFoto_GXI = "";
         A40000atraccionFoto_GXI = "";
         BC00027_A7atraccionId = new int[1] ;
         BC00027_A8atraccionNombre = new String[] {""} ;
         BC00027_A10paisNombre = new String[] {""} ;
         BC00027_A12categoriaNombre = new String[] {""} ;
         BC00027_A40000atraccionFoto_GXI = new String[] {""} ;
         BC00027_A17ciudadNombre = new String[] {""} ;
         BC00027_A9paisId = new int[1] ;
         BC00027_A15ciudadId = new int[1] ;
         BC00027_A11categoriaId = new int[1] ;
         BC00027_n11categoriaId = new bool[] {false} ;
         BC00027_A13atraccionFoto = new String[] {""} ;
         BC00024_A10paisNombre = new String[] {""} ;
         BC00025_A17ciudadNombre = new String[] {""} ;
         BC00026_A12categoriaNombre = new String[] {""} ;
         BC00028_A7atraccionId = new int[1] ;
         BC00023_A7atraccionId = new int[1] ;
         BC00023_A8atraccionNombre = new String[] {""} ;
         BC00023_A40000atraccionFoto_GXI = new String[] {""} ;
         BC00023_A9paisId = new int[1] ;
         BC00023_A15ciudadId = new int[1] ;
         BC00023_A11categoriaId = new int[1] ;
         BC00023_n11categoriaId = new bool[] {false} ;
         BC00023_A13atraccionFoto = new String[] {""} ;
         sMode2 = "";
         BC00022_A7atraccionId = new int[1] ;
         BC00022_A8atraccionNombre = new String[] {""} ;
         BC00022_A40000atraccionFoto_GXI = new String[] {""} ;
         BC00022_A9paisId = new int[1] ;
         BC00022_A15ciudadId = new int[1] ;
         BC00022_A11categoriaId = new int[1] ;
         BC00022_n11categoriaId = new bool[] {false} ;
         BC00022_A13atraccionFoto = new String[] {""} ;
         BC00029_A7atraccionId = new int[1] ;
         BC000213_A10paisNombre = new String[] {""} ;
         BC000214_A12categoriaNombre = new String[] {""} ;
         BC000215_A17ciudadNombre = new String[] {""} ;
         BC000216_A48proveedorId = new int[1] ;
         BC000216_A7atraccionId = new int[1] ;
         BC000217_A7atraccionId = new int[1] ;
         BC000217_A8atraccionNombre = new String[] {""} ;
         BC000217_A10paisNombre = new String[] {""} ;
         BC000217_A12categoriaNombre = new String[] {""} ;
         BC000217_A40000atraccionFoto_GXI = new String[] {""} ;
         BC000217_A17ciudadNombre = new String[] {""} ;
         BC000217_A9paisId = new int[1] ;
         BC000217_A15ciudadId = new int[1] ;
         BC000217_A11categoriaId = new int[1] ;
         BC000217_n11categoriaId = new bool[] {false} ;
         BC000217_A13atraccionFoto = new String[] {""} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.atraccion_bc__default(),
            new Object[][] {
                new Object[] {
               BC00022_A7atraccionId, BC00022_A8atraccionNombre, BC00022_A40000atraccionFoto_GXI, BC00022_A9paisId, BC00022_A15ciudadId, BC00022_A11categoriaId, BC00022_n11categoriaId, BC00022_A13atraccionFoto
               }
               , new Object[] {
               BC00023_A7atraccionId, BC00023_A8atraccionNombre, BC00023_A40000atraccionFoto_GXI, BC00023_A9paisId, BC00023_A15ciudadId, BC00023_A11categoriaId, BC00023_n11categoriaId, BC00023_A13atraccionFoto
               }
               , new Object[] {
               BC00024_A10paisNombre
               }
               , new Object[] {
               BC00025_A17ciudadNombre
               }
               , new Object[] {
               BC00026_A12categoriaNombre
               }
               , new Object[] {
               BC00027_A7atraccionId, BC00027_A8atraccionNombre, BC00027_A10paisNombre, BC00027_A12categoriaNombre, BC00027_A40000atraccionFoto_GXI, BC00027_A17ciudadNombre, BC00027_A9paisId, BC00027_A15ciudadId, BC00027_A11categoriaId, BC00027_n11categoriaId,
               BC00027_A13atraccionFoto
               }
               , new Object[] {
               BC00028_A7atraccionId
               }
               , new Object[] {
               BC00029_A7atraccionId
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC000213_A10paisNombre
               }
               , new Object[] {
               BC000214_A12categoriaNombre
               }
               , new Object[] {
               BC000215_A17ciudadNombre
               }
               , new Object[] {
               BC000216_A48proveedorId, BC000216_A7atraccionId
               }
               , new Object[] {
               BC000217_A7atraccionId, BC000217_A8atraccionNombre, BC000217_A10paisNombre, BC000217_A12categoriaNombre, BC000217_A40000atraccionFoto_GXI, BC000217_A17ciudadNombre, BC000217_A9paisId, BC000217_A15ciudadId, BC000217_A11categoriaId, BC000217_n11categoriaId,
               BC000217_A13atraccionFoto
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: Start */
         E12022 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound2 ;
      private short nIsDirty_2 ;
      private int trnEnded ;
      private int Z7atraccionId ;
      private int A7atraccionId ;
      private int Z9paisId ;
      private int A9paisId ;
      private int Z15ciudadId ;
      private int A15ciudadId ;
      private int Z11categoriaId ;
      private int A11categoriaId ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String Z8atraccionNombre ;
      private String A8atraccionNombre ;
      private String Z10paisNombre ;
      private String A10paisNombre ;
      private String Z17ciudadNombre ;
      private String A17ciudadNombre ;
      private String Z12categoriaNombre ;
      private String A12categoriaNombre ;
      private String sMode2 ;
      private bool n11categoriaId ;
      private bool mustCommit ;
      private String Z40000atraccionFoto_GXI ;
      private String A40000atraccionFoto_GXI ;
      private String Z13atraccionFoto ;
      private String A13atraccionFoto ;
      private Sdtatraccion bcatraccion ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC00027_A7atraccionId ;
      private String[] BC00027_A8atraccionNombre ;
      private String[] BC00027_A10paisNombre ;
      private String[] BC00027_A12categoriaNombre ;
      private String[] BC00027_A40000atraccionFoto_GXI ;
      private String[] BC00027_A17ciudadNombre ;
      private int[] BC00027_A9paisId ;
      private int[] BC00027_A15ciudadId ;
      private int[] BC00027_A11categoriaId ;
      private bool[] BC00027_n11categoriaId ;
      private String[] BC00027_A13atraccionFoto ;
      private String[] BC00024_A10paisNombre ;
      private String[] BC00025_A17ciudadNombre ;
      private String[] BC00026_A12categoriaNombre ;
      private int[] BC00028_A7atraccionId ;
      private int[] BC00023_A7atraccionId ;
      private String[] BC00023_A8atraccionNombre ;
      private String[] BC00023_A40000atraccionFoto_GXI ;
      private int[] BC00023_A9paisId ;
      private int[] BC00023_A15ciudadId ;
      private int[] BC00023_A11categoriaId ;
      private bool[] BC00023_n11categoriaId ;
      private String[] BC00023_A13atraccionFoto ;
      private int[] BC00022_A7atraccionId ;
      private String[] BC00022_A8atraccionNombre ;
      private String[] BC00022_A40000atraccionFoto_GXI ;
      private int[] BC00022_A9paisId ;
      private int[] BC00022_A15ciudadId ;
      private int[] BC00022_A11categoriaId ;
      private bool[] BC00022_n11categoriaId ;
      private String[] BC00022_A13atraccionFoto ;
      private int[] BC00029_A7atraccionId ;
      private String[] BC000213_A10paisNombre ;
      private String[] BC000214_A12categoriaNombre ;
      private String[] BC000215_A17ciudadNombre ;
      private int[] BC000216_A48proveedorId ;
      private int[] BC000216_A7atraccionId ;
      private int[] BC000217_A7atraccionId ;
      private String[] BC000217_A8atraccionNombre ;
      private String[] BC000217_A10paisNombre ;
      private String[] BC000217_A12categoriaNombre ;
      private String[] BC000217_A40000atraccionFoto_GXI ;
      private String[] BC000217_A17ciudadNombre ;
      private int[] BC000217_A9paisId ;
      private int[] BC000217_A15ciudadId ;
      private int[] BC000217_A11categoriaId ;
      private bool[] BC000217_n11categoriaId ;
      private String[] BC000217_A13atraccionFoto ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

   public class atraccion_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC00027 ;
          prmBC00027 = new Object[] {
          new Object[] {"@atraccionId",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00024 ;
          prmBC00024 = new Object[] {
          new Object[] {"@paisId",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00025 ;
          prmBC00025 = new Object[] {
          new Object[] {"@paisId",SqlDbType.Int,6,0} ,
          new Object[] {"@ciudadId",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00026 ;
          prmBC00026 = new Object[] {
          new Object[] {"@categoriaId",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00028 ;
          prmBC00028 = new Object[] {
          new Object[] {"@atraccionId",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00023 ;
          prmBC00023 = new Object[] {
          new Object[] {"@atraccionId",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00022 ;
          prmBC00022 = new Object[] {
          new Object[] {"@atraccionId",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00029 ;
          prmBC00029 = new Object[] {
          new Object[] {"@atraccionNombre",SqlDbType.NChar,20,0} ,
          new Object[] {"@atraccionFoto",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@atraccionFoto_GXI",SqlDbType.NVarChar,2048,0} ,
          new Object[] {"@paisId",SqlDbType.Int,6,0} ,
          new Object[] {"@ciudadId",SqlDbType.Int,6,0} ,
          new Object[] {"@categoriaId",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000210 ;
          prmBC000210 = new Object[] {
          new Object[] {"@atraccionNombre",SqlDbType.NChar,20,0} ,
          new Object[] {"@paisId",SqlDbType.Int,6,0} ,
          new Object[] {"@ciudadId",SqlDbType.Int,6,0} ,
          new Object[] {"@categoriaId",SqlDbType.Int,6,0} ,
          new Object[] {"@atraccionId",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000211 ;
          prmBC000211 = new Object[] {
          new Object[] {"@atraccionFoto",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@atraccionFoto_GXI",SqlDbType.NVarChar,2048,0} ,
          new Object[] {"@atraccionId",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000212 ;
          prmBC000212 = new Object[] {
          new Object[] {"@atraccionId",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000213 ;
          prmBC000213 = new Object[] {
          new Object[] {"@paisId",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000214 ;
          prmBC000214 = new Object[] {
          new Object[] {"@categoriaId",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000215 ;
          prmBC000215 = new Object[] {
          new Object[] {"@paisId",SqlDbType.Int,6,0} ,
          new Object[] {"@ciudadId",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000216 ;
          prmBC000216 = new Object[] {
          new Object[] {"@atraccionId",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000217 ;
          prmBC000217 = new Object[] {
          new Object[] {"@atraccionId",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC00022", "SELECT [atraccionId], [atraccionNombre], [atraccionFoto_GXI], [paisId], [ciudadId], [categoriaId], [atraccionFoto] FROM [atraccion] WITH (UPDLOCK) WHERE [atraccionId] = @atraccionId ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00022,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("BC00023", "SELECT [atraccionId], [atraccionNombre], [atraccionFoto_GXI], [paisId], [ciudadId], [categoriaId], [atraccionFoto] FROM [atraccion] WHERE [atraccionId] = @atraccionId ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00023,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("BC00024", "SELECT [paisNombre] FROM [pais] WHERE [paisId] = @paisId ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00024,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("BC00025", "SELECT [ciudadNombre] FROM [paisciudad] WHERE [paisId] = @paisId AND [ciudadId] = @ciudadId ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00025,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("BC00026", "SELECT [categoriaNombre] FROM [categoria] WHERE [categoriaId] = @categoriaId ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00026,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("BC00027", "SELECT TM1.[atraccionId], TM1.[atraccionNombre], T2.[paisNombre], T3.[categoriaNombre], TM1.[atraccionFoto_GXI], T4.[ciudadNombre], TM1.[paisId], TM1.[ciudadId], TM1.[categoriaId], TM1.[atraccionFoto] FROM ((([atraccion] TM1 INNER JOIN [pais] T2 ON T2.[paisId] = TM1.[paisId]) LEFT JOIN [categoria] T3 ON T3.[categoriaId] = TM1.[categoriaId]) INNER JOIN [paisciudad] T4 ON T4.[paisId] = TM1.[paisId] AND T4.[ciudadId] = TM1.[ciudadId]) WHERE TM1.[atraccionId] = @atraccionId ORDER BY TM1.[atraccionId]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00027,100, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("BC00028", "SELECT [atraccionId] FROM [atraccion] WHERE [atraccionId] = @atraccionId  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00028,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("BC00029", "INSERT INTO [atraccion]([atraccionNombre], [atraccionFoto], [atraccionFoto_GXI], [paisId], [ciudadId], [categoriaId]) VALUES(@atraccionNombre, @atraccionFoto, @atraccionFoto_GXI, @paisId, @ciudadId, @categoriaId); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC00029)
             ,new CursorDef("BC000210", "UPDATE [atraccion] SET [atraccionNombre]=@atraccionNombre, [paisId]=@paisId, [ciudadId]=@ciudadId, [categoriaId]=@categoriaId  WHERE [atraccionId] = @atraccionId", GxErrorMask.GX_NOMASK,prmBC000210)
             ,new CursorDef("BC000211", "UPDATE [atraccion] SET [atraccionFoto]=@atraccionFoto, [atraccionFoto_GXI]=@atraccionFoto_GXI  WHERE [atraccionId] = @atraccionId", GxErrorMask.GX_NOMASK,prmBC000211)
             ,new CursorDef("BC000212", "DELETE FROM [atraccion]  WHERE [atraccionId] = @atraccionId", GxErrorMask.GX_NOMASK,prmBC000212)
             ,new CursorDef("BC000213", "SELECT [paisNombre] FROM [pais] WHERE [paisId] = @paisId ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000213,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("BC000214", "SELECT [categoriaNombre] FROM [categoria] WHERE [categoriaId] = @categoriaId ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000214,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("BC000215", "SELECT [ciudadNombre] FROM [paisciudad] WHERE [paisId] = @paisId AND [ciudadId] = @ciudadId ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000215,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("BC000216", "SELECT TOP 1 [proveedorId], [atraccionId] FROM [proveedoratraccion] WHERE [atraccionId] = @atraccionId ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000216,1, GxCacheFrequency.OFF ,true,true )
             ,new CursorDef("BC000217", "SELECT TM1.[atraccionId], TM1.[atraccionNombre], T2.[paisNombre], T3.[categoriaNombre], TM1.[atraccionFoto_GXI], T4.[ciudadNombre], TM1.[paisId], TM1.[ciudadId], TM1.[categoriaId], TM1.[atraccionFoto] FROM ((([atraccion] TM1 INNER JOIN [pais] T2 ON T2.[paisId] = TM1.[paisId]) LEFT JOIN [categoria] T3 ON T3.[categoriaId] = TM1.[categoriaId]) INNER JOIN [paisciudad] T4 ON T4.[paisId] = TM1.[paisId] AND T4.[ciudadId] = TM1.[ciudadId]) WHERE TM1.[atraccionId] = @atraccionId ORDER BY TM1.[atraccionId]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000217,100, GxCacheFrequency.OFF ,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getMultimediaUri(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((String[]) buf[7])[0] = rslt.getMultimediaFile(7, rslt.getVarchar(3)) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getMultimediaUri(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((String[]) buf[7])[0] = rslt.getMultimediaFile(7, rslt.getVarchar(3)) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
                ((String[]) buf[4])[0] = rslt.getMultimediaUri(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                ((int[]) buf[7])[0] = rslt.getInt(8) ;
                ((int[]) buf[8])[0] = rslt.getInt(9) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(9);
                ((String[]) buf[10])[0] = rslt.getMultimediaFile(10, rslt.getVarchar(5)) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
                ((String[]) buf[4])[0] = rslt.getMultimediaUri(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                ((int[]) buf[7])[0] = rslt.getInt(8) ;
                ((int[]) buf[8])[0] = rslt.getInt(9) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(9);
                ((String[]) buf[10])[0] = rslt.getMultimediaFile(10, rslt.getVarchar(5)) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameterBlob(2, (String)parms[1], false);
                stmt.SetParameterMultimedia(3, (String)parms[2], (String)parms[1], "atraccion", "atraccionFoto");
                stmt.SetParameter(4, (int)parms[3]);
                stmt.SetParameter(5, (int)parms[4]);
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[6]);
                }
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[4]);
                }
                stmt.SetParameter(5, (int)parms[5]);
                return;
             case 9 :
                stmt.SetParameterBlob(1, (String)parms[0], false);
                stmt.SetParameterMultimedia(2, (String)parms[1], (String)parms[0], "atraccion", "atraccionFoto");
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
