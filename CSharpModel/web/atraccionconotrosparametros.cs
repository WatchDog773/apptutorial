/*
               File: atraccionConOtrosParametros
        Description: atraccion Con Otros Parametros
             Author: GeneXus C# Generator version 16_0_6-136889
       Generated on: 12/3/2019 21:6:5.69
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class atraccionconotrosparametros : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_5") == 0 )
         {
            A9paisId = (int)(NumberUtil.Val( GetNextPar( ), "."));
            AssignAttri("", false, "A9paisId", StringUtil.LTrimStr( (decimal)(A9paisId), 6, 0));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_5( A9paisId) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_6") == 0 )
         {
            A9paisId = (int)(NumberUtil.Val( GetNextPar( ), "."));
            AssignAttri("", false, "A9paisId", StringUtil.LTrimStr( (decimal)(A9paisId), 6, 0));
            A15ciudadId = (int)(NumberUtil.Val( GetNextPar( ), "."));
            AssignAttri("", false, "A15ciudadId", StringUtil.LTrimStr( (decimal)(A15ciudadId), 6, 0));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_6( A9paisId, A15ciudadId) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_7") == 0 )
         {
            A11categoriaId = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n11categoriaId = false;
            AssignAttri("", false, "A11categoriaId", StringUtil.LTrimStr( (decimal)(A11categoriaId), 6, 0));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_7( A11categoriaId) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_web_controls( ) ;
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 16_0_6-136889", 0) ;
            Form.Meta.addItem("description", "atraccion Con Otros Parametros", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtatraccionId_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("Carmine");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public atraccionconotrosparametros( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public atraccionconotrosparametros( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdmasterpage", "GeneXus.Programs.rwdmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "no-store");
               }
               if ( ! context.WillRedirect( ) )
               {
                  AddString( context.getJSONResponse( )) ;
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            DrawControls( ) ;
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void DrawControls( )
      {
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "WWAdvancedContainer", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divTitlecontainer_Internalname, 1, 0, "px", 0, "px", "TableTop", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         /* Text block */
         GxWebStd.gx_label_ctrl( context, lblTitle_Internalname, "atraccion Con Otros Parametros", "", "", lblTitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_atraccionConOtrosParametros.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         ClassString = "ErrorViewer";
         StyleString = "";
         GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divFormcontainer_Internalname, 1, 0, "px", 0, "px", "FormContainer", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divToolbarcell_Internalname, 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3 ToolbarCellClass", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group ActionGroup", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "btn-group", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
         ClassString = "BtnFirst";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_first_Internalname, "", "", bttBtn_first_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_first_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", TempTags, "", context.GetButtonType( ), "HLP_atraccionConOtrosParametros.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
         ClassString = "BtnPrevious";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_previous_Internalname, "", "", bttBtn_previous_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_previous_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", TempTags, "", context.GetButtonType( ), "HLP_atraccionConOtrosParametros.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
         ClassString = "BtnNext";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_next_Internalname, "", "", bttBtn_next_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_next_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", TempTags, "", context.GetButtonType( ), "HLP_atraccionConOtrosParametros.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
         ClassString = "BtnLast";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_last_Internalname, "", "", bttBtn_last_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_last_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ELAST."+"'", TempTags, "", context.GetButtonType( ), "HLP_atraccionConOtrosParametros.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
         ClassString = "BtnSelect";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_select_Internalname, "", "Select", bttBtn_select_Jsonclick, 4, "Select", "", StyleString, ClassString, bttBtn_select_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", TempTags, "gx.popup.openPrompt('"+"gx0020.aspx"+"',["+"{Ctrl:gx.dom.el('"+"ATRACCIONID"+"'), id:'"+"ATRACCIONID"+"'"+",IOType:'out',isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");"+"return false;", 2, "HLP_atraccionConOtrosParametros.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCellAdvanced", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtatraccionId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtatraccionId_Internalname, "Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtatraccionId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A7atraccionId), 6, 0, ".", "")), ((edtatraccionId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A7atraccionId), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A7atraccionId), "ZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtatraccionId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtatraccionId_Enabled, 0, "number", "1", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "", "HLP_atraccionConOtrosParametros.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtatraccionNombre_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtatraccionNombre_Internalname, "Nombre", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtatraccionNombre_Internalname, StringUtil.RTrim( A8atraccionNombre), StringUtil.RTrim( context.localUtil.Format( A8atraccionNombre, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtatraccionNombre_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtatraccionNombre_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "Name", "left", true, "", "HLP_atraccionConOtrosParametros.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtpaisId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtpaisId_Internalname, "pais Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtpaisId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A9paisId), 6, 0, ".", "")), ((edtpaisId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A9paisId), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A9paisId), "ZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtpaisId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtpaisId_Enabled, 0, "number", "1", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "", "HLP_atraccionConOtrosParametros.htm");
         /* Static images/pictures */
         ClassString = "gx-prompt Image";
         StyleString = "";
         sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
         GxWebStd.gx_bitmap( context, imgprompt_9_Internalname, sImgUrl, imgprompt_9_Link, "", "", context.GetTheme( ), imgprompt_9_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_atraccionConOtrosParametros.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtpaisNombre_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtpaisNombre_Internalname, "pais Nombre", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtpaisNombre_Internalname, StringUtil.RTrim( A10paisNombre), StringUtil.RTrim( context.localUtil.Format( A10paisNombre, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtpaisNombre_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtpaisNombre_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "Name", "left", true, "", "HLP_atraccionConOtrosParametros.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtcategoriaId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtcategoriaId_Internalname, "categoria Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtcategoriaId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A11categoriaId), 6, 0, ".", "")), ((edtcategoriaId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A11categoriaId), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A11categoriaId), "ZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtcategoriaId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtcategoriaId_Enabled, 0, "number", "1", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "", "HLP_atraccionConOtrosParametros.htm");
         /* Static images/pictures */
         ClassString = "gx-prompt Image";
         StyleString = "";
         sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
         GxWebStd.gx_bitmap( context, imgprompt_11_Internalname, sImgUrl, imgprompt_11_Link, "", "", context.GetTheme( ), imgprompt_11_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_atraccionConOtrosParametros.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtcategoriaNombre_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtcategoriaNombre_Internalname, "categoria Nombre", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtcategoriaNombre_Internalname, StringUtil.RTrim( A12categoriaNombre), StringUtil.RTrim( context.localUtil.Format( A12categoriaNombre, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtcategoriaNombre_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtcategoriaNombre_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "Name", "left", true, "", "HLP_atraccionConOtrosParametros.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+imgatraccionFoto_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, "", "Foto", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Static Bitmap Variable */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
         ClassString = "Attribute";
         StyleString = "";
         A13atraccionFoto_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( A13atraccionFoto))&&String.IsNullOrEmpty(StringUtil.RTrim( A40000atraccionFoto_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( A13atraccionFoto)));
         sImgUrl = (String.IsNullOrEmpty(StringUtil.RTrim( A13atraccionFoto)) ? A40000atraccionFoto_GXI : context.PathToRelativeUrl( A13atraccionFoto));
         GxWebStd.gx_bitmap( context, imgatraccionFoto_Internalname, sImgUrl, "", "", "", context.GetTheme( ), 1, imgatraccionFoto_Enabled, "", "", 1, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", "", "", "", 0, A13atraccionFoto_IsBlob, true, context.GetImageSrcSet( sImgUrl), "HLP_atraccionConOtrosParametros.htm");
         AssignProp("", false, imgatraccionFoto_Internalname, "URL", (String.IsNullOrEmpty(StringUtil.RTrim( A13atraccionFoto)) ? A40000atraccionFoto_GXI : context.PathToRelativeUrl( A13atraccionFoto)), true);
         AssignProp("", false, imgatraccionFoto_Internalname, "IsBlob", StringUtil.BoolToStr( A13atraccionFoto_IsBlob), true);
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtciudadId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtciudadId_Internalname, "ciudad Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtciudadId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A15ciudadId), 6, 0, ".", "")), ((edtciudadId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A15ciudadId), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A15ciudadId), "ZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtciudadId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtciudadId_Enabled, 0, "number", "1", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "", "HLP_atraccionConOtrosParametros.htm");
         /* Static images/pictures */
         ClassString = "gx-prompt Image";
         StyleString = "";
         sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
         GxWebStd.gx_bitmap( context, imgprompt_15_Internalname, sImgUrl, imgprompt_15_Link, "", "", context.GetTheme( ), imgprompt_15_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_atraccionConOtrosParametros.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtciudadNombre_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtciudadNombre_Internalname, "ciudad Nombre", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtciudadNombre_Internalname, StringUtil.RTrim( A17ciudadNombre), StringUtil.RTrim( context.localUtil.Format( A17ciudadNombre, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtciudadNombre_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtciudadNombre_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "Name", "left", true, "", "HLP_atraccionConOtrosParametros.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
         ClassString = "BtnEnter";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirm", bttBtn_enter_Jsonclick, 5, "Confirm", "", StyleString, ClassString, bttBtn_enter_Visible, bttBtn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_atraccionConOtrosParametros.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'',0)\"";
         ClassString = "BtnCancel";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Cancel", bttBtn_cancel_Jsonclick, 1, "Cancel", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_atraccionConOtrosParametros.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'',0)\"";
         ClassString = "BtnDelete";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Delete", bttBtn_delete_Jsonclick, 5, "Delete", "", StyleString, ClassString, bttBtn_delete_Visible, bttBtn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_atraccionConOtrosParametros.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "Center", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E110A2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read saved values. */
               Z7atraccionId = (int)(context.localUtil.CToN( cgiGet( "Z7atraccionId"), ".", ","));
               Z8atraccionNombre = cgiGet( "Z8atraccionNombre");
               Z9paisId = (int)(context.localUtil.CToN( cgiGet( "Z9paisId"), ".", ","));
               Z15ciudadId = (int)(context.localUtil.CToN( cgiGet( "Z15ciudadId"), ".", ","));
               Z11categoriaId = (int)(context.localUtil.CToN( cgiGet( "Z11categoriaId"), ".", ","));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ".", ","));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ".", ","));
               Gx_mode = cgiGet( "Mode");
               A40000atraccionFoto_GXI = cgiGet( "ATRACCIONFOTO_GXI");
               AV16Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtatraccionId_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtatraccionId_Internalname), ".", ",") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "ATRACCIONID");
                  AnyError = 1;
                  GX_FocusControl = edtatraccionId_Internalname;
                  AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A7atraccionId = 0;
                  AssignAttri("", false, "A7atraccionId", StringUtil.LTrimStr( (decimal)(A7atraccionId), 6, 0));
               }
               else
               {
                  A7atraccionId = (int)(context.localUtil.CToN( cgiGet( edtatraccionId_Internalname), ".", ","));
                  AssignAttri("", false, "A7atraccionId", StringUtil.LTrimStr( (decimal)(A7atraccionId), 6, 0));
               }
               A8atraccionNombre = cgiGet( edtatraccionNombre_Internalname);
               AssignAttri("", false, "A8atraccionNombre", A8atraccionNombre);
               if ( ( ( context.localUtil.CToN( cgiGet( edtpaisId_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtpaisId_Internalname), ".", ",") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PAISID");
                  AnyError = 1;
                  GX_FocusControl = edtpaisId_Internalname;
                  AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A9paisId = 0;
                  AssignAttri("", false, "A9paisId", StringUtil.LTrimStr( (decimal)(A9paisId), 6, 0));
               }
               else
               {
                  A9paisId = (int)(context.localUtil.CToN( cgiGet( edtpaisId_Internalname), ".", ","));
                  AssignAttri("", false, "A9paisId", StringUtil.LTrimStr( (decimal)(A9paisId), 6, 0));
               }
               A10paisNombre = cgiGet( edtpaisNombre_Internalname);
               AssignAttri("", false, "A10paisNombre", A10paisNombre);
               if ( ( ( context.localUtil.CToN( cgiGet( edtcategoriaId_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtcategoriaId_Internalname), ".", ",") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CATEGORIAID");
                  AnyError = 1;
                  GX_FocusControl = edtcategoriaId_Internalname;
                  AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A11categoriaId = 0;
                  n11categoriaId = false;
                  AssignAttri("", false, "A11categoriaId", StringUtil.LTrimStr( (decimal)(A11categoriaId), 6, 0));
               }
               else
               {
                  A11categoriaId = (int)(context.localUtil.CToN( cgiGet( edtcategoriaId_Internalname), ".", ","));
                  n11categoriaId = false;
                  AssignAttri("", false, "A11categoriaId", StringUtil.LTrimStr( (decimal)(A11categoriaId), 6, 0));
               }
               n11categoriaId = ((0==A11categoriaId) ? true : false);
               A12categoriaNombre = cgiGet( edtcategoriaNombre_Internalname);
               AssignAttri("", false, "A12categoriaNombre", A12categoriaNombre);
               A13atraccionFoto = cgiGet( imgatraccionFoto_Internalname);
               AssignAttri("", false, "A13atraccionFoto", A13atraccionFoto);
               if ( ( ( context.localUtil.CToN( cgiGet( edtciudadId_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtciudadId_Internalname), ".", ",") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CIUDADID");
                  AnyError = 1;
                  GX_FocusControl = edtciudadId_Internalname;
                  AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A15ciudadId = 0;
                  AssignAttri("", false, "A15ciudadId", StringUtil.LTrimStr( (decimal)(A15ciudadId), 6, 0));
               }
               else
               {
                  A15ciudadId = (int)(context.localUtil.CToN( cgiGet( edtciudadId_Internalname), ".", ","));
                  AssignAttri("", false, "A15ciudadId", StringUtil.LTrimStr( (decimal)(A15ciudadId), 6, 0));
               }
               A17ciudadNombre = cgiGet( edtciudadNombre_Internalname);
               AssignAttri("", false, "A17ciudadNombre", A17ciudadNombre);
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               getMultimediaValue(imgatraccionFoto_Internalname, ref  A13atraccionFoto, ref  A40000atraccionFoto_GXI);
               GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  AssignAttri("", false, "Gx_mode", Gx_mode);
                  AssignAttri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                  A7atraccionId = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AssignAttri("", false, "A7atraccionId", StringUtil.LTrimStr( (decimal)(A7atraccionId), 6, 0));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  AssignAttri("", false, "Gx_mode", Gx_mode);
                  AssignAttri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  AssignAttri("", false, "Gx_mode", Gx_mode);
                  AssignAttri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: Start */
                           E110A2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: After Trn */
                           E120A2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: After Trn */
            E120A2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( IsIns( )  )
            {
               /* Clear variables for new insertion. */
               InitAll0A2( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( IsIns( ) )
         {
            bttBtn_delete_Enabled = 0;
            AssignProp("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Enabled), 5, 0), true);
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         bttBtn_delete_Visible = 0;
         AssignProp("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Visible), 5, 0), true);
         bttBtn_first_Visible = 0;
         AssignProp("", false, bttBtn_first_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_first_Visible), 5, 0), true);
         bttBtn_previous_Visible = 0;
         AssignProp("", false, bttBtn_previous_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_previous_Visible), 5, 0), true);
         bttBtn_next_Visible = 0;
         AssignProp("", false, bttBtn_next_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_next_Visible), 5, 0), true);
         bttBtn_last_Visible = 0;
         AssignProp("", false, bttBtn_last_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_last_Visible), 5, 0), true);
         bttBtn_select_Visible = 0;
         AssignProp("", false, bttBtn_select_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_select_Visible), 5, 0), true);
         bttBtn_delete_Visible = 0;
         AssignProp("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Visible), 5, 0), true);
         if ( IsDsp( ) )
         {
            bttBtn_enter_Visible = 0;
            AssignProp("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrimStr( (decimal)(bttBtn_enter_Visible), 5, 0), true);
         }
         DisableAttributes0A2( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( IsDlt( ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption0A0( )
      {
      }

      protected void E110A2( )
      {
         /* Start Routine */
         if ( ! new isauthorized(context).executeUdp(  AV16Pgmname) )
         {
            CallWebObject(formatLink("notauthorized.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV16Pgmname)));
            context.wjLocDisableFrm = 1;
         }
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), null, "TransactionContext", "TravelAgency");
         AV11Insert_paisId = 0;
         AssignAttri("", false, "AV11Insert_paisId", StringUtil.LTrimStr( (decimal)(AV11Insert_paisId), 6, 0));
         AV12Insert_categoriaId = 0;
         AssignAttri("", false, "AV12Insert_categoriaId", StringUtil.LTrimStr( (decimal)(AV12Insert_categoriaId), 6, 0));
         AV13Insert_ciudadId = 0;
         AssignAttri("", false, "AV13Insert_ciudadId", StringUtil.LTrimStr( (decimal)(AV13Insert_ciudadId), 6, 0));
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV16Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV17GXV1 = 1;
            AssignAttri("", false, "AV17GXV1", StringUtil.LTrimStr( (decimal)(AV17GXV1), 8, 0));
            while ( AV17GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV14TrnContextAtt = ((SdtTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV17GXV1));
               if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "paisId") == 0 )
               {
                  AV11Insert_paisId = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
                  AssignAttri("", false, "AV11Insert_paisId", StringUtil.LTrimStr( (decimal)(AV11Insert_paisId), 6, 0));
               }
               else if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "categoriaId") == 0 )
               {
                  AV12Insert_categoriaId = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
                  AssignAttri("", false, "AV12Insert_categoriaId", StringUtil.LTrimStr( (decimal)(AV12Insert_categoriaId), 6, 0));
               }
               else if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "ciudadId") == 0 )
               {
                  AV13Insert_ciudadId = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
                  AssignAttri("", false, "AV13Insert_ciudadId", StringUtil.LTrimStr( (decimal)(AV13Insert_ciudadId), 6, 0));
               }
               AV17GXV1 = (int)(AV17GXV1+1);
               AssignAttri("", false, "AV17GXV1", StringUtil.LTrimStr( (decimal)(AV17GXV1), 8, 0));
            }
         }
      }

      protected void E120A2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            CallWebObject(formatLink("wwatraccion.aspx") );
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.setWebReturnParmsMetadata(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         pr_default.close(1);
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(4);
         returnInSub = true;
         if (true) return;
      }

      protected void ZM0A2( short GX_JID )
      {
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
            if ( ! IsIns( ) )
            {
               Z8atraccionNombre = T000A3_A8atraccionNombre[0];
               Z9paisId = T000A3_A9paisId[0];
               Z15ciudadId = T000A3_A15ciudadId[0];
               Z11categoriaId = T000A3_A11categoriaId[0];
            }
            else
            {
               Z8atraccionNombre = A8atraccionNombre;
               Z9paisId = A9paisId;
               Z15ciudadId = A15ciudadId;
               Z11categoriaId = A11categoriaId;
            }
         }
         if ( GX_JID == -4 )
         {
            Z7atraccionId = A7atraccionId;
            Z8atraccionNombre = A8atraccionNombre;
            Z13atraccionFoto = A13atraccionFoto;
            Z40000atraccionFoto_GXI = A40000atraccionFoto_GXI;
            Z9paisId = A9paisId;
            Z15ciudadId = A15ciudadId;
            Z11categoriaId = A11categoriaId;
            Z10paisNombre = A10paisNombre;
            Z12categoriaNombre = A12categoriaNombre;
            Z17ciudadNombre = A17ciudadNombre;
         }
      }

      protected void standaloneNotModal( )
      {
         imgprompt_9_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0030.aspx"+"',["+"{Ctrl:gx.dom.el('"+"PAISID"+"'), id:'"+"PAISID"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         imgprompt_11_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0040.aspx"+"',["+"{Ctrl:gx.dom.el('"+"CATEGORIAID"+"'), id:'"+"CATEGORIAID"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         imgprompt_15_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0051.aspx"+"',["+"{Ctrl:gx.dom.el('"+"PAISID"+"'), id:'"+"PAISID"+"'"+",IOType:'in'}"+","+"{Ctrl:gx.dom.el('"+"CIUDADID"+"'), id:'"+"CIUDADID"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            AssignProp("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Enabled), 5, 0), true);
         }
         else
         {
            bttBtn_delete_Enabled = 1;
            AssignProp("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Enabled), 5, 0), true);
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Enabled = 0;
            AssignProp("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_enter_Enabled), 5, 0), true);
         }
         else
         {
            bttBtn_enter_Enabled = 1;
            AssignProp("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_enter_Enabled), 5, 0), true);
         }
      }

      protected void Load0A2( )
      {
         /* Using cursor T000A7 */
         pr_default.execute(5, new Object[] {A7atraccionId});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound2 = 1;
            A8atraccionNombre = T000A7_A8atraccionNombre[0];
            AssignAttri("", false, "A8atraccionNombre", A8atraccionNombre);
            A10paisNombre = T000A7_A10paisNombre[0];
            AssignAttri("", false, "A10paisNombre", A10paisNombre);
            A12categoriaNombre = T000A7_A12categoriaNombre[0];
            AssignAttri("", false, "A12categoriaNombre", A12categoriaNombre);
            A40000atraccionFoto_GXI = T000A7_A40000atraccionFoto_GXI[0];
            AssignProp("", false, imgatraccionFoto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A13atraccionFoto)) ? A40000atraccionFoto_GXI : context.convertURL( context.PathToRelativeUrl( A13atraccionFoto))), true);
            AssignProp("", false, imgatraccionFoto_Internalname, "SrcSet", context.GetImageSrcSet( A13atraccionFoto), true);
            A17ciudadNombre = T000A7_A17ciudadNombre[0];
            AssignAttri("", false, "A17ciudadNombre", A17ciudadNombre);
            A9paisId = T000A7_A9paisId[0];
            AssignAttri("", false, "A9paisId", StringUtil.LTrimStr( (decimal)(A9paisId), 6, 0));
            A15ciudadId = T000A7_A15ciudadId[0];
            AssignAttri("", false, "A15ciudadId", StringUtil.LTrimStr( (decimal)(A15ciudadId), 6, 0));
            A11categoriaId = T000A7_A11categoriaId[0];
            AssignAttri("", false, "A11categoriaId", StringUtil.LTrimStr( (decimal)(A11categoriaId), 6, 0));
            n11categoriaId = T000A7_n11categoriaId[0];
            A13atraccionFoto = T000A7_A13atraccionFoto[0];
            AssignAttri("", false, "A13atraccionFoto", A13atraccionFoto);
            AssignProp("", false, imgatraccionFoto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A13atraccionFoto)) ? A40000atraccionFoto_GXI : context.convertURL( context.PathToRelativeUrl( A13atraccionFoto))), true);
            AssignProp("", false, imgatraccionFoto_Internalname, "SrcSet", context.GetImageSrcSet( A13atraccionFoto), true);
            ZM0A2( -4) ;
         }
         pr_default.close(5);
         OnLoadActions0A2( ) ;
      }

      protected void OnLoadActions0A2( )
      {
         AV16Pgmname = "atraccionConOtrosParametros";
         AssignAttri("", false, "AV16Pgmname", AV16Pgmname);
      }

      protected void CheckExtendedTable0A2( )
      {
         nIsDirty_2 = 0;
         Gx_BScreen = 1;
         standaloneModal( ) ;
         AV16Pgmname = "atraccionConOtrosParametros";
         AssignAttri("", false, "AV16Pgmname", AV16Pgmname);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A8atraccionNombre)) )
         {
            GX_msglist.addItem("Ingrese el nombre de la atraccion", 1, "ATRACCIONNOMBRE");
            AnyError = 1;
            GX_FocusControl = edtatraccionNombre_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T000A4 */
         pr_default.execute(2, new Object[] {A9paisId});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("No matching 'pais'.", "ForeignKeyNotFound", 1, "PAISID");
            AnyError = 1;
            GX_FocusControl = edtpaisId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A10paisNombre = T000A4_A10paisNombre[0];
         AssignAttri("", false, "A10paisNombre", A10paisNombre);
         pr_default.close(2);
         /* Using cursor T000A5 */
         pr_default.execute(3, new Object[] {A9paisId, A15ciudadId});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("No matching 'ciudad'.", "ForeignKeyNotFound", 1, "PAISID");
            AnyError = 1;
            GX_FocusControl = edtpaisId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A17ciudadNombre = T000A5_A17ciudadNombre[0];
         AssignAttri("", false, "A17ciudadNombre", A17ciudadNombre);
         pr_default.close(3);
         /* Using cursor T000A6 */
         pr_default.execute(4, new Object[] {n11categoriaId, A11categoriaId});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A11categoriaId) ) )
            {
               GX_msglist.addItem("No matching 'categoria'.", "ForeignKeyNotFound", 1, "CATEGORIAID");
               AnyError = 1;
               GX_FocusControl = edtcategoriaId_Internalname;
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A12categoriaNombre = T000A6_A12categoriaNombre[0];
         AssignAttri("", false, "A12categoriaNombre", A12categoriaNombre);
         pr_default.close(4);
      }

      protected void CloseExtendedTableCursors0A2( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(4);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_5( int A9paisId )
      {
         /* Using cursor T000A8 */
         pr_default.execute(6, new Object[] {A9paisId});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("No matching 'pais'.", "ForeignKeyNotFound", 1, "PAISID");
            AnyError = 1;
            GX_FocusControl = edtpaisId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A10paisNombre = T000A8_A10paisNombre[0];
         AssignAttri("", false, "A10paisNombre", A10paisNombre);
         GxWebStd.set_html_headers( context, 0, "", "");
         AddString( "[[") ;
         AddString( "\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A10paisNombre))+"\"") ;
         AddString( "]") ;
         if ( (pr_default.getStatus(6) == 101) )
         {
            AddString( ",") ;
            AddString( "101") ;
         }
         AddString( "]") ;
         pr_default.close(6);
      }

      protected void gxLoad_6( int A9paisId ,
                               int A15ciudadId )
      {
         /* Using cursor T000A9 */
         pr_default.execute(7, new Object[] {A9paisId, A15ciudadId});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("No matching 'ciudad'.", "ForeignKeyNotFound", 1, "PAISID");
            AnyError = 1;
            GX_FocusControl = edtpaisId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A17ciudadNombre = T000A9_A17ciudadNombre[0];
         AssignAttri("", false, "A17ciudadNombre", A17ciudadNombre);
         GxWebStd.set_html_headers( context, 0, "", "");
         AddString( "[[") ;
         AddString( "\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A17ciudadNombre))+"\"") ;
         AddString( "]") ;
         if ( (pr_default.getStatus(7) == 101) )
         {
            AddString( ",") ;
            AddString( "101") ;
         }
         AddString( "]") ;
         pr_default.close(7);
      }

      protected void gxLoad_7( int A11categoriaId )
      {
         /* Using cursor T000A10 */
         pr_default.execute(8, new Object[] {n11categoriaId, A11categoriaId});
         if ( (pr_default.getStatus(8) == 101) )
         {
            if ( ! ( (0==A11categoriaId) ) )
            {
               GX_msglist.addItem("No matching 'categoria'.", "ForeignKeyNotFound", 1, "CATEGORIAID");
               AnyError = 1;
               GX_FocusControl = edtcategoriaId_Internalname;
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A12categoriaNombre = T000A10_A12categoriaNombre[0];
         AssignAttri("", false, "A12categoriaNombre", A12categoriaNombre);
         GxWebStd.set_html_headers( context, 0, "", "");
         AddString( "[[") ;
         AddString( "\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A12categoriaNombre))+"\"") ;
         AddString( "]") ;
         if ( (pr_default.getStatus(8) == 101) )
         {
            AddString( ",") ;
            AddString( "101") ;
         }
         AddString( "]") ;
         pr_default.close(8);
      }

      protected void GetKey0A2( )
      {
         /* Using cursor T000A11 */
         pr_default.execute(9, new Object[] {A7atraccionId});
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound2 = 1;
         }
         else
         {
            RcdFound2 = 0;
         }
         pr_default.close(9);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000A3 */
         pr_default.execute(1, new Object[] {A7atraccionId});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0A2( 4) ;
            RcdFound2 = 1;
            A7atraccionId = T000A3_A7atraccionId[0];
            AssignAttri("", false, "A7atraccionId", StringUtil.LTrimStr( (decimal)(A7atraccionId), 6, 0));
            A8atraccionNombre = T000A3_A8atraccionNombre[0];
            AssignAttri("", false, "A8atraccionNombre", A8atraccionNombre);
            A40000atraccionFoto_GXI = T000A3_A40000atraccionFoto_GXI[0];
            AssignProp("", false, imgatraccionFoto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A13atraccionFoto)) ? A40000atraccionFoto_GXI : context.convertURL( context.PathToRelativeUrl( A13atraccionFoto))), true);
            AssignProp("", false, imgatraccionFoto_Internalname, "SrcSet", context.GetImageSrcSet( A13atraccionFoto), true);
            A9paisId = T000A3_A9paisId[0];
            AssignAttri("", false, "A9paisId", StringUtil.LTrimStr( (decimal)(A9paisId), 6, 0));
            A15ciudadId = T000A3_A15ciudadId[0];
            AssignAttri("", false, "A15ciudadId", StringUtil.LTrimStr( (decimal)(A15ciudadId), 6, 0));
            A11categoriaId = T000A3_A11categoriaId[0];
            AssignAttri("", false, "A11categoriaId", StringUtil.LTrimStr( (decimal)(A11categoriaId), 6, 0));
            n11categoriaId = T000A3_n11categoriaId[0];
            A13atraccionFoto = T000A3_A13atraccionFoto[0];
            AssignAttri("", false, "A13atraccionFoto", A13atraccionFoto);
            AssignProp("", false, imgatraccionFoto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A13atraccionFoto)) ? A40000atraccionFoto_GXI : context.convertURL( context.PathToRelativeUrl( A13atraccionFoto))), true);
            AssignProp("", false, imgatraccionFoto_Internalname, "SrcSet", context.GetImageSrcSet( A13atraccionFoto), true);
            Z7atraccionId = A7atraccionId;
            sMode2 = Gx_mode;
            Gx_mode = "DSP";
            AssignAttri("", false, "Gx_mode", Gx_mode);
            AssignAttri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
            standaloneModal( ) ;
            Load0A2( ) ;
            if ( AnyError == 1 )
            {
               RcdFound2 = 0;
               InitializeNonKey0A2( ) ;
            }
            Gx_mode = sMode2;
            AssignAttri("", false, "Gx_mode", Gx_mode);
            AssignAttri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         }
         else
         {
            RcdFound2 = 0;
            InitializeNonKey0A2( ) ;
            sMode2 = Gx_mode;
            Gx_mode = "DSP";
            AssignAttri("", false, "Gx_mode", Gx_mode);
            AssignAttri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
            standaloneModal( ) ;
            Gx_mode = sMode2;
            AssignAttri("", false, "Gx_mode", Gx_mode);
            AssignAttri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0A2( ) ;
         if ( RcdFound2 == 0 )
         {
            Gx_mode = "INS";
            AssignAttri("", false, "Gx_mode", Gx_mode);
            AssignAttri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         }
         else
         {
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
            AssignAttri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound2 = 0;
         /* Using cursor T000A12 */
         pr_default.execute(10, new Object[] {A7atraccionId});
         if ( (pr_default.getStatus(10) != 101) )
         {
            while ( (pr_default.getStatus(10) != 101) && ( ( T000A12_A7atraccionId[0] < A7atraccionId ) ) )
            {
               pr_default.readNext(10);
            }
            if ( (pr_default.getStatus(10) != 101) && ( ( T000A12_A7atraccionId[0] > A7atraccionId ) ) )
            {
               A7atraccionId = T000A12_A7atraccionId[0];
               AssignAttri("", false, "A7atraccionId", StringUtil.LTrimStr( (decimal)(A7atraccionId), 6, 0));
               RcdFound2 = 1;
            }
         }
         pr_default.close(10);
      }

      protected void move_previous( )
      {
         RcdFound2 = 0;
         /* Using cursor T000A13 */
         pr_default.execute(11, new Object[] {A7atraccionId});
         if ( (pr_default.getStatus(11) != 101) )
         {
            while ( (pr_default.getStatus(11) != 101) && ( ( T000A13_A7atraccionId[0] > A7atraccionId ) ) )
            {
               pr_default.readNext(11);
            }
            if ( (pr_default.getStatus(11) != 101) && ( ( T000A13_A7atraccionId[0] < A7atraccionId ) ) )
            {
               A7atraccionId = T000A13_A7atraccionId[0];
               AssignAttri("", false, "A7atraccionId", StringUtil.LTrimStr( (decimal)(A7atraccionId), 6, 0));
               RcdFound2 = 1;
            }
         }
         pr_default.close(11);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0A2( ) ;
         if ( IsIns( ) )
         {
            /* Insert record */
            GX_FocusControl = edtatraccionId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0A2( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound2 == 1 )
            {
               if ( A7atraccionId != Z7atraccionId )
               {
                  A7atraccionId = Z7atraccionId;
                  AssignAttri("", false, "A7atraccionId", StringUtil.LTrimStr( (decimal)(A7atraccionId), 6, 0));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "ATRACCIONID");
                  AnyError = 1;
                  GX_FocusControl = edtatraccionId_Internalname;
                  AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( IsDlt( ) )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtatraccionId_Internalname;
                  AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  AssignAttri("", false, "Gx_mode", Gx_mode);
                  AssignAttri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                  /* Update record */
                  Update0A2( ) ;
                  GX_FocusControl = edtatraccionId_Internalname;
                  AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A7atraccionId != Z7atraccionId )
               {
                  Gx_mode = "INS";
                  AssignAttri("", false, "Gx_mode", Gx_mode);
                  AssignAttri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                  /* Insert record */
                  GX_FocusControl = edtatraccionId_Internalname;
                  AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0A2( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "ATRACCIONID");
                     AnyError = 1;
                     GX_FocusControl = edtatraccionId_Internalname;
                     AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     AssignAttri("", false, "Gx_mode", Gx_mode);
                     AssignAttri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                     /* Insert record */
                     GX_FocusControl = edtatraccionId_Internalname;
                     AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0A2( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A7atraccionId != Z7atraccionId )
         {
            A7atraccionId = Z7atraccionId;
            AssignAttri("", false, "A7atraccionId", StringUtil.LTrimStr( (decimal)(A7atraccionId), 6, 0));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "ATRACCIONID");
            AnyError = 1;
            GX_FocusControl = edtatraccionId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtatraccionId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
            AssignAttri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         getEqualNoModal( ) ;
         if ( RcdFound2 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "ATRACCIONID");
            AnyError = 1;
            GX_FocusControl = edtatraccionId_Internalname;
            AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtatraccionNombre_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         ScanStart0A2( ) ;
         if ( RcdFound2 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
            AssignAttri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         }
         GX_FocusControl = edtatraccionNombre_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd0A2( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         move_previous( ) ;
         if ( RcdFound2 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
            AssignAttri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         }
         GX_FocusControl = edtatraccionNombre_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         move_next( ) ;
         if ( RcdFound2 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
            AssignAttri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         }
         GX_FocusControl = edtatraccionNombre_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         ScanStart0A2( ) ;
         if ( RcdFound2 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound2 != 0 )
            {
               ScanNext0A2( ) ;
            }
            Gx_mode = "UPD";
            AssignAttri("", false, "Gx_mode", Gx_mode);
            AssignAttri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         }
         GX_FocusControl = edtatraccionNombre_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd0A2( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency0A2( )
      {
         if ( ! IsIns( ) )
         {
            /* Using cursor T000A2 */
            pr_default.execute(0, new Object[] {A7atraccionId});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"atraccion"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z8atraccionNombre, T000A2_A8atraccionNombre[0]) != 0 ) || ( Z9paisId != T000A2_A9paisId[0] ) || ( Z15ciudadId != T000A2_A15ciudadId[0] ) || ( Z11categoriaId != T000A2_A11categoriaId[0] ) )
            {
               if ( StringUtil.StrCmp(Z8atraccionNombre, T000A2_A8atraccionNombre[0]) != 0 )
               {
                  GXUtil.WriteLog("atraccionconotrosparametros:[seudo value changed for attri]"+"atraccionNombre");
                  GXUtil.WriteLogRaw("Old: ",Z8atraccionNombre);
                  GXUtil.WriteLogRaw("Current: ",T000A2_A8atraccionNombre[0]);
               }
               if ( Z9paisId != T000A2_A9paisId[0] )
               {
                  GXUtil.WriteLog("atraccionconotrosparametros:[seudo value changed for attri]"+"paisId");
                  GXUtil.WriteLogRaw("Old: ",Z9paisId);
                  GXUtil.WriteLogRaw("Current: ",T000A2_A9paisId[0]);
               }
               if ( Z15ciudadId != T000A2_A15ciudadId[0] )
               {
                  GXUtil.WriteLog("atraccionconotrosparametros:[seudo value changed for attri]"+"ciudadId");
                  GXUtil.WriteLogRaw("Old: ",Z15ciudadId);
                  GXUtil.WriteLogRaw("Current: ",T000A2_A15ciudadId[0]);
               }
               if ( Z11categoriaId != T000A2_A11categoriaId[0] )
               {
                  GXUtil.WriteLog("atraccionconotrosparametros:[seudo value changed for attri]"+"categoriaId");
                  GXUtil.WriteLogRaw("Old: ",Z11categoriaId);
                  GXUtil.WriteLogRaw("Current: ",T000A2_A11categoriaId[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"atraccion"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0A2( )
      {
         BeforeValidate0A2( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0A2( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0A2( 0) ;
            CheckOptimisticConcurrency0A2( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0A2( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0A2( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000A14 */
                     pr_default.execute(12, new Object[] {A8atraccionNombre, A13atraccionFoto, A40000atraccionFoto_GXI, A9paisId, A15ciudadId, n11categoriaId, A11categoriaId});
                     A7atraccionId = T000A14_A7atraccionId[0];
                     AssignAttri("", false, "A7atraccionId", StringUtil.LTrimStr( (decimal)(A7atraccionId), 6, 0));
                     pr_default.close(12);
                     dsDefault.SmartCacheProvider.SetUpdated("atraccion") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                           ResetCaption0A0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0A2( ) ;
            }
            EndLevel0A2( ) ;
         }
         CloseExtendedTableCursors0A2( ) ;
      }

      protected void Update0A2( )
      {
         BeforeValidate0A2( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0A2( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0A2( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0A2( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0A2( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000A15 */
                     pr_default.execute(13, new Object[] {A8atraccionNombre, A9paisId, A15ciudadId, n11categoriaId, A11categoriaId, A7atraccionId});
                     pr_default.close(13);
                     dsDefault.SmartCacheProvider.SetUpdated("atraccion") ;
                     if ( (pr_default.getStatus(13) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"atraccion"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0A2( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), "SuccessfullyUpdated", 0, "", true);
                           ResetCaption0A0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0A2( ) ;
         }
         CloseExtendedTableCursors0A2( ) ;
      }

      protected void DeferredUpdate0A2( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor T000A16 */
            pr_default.execute(14, new Object[] {A13atraccionFoto, A40000atraccionFoto_GXI, A7atraccionId});
            pr_default.close(14);
            dsDefault.SmartCacheProvider.SetUpdated("atraccion") ;
         }
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         AssignAttri("", false, "Gx_mode", Gx_mode);
         AssignAttri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         BeforeValidate0A2( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0A2( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0A2( ) ;
            AfterConfirm0A2( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0A2( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000A17 */
                  pr_default.execute(15, new Object[] {A7atraccionId});
                  pr_default.close(15);
                  dsDefault.SmartCacheProvider.SetUpdated("atraccion") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound2 == 0 )
                        {
                           InitAll0A2( ) ;
                           Gx_mode = "INS";
                           AssignAttri("", false, "Gx_mode", Gx_mode);
                           AssignAttri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           AssignAttri("", false, "Gx_mode", Gx_mode);
                           AssignAttri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), "SuccessfullyDeleted", 0, "", true);
                        ResetCaption0A0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode2 = Gx_mode;
         Gx_mode = "DLT";
         AssignAttri("", false, "Gx_mode", Gx_mode);
         AssignAttri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         EndLevel0A2( ) ;
         Gx_mode = sMode2;
         AssignAttri("", false, "Gx_mode", Gx_mode);
         AssignAttri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
      }

      protected void OnDeleteControls0A2( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            AV16Pgmname = "atraccionConOtrosParametros";
            AssignAttri("", false, "AV16Pgmname", AV16Pgmname);
            /* Using cursor T000A18 */
            pr_default.execute(16, new Object[] {A9paisId});
            A10paisNombre = T000A18_A10paisNombre[0];
            AssignAttri("", false, "A10paisNombre", A10paisNombre);
            pr_default.close(16);
            /* Using cursor T000A19 */
            pr_default.execute(17, new Object[] {n11categoriaId, A11categoriaId});
            A12categoriaNombre = T000A19_A12categoriaNombre[0];
            AssignAttri("", false, "A12categoriaNombre", A12categoriaNombre);
            pr_default.close(17);
            /* Using cursor T000A20 */
            pr_default.execute(18, new Object[] {A9paisId, A15ciudadId});
            A17ciudadNombre = T000A20_A17ciudadNombre[0];
            AssignAttri("", false, "A17ciudadNombre", A17ciudadNombre);
            pr_default.close(18);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000A21 */
            pr_default.execute(19, new Object[] {A7atraccionId});
            if ( (pr_default.getStatus(19) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"atraccion"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(19);
         }
      }

      protected void EndLevel0A2( )
      {
         if ( ! IsIns( ) )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0A2( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(16);
            pr_default.close(18);
            pr_default.close(17);
            context.CommitDataStores("atraccionconotrosparametros",pr_default);
            if ( AnyError == 0 )
            {
               ConfirmValues0A0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(16);
            pr_default.close(18);
            pr_default.close(17);
            context.RollbackDataStores("atraccionconotrosparametros",pr_default);
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0A2( )
      {
         /* Scan By routine */
         /* Using cursor T000A22 */
         pr_default.execute(20);
         RcdFound2 = 0;
         if ( (pr_default.getStatus(20) != 101) )
         {
            RcdFound2 = 1;
            A7atraccionId = T000A22_A7atraccionId[0];
            AssignAttri("", false, "A7atraccionId", StringUtil.LTrimStr( (decimal)(A7atraccionId), 6, 0));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0A2( )
      {
         /* Scan next routine */
         pr_default.readNext(20);
         RcdFound2 = 0;
         if ( (pr_default.getStatus(20) != 101) )
         {
            RcdFound2 = 1;
            A7atraccionId = T000A22_A7atraccionId[0];
            AssignAttri("", false, "A7atraccionId", StringUtil.LTrimStr( (decimal)(A7atraccionId), 6, 0));
         }
      }

      protected void ScanEnd0A2( )
      {
         pr_default.close(20);
      }

      protected void AfterConfirm0A2( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0A2( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0A2( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0A2( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0A2( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0A2( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0A2( )
      {
         edtatraccionId_Enabled = 0;
         AssignProp("", false, edtatraccionId_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtatraccionId_Enabled), 5, 0), true);
         edtatraccionNombre_Enabled = 0;
         AssignProp("", false, edtatraccionNombre_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtatraccionNombre_Enabled), 5, 0), true);
         edtpaisId_Enabled = 0;
         AssignProp("", false, edtpaisId_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtpaisId_Enabled), 5, 0), true);
         edtpaisNombre_Enabled = 0;
         AssignProp("", false, edtpaisNombre_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtpaisNombre_Enabled), 5, 0), true);
         edtcategoriaId_Enabled = 0;
         AssignProp("", false, edtcategoriaId_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtcategoriaId_Enabled), 5, 0), true);
         edtcategoriaNombre_Enabled = 0;
         AssignProp("", false, edtcategoriaNombre_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtcategoriaNombre_Enabled), 5, 0), true);
         imgatraccionFoto_Enabled = 0;
         AssignProp("", false, imgatraccionFoto_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(imgatraccionFoto_Enabled), 5, 0), true);
         edtciudadId_Enabled = 0;
         AssignProp("", false, edtciudadId_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtciudadId_Enabled), 5, 0), true);
         edtciudadNombre_Enabled = 0;
         AssignProp("", false, edtciudadNombre_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(edtciudadNombre_Enabled), 5, 0), true);
      }

      protected void send_integrity_lvl_hashes0A2( )
      {
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues0A0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 136889), false, true);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 136889), false, true);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 136889), false, true);
         context.AddJavascriptSource("gxcfg.js", "?2019123216669", false, true);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + "-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("atraccionconotrosparametros.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" title=\"submit\" style=\"display:none\" disabled>") ;
         AssignProp("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "Z7atraccionId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z7atraccionId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z8atraccionNombre", StringUtil.RTrim( Z8atraccionNombre));
         GxWebStd.gx_hidden_field( context, "Z9paisId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z9paisId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z15ciudadId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z15ciudadId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z11categoriaId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z11categoriaId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_Mode", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "gxhash_vTRNCONTEXT", GetSecureSignedToken( "", AV9TrnContext, context));
         GxWebStd.gx_hidden_field( context, "ATRACCIONFOTO_GXI", A40000atraccionFoto_GXI);
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV16Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         GXCCtlgxBlob = "ATRACCIONFOTO" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, A13atraccionFoto);
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendSecurityToken(sPrefix);
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("atraccionconotrosparametros.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "atraccionConOtrosParametros" ;
      }

      public override String GetPgmdesc( )
      {
         return "atraccion Con Otros Parametros" ;
      }

      protected void InitializeNonKey0A2( )
      {
         A8atraccionNombre = "";
         AssignAttri("", false, "A8atraccionNombre", A8atraccionNombre);
         A9paisId = 0;
         AssignAttri("", false, "A9paisId", StringUtil.LTrimStr( (decimal)(A9paisId), 6, 0));
         A10paisNombre = "";
         AssignAttri("", false, "A10paisNombre", A10paisNombre);
         A11categoriaId = 0;
         n11categoriaId = false;
         AssignAttri("", false, "A11categoriaId", StringUtil.LTrimStr( (decimal)(A11categoriaId), 6, 0));
         n11categoriaId = ((0==A11categoriaId) ? true : false);
         A12categoriaNombre = "";
         AssignAttri("", false, "A12categoriaNombre", A12categoriaNombre);
         A13atraccionFoto = "";
         AssignAttri("", false, "A13atraccionFoto", A13atraccionFoto);
         AssignProp("", false, imgatraccionFoto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A13atraccionFoto)) ? A40000atraccionFoto_GXI : context.convertURL( context.PathToRelativeUrl( A13atraccionFoto))), true);
         AssignProp("", false, imgatraccionFoto_Internalname, "SrcSet", context.GetImageSrcSet( A13atraccionFoto), true);
         A40000atraccionFoto_GXI = "";
         AssignProp("", false, imgatraccionFoto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A13atraccionFoto)) ? A40000atraccionFoto_GXI : context.convertURL( context.PathToRelativeUrl( A13atraccionFoto))), true);
         AssignProp("", false, imgatraccionFoto_Internalname, "SrcSet", context.GetImageSrcSet( A13atraccionFoto), true);
         A15ciudadId = 0;
         AssignAttri("", false, "A15ciudadId", StringUtil.LTrimStr( (decimal)(A15ciudadId), 6, 0));
         A17ciudadNombre = "";
         AssignAttri("", false, "A17ciudadNombre", A17ciudadNombre);
         Z8atraccionNombre = "";
         Z9paisId = 0;
         Z15ciudadId = 0;
         Z11categoriaId = 0;
      }

      protected void InitAll0A2( )
      {
         A7atraccionId = 0;
         AssignAttri("", false, "A7atraccionId", StringUtil.LTrimStr( (decimal)(A7atraccionId), 6, 0));
         InitializeNonKey0A2( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ));
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2019123216675", true, true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.eng.js", "?"+GetCacheInvalidationToken( ), false, true);
         context.AddJavascriptSource("atraccionconotrosparametros.js", "?2019123216675", false, true);
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTitle_Internalname = "TITLE";
         divTitlecontainer_Internalname = "TITLECONTAINER";
         bttBtn_first_Internalname = "BTN_FIRST";
         bttBtn_previous_Internalname = "BTN_PREVIOUS";
         bttBtn_next_Internalname = "BTN_NEXT";
         bttBtn_last_Internalname = "BTN_LAST";
         bttBtn_select_Internalname = "BTN_SELECT";
         divToolbarcell_Internalname = "TOOLBARCELL";
         edtatraccionId_Internalname = "ATRACCIONID";
         edtatraccionNombre_Internalname = "ATRACCIONNOMBRE";
         edtpaisId_Internalname = "PAISID";
         edtpaisNombre_Internalname = "PAISNOMBRE";
         edtcategoriaId_Internalname = "CATEGORIAID";
         edtcategoriaNombre_Internalname = "CATEGORIANOMBRE";
         imgatraccionFoto_Internalname = "ATRACCIONFOTO";
         edtciudadId_Internalname = "CIUDADID";
         edtciudadNombre_Internalname = "CIUDADNOMBRE";
         divFormcontainer_Internalname = "FORMCONTAINER";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
         imgprompt_9_Internalname = "PROMPT_9";
         imgprompt_11_Internalname = "PROMPT_11";
         imgprompt_15_Internalname = "PROMPT_15";
      }

      public override void initialize_properties( )
      {
         context.SetDefaultTheme("Carmine");
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "atraccion Con Otros Parametros";
         bttBtn_delete_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Enabled = 1;
         bttBtn_enter_Visible = 1;
         edtciudadNombre_Jsonclick = "";
         edtciudadNombre_Enabled = 0;
         imgprompt_15_Visible = 1;
         imgprompt_15_Link = "";
         edtciudadId_Jsonclick = "";
         edtciudadId_Enabled = 1;
         imgatraccionFoto_Enabled = 1;
         edtcategoriaNombre_Jsonclick = "";
         edtcategoriaNombre_Enabled = 0;
         imgprompt_11_Visible = 1;
         imgprompt_11_Link = "";
         edtcategoriaId_Jsonclick = "";
         edtcategoriaId_Enabled = 1;
         edtpaisNombre_Jsonclick = "";
         edtpaisNombre_Enabled = 0;
         imgprompt_9_Visible = 1;
         imgprompt_9_Link = "";
         edtpaisId_Jsonclick = "";
         edtpaisId_Enabled = 1;
         edtatraccionNombre_Jsonclick = "";
         edtatraccionNombre_Enabled = 1;
         edtatraccionId_Jsonclick = "";
         edtatraccionId_Enabled = 1;
         bttBtn_select_Visible = 1;
         bttBtn_last_Visible = 1;
         bttBtn_next_Visible = 1;
         bttBtn_previous_Visible = 1;
         bttBtn_first_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void init_web_controls( )
      {
         /* End function init_web_controls */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         AssignAttri("", false, "IsConfirmed", StringUtil.LTrimStr( (decimal)(IsConfirmed), 4, 0));
         getEqualNoModal( ) ;
         GX_FocusControl = edtatraccionNombre_Internalname;
         AssignAttri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      protected bool IsIns( )
      {
         return ((StringUtil.StrCmp(Gx_mode, "INS")==0) ? true : false) ;
      }

      protected bool IsDlt( )
      {
         return ((StringUtil.StrCmp(Gx_mode, "DLT")==0) ? true : false) ;
      }

      protected bool IsUpd( )
      {
         return ((StringUtil.StrCmp(Gx_mode, "UPD")==0) ? true : false) ;
      }

      protected bool IsDsp( )
      {
         return ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? true : false) ;
      }

      public void Valid_Atraccionid( )
      {
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         send_integrity_footer_hashes( ) ;
         dynload_actions( ) ;
         /*  Sending validation outputs */
         AssignAttri("", false, "A8atraccionNombre", StringUtil.RTrim( A8atraccionNombre));
         AssignAttri("", false, "A9paisId", StringUtil.LTrim( StringUtil.NToC( (decimal)(A9paisId), 6, 0, ".", "")));
         AssignAttri("", false, "A11categoriaId", StringUtil.LTrim( StringUtil.NToC( (decimal)(A11categoriaId), 6, 0, ".", "")));
         AssignAttri("", false, "A13atraccionFoto", context.PathToRelativeUrl( A13atraccionFoto));
         AssignAttri("", false, "A40000atraccionFoto_GXI", A40000atraccionFoto_GXI);
         AssignAttri("", false, "A15ciudadId", StringUtil.LTrim( StringUtil.NToC( (decimal)(A15ciudadId), 6, 0, ".", "")));
         AssignAttri("", false, "AV16Pgmname", StringUtil.RTrim( AV16Pgmname));
         AssignAttri("", false, "A10paisNombre", StringUtil.RTrim( A10paisNombre));
         AssignAttri("", false, "A17ciudadNombre", StringUtil.RTrim( A17ciudadNombre));
         AssignAttri("", false, "A12categoriaNombre", StringUtil.RTrim( A12categoriaNombre));
         AssignAttri("", false, "Gx_mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "Z7atraccionId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z7atraccionId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z8atraccionNombre", StringUtil.RTrim( Z8atraccionNombre));
         GxWebStd.gx_hidden_field( context, "Z9paisId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z9paisId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z11categoriaId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z11categoriaId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z13atraccionFoto", context.PathToRelativeUrl( Z13atraccionFoto));
         GxWebStd.gx_hidden_field( context, "Z40000atraccionFoto_GXI", Z40000atraccionFoto_GXI);
         GxWebStd.gx_hidden_field( context, "Z15ciudadId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z15ciudadId), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "ZV16Pgmname", StringUtil.RTrim( ZV16Pgmname));
         GxWebStd.gx_hidden_field( context, "Z10paisNombre", StringUtil.RTrim( Z10paisNombre));
         GxWebStd.gx_hidden_field( context, "Z17ciudadNombre", StringUtil.RTrim( Z17ciudadNombre));
         GxWebStd.gx_hidden_field( context, "Z12categoriaNombre", StringUtil.RTrim( Z12categoriaNombre));
         AssignProp("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_delete_Enabled), 5, 0), true);
         AssignProp("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrimStr( (decimal)(bttBtn_enter_Enabled), 5, 0), true);
         SendCloseFormHiddens( ) ;
      }

      public void Valid_Paisid( )
      {
         /* Using cursor T000A18 */
         pr_default.execute(16, new Object[] {A9paisId});
         if ( (pr_default.getStatus(16) == 101) )
         {
            GX_msglist.addItem("No matching 'pais'.", "ForeignKeyNotFound", 1, "PAISID");
            AnyError = 1;
            GX_FocusControl = edtpaisId_Internalname;
         }
         A10paisNombre = T000A18_A10paisNombre[0];
         pr_default.close(16);
         dynload_actions( ) ;
         /*  Sending validation outputs */
         AssignAttri("", false, "A10paisNombre", StringUtil.RTrim( A10paisNombre));
      }

      public void Valid_Categoriaid( )
      {
         n11categoriaId = false;
         /* Using cursor T000A19 */
         pr_default.execute(17, new Object[] {n11categoriaId, A11categoriaId});
         if ( (pr_default.getStatus(17) == 101) )
         {
            if ( ! ( (0==A11categoriaId) ) )
            {
               GX_msglist.addItem("No matching 'categoria'.", "ForeignKeyNotFound", 1, "CATEGORIAID");
               AnyError = 1;
               GX_FocusControl = edtcategoriaId_Internalname;
            }
         }
         A12categoriaNombre = T000A19_A12categoriaNombre[0];
         pr_default.close(17);
         dynload_actions( ) ;
         /*  Sending validation outputs */
         AssignAttri("", false, "A12categoriaNombre", StringUtil.RTrim( A12categoriaNombre));
      }

      public void Valid_Ciudadid( )
      {
         /* Using cursor T000A20 */
         pr_default.execute(18, new Object[] {A9paisId, A15ciudadId});
         if ( (pr_default.getStatus(18) == 101) )
         {
            GX_msglist.addItem("No matching 'ciudad'.", "ForeignKeyNotFound", 1, "PAISID");
            AnyError = 1;
            GX_FocusControl = edtpaisId_Internalname;
         }
         A17ciudadNombre = T000A20_A17ciudadNombre[0];
         pr_default.close(18);
         dynload_actions( ) ;
         /*  Sending validation outputs */
         AssignAttri("", false, "A17ciudadNombre", StringUtil.RTrim( A17ciudadNombre));
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}]");
         setEventMetadata("ENTER",",oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',hsh:true}]");
         setEventMetadata("REFRESH",",oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E120A2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',hsh:true}]");
         setEventMetadata("AFTER TRN",",oparms:[]}");
         setEventMetadata("VALID_ATRACCIONID","{handler:'Valid_Atraccionid',iparms:[{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',hsh:true},{av:'A7atraccionId',fld:'ATRACCIONID',pic:'ZZZZZ9'},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true},{av:'AV16Pgmname',fld:'vPGMNAME',pic:''}]");
         setEventMetadata("VALID_ATRACCIONID",",oparms:[{av:'A8atraccionNombre',fld:'ATRACCIONNOMBRE',pic:''},{av:'A9paisId',fld:'PAISID',pic:'ZZZZZ9'},{av:'A11categoriaId',fld:'CATEGORIAID',pic:'ZZZZZ9'},{av:'A13atraccionFoto',fld:'ATRACCIONFOTO',pic:''},{av:'A40000atraccionFoto_GXI',fld:'ATRACCIONFOTO_GXI',pic:''},{av:'A15ciudadId',fld:'CIUDADID',pic:'ZZZZZ9'},{av:'AV16Pgmname',fld:'vPGMNAME',pic:''},{av:'A10paisNombre',fld:'PAISNOMBRE',pic:''},{av:'A17ciudadNombre',fld:'CIUDADNOMBRE',pic:''},{av:'A12categoriaNombre',fld:'CATEGORIANOMBRE',pic:''},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true},{av:'Z7atraccionId'},{av:'Z8atraccionNombre'},{av:'Z9paisId'},{av:'Z11categoriaId'},{av:'Z13atraccionFoto'},{av:'Z40000atraccionFoto_GXI'},{av:'Z15ciudadId'},{av:'ZV16Pgmname'},{av:'Z10paisNombre'},{av:'Z17ciudadNombre'},{av:'Z12categoriaNombre'},{ctrl:'BTN_DELETE',prop:'Enabled'},{ctrl:'BTN_ENTER',prop:'Enabled'}]}");
         setEventMetadata("VALID_ATRACCIONNOMBRE","{handler:'Valid_Atraccionnombre',iparms:[]");
         setEventMetadata("VALID_ATRACCIONNOMBRE",",oparms:[]}");
         setEventMetadata("VALID_PAISID","{handler:'Valid_Paisid',iparms:[{av:'A9paisId',fld:'PAISID',pic:'ZZZZZ9'},{av:'A10paisNombre',fld:'PAISNOMBRE',pic:''}]");
         setEventMetadata("VALID_PAISID",",oparms:[{av:'A10paisNombre',fld:'PAISNOMBRE',pic:''}]}");
         setEventMetadata("VALID_CATEGORIAID","{handler:'Valid_Categoriaid',iparms:[{av:'A11categoriaId',fld:'CATEGORIAID',pic:'ZZZZZ9'},{av:'A12categoriaNombre',fld:'CATEGORIANOMBRE',pic:''}]");
         setEventMetadata("VALID_CATEGORIAID",",oparms:[{av:'A12categoriaNombre',fld:'CATEGORIANOMBRE',pic:''}]}");
         setEventMetadata("VALID_CIUDADID","{handler:'Valid_Ciudadid',iparms:[{av:'A9paisId',fld:'PAISID',pic:'ZZZZZ9'},{av:'A15ciudadId',fld:'CIUDADID',pic:'ZZZZZ9'},{av:'A17ciudadNombre',fld:'CIUDADNOMBRE',pic:''}]");
         setEventMetadata("VALID_CIUDADID",",oparms:[{av:'A17ciudadNombre',fld:'CIUDADNOMBRE',pic:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(16);
         pr_default.close(18);
         pr_default.close(17);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z8atraccionNombre = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         lblTitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_first_Jsonclick = "";
         bttBtn_previous_Jsonclick = "";
         bttBtn_next_Jsonclick = "";
         bttBtn_last_Jsonclick = "";
         bttBtn_select_Jsonclick = "";
         A8atraccionNombre = "";
         sImgUrl = "";
         A10paisNombre = "";
         A12categoriaNombre = "";
         A13atraccionFoto = "";
         A40000atraccionFoto_GXI = "";
         A17ciudadNombre = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         Gx_mode = "";
         AV16Pgmname = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9TrnContext = new SdtTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV14TrnContextAtt = new SdtTransactionContext_Attribute(context);
         Z13atraccionFoto = "";
         Z40000atraccionFoto_GXI = "";
         Z10paisNombre = "";
         Z12categoriaNombre = "";
         Z17ciudadNombre = "";
         T000A7_A7atraccionId = new int[1] ;
         T000A7_A8atraccionNombre = new String[] {""} ;
         T000A7_A10paisNombre = new String[] {""} ;
         T000A7_A12categoriaNombre = new String[] {""} ;
         T000A7_A40000atraccionFoto_GXI = new String[] {""} ;
         T000A7_A17ciudadNombre = new String[] {""} ;
         T000A7_A9paisId = new int[1] ;
         T000A7_A15ciudadId = new int[1] ;
         T000A7_A11categoriaId = new int[1] ;
         T000A7_n11categoriaId = new bool[] {false} ;
         T000A7_A13atraccionFoto = new String[] {""} ;
         T000A4_A10paisNombre = new String[] {""} ;
         T000A5_A17ciudadNombre = new String[] {""} ;
         T000A6_A12categoriaNombre = new String[] {""} ;
         T000A8_A10paisNombre = new String[] {""} ;
         T000A9_A17ciudadNombre = new String[] {""} ;
         T000A10_A12categoriaNombre = new String[] {""} ;
         T000A11_A7atraccionId = new int[1] ;
         T000A3_A7atraccionId = new int[1] ;
         T000A3_A8atraccionNombre = new String[] {""} ;
         T000A3_A40000atraccionFoto_GXI = new String[] {""} ;
         T000A3_A9paisId = new int[1] ;
         T000A3_A15ciudadId = new int[1] ;
         T000A3_A11categoriaId = new int[1] ;
         T000A3_n11categoriaId = new bool[] {false} ;
         T000A3_A13atraccionFoto = new String[] {""} ;
         sMode2 = "";
         T000A12_A7atraccionId = new int[1] ;
         T000A13_A7atraccionId = new int[1] ;
         T000A2_A7atraccionId = new int[1] ;
         T000A2_A8atraccionNombre = new String[] {""} ;
         T000A2_A40000atraccionFoto_GXI = new String[] {""} ;
         T000A2_A9paisId = new int[1] ;
         T000A2_A15ciudadId = new int[1] ;
         T000A2_A11categoriaId = new int[1] ;
         T000A2_n11categoriaId = new bool[] {false} ;
         T000A2_A13atraccionFoto = new String[] {""} ;
         T000A14_A7atraccionId = new int[1] ;
         T000A18_A10paisNombre = new String[] {""} ;
         T000A19_A12categoriaNombre = new String[] {""} ;
         T000A20_A17ciudadNombre = new String[] {""} ;
         T000A21_A48proveedorId = new int[1] ;
         T000A21_A7atraccionId = new int[1] ;
         T000A22_A7atraccionId = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXCCtlgxBlob = "";
         ZV16Pgmname = "";
         ZZ8atraccionNombre = "";
         ZZ13atraccionFoto = "";
         ZZ40000atraccionFoto_GXI = "";
         ZZV16Pgmname = "";
         ZZ10paisNombre = "";
         ZZ17ciudadNombre = "";
         ZZ12categoriaNombre = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.atraccionconotrosparametros__default(),
            new Object[][] {
                new Object[] {
               T000A2_A7atraccionId, T000A2_A8atraccionNombre, T000A2_A40000atraccionFoto_GXI, T000A2_A9paisId, T000A2_A15ciudadId, T000A2_A11categoriaId, T000A2_n11categoriaId, T000A2_A13atraccionFoto
               }
               , new Object[] {
               T000A3_A7atraccionId, T000A3_A8atraccionNombre, T000A3_A40000atraccionFoto_GXI, T000A3_A9paisId, T000A3_A15ciudadId, T000A3_A11categoriaId, T000A3_n11categoriaId, T000A3_A13atraccionFoto
               }
               , new Object[] {
               T000A4_A10paisNombre
               }
               , new Object[] {
               T000A5_A17ciudadNombre
               }
               , new Object[] {
               T000A6_A12categoriaNombre
               }
               , new Object[] {
               T000A7_A7atraccionId, T000A7_A8atraccionNombre, T000A7_A10paisNombre, T000A7_A12categoriaNombre, T000A7_A40000atraccionFoto_GXI, T000A7_A17ciudadNombre, T000A7_A9paisId, T000A7_A15ciudadId, T000A7_A11categoriaId, T000A7_n11categoriaId,
               T000A7_A13atraccionFoto
               }
               , new Object[] {
               T000A8_A10paisNombre
               }
               , new Object[] {
               T000A9_A17ciudadNombre
               }
               , new Object[] {
               T000A10_A12categoriaNombre
               }
               , new Object[] {
               T000A11_A7atraccionId
               }
               , new Object[] {
               T000A12_A7atraccionId
               }
               , new Object[] {
               T000A13_A7atraccionId
               }
               , new Object[] {
               T000A14_A7atraccionId
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000A18_A10paisNombre
               }
               , new Object[] {
               T000A19_A12categoriaNombre
               }
               , new Object[] {
               T000A20_A17ciudadNombre
               }
               , new Object[] {
               T000A21_A48proveedorId, T000A21_A7atraccionId
               }
               , new Object[] {
               T000A22_A7atraccionId
               }
            }
         );
         AV16Pgmname = "atraccionConOtrosParametros";
         AV16Pgmname = "atraccionConOtrosParametros";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound2 ;
      private short nIsDirty_2 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private int Z7atraccionId ;
      private int Z9paisId ;
      private int Z15ciudadId ;
      private int Z11categoriaId ;
      private int A9paisId ;
      private int A15ciudadId ;
      private int A11categoriaId ;
      private int trnEnded ;
      private int bttBtn_first_Visible ;
      private int bttBtn_previous_Visible ;
      private int bttBtn_next_Visible ;
      private int bttBtn_last_Visible ;
      private int bttBtn_select_Visible ;
      private int A7atraccionId ;
      private int edtatraccionId_Enabled ;
      private int edtatraccionNombre_Enabled ;
      private int edtpaisId_Enabled ;
      private int imgprompt_9_Visible ;
      private int edtpaisNombre_Enabled ;
      private int edtcategoriaId_Enabled ;
      private int imgprompt_11_Visible ;
      private int edtcategoriaNombre_Enabled ;
      private int imgatraccionFoto_Enabled ;
      private int edtciudadId_Enabled ;
      private int imgprompt_15_Visible ;
      private int edtciudadNombre_Enabled ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_enter_Enabled ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int bttBtn_delete_Enabled ;
      private int AV11Insert_paisId ;
      private int AV12Insert_categoriaId ;
      private int AV13Insert_ciudadId ;
      private int AV17GXV1 ;
      private int idxLst ;
      private int ZZ7atraccionId ;
      private int ZZ9paisId ;
      private int ZZ11categoriaId ;
      private int ZZ15ciudadId ;
      private String sPrefix ;
      private String Z8atraccionNombre ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtatraccionId_Internalname ;
      private String divMaintable_Internalname ;
      private String divTitlecontainer_Internalname ;
      private String lblTitle_Internalname ;
      private String lblTitle_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String divFormcontainer_Internalname ;
      private String divToolbarcell_Internalname ;
      private String TempTags ;
      private String bttBtn_first_Internalname ;
      private String bttBtn_first_Jsonclick ;
      private String bttBtn_previous_Internalname ;
      private String bttBtn_previous_Jsonclick ;
      private String bttBtn_next_Internalname ;
      private String bttBtn_next_Jsonclick ;
      private String bttBtn_last_Internalname ;
      private String bttBtn_last_Jsonclick ;
      private String bttBtn_select_Internalname ;
      private String bttBtn_select_Jsonclick ;
      private String edtatraccionId_Jsonclick ;
      private String edtatraccionNombre_Internalname ;
      private String A8atraccionNombre ;
      private String edtatraccionNombre_Jsonclick ;
      private String edtpaisId_Internalname ;
      private String edtpaisId_Jsonclick ;
      private String sImgUrl ;
      private String imgprompt_9_Internalname ;
      private String imgprompt_9_Link ;
      private String edtpaisNombre_Internalname ;
      private String A10paisNombre ;
      private String edtpaisNombre_Jsonclick ;
      private String edtcategoriaId_Internalname ;
      private String edtcategoriaId_Jsonclick ;
      private String imgprompt_11_Internalname ;
      private String imgprompt_11_Link ;
      private String edtcategoriaNombre_Internalname ;
      private String A12categoriaNombre ;
      private String edtcategoriaNombre_Jsonclick ;
      private String imgatraccionFoto_Internalname ;
      private String edtciudadId_Internalname ;
      private String edtciudadId_Jsonclick ;
      private String imgprompt_15_Internalname ;
      private String imgprompt_15_Link ;
      private String edtciudadNombre_Internalname ;
      private String A17ciudadNombre ;
      private String edtciudadNombre_Jsonclick ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String Gx_mode ;
      private String AV16Pgmname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z10paisNombre ;
      private String Z12categoriaNombre ;
      private String Z17ciudadNombre ;
      private String sMode2 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXCCtlgxBlob ;
      private String ZV16Pgmname ;
      private String ZZ8atraccionNombre ;
      private String ZZV16Pgmname ;
      private String ZZ10paisNombre ;
      private String ZZ17ciudadNombre ;
      private String ZZ12categoriaNombre ;
      private bool entryPointCalled ;
      private bool n11categoriaId ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A13atraccionFoto_IsBlob ;
      private bool returnInSub ;
      private String A40000atraccionFoto_GXI ;
      private String Z40000atraccionFoto_GXI ;
      private String ZZ40000atraccionFoto_GXI ;
      private String A13atraccionFoto ;
      private String Z13atraccionFoto ;
      private String ZZ13atraccionFoto ;
      private IGxSession AV10WebSession ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T000A7_A7atraccionId ;
      private String[] T000A7_A8atraccionNombre ;
      private String[] T000A7_A10paisNombre ;
      private String[] T000A7_A12categoriaNombre ;
      private String[] T000A7_A40000atraccionFoto_GXI ;
      private String[] T000A7_A17ciudadNombre ;
      private int[] T000A7_A9paisId ;
      private int[] T000A7_A15ciudadId ;
      private int[] T000A7_A11categoriaId ;
      private bool[] T000A7_n11categoriaId ;
      private String[] T000A7_A13atraccionFoto ;
      private String[] T000A4_A10paisNombre ;
      private String[] T000A5_A17ciudadNombre ;
      private String[] T000A6_A12categoriaNombre ;
      private String[] T000A8_A10paisNombre ;
      private String[] T000A9_A17ciudadNombre ;
      private String[] T000A10_A12categoriaNombre ;
      private int[] T000A11_A7atraccionId ;
      private int[] T000A3_A7atraccionId ;
      private String[] T000A3_A8atraccionNombre ;
      private String[] T000A3_A40000atraccionFoto_GXI ;
      private int[] T000A3_A9paisId ;
      private int[] T000A3_A15ciudadId ;
      private int[] T000A3_A11categoriaId ;
      private bool[] T000A3_n11categoriaId ;
      private String[] T000A3_A13atraccionFoto ;
      private int[] T000A12_A7atraccionId ;
      private int[] T000A13_A7atraccionId ;
      private int[] T000A2_A7atraccionId ;
      private String[] T000A2_A8atraccionNombre ;
      private String[] T000A2_A40000atraccionFoto_GXI ;
      private int[] T000A2_A9paisId ;
      private int[] T000A2_A15ciudadId ;
      private int[] T000A2_A11categoriaId ;
      private bool[] T000A2_n11categoriaId ;
      private String[] T000A2_A13atraccionFoto ;
      private int[] T000A14_A7atraccionId ;
      private String[] T000A18_A10paisNombre ;
      private String[] T000A19_A12categoriaNombre ;
      private String[] T000A20_A17ciudadNombre ;
      private int[] T000A21_A48proveedorId ;
      private int[] T000A21_A7atraccionId ;
      private int[] T000A22_A7atraccionId ;
      private GXWebForm Form ;
      private SdtTransactionContext AV9TrnContext ;
      private SdtTransactionContext_Attribute AV14TrnContextAtt ;
   }

   public class atraccionconotrosparametros__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new UpdateCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000A7 ;
          prmT000A7 = new Object[] {
          new Object[] {"@atraccionId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A4 ;
          prmT000A4 = new Object[] {
          new Object[] {"@paisId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A5 ;
          prmT000A5 = new Object[] {
          new Object[] {"@paisId",SqlDbType.Int,6,0} ,
          new Object[] {"@ciudadId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A6 ;
          prmT000A6 = new Object[] {
          new Object[] {"@categoriaId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A8 ;
          prmT000A8 = new Object[] {
          new Object[] {"@paisId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A9 ;
          prmT000A9 = new Object[] {
          new Object[] {"@paisId",SqlDbType.Int,6,0} ,
          new Object[] {"@ciudadId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A10 ;
          prmT000A10 = new Object[] {
          new Object[] {"@categoriaId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A11 ;
          prmT000A11 = new Object[] {
          new Object[] {"@atraccionId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A3 ;
          prmT000A3 = new Object[] {
          new Object[] {"@atraccionId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A12 ;
          prmT000A12 = new Object[] {
          new Object[] {"@atraccionId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A13 ;
          prmT000A13 = new Object[] {
          new Object[] {"@atraccionId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A2 ;
          prmT000A2 = new Object[] {
          new Object[] {"@atraccionId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A14 ;
          prmT000A14 = new Object[] {
          new Object[] {"@atraccionNombre",SqlDbType.NChar,20,0} ,
          new Object[] {"@atraccionFoto",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@atraccionFoto_GXI",SqlDbType.NVarChar,2048,0} ,
          new Object[] {"@paisId",SqlDbType.Int,6,0} ,
          new Object[] {"@ciudadId",SqlDbType.Int,6,0} ,
          new Object[] {"@categoriaId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A15 ;
          prmT000A15 = new Object[] {
          new Object[] {"@atraccionNombre",SqlDbType.NChar,20,0} ,
          new Object[] {"@paisId",SqlDbType.Int,6,0} ,
          new Object[] {"@ciudadId",SqlDbType.Int,6,0} ,
          new Object[] {"@categoriaId",SqlDbType.Int,6,0} ,
          new Object[] {"@atraccionId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A16 ;
          prmT000A16 = new Object[] {
          new Object[] {"@atraccionFoto",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@atraccionFoto_GXI",SqlDbType.NVarChar,2048,0} ,
          new Object[] {"@atraccionId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A17 ;
          prmT000A17 = new Object[] {
          new Object[] {"@atraccionId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A21 ;
          prmT000A21 = new Object[] {
          new Object[] {"@atraccionId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A22 ;
          prmT000A22 = new Object[] {
          } ;
          Object[] prmT000A18 ;
          prmT000A18 = new Object[] {
          new Object[] {"@paisId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A19 ;
          prmT000A19 = new Object[] {
          new Object[] {"@categoriaId",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000A20 ;
          prmT000A20 = new Object[] {
          new Object[] {"@paisId",SqlDbType.Int,6,0} ,
          new Object[] {"@ciudadId",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T000A2", "SELECT [atraccionId], [atraccionNombre], [atraccionFoto_GXI], [paisId], [ciudadId], [categoriaId], [atraccionFoto] FROM [atraccion] WITH (UPDLOCK) WHERE [atraccionId] = @atraccionId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A2,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000A3", "SELECT [atraccionId], [atraccionNombre], [atraccionFoto_GXI], [paisId], [ciudadId], [categoriaId], [atraccionFoto] FROM [atraccion] WHERE [atraccionId] = @atraccionId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A3,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000A4", "SELECT [paisNombre] FROM [pais] WHERE [paisId] = @paisId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A4,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000A5", "SELECT [ciudadNombre] FROM [paisciudad] WHERE [paisId] = @paisId AND [ciudadId] = @ciudadId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A5,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000A6", "SELECT [categoriaNombre] FROM [categoria] WHERE [categoriaId] = @categoriaId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A6,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000A7", "SELECT TM1.[atraccionId], TM1.[atraccionNombre], T2.[paisNombre], T3.[categoriaNombre], TM1.[atraccionFoto_GXI], T4.[ciudadNombre], TM1.[paisId], TM1.[ciudadId], TM1.[categoriaId], TM1.[atraccionFoto] FROM ((([atraccion] TM1 INNER JOIN [pais] T2 ON T2.[paisId] = TM1.[paisId]) LEFT JOIN [categoria] T3 ON T3.[categoriaId] = TM1.[categoriaId]) INNER JOIN [paisciudad] T4 ON T4.[paisId] = TM1.[paisId] AND T4.[ciudadId] = TM1.[ciudadId]) WHERE TM1.[atraccionId] = @atraccionId ORDER BY TM1.[atraccionId]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000A7,100, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000A8", "SELECT [paisNombre] FROM [pais] WHERE [paisId] = @paisId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A8,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000A9", "SELECT [ciudadNombre] FROM [paisciudad] WHERE [paisId] = @paisId AND [ciudadId] = @ciudadId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A9,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000A10", "SELECT [categoriaNombre] FROM [categoria] WHERE [categoriaId] = @categoriaId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A10,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000A11", "SELECT [atraccionId] FROM [atraccion] WHERE [atraccionId] = @atraccionId  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000A11,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000A12", "SELECT TOP 1 [atraccionId] FROM [atraccion] WHERE ( [atraccionId] > @atraccionId) ORDER BY [atraccionId]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000A12,1, GxCacheFrequency.OFF ,true,true )
             ,new CursorDef("T000A13", "SELECT TOP 1 [atraccionId] FROM [atraccion] WHERE ( [atraccionId] < @atraccionId) ORDER BY [atraccionId] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000A13,1, GxCacheFrequency.OFF ,true,true )
             ,new CursorDef("T000A14", "INSERT INTO [atraccion]([atraccionNombre], [atraccionFoto], [atraccionFoto_GXI], [paisId], [ciudadId], [categoriaId]) VALUES(@atraccionNombre, @atraccionFoto, @atraccionFoto_GXI, @paisId, @ciudadId, @categoriaId); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000A14)
             ,new CursorDef("T000A15", "UPDATE [atraccion] SET [atraccionNombre]=@atraccionNombre, [paisId]=@paisId, [ciudadId]=@ciudadId, [categoriaId]=@categoriaId  WHERE [atraccionId] = @atraccionId", GxErrorMask.GX_NOMASK,prmT000A15)
             ,new CursorDef("T000A16", "UPDATE [atraccion] SET [atraccionFoto]=@atraccionFoto, [atraccionFoto_GXI]=@atraccionFoto_GXI  WHERE [atraccionId] = @atraccionId", GxErrorMask.GX_NOMASK,prmT000A16)
             ,new CursorDef("T000A17", "DELETE FROM [atraccion]  WHERE [atraccionId] = @atraccionId", GxErrorMask.GX_NOMASK,prmT000A17)
             ,new CursorDef("T000A18", "SELECT [paisNombre] FROM [pais] WHERE [paisId] = @paisId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A18,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000A19", "SELECT [categoriaNombre] FROM [categoria] WHERE [categoriaId] = @categoriaId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A19,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000A20", "SELECT [ciudadNombre] FROM [paisciudad] WHERE [paisId] = @paisId AND [ciudadId] = @ciudadId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A20,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000A21", "SELECT TOP 1 [proveedorId], [atraccionId] FROM [proveedoratraccion] WHERE [atraccionId] = @atraccionId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000A21,1, GxCacheFrequency.OFF ,true,true )
             ,new CursorDef("T000A22", "SELECT [atraccionId] FROM [atraccion] ORDER BY [atraccionId]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000A22,100, GxCacheFrequency.OFF ,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getMultimediaUri(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((String[]) buf[7])[0] = rslt.getMultimediaFile(7, rslt.getVarchar(3)) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getMultimediaUri(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((String[]) buf[7])[0] = rslt.getMultimediaFile(7, rslt.getVarchar(3)) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
                ((String[]) buf[4])[0] = rslt.getMultimediaUri(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                ((int[]) buf[7])[0] = rslt.getInt(8) ;
                ((int[]) buf[8])[0] = rslt.getInt(9) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(9);
                ((String[]) buf[10])[0] = rslt.getMultimediaFile(10, rslt.getVarchar(5)) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameterBlob(2, (String)parms[1], false);
                stmt.SetParameterMultimedia(3, (String)parms[2], (String)parms[1], "atraccion", "atraccionFoto");
                stmt.SetParameter(4, (int)parms[3]);
                stmt.SetParameter(5, (int)parms[4]);
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[6]);
                }
                return;
             case 13 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[4]);
                }
                stmt.SetParameter(5, (int)parms[5]);
                return;
             case 14 :
                stmt.SetParameterBlob(1, (String)parms[0], false);
                stmt.SetParameterMultimedia(2, (String)parms[1], (String)parms[0], "atraccion", "atraccionFoto");
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
