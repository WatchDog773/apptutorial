/*
               File: insertarCategoriaActualizarAtraccion
        Description: insertar Categoria Actualizar Atraccion
             Author: GeneXus C# Generator version 16_0_6-136889
       Generated on: 12/3/2019 22:12:7.29
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class insertarcategoriaactualizaratraccion : GXProcedure
   {
      public insertarcategoriaactualizaratraccion( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public insertarcategoriaactualizaratraccion( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         initialize();
         executePrivate();
      }

      public void executeSubmit( )
      {
         insertarcategoriaactualizaratraccion objinsertarcategoriaactualizaratraccion;
         objinsertarcategoriaactualizaratraccion = new insertarcategoriaactualizaratraccion();
         objinsertarcategoriaactualizaratraccion.context.SetSubmitInitialConfig(context);
         objinsertarcategoriaactualizaratraccion.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objinsertarcategoriaactualizaratraccion);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((insertarcategoriaactualizaratraccion)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8categoria.gxTpr_Categorianombre = "Sitio Turistico";
         AV8categoria.Insert();
         if ( AV8categoria.Success() )
         {
            /* Using cursor P000M2 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               A9paisId = P000M2_A9paisId[0];
               A15ciudadId = P000M2_A15ciudadId[0];
               A11categoriaId = P000M2_A11categoriaId[0];
               n11categoriaId = P000M2_n11categoriaId[0];
               A12categoriaNombre = P000M2_A12categoriaNombre[0];
               A17ciudadNombre = P000M2_A17ciudadNombre[0];
               A7atraccionId = P000M2_A7atraccionId[0];
               A17ciudadNombre = P000M2_A17ciudadNombre[0];
               A12categoriaNombre = P000M2_A12categoriaNombre[0];
               AV9atraccion.gxTpr_Atraccionid = A7atraccionId;
               AV9atraccion.gxTpr_Categoriaid = AV8categoria.gxTpr_Categoriaid;
               AV9atraccion.Update();
               pr_default.readNext(0);
            }
            pr_default.close(0);
            context.CommitDataStores("insertarcategoriaactualizaratraccion",pr_default);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV8categoria = new Sdtcategoria(context);
         scmdbuf = "";
         P000M2_A9paisId = new int[1] ;
         P000M2_A15ciudadId = new int[1] ;
         P000M2_A11categoriaId = new int[1] ;
         P000M2_n11categoriaId = new bool[] {false} ;
         P000M2_A12categoriaNombre = new String[] {""} ;
         P000M2_A17ciudadNombre = new String[] {""} ;
         P000M2_A7atraccionId = new int[1] ;
         A12categoriaNombre = "";
         A17ciudadNombre = "";
         AV9atraccion = new Sdtatraccion(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.insertarcategoriaactualizaratraccion__default(),
            new Object[][] {
                new Object[] {
               P000M2_A9paisId, P000M2_A15ciudadId, P000M2_A11categoriaId, P000M2_n11categoriaId, P000M2_A12categoriaNombre, P000M2_A17ciudadNombre, P000M2_A7atraccionId
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A9paisId ;
      private int A15ciudadId ;
      private int A11categoriaId ;
      private int A7atraccionId ;
      private String scmdbuf ;
      private String A12categoriaNombre ;
      private String A17ciudadNombre ;
      private bool n11categoriaId ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P000M2_A9paisId ;
      private int[] P000M2_A15ciudadId ;
      private int[] P000M2_A11categoriaId ;
      private bool[] P000M2_n11categoriaId ;
      private String[] P000M2_A12categoriaNombre ;
      private String[] P000M2_A17ciudadNombre ;
      private int[] P000M2_A7atraccionId ;
      private Sdtcategoria AV8categoria ;
      private Sdtatraccion AV9atraccion ;
   }

   public class insertarcategoriaactualizaratraccion__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP000M2 ;
          prmP000M2 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("P000M2", "SELECT T1.[paisId], T1.[ciudadId], T1.[categoriaId], T3.[categoriaNombre], T2.[ciudadNombre], T1.[atraccionId] FROM (([atraccion] T1 INNER JOIN [paisciudad] T2 ON T2.[paisId] = T1.[paisId] AND T2.[ciudadId] = T1.[ciudadId]) LEFT JOIN [categoria] T3 ON T3.[categoriaId] = T1.[categoriaId]) WHERE (T2.[ciudadNombre] = 'Beigin') AND (T3.[categoriaNombre] = 'Monumento') ORDER BY T1.[atraccionId] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000M2,100, GxCacheFrequency.OFF ,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 20) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 20) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
